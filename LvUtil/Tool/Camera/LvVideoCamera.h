//
//  LvVideoProcessor.h
//  AVFoundationTest
//
//  Created by Zhang Ming on 12-11-9.
//  Copyright (c) 2012年 Lver. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>

@protocol LvVideoCameraDelegate <NSObject>

- (void)renderCaptureToImage:(UIImage *)image;

@end

@interface LvVideoCamera : NSObject<AVCaptureVideoDataOutputSampleBufferDelegate> {
    AVCaptureDeviceInput *videoInput;
    
    dispatch_semaphore_t frameRenderingSemaphore;
    
    BOOL capturePaused;
}

@property (strong) AVCaptureSession *session;
@property (strong) AVCaptureVideoPreviewLayer *previewLayer;
@property (strong) AVCaptureStillImageOutput *output;
@property (nonatomic) id<LvVideoCameraDelegate> delegate;
@property (nonatomic, strong) UIImage *image;

- (void)startCameraCapture;
- (void)stopCameraCapture;

- (void)embedPreviewInView:(UIView *)view;

- (void)capturePhotoAsImageWithCompletionHandler:(void (^)(UIImage *proceed, NSError *error)) block;

@end
