//
//  LvVideoProcessor.m
//  AVFoundationTest
//
//  Created by Zhang Ming on 12-11-9.
//  Copyright (c) 2012年 Lver. All rights reserved.
//

#import "LvVideoCamera.h"

@interface LvVideoCamera() {
    dispatch_queue_t cameraProcessingQueue;
    AVCaptureVideoDataOutput *videoOutput;
    AVCaptureStillImageOutput *photoOutput;
}

@end

@implementation LvVideoCamera

- (void)dealloc
{
    [self stopCameraCapture];
    
    [videoOutput setSampleBufferDelegate:nil queue:dispatch_get_main_queue()];
    
    if (cameraProcessingQueue != NULL)
        dispatch_release(cameraProcessingQueue);
    
    [self removeInputsAndOutputs];
}

- (id)init
{
    if (self = [super init]) {
        
        capturePaused = NO;
        cameraProcessingQueue = dispatch_queue_create("com.iiseeuu.cameraProcessingQueue", NULL);
        frameRenderingSemaphore = dispatch_semaphore_create(1);
        
        // Create the session
        _session = [[AVCaptureSession alloc] init];
        [_session beginConfiguration];
        
        // Configure the session to produce lower resolution video frames, if your
        // processing algorithm can cope. We'll specify medium quality for the
        // chosen device.
        _session.sessionPreset = AVCaptureSessionPresetHigh;
        
        // Find a suitable AVCaptureDevice
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        
        NSError *error = nil;
        // Create a device input with the device and add it to the session.
        videoInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
        
        if (!videoInput) {
            NSLog(@"create input error");
        }
        
        if ([_session canAddInput:videoInput])
            [_session addInput:videoInput];
        
//        // Create a VideoDataOutput and add it to the session
//        videoOutput = [[AVCaptureVideoDataOutput alloc] init];
//        [videoOutput setAlwaysDiscardsLateVideoFrames:NO];
//        videoOutput.videoSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA]
//                                                           forKey:(id)kCVPixelBufferPixelFormatTypeKey];
//        
//        // Configure your output.
//        [videoOutput setSampleBufferDelegate:self queue:cameraProcessingQueue];
//        if ([_session canAddOutput:videoOutput])
//            [_session addOutput:videoOutput];
        
        // create a ImageOutput and add it to the session
        photoOutput = [[AVCaptureStillImageOutput alloc] init];
        [photoOutput setOutputSettings:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA]
                                                                   forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
        if ([_session canAddOutput:photoOutput])
            [_session addOutput:photoOutput];
        
        _previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_session];
        [_previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        
        [_session commitConfiguration];
        
    }
    
    return self;
}

- (void)removeInputsAndOutputs
{
    [_session removeInput:videoInput];
//    [_session removeOutput:videoOutput];
    [_session removeOutput:photoOutput];
}

- (void)startCameraCapture
{
    if (![_session isRunning])
        [_session startRunning];
}

- (void)stopCameraCapture
{
    if ([_session isRunning])
        [_session stopRunning];
}

- (void)embedPreviewInView:(UIView *)view
{
    _previewLayer.frame = view.bounds;
    [view.layer addSublayer:_previewLayer];
}

- (void)capturePhotoAsImageWithCompletionHandler:(void (^)(UIImage *proceed, NSError *error)) block
{
    dispatch_semaphore_wait(frameRenderingSemaphore, DISPATCH_TIME_FOREVER);
    
    [photoOutput captureStillImageAsynchronouslyFromConnection:[[photoOutput connections] objectAtIndex:0] completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
        dispatch_semaphore_signal(frameRenderingSemaphore);
        
        [self captureOutput:photoOutput didOutputSampleBuffer:imageDataSampleBuffer
             fromConnection:photoOutput.connections[0]];
        
        UIImage *image = [self imageFromCurrentlyProcessedOutput:imageDataSampleBuffer];//[self imageFromSampleBuffer:imageDataSampleBuffer];
        
        dispatch_semaphore_wait(frameRenderingSemaphore, DISPATCH_TIME_FOREVER);
        
        block(image, error);
    }];
}

- (UIImage *)imageFromCurrentlyProcessedOutput:(CMSampleBufferRef)imageDataSampleBuffer
{
    UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
    UIImageOrientation imageOrientation = UIImageOrientationLeft;
	switch (deviceOrientation)
    {
		case UIDeviceOrientationPortrait:
			imageOrientation = UIImageOrientationUp;
			break;
		case UIDeviceOrientationPortraitUpsideDown:
			imageOrientation = UIImageOrientationDown;
			break;
		case UIDeviceOrientationLandscapeLeft:
			imageOrientation = UIImageOrientationLeft;
			break;
		case UIDeviceOrientationLandscapeRight:
			imageOrientation = UIImageOrientationRight;
			break;
		default:
			imageOrientation = UIImageOrientationUp;
			break;
	}
    
    return [self imageFromSampleBuffer:imageDataSampleBuffer orientation:UIImageOrientationRight];
}

- (UIImage *)imageFromCurrentlyProcessedOutputWithOrientation:(UIImageOrientation)imageOrientation
{
    return nil;
}


#pragma mark - AVCaptureVideoDataOutputSampleBufferDelegate methods
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    
}

- (UIImage *)imageFromSampleBuffer:(CMSampleBufferRef)sampleBuffer orientation:(UIImageOrientation)orientation
{
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // Get the number of bytes per row for the pixel buffer
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    // Get the pixel buffer width and height
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    // Create a bitmap graphics context with the sample buffer data
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    
    // Create a Quartz image from the pixel data in the bitmap graphics context
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
    
    // Free up the context and color space
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    // Create an image object from the Quartz image
    UIImage *image = [UIImage imageWithCGImage:quartzImage scale:1.0 orientation:orientation];
    
    // Release the Quartz image
    CGImageRelease(quartzImage);
    
    dispatch_semaphore_signal(frameRenderingSemaphore);
    
    return image;
}

@end
