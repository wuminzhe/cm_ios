//
//  MZClient.h
//  iCard
//
//  Created by Zhang Ming on 13-1-25.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MZClient : NSObject

@property (readonly, nonatomic) NSString *clientId;
@property (readonly, nonatomic) NSString *clientSecret;
@property (readonly, nonatomic) NSURL *redirectURL;
@property (strong, nonatomic) NSURL *authorizeURL;


- (id)initWithClientId:(NSString *)clientId
                secret:(NSString *)clientSecret
           redirectURL:(NSURL *)redirectURL;


@end


@interface MZToken : NSObject<NSCoding>

@property (copy, nonatomic) NSString *uid;
@property (copy, nonatomic) NSString *accessToken;
@property (copy, nonatomic) NSString *refreshToken;
@property (copy, nonatomic) NSDate *expiresAt;

@property (copy, nonatomic) NSString *openId;              // 腾讯

@end