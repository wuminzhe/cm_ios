//
//  MZOAuthController.h
//  iCard
//
//  Created by Zhang Ming on 13-1-25.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MZClient.h"

@protocol MZOAuthControllerDelegate <NSObject>

@required

- (void)authorizeUsingWebView:(UIWebView *)webView;

@end

@interface MZOAuthController : UIViewController<UIWebViewDelegate> {
    
}

@property (strong, nonatomic) UIWebView *webView;
@property (copy, nonatomic) NSString *requestURLString;

@property (assign, nonatomic) id<MZOAuthControllerDelegate> delegate;

@end
