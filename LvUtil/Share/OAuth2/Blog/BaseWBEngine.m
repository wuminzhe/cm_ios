//
//  BaseWBEngine.m
//  iCard
//
//  Created by Zhang Ming on 13-1-25.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseWBEngine.h"

@implementation BaseWBEngine

#pragma mark - initialize methods
- (id)initWithClient:(MZClient *)client
{
    if (self = [super init]) {
        self.client = client;
    }
    
    return self;
}

- (void)authorize
{
    [self authorize:nil inSafari:NO];
}

- (void)authorize:(NSDictionary *)additionParams
{
    [self authorize:additionParams inSafari:NO];
}

- (void)authorizeInSafari:(BOOL)inSafari
{
    [self authorize:nil inSafari:inSafari];
}

- (void)authorize:(NSDictionary *)additionParams inSafari:(BOOL)inSafari
{
    if (inSafari) {
        
    } else {
        self.oauthController = [[MZOAuthController alloc] init];
        self.oauthController.delegate = self;
        self.oauthController.requestURLString = self.requestURLStirng;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.oauthController];
        [self.rootViewController presentModalViewController:navigationController animated:YES];
    }
}

- (NSMutableDictionary *)encodeParams:(NSDictionary *)params
{
    NSMutableDictionary *encodedParams = nil;
    
    if (params.count > 0) {
        encodedParams = [[NSMutableDictionary alloc] initWithCapacity:params.count];
        
        for (NSString *key in params.allKeys) {
            [encodedParams setObject:[[params objectForKey:key] URLEncodedString] forKey:key];
        }
    }
    
    return encodedParams;
}

- (NSString *)paramString:(NSDictionary *)params
{
    NSMutableString *paramString = nil;
    
    if (params.count > 0) {
        paramString = [[NSMutableString alloc] init];
        
        for (NSString *key in params.allKeys) {
            [paramString appendFormat:@"&%@=%@", key, [params objectForKey:key]];
        }
        
        return [paramString substringFromIndex:1];
    }
    
    return paramString;
}

@end
