//
//  QQEngine.m
//  iCard
//
//  Created by MingZhang on 13-1-26.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "QQEngine.h"

@implementation QQEngine

- (id)init
{
    if (self = [super init]) {
        self.client = [[MZClient alloc] initWithClientId:kQQAppKey secret:kQQAppSecret redirectURL:[NSURL URLWithString:kQQAppRedirect]];
        self.client.authorizeURL = [NSURL URLWithString:kQQAppAuthorize];
        
        self.additionParams = @{ @"response_type" : @"token"};
        
        self.token = [NSKeyedUnarchiver unarchiveObjectWithFile:kQQLvTokenSavePath];
        
        self.httpClient = [[HTTPClient alloc] init];
        self.httpClient.delegate = self;
    }
    
    return self;
}

- (void)authorize:(NSDictionary *)additionParams inSafari:(BOOL)inSafari
{
    NSString *urlString = self.client.authorizeURL.absoluteString;
    
    NSDictionary *baseParams = @{ @"client_id" : self.client.clientId, @"redirect_uri" : self.client.redirectURL.absoluteString};
    
    self.requestURLStirng = [urlString stringByAppendingFormat:@"?%@", [baseParams stringFromEncodedComponents]];
    
    self.requestURLStirng = [self.requestURLStirng stringByAppendingFormat:@"&%@", [self.additionParams stringFromEncodedComponents]];
    
    [super authorize:additionParams inSafari:inSafari];
}

#pragma mark - API
- (BOOL)isLoggedIn
{
    return self.token.accessToken && self.token.refreshToken && self.token.openId;
}

- (BOOL)isAuthorizeExpired
{
    NSDate *now = [NSDate date];
    
    return ([now compare:self.token.expiresAt] == NSOrderedDescending);
}

- (BOOL)isAuthValid
{
    return [self isLoggedIn] && ![self isAuthorizeExpired];
}

- (void)logIn
{
    if ([self isAuthValid]) {
        
    } else {
        [self authorize];
    }
}

- (void)logOut
{
    [NSKeyedArchiver archiveRootObject:nil toFile:kQQLvTokenSavePath];
    self.token = nil;
}

#pragma mark - Request
- (NSURLRequest *)requestWithPath:(NSString *)path params:(NSDictionary *)params httpMethod:(NSString *)method
{
    // 解析params参数
    NSMutableDictionary *requestParams = [[NSMutableDictionary alloc] initWithDictionary:params];
    NSData *postData = nil;
    
    requestParams[@"access_token"] = self.token.accessToken;
    requestParams[@"oauth_consumer_key"] = kTencentAppKey;
    requestParams[@"openid"] = self.token.openId;
    
    NSArray *keys = requestParams.allKeys;
    
    for (NSString *key in keys) {
        if ([requestParams[key] isKindOfClass:[UIImage class]]) {
            postData = UIImagePNGRepresentation(requestParams[key]);
            
            [requestParams removeObjectForKey:key];
        } else if ([requestParams[key] isKindOfClass:[NSData class]]) {
            postData = requestParams[key];
            
            [requestParams removeObjectForKey:key];
        }
    }
    
    NSString *fullURLString = [kQQAppOAuth2APIDomain stringByAppendingString:path];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:fullURLString] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:kQQAppRequestTimeOutInterval];
    
    if ([[method uppercaseString] isEqualToString:@"POST"]) {
        if (postData)
            [self.httpClient setMultipartFormData:request params:requestParams data:postData filename:@"file" contentType:@"image/jpeg"];
        else {
            [request setHTTPMethod:@"POST"];
            NSString *paramString = [self paramString:[self encodeParams:requestParams]] ;
            [request setHTTPBody:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
            self.httpClient.mutableURLRequest = request;
        }
    }
    
    [self.httpClient connect];
    
    return request;
}

#pragma mark - MZOAuthControllerDelegate methods
- (void)authorizeUsingWebView:(UIWebView *)webView
{
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.requestURLStirng]]];
    
    webView.delegate = self;
}

#pragma mark - UIWebViewDelegate methods
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *response = webView.request.URL.absoluteString;
    
    // 如果返回的字符串中含有access_token字符串，表示授权成功
    if ([response rangeOfString:@"access_token"].location != NSNotFound) {
        
        if (self.isAuthorized)
            return;
        
        // 根据 & 号进行分割
        NSArray *pairs = [response componentsSeparatedByString:@"&"];
        
        if (!self.token)
            self.token = [[MZToken alloc] init];
        
        for (NSString *pair in pairs) {
            NSArray *elements = [pair componentsSeparatedByString:@"="];
            
            if ([elements[0] rangeOfString:@"access_token"].location != NSNotFound) {
                self.token.accessToken = elements[1];
                
            } else if ([elements[0] isEqualToString:@"expires_in"]) {
                NSTimeInterval timeInterval  = [[elements[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] doubleValue];
                
                NSDate *expiresIn = [NSDate dateWithTimeIntervalSinceNow:timeInterval];
                
                self.token.expiresAt = expiresIn;
            } else if ([elements[0] isEqualToString:@"openid"]) {
                self.token.openId = elements[1];
            }
        }
        
        // 序列化
        [NSKeyedArchiver archiveRootObject:self.token toFile:kQQLvTokenSavePath];
        
        [self.oauthController dismissModalViewControllerAnimated:YES];
        
        if ([self.delegate respondsToSelector:@selector(oauthClient:receiveAccessToken:)]) {
            [self.delegate oauthClient:self receiveAccessToken:self.token];
        }
        
        if ([self.delegate respondsToSelector:@selector(oauthClientDidLogIn:)]) {
            [self.delegate oauthClientDidLogIn:self];
        }
        
        self.isAuthorized = YES;
    }
}

#pragma mark - Private methods
- (void)handleError:(NSError *)error
{
    NSLog(@"error = %@", error);
}

#pragma mark - HTTPClientDelegate methods
- (void)handleRequest:(NSURLRequest *)request didReceiveResponse:(NSURLResponse *)response
{
    
}

- (void)handleResponseData:(NSMutableData *)responseData
{
    if ([self.delegate respondsToSelector:@selector(oauthClientRequestDidSuccess:)]) {
        [self.delegate oauthClientRequestDidSuccess:self];
    }
}

- (void)handleRequestFailed:(NSError *)error
{
    [self handleError:error];
    
    if ([self.delegate respondsToSelector:@selector(oauthClientRequestDidFail:errorMsg:)]) {
        [self.delegate oauthClientRequestDidFail:self errorMsg:self.errorMsg];
    }
}

@end
