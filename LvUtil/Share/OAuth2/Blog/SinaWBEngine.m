//
//  SinaBlog.m
//  iCard
//
//  Created by Zhang Ming on 13-1-25.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "SinaWBEngine.h"

@implementation SinaWBEngine


#pragma mark - SinaBlog instance methods
- (id)init
{
    if (self = [super init]) {
        self.client = [[MZClient alloc] initWithClientId:kSinaAppKey secret:kSinaAppSecret redirectURL:[NSURL URLWithString:kSinaAppRedirect]];
        self.client.authorizeURL = [NSURL URLWithString:kSinaAppAuthorize];
        self.additionParams = @{ @"response_type" : @"token", @"display" : @"mobile" };
        
        self.token = [NSKeyedUnarchiver unarchiveObjectWithFile:kSinaLvTokenSavePath];
        
        self.httpClient = [[HTTPClient alloc] init];
        self.httpClient.delegate = self;
    }
    
    return self;
}

- (void)authorize:(NSDictionary *)additionParams inSafari:(BOOL)inSafari
{
    NSString *urlString = self.client.authorizeURL.absoluteString;
    
    NSDictionary *baseParams = @{ @"client_id" : self.client.clientId, @"redirect_uri" : self.client.redirectURL.absoluteString};
    
    self.requestURLStirng = [urlString stringByAppendingFormat:@"?%@", [baseParams stringFromEncodedComponents]];
    
    self.requestURLStirng = [self.requestURLStirng stringByAppendingFormat:@"&%@", [self.additionParams stringFromEncodedComponents]];
    
    [super authorize:additionParams inSafari:inSafari];
}

#pragma mark - API 
- (BOOL)isLoggedIn
{
    return self.token.uid && self.token.accessToken && self.token.expiresAt;
}

- (BOOL)isAuthorizeExpired
{
    NSDate *now = [NSDate date];
    
    return ([now compare:self.token.expiresAt] == NSOrderedDescending);
}

- (BOOL)isAuthValid
{
    return [self isLoggedIn] && ![self isAuthorizeExpired];
}

- (void)logIn
{
    if ([self isAuthValid]) {
        
    } else {
        [self authorize];
    }
}

- (void)logOut
{
    [NSKeyedArchiver archiveRootObject:nil toFile:kSinaLvTokenSavePath];
    self.token = nil;
}


#pragma mark - Request
- (NSURLRequest *)requestWithPath:(NSString *)path params:(NSDictionary *)params httpMethod:(NSString *)method
{
    // 解析params参数
    NSMutableDictionary *requestParams = [[NSMutableDictionary alloc] initWithDictionary:params];
    NSData *postData = nil;

    requestParams[@"access_token"] = self.token.accessToken;
    
    NSArray *keys = requestParams.allKeys;
    
    for (NSString *key in keys) {
        if ([requestParams[key] isKindOfClass:[UIImage class]]) {
            postData = UIImagePNGRepresentation(requestParams[key]);
            
            [requestParams removeObjectForKey:key];
        } else if ([requestParams[key] isKindOfClass:[NSData class]]) {
            postData = requestParams[key];
            
            [requestParams removeObjectForKey:key];
        }
    }
    
    NSString *fullURLString = [kSinaAppOAuth2APIDomain stringByAppendingString:path];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:fullURLString] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:kSinaAppRequestTimeOutInterval];
    
    if ([[method uppercaseString] isEqualToString:@"POST"]) {
        if (postData)
            [self.httpClient setMultipartFormData:request params:requestParams data:postData filename:@"file" contentType:@"image/png"];
        else {
            [request setHTTPMethod:@"POST"];
            NSString *paramString = [self paramString:[self encodeParams:requestParams]] ;
            [request setHTTPBody:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
            self.httpClient.mutableURLRequest = request;
        }
    }
    
    [self.httpClient connect];
    
    return request;
}

#pragma mark - MZOAuthControllerDelegate methods
- (void)authorizeUsingWebView:(UIWebView *)webView
{
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.requestURLStirng]]];
    
    webView.delegate = self;
}

#pragma mark - UIWebViewDelegate methods
 - (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *response = webView.request.URL.absoluteString;
    // 如果返回的字符串中含有access_token字符串，表示授权成功
    if ([response rangeOfString:@"access_token"].location != NSNotFound) {
        
        if (self.isAuthorized)
            return;
        
        // 根据 & 号进行分割
        NSArray *pairs = [response componentsSeparatedByString:@"&"];
        
        if (!self.token)
            self.token = [[MZToken alloc] init];
        
        for (NSString *pair in pairs) {
            NSArray *elements = [pair componentsSeparatedByString:@"="];
            
            if ([elements[0] rangeOfString:@"access_token"].location != NSNotFound) {
                self.token.accessToken = elements[1];
            } else if ([elements[0] isEqualToString:@"expires_in"]) {
                NSTimeInterval timeInterval  = [[elements[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] doubleValue];
                
                NSDate *expiresIn = [NSDate dateWithTimeIntervalSinceNow:timeInterval];
                
                self.token.expiresAt = expiresIn;
            } else if ([elements[0] isEqualToString:@"uid"]) {
                self.token.uid = elements[1];
            }
        }
        
        // 序列化
        [NSKeyedArchiver archiveRootObject:self.token toFile:kSinaLvTokenSavePath];
        
        [self.oauthController dismissModalViewControllerAnimated:YES];
        
        if ([self.delegate respondsToSelector:@selector(oauthClient:receiveAccessToken:)]) {
            [self.delegate oauthClient:self receiveAccessToken:self.token];
        }
        
        self.isAuthorized = YES;
    }
}

#pragma mark - Private methods
- (void)handleError:(NSError *)error
{
    NSLog(@"error = %@", error);
}

#pragma mark - HTTPClientDelegate methods
- (void)handleRequest:(NSURLRequest *)request didReceiveResponse:(NSURLResponse *)response
{
    
}

- (void)handleResponseData:(NSMutableData *)responseData
{
    NSDictionary *response = [responseData objectFromJSONData];
    
    if (response) {
        NSInteger errorCode = 0;
        
        errorCode = [[response stringForKey:@"error_code"] integerValue];
        
        if (errorCode != 0) {
            
            NSString *errorDescription = [response stringForKey:@"error"];
            
            // 可以包装成error传递回去
            
            if ([self.delegate respondsToSelector:@selector(oauthClientRequestDidFail:errorMsg:)]) {
                [self.delegate oauthClientRequestDidFail:self errorMsg:errorDescription];
            }
        } else {
            
            if ([self.delegate respondsToSelector:@selector(oauthClientRequestDidSuccess:)]) {
                [self.delegate oauthClientRequestDidSuccess:self];
            }
        }
    }
}

- (void)handleRequestFailed:(NSError *)error
{
    [self handleError:error];
    
    if ([self.delegate respondsToSelector:@selector(oauthClientRequestDidFail:errorMsg:)]) {
        [self.delegate oauthClientRequestDidFail:self errorMsg:self.errorMsg];
    }
}

@end
