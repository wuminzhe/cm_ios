//
//  BaseWBEngine.h
//  iCard
//
//  Created by Zhang Ming on 13-1-25.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZOAuthController.h"
#import "MZWBConstant.h"
#import "MZClient.h"
#import "HTTPClient.h"
#import "NSString+Coding.h"
#import "JSONKit.h"
#import "NSDictionary+Extension.h"

@protocol OAuthClientDelegate <NSObject>

@optional
- (void)oauthClient:(id)engine receiveAccessToken:(MZToken *)token;
- (void)oauthClientDidCancel:(id)engine;

- (void)oauthClientDidLogIn:(id)engine;
- (void)oauthClientDidLogOut:(id)engine;

- (void)oauthClientRequestDidSuccess:(id)engine;
- (void)oauthClientRequestDidFail:(id)engine errorMsg:(NSString *)errorMsg;

@end

@interface BaseWBEngine : NSObject<UIWebViewDelegate, MZOAuthControllerDelegate, HTTPClientDelegate>

@property (copy, nonatomic) NSString *requestURLStirng;
@property (strong, nonatomic) MZClient *client;
@property (strong, nonatomic) MZToken *token;
@property (strong, nonatomic) NSDictionary *additionParams;
@property (assign, nonatomic) UIViewController *rootViewController;
@property (strong, nonatomic) MZOAuthController *oauthController;
@property (assign, nonatomic) id<OAuthClientDelegate> delegate;
@property (assign, nonatomic) BOOL isAuthorized;

@property (strong, nonatomic) HTTPClient *httpClient;
@property (strong, nonatomic) NSString *errorMsg;

- (id)initWithClient:(MZClient *)client;

- (void)authorize;

- (void)authorize:(NSDictionary *)additionParams;

- (void)authorizeInSafari:(BOOL)inSafari;

- (void)authorize:(NSDictionary *)additionParams inSafari:(BOOL)inSafari;

- (BOOL)isLoggedIn;
- (BOOL)isAuthorizeExpired;
- (BOOL)isAuthValid;

- (void)logIn;
- (void)logOut;

- (NSMutableDictionary *)encodeParams:(NSDictionary *)params;
- (NSString *)paramString:(NSDictionary *)params;

- (NSURLRequest *)requestWithPath:(NSString *)path params:(NSDictionary *)params httpMethod:(NSString *)method;

@end
