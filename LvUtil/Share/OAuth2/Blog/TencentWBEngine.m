//
//  TencentWBEngine.m
//  iCard
//
//  Created by MingZhang on 13-1-26.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "TencentWBEngine.h"

@implementation TencentWBEngine

- (id)init
{
    if (self = [super init]) {
        self.client = [[MZClient alloc] initWithClientId:kTencentAppKey secret:kTencentAppSecret redirectURL:[NSURL URLWithString:kTencentAppRedirect]];
        self.client.authorizeURL = [NSURL URLWithString:KTencentAppAuthorize];
        
        self.additionParams = @{ @"response_type" : @"token", @"appfrom" : @"ios"};
        
        self.token = [NSKeyedUnarchiver unarchiveObjectWithFile:kTencentLvTokenSavePath];
        
        self.httpClient = [[HTTPClient alloc] init];
        self.httpClient.delegate = self;
    }
    
    return self;
}

- (void)authorize:(NSDictionary *)additionParams inSafari:(BOOL)inSafari
{
    NSString *urlString = self.client.authorizeURL.absoluteString;
    
    NSDictionary *baseParams = @{ @"client_id" : self.client.clientId, @"redirect_uri" : self.client.redirectURL.absoluteString};
    
    self.requestURLStirng = [urlString stringByAppendingFormat:@"?%@", [baseParams stringFromEncodedComponents]];
    
    self.requestURLStirng = [self.requestURLStirng stringByAppendingFormat:@"&%@", [self.additionParams stringFromEncodedComponents]];
    
    [super authorize:additionParams inSafari:inSafari];
}

#pragma mark - API
- (BOOL)isLoggedIn
{
    return self.token.uid && self.token.accessToken && self.token.refreshToken;
}

- (BOOL)isAuthorizeExpired
{
    NSDate *now = [NSDate date];
    
    return ([now compare:self.token.expiresAt] == NSOrderedDescending);
}

- (BOOL)isAuthValid
{
    return [self isLoggedIn] && ![self isAuthorizeExpired];
}

- (void)logIn
{
    if ([self isAuthValid]) {
        
    } else {
        [self authorize];
    }
}

- (void)logOut
{
    [NSKeyedArchiver archiveRootObject:nil toFile:kTencentLvTokenSavePath];
    self.token = nil;
}

#pragma mark - Request
- (NSURLRequest *)requestWithPath:(NSString *)path params:(NSDictionary *)params httpMethod:(NSString *)method
{
    // 解析params参数
    NSMutableDictionary *requestParams = [[NSMutableDictionary alloc] initWithDictionary:params];
    NSData *postData = nil;
    
    requestParams[@"access_token"] = self.token.accessToken;
    requestParams[@"oauth_consumer_key"] = kTencentAppKey;
    requestParams[@"openid"] = self.token.openId;
    requestParams[@"oauth_version"] = @"2.a";
    requestParams[@"scope"] = @"all";
    requestParams[@"clientip"] = @"10.10.1.31";
    requestParams[@"format"] = @"json";
    
    NSArray *keys = requestParams.allKeys;
    
    for (NSString *key in keys) {
        if ([requestParams[key] isKindOfClass:[UIImage class]]) {
            postData = UIImagePNGRepresentation(requestParams[key]);
            
            [requestParams removeObjectForKey:key];
        } else if ([requestParams[key] isKindOfClass:[NSData class]]) {
            postData = requestParams[key];
            
            [requestParams removeObjectForKey:key];
        }
    }
    
    NSString *fullURLString = [kTencentAppOAuth2APIDomain stringByAppendingString:path];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:fullURLString] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:kTencentAppRequestTimeOutInterval];
    
    if ([[method uppercaseString] isEqualToString:@"POST"]) {
        if (postData)
            [self.httpClient setMultipartFormData:request params:requestParams data:postData filename:@"file" contentType:@"image/jpeg"];
        else {
            [request setHTTPMethod:@"POST"];
            NSString *paramString = [self paramString:[self encodeParams:requestParams]] ;
            [request setHTTPBody:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
            self.httpClient.mutableURLRequest = request;
        }
    }
    
    [self.httpClient connect];
    
    return request;
}

#pragma mark - MZOAuthControllerDelegate methods
- (void)authorizeUsingWebView:(UIWebView *)webView
{
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.requestURLStirng]]];
    
    webView.delegate = self;
}

#pragma mark - UIWebViewDelegate methods
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *response = webView.request.URL.absoluteString;
    
    // 如果返回的字符串中含有access_token字符串，表示授权成功
    if ([response rangeOfString:@"access_token"].location != NSNotFound) {
        
        if (self.isAuthorized)
            return;
        
        // 根据 & 号进行分割
        NSArray *pairs = [response componentsSeparatedByString:@"&"];
        
        if (!self.token)
            self.token = [[MZToken alloc] init];
        
        for (NSString *pair in pairs) {
            NSArray *elements = [pair componentsSeparatedByString:@"="];
            
            if ([elements[0] rangeOfString:@"access_token"].location != NSNotFound) {
                self.token.accessToken = elements[1];
                
            } else if ([elements[0] isEqualToString:@"expires_in"]) {
                NSTimeInterval timeInterval  = [[elements[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] doubleValue];
                
                NSDate *expiresIn = [NSDate dateWithTimeIntervalSinceNow:timeInterval];
                
                self.token.expiresAt = expiresIn;
            } else if ([elements[0] isEqualToString:@"name"]) {
                self.token.uid = elements[1];
                
            } else if ([elements[0] isEqualToString:@"refresh_token"]) {
                self.token.refreshToken = elements[1];
                
            } else if ([elements[0] isEqualToString:@"openid"]) {
                self.token.openId = elements[1];
            }
        }
        
        // 序列化
        [NSKeyedArchiver archiveRootObject:self.token toFile:kTencentLvTokenSavePath];
        
        [self.oauthController dismissModalViewControllerAnimated:YES];
        
        if ([self.delegate respondsToSelector:@selector(oauthClient:receiveAccessToken:)]) {
            [self.delegate oauthClient:self receiveAccessToken:self.token];
        }
        
        self.isAuthorized = YES;
    }
}

#pragma mark - Private methods
- (void)handleError:(NSError *)error
{
    NSLog(@"error = %@", error);
}

#pragma mark - HTTPClientDelegate methods
- (void)handleRequest:(NSURLRequest *)request didReceiveResponse:(NSURLResponse *)response
{
    
}

- (void)handleResponseData:(NSMutableData *)responseData
{
    NSDictionary *response = [responseData objectFromJSONData];
    
    if (response) {
        NSInteger errCode = 0;
        
        errCode = [response stringForKey:@"errcode"].integerValue;
        
        if (errCode != 0) {
            NSString *errDescription = [response stringForKey:@"msg"];
            
            if ([self.delegate respondsToSelector:@selector(oauthClientRequestDidFail:errorMsg:)]) {
                [self.delegate oauthClientRequestDidFail:self errorMsg:errDescription];
            }
        } else {
            if ([self.delegate respondsToSelector:@selector(oauthClientRequestDidSuccess:)]) {
                [self.delegate oauthClientRequestDidSuccess:self];
            }
        }
    }
}

- (void)handleRequestFailed:(NSError *)error
{
    [self handleError:error];
    
    if ([self.delegate respondsToSelector:@selector(oauthClientRequestDidFail:errorMsg:)]) {
        [self.delegate oauthClientRequestDidFail:self errorMsg:self.errorMsg];
    }
}

@end
