//
//  MZClient.m
//  iCard
//
//  Created by Zhang Ming on 13-1-25.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MZClient.h"

@implementation MZClient

- (id)initWithClientId:(NSString *)clientId secret:(NSString *)clientSecret redirectURL:(NSURL *)redirectURL
{
    if (self = [super init]) {
        _clientId = [clientId copy];
        _clientSecret = [clientSecret copy];
        _redirectURL = [redirectURL copy];
    }
    
    return self;
}

@end


@implementation MZToken

#pragma mark -
#pragma mark NSCoding

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_uid forKey:@"uid"];
    [aCoder encodeObject:_accessToken forKey:@"accessToken"];
    [aCoder encodeObject:_expiresAt forKey:@"expiresAt"];
    [aCoder encodeObject:_refreshToken forKey:@"refreshToken"];
    [aCoder encodeObject:_openId forKey:@"openId"];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init]) {
        self.uid = [aDecoder decodeObjectForKey:@"uid"];
        self.accessToken = [aDecoder decodeObjectForKey:@"accessToken"];
        self.expiresAt = [aDecoder decodeObjectForKey:@"expiresAt"];
        self.refreshToken = [aDecoder decodeObjectForKey:@"refreshToken"];
        self.openId = [aDecoder decodeObjectForKey:@"openId"];
    }
    return self;
}

@end