//
//  MZWBConstant.h
//  iCard
//
//  Created by Zhang Ming on 13-1-25.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#define kDefaultMZTokenSavePath [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]

/// 新浪微博

#define kSinaAppRequestTimeOutInterval      180.0

#define kSinaAppKey                         @"3801945043"
#define kSinaAppSecret                      @"53d6ad49b51880efd70e99a4fbb299fe"
#define kSinaAppRedirect                    @"http://www.51ika.com"
#define kSinaAppAuthorize                   @"https://api.t.sina.com.cn/oauth2/authorize"
#define kSinaAppOAuth2APIDomain             @"https://api.weibo.com/2/"
#define kSinaLvTokenSavePath                [kDefaultMZTokenSavePath stringByAppendingPathComponent:@"SinaMZToken.cache"]

/// 腾讯微博

#define kTencentAppRequestTimeOutInterval   180.0  

#define kTencentAppKey                      @"801184473"
#define kTencentAppSecret                   @"2319fbee0ae70653e2277a3369c7a58c"
#define kTencentAppRedirect                 @"http://www.51ika.com"
#define KTencentAppAuthorize                @"https://open.t.qq.com/cgi-bin/oauth2/authorize"
#define kTencentAppOAuth2APIDomain          @"https://open.t.qq.com/api/"
#define kTencentLvTokenSavePath             [kDefaultMZTokenSavePath stringByAppendingPathComponent:@"TencentMZToken.cache"]

/// QQ平台

#define kQQAppRequestTimeOutInterval         180.0

#define kQQAppKey                           @"100370102"
#define kQQAppSecret                        @"ca64c12b1c2f784317e506a983fbd8ff"
#define kQQAppRedirect                      @"http://www.qq.com"
#define kQQAppAuthorize                     @"https://graph.qq.com/oauth2.0/authorize"
#define kQQAppOAuth2APIDomain               @"https://graph.qq.com/"
#define kQQLvTokenSavePath                  [kDefaultMZTokenSavePath stringByAppendingPathComponent:@"QQMZToken.cache"]