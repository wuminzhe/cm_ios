//
//  RFC1867.m
//  iCard
//
//  Created by MingZhang on 13-1-26.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "RFC1867.h"

@implementation RFC1867

- (NSString *)nameValString:(NSDictionary *)dict
{
    NSString *result = [NSString string];
    
    NSArray *keys = dict.allKeys;
    
    for (int i = 0; i < keys.count; i++) {
        result = [result stringByAppendingString:@"--"];
        result = [result stringByAppendingString:FORM_BOUNDARY];
        result = [result stringByAppendingString:@"\r\nContent-Disposition: form-data; name=\""];
        result = [result stringByAppendingString:keys[i]];
        result = [result stringByAppendingString:@"\"\r\n\r\n"];
        result = [result stringByAppendingString:[dict valueForKey:keys[i]]];
        result = [result stringByAppendingString:@"\r\n"];
    }
    
    return result;
}

- (NSMutableData *)getMultipartFormData:(NSDictionary *)dict
                                   data:(NSData *)data
                                   name:(NSString *)aName
                               filename:(NSString *)aFilename
                            contentType:(NSString *)aContentType
{
    NSMutableData *result = [NSMutableData data];
    
    NSString *param  = [self nameValString:dict];
    
    param = [param stringByAppendingFormat:@"--%@\r\n", FORM_BOUNDARY];
    param = [param stringByAppendingFormat:@"Content-Disposition: form-data; name=\"%@\";filename=\"%@\"\r\nContent-Type: %@\r\n\r\n", aName, aFilename, aContentType];
    
    [result appendData:[param dataUsingEncoding:NSUTF8StringEncoding]];
    [result appendData:data];
    
    NSString *footer = [NSString stringWithFormat:@"\r\n--%@--\r\n", FORM_BOUNDARY];
    [result appendData:[footer dataUsingEncoding:NSUTF8StringEncoding]];
    
    return result;
}

@end
