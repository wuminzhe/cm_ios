//
//  HTTPClient.m
//  iCard
//
//  Created by MingZhang on 13-1-26.
//  Copyright (c) 2013年 Leisure. All rights reserved.
//

#import "HTTPClient.h"
#import "RFC1867.h"

@implementation HTTPClient

- (void)setMultipartFormData:(NSMutableURLRequest *)aRequest
                      params:(NSDictionary *)aParams
                        data:(NSData *)aData
                    filename:(NSString *)aFilename
                 contentType:(NSString *)aContentType
{
    RFC1867 *rfc = [[RFC1867 alloc] init];
    NSMutableData *data = [rfc getMultipartFormData:aParams data:aData name:@"pic" filename:aFilename contentType:aContentType];
    
    [aRequest setHTTPMethod:@"POST"];
    [aRequest setHTTPShouldHandleCookies:NO];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", FORM_BOUNDARY];
    [aRequest setValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    if (data) {
    
        [aRequest setValue:[NSString stringWithFormat:@"%d", data.length] forHTTPHeaderField:@"Content-Length"];
        [aRequest setHTTPBody:data];
    }
    
    self.mutableURLRequest = aRequest;
}

- (void)connect
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    self.connection = [[NSURLConnection alloc] initWithRequest:self.mutableURLRequest
                                                      delegate:self
                                              startImmediately:YES];
}

- (void)disconnect
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.responseData = nil;
    
    [self.connection cancel];
    self.connection = nil;
}

#pragma mark - NSURLConnectionDataDelegate methods
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    return nil;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.responseData = [[NSMutableData alloc] init];
    
    if ([self.delegate respondsToSelector:@selector(handleRequest:didReceiveResponse:)])
        [self.delegate handleRequest:self.mutableURLRequest didReceiveResponse:response];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.responseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"string = %@", [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding]);
    
    if ([self.delegate respondsToSelector:@selector(handleResponseData:)])
        [self.delegate handleResponseData:self.responseData];
    
    self.responseData = nil;
    
    [connection cancel];
    self.connection = nil;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - NSURLConnectionDelegate methods
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
//    NSLog(@"error = %@", error);
    
    if ([self.delegate respondsToSelector:@selector(handleRequestFailed:)])
        [self.delegate handleRequestFailed:error];
    
    self.responseData = nil;
    
    [connection cancel];
    self.connection = nil;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

@end
