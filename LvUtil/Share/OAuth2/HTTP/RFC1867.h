//
//  RFC1867.h
//  iCard
//
//  Created by MingZhang on 13-1-26.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

#define FORM_BOUNDARY               @"MZ20130126140256"

@interface RFC1867 : NSObject

- (NSString *)nameValString:(NSDictionary *)params;

- (NSMutableData *)getMultipartFormData:(NSDictionary *)dict
                                   data:(NSData *)data
                                   name:(NSString *)aName
                               filename:(NSString *)aFilename
                            contentType:(NSString *)aContentType;

@end
