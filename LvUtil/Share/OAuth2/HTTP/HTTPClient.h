//
//  HTTPClient.h
//  iCard
//
//  Created by MingZhang on 13-1-26.
//  Copyright (c) 2013年 Leisure. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HTTPClientDelegate <NSObject>

@optional
- (void)handleRequest:(NSURLRequest *)request didReceiveResponse:(NSURLResponse *)response;
- (void)handleResponseData:(NSMutableData *)responseData;
- (void)handleRequestFailed:(NSError *)error;

@end

@interface HTTPClient : NSObject<NSURLConnectionDelegate, NSURLConnectionDataDelegate> {
    
}

@property (strong, nonatomic) NSMutableURLRequest *mutableURLRequest;
@property (strong, nonatomic) NSMutableData *responseData;
@property (strong, nonatomic) NSURLConnection *connection;
@property (assign, nonatomic) id<HTTPClientDelegate> delegate;

- (void)setMultipartFormData:(NSMutableURLRequest *)aRequest
                      params:(NSDictionary *)aParams
                        data:(NSData *)aData
                    filename:(NSString *)aFilename
                 contentType:(NSString *)aContentType;


- (void)connect;
- (void)disconnect;

@end
