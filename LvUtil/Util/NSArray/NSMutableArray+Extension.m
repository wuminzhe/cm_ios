//
//  NSMutableArray+Extension.m
//  LvUtil
//
//  Created by Zhang Ming on 12-9-14.
//  Copyright (c) 2012年 Zhang Ming. All rights reserved.
//

#import "NSMutableArray+Extension.h"

@implementation NSMutableArray (Extension)

- (void)removeFirstObject
{
    if (self.count > 0)
        return [self removeObjectAtIndex:0];
}

@end
