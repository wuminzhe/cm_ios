//
//  NSArray+Extension.h
//  LvUtil
//
//  Created by Zhang Ming on 12-9-14.
//  Copyright (c) 2012年 Zhang Ming. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Extension)

- (id)firstObject;

@end
