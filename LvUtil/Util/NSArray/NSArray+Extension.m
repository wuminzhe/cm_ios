//
//  NSArray+Extension.m
//  LvUtil
//
//  Created by Zhang Ming on 12-9-14.
//  Copyright (c) 2012年 Zhang Ming. All rights reserved.
//

#import "NSArray+Extension.h"

@implementation NSArray (Extension)

- (id)firstObject
{
    if (self.count > 0) {
        return [self objectAtIndex:0];
    }
    
    return nil;
}

@end
