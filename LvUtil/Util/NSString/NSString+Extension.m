//
//  NSString+Extension.m
//  LvUtil
//
//  Created by Zhang Ming on 12-9-14.
//  Copyright (c) 2012年 Zhang Ming. All rights reserved.
//

#import "NSString+Extension.h"

@implementation NSString (Extension)


- (CGSize)calculateTextSize:(CGFloat)width andFont:(UIFont *)font
{
    return [self sizeWithFont:font
            constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)
                lineBreakMode:UILineBreakModeWordWrap];
}

/*  Carrier name: [中国移动]
    Mobile Country Code: [460]
    Mobile Network Code:[00]
    ISO Country Code:[cn]
    Allows VOIP? [YES]
 */
+ (NSString *)getCarrierName
{
    CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = info.subscriberCellularProvider;
    NSLog(@"carrier:%@", [carrier description]);
    return carrier.carrierName;
}

+ (NSString *)getDateString
{
    NSDate *now = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyyMMddhhmmss";
    
    return [dateFormatter stringFromDate:now];
}

@end
