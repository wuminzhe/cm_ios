//
//  NSString+Coding.h
//  LvUtil
//
//  Created by Zhang Ming on 12-9-14.
//  Copyright (c) 2012年 Zhang Ming. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Coding)

- (NSString *)URLEncodedString;

- (NSString *)URLDecodedString;

@end
