//
//  NSString+Verify.h
//  LvUtil
//
//  Created by Zhang Ming on 12-9-14.
//  Copyright (c) 2012年 Zhang Ming. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Verify)

- (BOOL)isValidEmail;

// 验证字符串是否都是数字
- (BOOL)isValidPhoneNumber;

- (BOOL)isValidCreditCardNumber;

@end
