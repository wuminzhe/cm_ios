//
//  NSString+Verify.m
//  LvUtil
//
//  Created by Zhang Ming on 12-9-14.
//  Copyright (c) 2012年 Zhang Ming. All rights reserved.
//

#import "NSString+Verify.h"

@implementation NSString (Verify)

- (BOOL)isValidEmail
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

- (BOOL)isValidPhoneNumber
{
    NSString *numberRegex = @"[0-9]{11}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegex];
    return [numberTest evaluateWithObject:self];
}

- (BOOL)isValidCreditCardNumber
{
    int len = self.length;
    
    if (len < 13 || len > 19) 
        return  NO;
    
    int sum = 0;
    
    /* 信用卡卡号规则验证
     * Issuer Identifier    Card Number                            Length
     * Diner’s Club         300xxx-305xxx, 3095xx,36xxxx, 38xxxx   14
     * American Express     34xxxx, 37xxxx                         15
     * VISA                 4xxxxx                                 13, 16
     * MasterCard           51xxxx-55xxxx                          16
     * JCB                  3528xx-358xxx                          16
     * Discover             6011xx                                 16
     * 银联                  622126-622925                          16
     * 算法
     * 1.偶数位卡号奇数位上数字*2，奇数位卡号偶数位上数字*2。
     * 2.大于等于10的位数减9。
     * 3.全部数字加起来。
     * 4.结果不是10的倍数的卡号非法。
     */
    for (int i = 0; i < len; i++) {
        NSRange range = NSMakeRange(i, 1);
        int multiResult = 0;
        int number = [[self substringWithRange:range] integerValue];
        
        if (len % 2 == 0) {
            if (i % 2 == 0) {
                multiResult = number * 2;
                
                if (multiResult >= 10) multiResult -= 9;
                
                sum += multiResult;
            } else {
                sum += number;
            }
        } else {
            if (i % 2 == 1) {
                multiResult = number * 2;
                if (multiResult >= 10) multiResult -= 9;
                
                sum += multiResult;
            } else {
                sum += number;
            }
        }
    }
    
    NSLog(@"sum = %d", sum);
    if (sum % 10 == 0)
        return YES;
    
    return NO;
}

@end
