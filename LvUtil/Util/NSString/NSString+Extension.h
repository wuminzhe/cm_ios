//
//  NSString+Extension.h
//  LvUtil
//
//  Created by Zhang Ming on 12-9-14.
//  Copyright (c) 2012年 Zhang Ming. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

@interface NSString (Extension)

// 计算文字的高度
- (CGSize)calculateTextSize:(CGFloat)width andFont:(UIFont *)font;

+ (NSString *)getCarrierName;

+ (NSString *)getDateString;
@end