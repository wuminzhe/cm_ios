//
//  UIControl+Blocks.h
//  TestOrientation
//
//  Created by Zhang Ming on 12-9-18.
//  Copyright (c) 2012年 lver. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIControl (Blocks)

- (void)addActionCompletionBlock:(void (^)(id sender))actionCompletionBlock
                 forControEvents:(UIControlEvents)controlEvents;

- (void)removeActionCompletionBlocksForControlEvents:(UIControlEvents)controlEvents;

@end
