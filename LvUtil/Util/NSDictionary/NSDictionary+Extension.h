//
//  NSDictionary+Extension.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-22.
//  Copyright (c) 2012年 lver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Extension)

// 解析json数据
- (NSString *)stringForKey:(NSString *)key;

- (NSString *)stringFromEncodedComponents;

@end
