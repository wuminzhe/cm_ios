//
//  NSDictionary+Extension.m
//  cardMaster
//
//  Created by Zhang Ming on 12-10-22.
//  Copyright (c) 2012年 lver. All rights reserved.
//

#import "NSDictionary+Extension.h"
#import "NSString+Coding.h"

@implementation NSDictionary (Extension)

- (NSString *)stringForKey:(NSString *)key
{
    id jsonValue = [self valueForKey:key];
    
    if (jsonValue == [NSNull null])
        return nil;
    
    return [jsonValue description];
}

- (NSString *)stringFromEncodedComponents
{
    NSMutableArray *arguments = [NSMutableArray arrayWithCapacity:self.count];
    
    for (NSString *key in self.allKeys) {
        [arguments addObject:[NSString stringWithFormat:@"%@=%@", [key URLEncodedString], [[self valueForKey:key] URLEncodedString]]];
    }
    
    return [arguments componentsJoinedByString:@"&"];
}

@end
