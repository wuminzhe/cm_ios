//
//  LvCheckBox.h
//  cardMaster
//
//  Created by Zhang Ming on 12-12-1.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LvCheckBox : UIButton

@property (nonatomic, assign) BOOL isChecked;

@end
