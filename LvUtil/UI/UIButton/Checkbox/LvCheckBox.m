//
//  LvCheckBox.m
//  cardMaster
//
//  Created by Zhang Ming on 12-12-1.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "LvCheckBox.h"

@implementation LvCheckBox

-(void)checkBoxClicked{
	if(self.isChecked ==NO){
		self.isChecked =YES;
	}else{
		self.isChecked =NO;
	}
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.isChecked = NO;
        
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        [self addTarget:self
                 action:@selector(checkBoxClicked)
       forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)setIsChecked:(BOOL)checked
{
    _isChecked = checked;
    
    if (checked == NO) {
        [self setImage:[UIImage imageNamed:@"feedback_btn.png"]
              forState:UIControlStateNormal];
    } else {
        [self setImage:[UIImage imageNamed:@"feedback_btn_click.png"]
              forState:UIControlStateNormal];
    }
}

@end
