//
//  MZBubbleCell.m
//  CardMaster
//
//  Created by Zhang Ming on 12-12-27.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MZBubbleCell.h"

@interface MZBubbleCell ()

- (void)setupInternalData;

@end

@implementation MZBubbleCell

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
	[self setupInternalData];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 320, 20)];
        headerLabel.font = [UIFont boldSystemFontOfSize:12.0];
        headerLabel.textColor = [UIColor darkGrayColor];
        headerLabel.textAlignment = UITextAlignmentCenter;
        headerLabel.backgroundColor = [UIColor clearColor];
        
        contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        contentLabel.numberOfLines = 0;
        contentLabel.lineBreakMode = UILineBreakModeWordWrap;
        contentLabel.backgroundColor = [UIColor clearColor];
        
        bubbleImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        
        [self addSubview:bubbleImage];
        [self addSubview:headerLabel];
        [self addSubview:contentLabel];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Properties methods
- (void)setDataInternal:(MZBubbleDataInternal *)dataInternal
{
    _dataInternal = dataInternal;
    
    [self setupInternalData];
}

#pragma mark - Private methods
- (void)setupInternalData
{
    if (self.dataInternal.header) {
        headerLabel.hidden = NO;
        headerLabel.text = self.dataInternal.header;
    } else {
        headerLabel.hidden = YES;
    }
    
    BubbleType type = _dataInternal.data.type;
    float x = (type == BubbleTypeSomeoneElse) ? 20 : self.frame.size.width - 20 - self.dataInternal.labelSize.width;
    float y = 5 + (self.dataInternal.header ? 30 : 0);
    
    contentLabel.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
    contentLabel.frame = CGRectMake(x, y, self.dataInternal.labelSize.width, self.dataInternal.labelSize.height);
    contentLabel.text = self.dataInternal.data.text;
    
    if (type == BubbleTypeSomeoneElse) {
        bubbleImage.image = [[UIImage imageNamed:@"bubbleSomeone.png"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
        bubbleImage.frame = CGRectMake(x - 18, y - 4, self.dataInternal.labelSize.width + 30, self.dataInternal.labelSize.height + 15);
    } else {
        bubbleImage.image = [[UIImage imageNamed:@"bubbleMine.png"] stretchableImageWithLeftCapWidth:15 topCapHeight:14];
        bubbleImage.frame = CGRectMake(x - 9, y - 4, self.dataInternal.labelSize.width + 26, self.dataInternal.labelSize.height + 15);
    }
}

@end
