//
//  MZBubbleTableView.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-26.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MZBubbleTableView;
@class MZBubbleData;

@protocol MZBubbleTableViewDataSource <NSObject>

@required
- (NSInteger)rowsForBubbleTable:(MZBubbleTableView *)tableView;
- (MZBubbleData *)bubbleTableView:(MZBubbleTableView *)tableView dataForRow:(NSInteger)row;

@end

@interface MZBubbleTableView : UITableView<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) id <MZBubbleTableViewDataSource> bubbleDataSource;
@property (nonatomic, assign) NSTimeInterval snapInterval;

- (void)scrollToEnd;

@end
