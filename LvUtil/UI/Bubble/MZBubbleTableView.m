//
//  MZBubbleTableView.m
//  CardMaster
//
//  Created by Zhang Ming on 12-12-26.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MZBubbleTableView.h"
#import "MZBubbleDataInternal.h"
#import "MZBubbleCell.h"

@interface MZBubbleTableView ()

@property (nonatomic, strong) NSMutableDictionary *bubbleDictionary;

@end

@implementation MZBubbleTableView

#pragma mark - MZBubbleTableView instance methods
- (void)initialize
{
    self.backgroundColor = [UIColor clearColor];
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
    assert(self.style == UITableViewStylePlain);
    
    self.delegate = self;
    self.dataSource = self;
    
    self.snapInterval = 120;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:UITableViewStylePlain];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - Override
- (void)reloadData
{
    self.bubbleDictionary = nil;
    
    int count = 0;
    if (self.bubbleDataSource && (count = [self.bubbleDataSource rowsForBubbleTable:self]) > 0) {
        self.bubbleDictionary = [[NSMutableDictionary alloc] init];
        NSMutableArray *bubbleData = [[NSMutableArray alloc] initWithCapacity:count];
        
        for (int i = 0; i < count; i++) {
            NSObject *object = [self.bubbleDataSource bubbleTableView:self dataForRow:i];
            assert([object isKindOfClass:[MZBubbleData class]]);
            [bubbleData addObject:object];
        }
        
        [bubbleData sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
             MZBubbleData *bubbleData1 = (MZBubbleData *)obj1;
             MZBubbleData *bubbleData2 = (MZBubbleData *)obj2;
             
             return [bubbleData1.date compare:bubbleData2.date];
         }];
        
        NSDate *last = [NSDate dateWithTimeIntervalSince1970:0];
        NSMutableArray *currentSection = nil;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        
        for (int i = 0; i < count; i++) {
            MZBubbleDataInternal *dataInternal = [[MZBubbleDataInternal alloc] init];
            dataInternal.data = (MZBubbleData *)[bubbleData objectAtIndex:i];
            
            dataInternal.labelSize = [(dataInternal.data.text ? dataInternal.data.text : @"") sizeWithFont:[UIFont systemFontOfSize:[UIFont systemFontSize]] constrainedToSize:CGSizeMake(220, 9999) lineBreakMode:UILineBreakModeWordWrap];
            
            dataInternal.height = dataInternal.labelSize.height + 5 + 11;
            
            dataInternal.header = nil;
            
            if ([dataInternal.data.date timeIntervalSinceDate:last] > self.snapInterval) {
                currentSection = [[NSMutableArray alloc] init];
                [self.bubbleDictionary setObject:currentSection forKey:[NSString stringWithFormat:@"%d",i]];
                dataInternal.header = [dateFormatter stringFromDate:dataInternal.data.date];
                
                dataInternal.height += 30;
            }
            
            [currentSection addObject:dataInternal];
            last = dataInternal.data.date;
        }
    }
    
    [super reloadData];
}

#pragma mark - Custom methods
- (void)scrollToEnd
{
    if(!self.bubbleDictionary)  return;
    
    int section = self.bubbleDictionary.allKeys.count - 1;
    
    NSArray *keys = self.bubbleDictionary.allKeys;
    
    NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [((NSString *)obj1) compare:((NSString *)obj2) options:NSNumericSearch];
    }];

    NSString *key = sortedArray[section];
    
    int row = [self.bubbleDictionary[key] count] - 1;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    
    [self scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (!self.bubbleDictionary)
        return 0;
    
    return self.bubbleDictionary.allKeys.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *keys = self.bubbleDictionary.allKeys;
    
    NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [((NSString *)obj1) compare:((NSString *)obj2) options:NSNumericSearch];
    }];
    
    NSString *key = sortedArray[section];
    
    return [self.bubbleDictionary[key] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%s", __FUNCTION__);
    
    static NSString *identifier = @"BubbleCell";
    
    NSArray *keys = self.bubbleDictionary.allKeys;
    NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [((NSString *)obj1) compare:((NSString *)obj2) options:NSNumericSearch];
    }];
    
    NSString *key = [sortedArray objectAtIndex:indexPath.section];
    MZBubbleDataInternal *bubbleDataInternal = (MZBubbleDataInternal *)[[self.bubbleDictionary objectForKey:key] objectAtIndex:indexPath.row];
    
    MZBubbleCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[MZBubbleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.dataInternal = bubbleDataInternal;
    return cell;
}

#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%s", __FUNCTION__);
    NSArray *keys = self.bubbleDictionary.allKeys;
    
    NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [((NSString *)obj1) compare:((NSString *)obj2) options:NSNumericSearch];
    }];
    
    NSString *key = sortedArray[indexPath.section];
    
    MZBubbleDataInternal *bubbleDataInternal = (MZBubbleDataInternal *)[[self.bubbleDictionary objectForKey:key] objectAtIndex:indexPath.row];
    
    return bubbleDataInternal.height;
}

@end
