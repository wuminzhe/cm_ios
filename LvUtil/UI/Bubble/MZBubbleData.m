//
//  MZBubbleData.m
//  CardMaster
//
//  Created by Zhang Ming on 12-12-27.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MZBubbleData.h"

@implementation MZBubbleData
- (id)initWithText:(NSString *)text andDate:(NSDate *)date andType:(BubbleType)type
{
    if (self = [super init]) {
        self.text = text;
        
        if (!_text || _text.length == 0)
            _text = @"";
        
        _date = date;
        
        _type = type;
    }

    return self;
}

+ (id)dataWithText:(NSString *)text andDate:(NSDate *)date andType:(BubbleType)type
{
    return [[MZBubbleData alloc] initWithText:text andDate:date andType:type];
}

@end
