//
//  MZBubbleData.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-27.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    BubbleTypeMine = 1,
    BubbleTypeSomeoneElse
} BubbleType;

@interface MZBubbleData : NSObject

@property (nonatomic, strong, readonly) NSDate *date;
@property (nonatomic, assign) BubbleType type;
@property (nonatomic, strong) NSString *text;

- (id)initWithText:(NSString *)text andDate:(NSDate *)date andType:(BubbleType)type;
+ (id)dataWithText:(NSString *)text andDate:(NSDate *)date andType:(BubbleType)type;

@end
