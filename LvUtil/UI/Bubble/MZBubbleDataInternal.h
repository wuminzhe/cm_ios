//
//  MZBubbleDataInternal.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-27.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MZBubbleData;

@interface MZBubbleDataInternal : NSObject

@property (nonatomic, strong) MZBubbleData *data;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGSize labelSize;
@property (nonatomic, strong) NSString *header;

@end
