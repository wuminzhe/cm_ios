//
//  MZBubbleCell.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-27.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MZBubbleData.h"
#import "MZBubbleDataInternal.h"

@interface MZBubbleCell : UITableViewCell {
    UILabel *headerLabel;
    UILabel *contentLabel;
    UIImageView *bubbleImage;
}

@property (nonatomic, strong) MZBubbleDataInternal *dataInternal;

@end
