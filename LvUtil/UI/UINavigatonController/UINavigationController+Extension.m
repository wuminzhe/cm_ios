//
//  UINavigationController+Extension.m
//  cardMaster
//
//  Created by Zhang Ming on 12-10-16.
//  Copyright (c) 2012年 Lver. All rights reserved.
//

#import "UINavigationController+Extension.h"
#import <QuartzCore/QuartzCore.h>

@implementation UINavigationController (Extension)

- (void)setNavigationBarBackgroundImage:(UIImage *)image
{
    if (!image)
        return;
    
    if ([self.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
        [self.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    } else {
        self.navigationBar.layer.contents = (id)image.CGImage;
    }
}

@end
