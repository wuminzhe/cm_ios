//
//  UINavigationBar+Extension.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-1.
//  Copyright (c) 2012年 Lver. All rights reserved.
//

#import "UINavigationBar+Extension.h"

@implementation UINavigationBar (Extension)

- (void)setBackgroundImage:(UIImage *)backgroundImage
{
    if ([self respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
        [self setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    } else {
        self.layer.contents = (id)backgroundImage.CGImage;
    }
}

@end
