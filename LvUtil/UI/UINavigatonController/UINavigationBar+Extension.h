//
//  UINavigationBar+Extension.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-1.
//  Copyright (c) 2012年 Lver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface UINavigationBar (Extension)

- (void)setBackgroundImage:(UIImage *)backgroundImage;

@end
