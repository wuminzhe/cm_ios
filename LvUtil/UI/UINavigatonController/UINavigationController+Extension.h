//
//  UINavigationController+Extension.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-16.
//  Copyright (c) 2012年 Lver. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Extension)

- (void)setNavigationBarBackgroundImage:(UIImage *)image;

@end
