//
//  DiscountFeedbackViewController.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-29.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "DiscountFeedbackViewController.h"
#import "GlobalHeader.h"
#import "Discount.h"
#import "CMConstants.h"
#import "CMUtil.h"

@interface DiscountFeedbackViewController ()

@end

@implementation DiscountFeedbackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"反馈";
    
    nameLabel.text = _discount.shopName;
    
    discountCheckBox = [[LvCheckBox alloc] initWithFrame:CGRectMake(273, 82, 27, 23)];
    
    [theScrollView addSubview:discountCheckBox];
    
    addressCheckBox = [[LvCheckBox alloc] initWithFrame:CGRectMake(273, 134, 27, 23)];
    
    [theScrollView addSubview:addressCheckBox];
    
    phoneCheckBox = [[LvCheckBox alloc] initWithFrame:CGRectMake(273, 186, 27, 23)];
    
    [theScrollView addSubview:phoneCheckBox];
    
    engine = nil;
}
- (void)viewDidUnload
{
    feedTextView = nil;
    theScrollView = nil;
    nameLabel = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backgroundTap:(id)sender
{
    [feedTextView resignFirstResponder];
    
    [theScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (IBAction)send:(id)sender
{
    [self backgroundTap:nil];
    
    if (!engine) {
        engine = [[DiscountEngine alloc] initWithHostName:kServerAddr];
    }
    
    NSMutableString *content = [[NSMutableString alloc] init];
    
    if (discountCheckBox.isChecked) {
        [content appendString:@"折扣不符,"];
    }
    if (addressCheckBox.isChecked) {
        [content appendString:@"地址有误,"];
    }
    if (phoneCheckBox.isChecked) {
        [content appendString:@"电话不通,"];
    }
    
    if (feedTextView.text.length > 140) {
        [self quickAlertView:@"输入的内容超过140个字符!"];
        return;
    }
    
    [content appendString:feedTextView.text];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:3];
    params[@"code"] = [CMUtil getCode];
    params[@"discount_id"] = _discount.discountID;
    params[@"content"] = feedTextView.text;
    
    [engine feedback:params onCompletion:^(BOOL status) {
        if (status) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"信息"
                                                                message:@"提交成功！"
                                                               delegate:self
                                                      cancelButtonTitle:@"确定"
                                                      otherButtonTitles:nil];
            [alertView show];
        } else {
            [self quickAlertView:@"提交失败！"];
        }
    } onError:^(NSError *error) {
        [self quickAlertView:@"提交失败！"];
    }];
}

#pragma mark - UITextViewDelegate methods
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [theScrollView setContentOffset:CGPointMake(0, 224) animated:YES];
}

#pragma mark - UIAlertViewDelegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
