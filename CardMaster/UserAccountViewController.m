//
//  UserAccountViewController.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-26.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "UserAccountViewController.h"
#import "PasswordChangeViewController.h"
#import "LoginViewController.h"
#import "CMConstants.h"
#import "SinaWeibo.h"
#import "TencentOAuth.h"
#import "CreditCard.h"
#import "SQLGlobal.h"
#import "CMUtil.h"
#import "GlobalHeader.h"
#import "UserEngine.h"
#import "Reminder.h"

@interface UserAccountViewController () {
    NSString *loginWay;
    
    BOOL loadCardDone;
    BOOL loadReminderDone;
    BOOL loadFavoriteDiscounts;
    
    int needDownloadImages;
    int finishDownloadImages;
}

@end

@implementation UserAccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        needDownloadImages = 0;
        finishDownloadImages = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"个人账户";
    
    indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicatorView setCenter:CGPointMake(160, 240)];
    [self.view addSubview:indicatorView];
    
    _sinaOAuth = [[SinaWeibo alloc] initWithAppKey:kSinaAppKey
                                         appSecret:kSinaAppSecret
                                    appRedirectURI:kSinaAppRedirect
                                       andDelegate:self];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *sinaweiboInfo = [defaults objectForKey:@"SinaWeiboAuthData"];
    if ([sinaweiboInfo objectForKey:@"AccessTokenKey"] && [sinaweiboInfo objectForKey:@"ExpirationDateKey"] && [sinaweiboInfo objectForKey:@"UserIDKey"])
    {
        _sinaOAuth.accessToken = [sinaweiboInfo objectForKey:@"AccessTokenKey"];
        _sinaOAuth.expirationDate = [sinaweiboInfo objectForKey:@"ExpirationDateKey"];
        _sinaOAuth.userID = [sinaweiboInfo objectForKey:@"UserIDKey"];
    }
    
    weiboEngine = [[TCWBEngine alloc] initWithAppKey:kTencentAppKey andSecret:kTencentAppSecret andRedirectUrl:kTencentAppRedirect];
    [weiboEngine setRootViewController:self];
    
    loginWay = [[NSUserDefaults standardUserDefaults] valueForKey:@"LoginWay"];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    [[UIApplication sharedApplication].keyWindow addSubview:hud];
    hud.delegate = self;
    
    engine = nil;
    loadFavoriteDiscounts = NO;
    
    NSLog(@"sina login = %d", _sinaOAuth.isLoggedIn);
    NSLog(@"tencent login = %d", weiboEngine.isLoggedIn);
}

- (void)viewDidUnload
{
    mTableView = nil;
    
    [super viewDidUnload];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.code && (self.code.length != 0)) {
        if (![self.code isEqualToString:[CMUtil getCode]]) {
            [self recoverUserInfo:self.code];
        }
    }
    
    // 如果是新注册的用户
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"NewUser"]) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"NewUser"];
        [self recoverUserInfo:[CMUtil getCode]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions methods
- (void)backAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)recoverUserInfo:(NSString *)code
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"NeedReload"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [hud show:YES];
    hud.labelText = @"正在恢复数据";
    // 取消所有还款提醒
    NSArray *notifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    
    NSMutableArray *cards = [[SQLGlobal sharedInstance] getLocalCards:code];
    
    for (int i = 0; i < cards.count; i++) {
        CreditCard *card = cards[i];
        
        for (int i = notifications.count - 1; i >= 0; i--) {
            UILocalNotification *notification = notifications[i];
            
            if ([notification.userInfo[@"cardID"] isEqualToString:card.cardNumber]) {
                [[UIApplication sharedApplication] cancelLocalNotification:notification];
            }
        }
    }
    // 下载用户卡片信息
    if (!engine) {
        engine = [[UserEngine alloc] initWithHostName:kServerAddr];
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"code"] = [CMUtil getCode];
    
    [engine getAllCards:params onCompletion:^(NSMutableArray *results) {
        if (results) {
            for (CreditCard *card in results) {
                [[SQLGlobal sharedInstance] updateCard:card];
                
//                // 卡片图片
//                NSString *downloadURLString = [NSString stringWithFormat:@"http://%@/assets/upload/cards/%@", kServerAddr, card.cardPic];
//                NSString *imagePath = [MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:card.cardPic];
//                // download image
//                [engine downloadFatAssFileFrom:downloadURLString
//                                        toFile:imagePath];
                
                if (card.cardPic) {
                    needDownloadImages++;
                    NSLog(@"needDownloadImages = %d", needDownloadImages);
                    // 卡片图片
                    NSString *downloadURLString = [NSString stringWithFormat:@"assets/upload/cards/%@", card.cardPic];
                    NSString *imagePath = [MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:card.cardPic];
                    // download image
                    [engine downloadImage:downloadURLString destPath:imagePath onCompletion:^(BOOL status) {
                        finishDownloadImages++;
                        [self hideHud];
                        NSLog(@"finishDownloadImages = %d", finishDownloadImages);
                    } onError:^(NSError *error) {
                        finishDownloadImages++;
                        [self hideHud];
                        NSLog(@"finishDownloadImages = %d", finishDownloadImages);
                    }];
                }
                
                if (card.bankIcon) {
                    needDownloadImages++;
                    NSLog(@"needDownloadImages = %d", needDownloadImages);
                    
                    // 卡片Icon
                    NSString *downloadIconURLString = [NSString stringWithFormat:@"assets/upload/%@", card.bankIcon];
                    
                    NSString *iconPath = [MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:[card.bankIcon lastPathComponent]];
                    [engine downloadImage:downloadIconURLString destPath:iconPath onCompletion:^(BOOL status) {
                        finishDownloadImages++;
                        [self hideHud];
                        NSLog(@"finishDownloadImages = %d", finishDownloadImages);
                    } onError:^(NSError *error) {
                        finishDownloadImages++;
                        [self hideHud];
                        NSLog(@"finishDownloadImages = %d", finishDownloadImages);
                    }];
                }
                
//                // 卡片Icon
//                NSString *downloadIconURLString = [NSString stringWithFormat:@"http://%@/assets/upload/%@", kServerAddr, card.bankIcon];
//                
//                NSString *iconPath = [MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:[card.bankIcon lastPathComponent]];
//                
//                [engine downloadFatAssFileFrom:downloadIconURLString toFile:iconPath];
            }
        }
        
        loadCardDone = YES;
     [self hideHud];
    } onError:^(NSError *error) {
        loadCardDone = YES;
        [self hideHud];
    }];
    
    // 下载用户还款提醒信息
    [engine getAllReminders:params onCompletion:^(NSMutableArray *results) {
        if (results) {
            for (Reminder *reminder in results) {
                [[SQLGlobal sharedInstance] updateReminder:reminder];
            }
        }
        
        loadReminderDone = YES;
        
        [self hideHud];
    } onError:^(NSError *error) {
        loadReminderDone = YES;
        [self hideHud];
    }];
    
    
    // 下载用户优惠收藏
    [engine getFavoriteDiscounts:params onCompletion:^(NSMutableArray *results) {
        if (results) {
            for (Discount *discount in results)
                [[SQLGlobal sharedInstance] updateDiscount:discount];
        }
        
        loadFavoriteDiscounts = YES;
        [self hideHud];
    } onError:^(NSError *error) {
        loadFavoriteDiscounts = YES;
        [self hideHud];
    }];
}

- (void)hideHud
{
    NSLog(@"hideHud:needDownloadImages = %d", needDownloadImages);
    NSLog(@"hideHud:finishDownloadImages = %d", finishDownloadImages);
    if (loadReminderDone && loadCardDone && loadFavoriteDiscounts && (needDownloadImages == finishDownloadImages)) {
        [hud hide:YES];
    }
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return 1;
    else
        return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"UserAccountCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    if (indexPath.section == 0) {
        cell.textLabel.text = @"账户";
        cell.detailTextLabel.text = loginWay;
    } else {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"新浪微博";
            cell.detailTextLabel.text = _sinaOAuth.isLoggedIn ? @"已绑定" : @"未绑定";
        } else if (indexPath.row == 1) {
            cell.textLabel.text = @"腾讯微博";
            cell.detailTextLabel.text = [weiboEngine isLoggedIn] ? @"已绑定" : @"未绑定";
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        
        if ([loginWay isEqualToString:@"QQ账号登录"] || [loginWay isEqualToString:@"新浪微博登录"]) {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"切换用户", nil];
            [actionSheet showInView:self.view];
            return;
        }
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"切换用户", @"修改密码", nil];
        [actionSheet showInView:self.view];
        
    } else {
        NSString *message = nil;
        
        if (indexPath.row == 0) {
            if (_sinaOAuth.isLoggedIn)
                message = @"是否取消绑定？";
            else
                message = @"是否绑定？";
        } else {
            if (weiboEngine.isLoggedIn)
                message = @"是否取消绑定？";
            else
                message = @"是否绑定？";
        }
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"信息"
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:@"确定"
                                                  otherButtonTitles:@"取消", nil];
        alertView.tag = indexPath.row;
        
        [alertView show];
        return;
    }
}

#pragma mark - UIActionSheetDelegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"buttonIndex = %d", buttonIndex);
    
    if (buttonIndex == 0) {
        //            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"LoginSuccess"];
        
        LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        
        [self.navigationController pushViewController:loginViewController animated:YES];
        
        //            [self.navigationController popViewControllerAnimated:YES];
    } else if (buttonIndex == 1) {
        
        if ([loginWay isEqualToString:@"QQ账号登录"] || [loginWay isEqualToString:@"新浪微博登录"]) {
            return;
        }
        
        PasswordChangeViewController *passwordChangeViewController = [[PasswordChangeViewController alloc] initWithNibName:@"PasswordChangeViewController" bundle:nil];
        
        [self.navigationController pushViewController:passwordChangeViewController animated:YES];
    }
}

#pragma mark - UIAlertViewDelegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    int tag = alertView.tag;
    
    if (buttonIndex == 0) {
        // 点击新浪微博
        if (tag == 0) {
            // 如果已经绑定了，则进行解绑
            if (_sinaOAuth.isLoggedIn) {
                // 登出
                [_sinaOAuth logOut];
                // 移除保存的token
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SinaWeiboAuthData"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            // 绑定
            } else {
                [_sinaOAuth logIn];
            }
        } else if (tag == 1) {
            // 如果已经绑定了，则进行解绑
            if ([weiboEngine isLoggedIn]) {
                [weiboEngine logOut];
            } else {
                [weiboEngine logInWithDelegate:self
                                     onSuccess:@selector(onSuccessLogin)
                                     onFailure:@selector(onFailureLogin:)];
            }
        }
    }
    
    [mTableView reloadData];
}

#pragma mark - Tencent login callback

//登录成功回调
- (void)onSuccessLogin
{
    [indicatorView stopAnimating];
    [mTableView reloadData];
}

//登录失败回调
- (void)onFailureLogin:(NSError *)error
{
    [indicatorView stopAnimating];
    NSString *message = [[NSString alloc] initWithFormat:@"%@",[NSNumber numberWithInteger:[error code]]];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[error domain]
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
    [alertView show];
}

//授权成功回调
- (void)onSuccessAuthorizeLogin
{
    [indicatorView stopAnimating];
    [mTableView reloadData];
}

#pragma mark - SinaWeibo Delegate
- (void)sinaweiboDidLogIn:(SinaWeibo *)sinaweibo
{
    NSLog(@"sinaweiboDidLogIn userID = %@ accesstoken = %@ expirationDate = %@ refresh_token = %@", sinaweibo.userID, sinaweibo.accessToken, sinaweibo.expirationDate,sinaweibo.refreshToken);
    
    NSDictionary *authData = [NSDictionary dictionaryWithObjectsAndKeys:
                              sinaweibo.accessToken, @"AccessTokenKey",
                              sinaweibo.expirationDate, @"ExpirationDateKey",
                              sinaweibo.userID, @"UserIDKey",
                              sinaweibo.refreshToken, @"refresh_token", nil];
    
    [[NSUserDefaults standardUserDefaults] setObject:authData forKey:@"SinaWeiboAuthData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    _sinaOAuth = sinaweibo;
    [mTableView reloadData];
}

- (void)sinaweiboDidLogOut:(SinaWeibo *)sinaweibo
{
    NSLog(@"sinaweiboDidLogOut");
}

- (void)sinaweiboLogInDidCancel:(SinaWeibo *)sinaweibo
{
    NSLog(@"sinaweiboLogInDidCancel");
}

- (void)sinaweibo:(SinaWeibo *)sinaweibo logInDidFailWithError:(NSError *)error
{
    NSLog(@"sinaweibo logInDidFailWithError %@", error);
}

- (void)sinaweibo:(SinaWeibo *)sinaweibo accessTokenInvalidOrExpired:(NSError *)error
{
    NSLog(@"sinaweiboAccessTokenInvalidOrExpired %@", error);
}


@end
