//
//  WalletCell.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-16.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "WalletCell.h"
#import "CreditCard.h"
#import "SwitchCard.h"

@interface WalletCell()

@property (strong,nonatomic) SwitchCard * switchCard;
@property (strong,nonatomic) UIImageView * frontImgView;

@end

@implementation WalletCell
@synthesize cardButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Properties
- (void)setFreeDays:(NSString *)freeDays
{
    _freeDays = freeDays;
}

#pragma mark - WalletCell instance methods
- (void)registerClickEventWithTarget:(id)target selector:(SEL)selector
{
    if (self.switchCard)
    {
        [self.switchCard.frontBtn addTarget:target
                                     action:selector
                           forControlEvents:UIControlEventTouchUpInside];
        [self.switchCard.backBtn addTarget:target
                                     action:selector
                           forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [cardButton addTarget:target
                       action:selector
             forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)registerBackClickEventWithTarget:(id)target selector:(SEL)selector
{
    [self.switchCard.backBtn addTarget:target
                                 action:selector
                       forControlEvents:UIControlEventTouchUpInside];
}

- (void)loadCard:(CreditCard *)card index:(NSInteger)index
{
    if (!card)
    {
        cardButton.hidden = YES;
        _bankLogoView.hidden = YES;
        _cardInfoLabel.hidden = YES;
        freeLabel.hidden = YES;
        activity.hidden = YES;
        waitLabel.hidden = YES;
        [cardButton setImage:nil forState:UIControlStateNormal];
        [cardButton setImage:nil forState:UIControlStateHighlighted];
    }
    else
    {
        cardButton.hidden = NO;
        _bankLogoView.hidden = card.addingCard;
        _cardInfoLabel.hidden = card.addingCard;
        freeLabel.hidden = card.addingCard;
        activity.hidden = !card.addingCard;
        waitLabel.hidden = !card.addingCard;
    
        if (card.addingCard) {
            [activity startAnimating];
        }
        
        if (card.productName.length > 0)
            _cardInfoLabel.text = card.productName;
        else
            _cardInfoLabel.text = [NSString stringWithFormat:@"%@    %@", card.bankName, [card.cardNumber substringFromIndex:card.cardNumber.length - 4]];
        
        if ([card.bankID isEqualToString:@"cmbchina"]) {
            [cardButton setImage:[UIImage imageNamed:@"cmbchina.png"]
                        forState:UIControlStateNormal];
        } else if ([card.bankID isEqualToString:@"ccb"]) {
            [cardButton setImage:[UIImage imageNamed:@"ccb.png"]
                        forState:UIControlStateNormal];
        } else if ([card.bankID isEqualToString:@"abchina"]) {
            [cardButton setImage:[UIImage imageNamed:@"abchina.png"]
                        forState:UIControlStateNormal];
        } else if ([card.bankID isEqualToString:@"bankcomm"]) {
            [cardButton setImage:[UIImage imageNamed:@"bankcomm.png"]
                        forState:UIControlStateNormal];
        } else {
            [cardButton setImage:[UIImage imageNamed:@"bank_default.png"]
                        forState:UIControlStateNormal];
        }

        [cardButton setImage:nil forState:UIControlStateHighlighted];
    }
    
    cardButton.tag = index;
    
    if (!card || card.addingCard)
    {
        self.switchCard.hidden = YES;
    }
    else
    {
        [self.switchCard loadCard:card];
        self.switchCard.frontBtn.tag = index;
        self.switchCard.hidden = NO;
    }
}

- (void)addSwitchCard:(BOOL)normal
{
    if (normal)
    {
        self.switchCard = [SwitchCard getNormalInstance];
    }
    else
    {
        self.switchCard = [SwitchCard getTopInstance];
    }
    self.switchCard.frame = self.cardButton.frame;
    self.switchCard.autoresizingMask = self.cardButton.autoresizingMask;
    [self.contentView addSubview:self.switchCard];
    self.switchCard.hidden = YES;
}

#pragma mark - WalletCell class methods
+ (WalletCell *)getNomalInstance
{
    WalletCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"WalletCell" owner:nil options:nil] lastObject];
    [cell addSwitchCard:YES];
    return cell;
}

+ (WalletCell *)getTopInstance
{
    WalletCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"TopWalletCell" owner:nil options:nil] lastObject];
    [cell addSwitchCard:NO];
    return cell;
}

+ (WalletCell *)getBottomInstance
{
    WalletCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"BottomWalletCell" owner:nil options:nil] lastObject];
    return cell;
}

+ (WalletCell *)getHeaderInstance
{
    WalletCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"HeaderWalletCell" owner:nil options:nil] lastObject];
    return cell;
}

@end
