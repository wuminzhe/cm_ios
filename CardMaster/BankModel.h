//
//  BankModel.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-28.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BankModel : NSObject

@property (nonatomic, strong) NSString *bankID;                 // 银行ID
@property (nonatomic, strong) NSString *bankName;               // 银行名称
@property (nonatomic, strong) NSMutableArray *telephones;       // 银行电话服务
@property (nonatomic, strong) NSMutableArray *messages;         // 银行短信服务

@property (nonatomic, strong) NSString *mobileNo;               // 移动
@property (nonatomic, strong) NSString *unicomNo;               // 联通
@property (nonatomic, strong) NSString *telecomNo;              // 电信

@end
