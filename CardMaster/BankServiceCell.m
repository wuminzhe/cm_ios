//
//  BankServiceCell.m
//  CardMaster
//
//  Created by Zhang Ming on 12-12-29.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BankServiceCell.h"
#import "BankServiceDataInternal.h"

@implementation BankServiceCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 6, 281, 21)];
        infoLabel.backgroundColor = [UIColor clearColor];
        infoLabel.numberOfLines = 0;
        infoLabel.lineBreakMode = UILineBreakModeWordWrap;
        infoLabel.textColor = [UIColor whiteColor];
        infoLabel.font = [UIFont systemFontOfSize:15.0];
        [self addSubview:infoLabel];
        
        sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        sendButton.frame = CGRectMake(19, 0, 281, 38);
        
        [sendButton addTarget:self
                       action:@selector(handleButtonClick:)
             forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:sendButton];
        
        self.backgroundColor = [UIColor darkGrayColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Properties
- (void)setDataInternal:(BankServiceDataInternal *)dataInternal
{
    _dataInternal = dataInternal;
    
    [self setupDataInternal];
}

#pragma mark - Private methods
- (void)setupDataInternal
{
    if (_dataInternal.type == 0) {
        [sendButton setImage:[UIImage imageNamed:@"call.png"]
                    forState:UIControlStateNormal];
        
        [sendButton setImage:[UIImage imageNamed:@"call_click.png"]
                    forState:UIControlStateHighlighted];
    } else {
        [sendButton setImage:[UIImage imageNamed:@"send_message_btn.png"]
                    forState:UIControlStateNormal];
        
        [sendButton setImage:[UIImage imageNamed:@"send_message_btn_click.png"]
                    forState:UIControlStateHighlighted];
    }
    
    infoLabel.text = _dataInternal.tip;
    
    CGSize tipSize = [_dataInternal.tip sizeWithFont:[UIFont systemFontOfSize:15.0] constrainedToSize:CGSizeMake(280, 9999) lineBreakMode:UILineBreakModeWordWrap];
    
    CGRect viewRect = infoLabel.frame;
    viewRect.size = tipSize;
    
    infoLabel.frame = viewRect;
    
    // 设置textfield
    for (int i = 0; i < _dataInternal.list.count; i++) {
        viewRect.origin.y = viewRect.origin.y + viewRect.size.height + 10;
        viewRect.size.height = 40;
        viewRect.size.width = 281;
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"service_textfield_bg.png"]];
        imageView.frame = viewRect;
        [self addSubview:imageView];
        
        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(viewRect.origin.x + 6, viewRect.origin.y, viewRect.size.width - 6 * 2, 40)];
        textField.tag = i + 1000;
        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        textField.borderStyle = UITextBorderStyleNone;
        textField.delegate = self;
        if ([_dataInternal.list[i] isEqualToString:@"[PASSWORD]"]) {
            textField.placeholder = @"请输入查询密码（不保存本地）";
            textField.keyboardType = UIKeyboardTypeNumberPad;
        } else {
            textField.placeholder = [NSString stringWithFormat:@"请输入%@（保存本地）", _dataInternal.values[i]];
        }
        
        NSString *key = _dataInternal.list[i];
        textField.text = _dataInternal.codeList[key];
        
        [self addSubview:textField];
    }
    viewRect.origin.y = viewRect.origin.y + viewRect.size.height + 10;
    viewRect.size = sendButton.frame.size;
    
    sendButton.frame = viewRect;
}

- (void)handleButtonClick:(id)sender
{
    for (int i = 0; i < _dataInternal.list.count; i++) {
        UITextField *textField = (UITextField *)[self viewWithTag:1000 + i];
        NSString *key = _dataInternal.list[i];
        NSString *value = textField.text;
        
        if (value.length > 0) {
            [_dataInternal.codeList setObject:value forKey:key];
        }
    }
    
    if (_delegate && [_delegate respondsToSelector:@selector(handleButtonClick:)])
        [_delegate handleButtonClick:_dataInternal];
}

#pragma mark - UITextFieldDelegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"frame = %@", NSStringFromCGRect(textField.frame));
    
    if (_delegate && [_delegate respondsToSelector:@selector(handleFirstResponder:)])
        [_delegate handleFirstResponder:textField];
}

@end
