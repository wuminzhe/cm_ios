//
//  CMConstants.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-12.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#define CARD_IMAGE_FILE                 @"card"
#define DISCOUNT_IMAGE_FILE             @"discount"
#define kNumberOfDays                   31
#define kNumberOfPreDays                7
#define kNumberOfTimeSegment            24

#define kFilterAnimtaionDuration        0.25

#define kChangeUser                     @"ChangeUser"

//#define kDefaultLvTokenSavePath [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]
//#define kSinaLvTokenSavePath    [kDefaultLvTokenSavePath stringByAppendingPathComponent:@"SinaLvToken.cache"]
//#define kTecentLvTokenSavePath  [kDefaultLvTokenSavePath stringByAppendingPathComponent:@"TecentLvToken.cache"]