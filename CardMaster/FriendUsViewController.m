//
//  FriendUsViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 13-1-15.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "FriendUsViewController.h"

@interface FriendUsViewController ()

@property (nonatomic, strong) SinaWBEngine *sinaEngine;
@property (nonatomic, strong) TencentWBEngine *tencentEngine;

@end

@implementation FriendUsViewController
{
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"关注我们";
    
//    _sinaOAuth = [[SinaWeibo alloc] initWithAppKey:@"3315880788"
//                                         appSecret:@"c4a65b008b2556e9d43c9cac9f01fe6e"
//                                    appRedirectURI:@"http://"
//                                       andDelegate:self];
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSDictionary *sinaweiboInfo = [defaults objectForKey:@"SinaWeiboAuthData"];
//    if ([sinaweiboInfo objectForKey:@"AccessTokenKey"] && [sinaweiboInfo objectForKey:@"ExpirationDateKey"] && [sinaweiboInfo objectForKey:@"UserIDKey"])
//    {
//        _sinaOAuth.accessToken = [sinaweiboInfo objectForKey:@"AccessTokenKey"];
//        _sinaOAuth.expirationDate = [sinaweiboInfo objectForKey:@"ExpirationDateKey"];
//        _sinaOAuth.userID = [sinaweiboInfo objectForKey:@"UserIDKey"];
//    }
//    
//    weiboEngine = [[TCWBEngine alloc] initWithAppKey:WiressSDKDemoAppKey andSecret:WiressSDKDemoAppSecret andRedirectUrl:REDIRECTURI];
//    [weiboEngine setRootViewController:self];
    
    self.sinaEngine = [[SinaWBEngine alloc] init];
    self.sinaEngine.rootViewController = self;
    self.sinaEngine.delegate = self;
    
    self.tencentEngine = [[TencentWBEngine alloc] init];
    self.tencentEngine.rootViewController = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)friendWithSina:(id)sender
{
//    if (_sinaOAuth.isLoggedIn) {
//        if (!_sinaOAuth.isAuthValid) {
//            [_sinaOAuth logIn];
//        } else {
//            [self handleFriendWithSina];
//        }
//    } else {
//        [_sinaOAuth logIn];
//    }
    
    if ([self.sinaEngine isAuthValid]) {
        [self handleFriendWithSina];
    } else {
        [self.sinaEngine logIn];
    }
}

- (void)handleFriendWithSina
{
//    [_sinaOAuth requestWithURL:@"friendships/create.json"
//                        params:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"2301392452", @"uid", nil]
//                    httpMethod:@"POST"
//                      delegate:self];
    
    [self.sinaEngine requestWithPath:@"friendships/create.json" params:@{@"screen_name" : @"信用卡管家"} httpMethod:@"POST"];
}

- (IBAction)friendWithTencent:(id)sender
{
//    if (weiboEngine.isLoggedIn && !weiboEngine.isAuthorizeExpired) {
//        [self handleFriendWithTencent];
//    } else {
//        [weiboEngine logInWithDelegate:self
//                             onSuccess:@selector(onSuccessLogin)
//                             onFailure:@selector(onFailureLogin:)];
//    }
    
    if ([self.tencentEngine isAuthValid]) {
        [self handleFriendWithTencent];
    } else {
        [self.tencentEngine logIn];
    }
}

- (void)handleFriendWithTencent
{
//    [weiboEngine addFriendsWithFormat:@"json"
//                                names:@"api_weibo"
//                           andOpenIDs:nil
//                          parReserved:nil
//                             delegate:self
//                            onSuccess:@selector(successCallBack:)
//                            onFailure:@selector(failureCallBack:)];
    
//    [self.sinaEngine requestWithPath:@"friendships/create.json" params:@{@"screen_name" : @"wakgj"} httpMethod:@"POST"];
    
    [self.tencentEngine requestWithPath:@"friends/add" params:@{@"name" : @"chcc_51ika"} httpMethod:@"POST"];
}

- (IBAction)friendWithWeChat:(id)sender
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    [pasteboard setString:@"woaikaguanjia"];
    
    [WXApi openWXApp];
}

//#pragma mark - Private methods
//- (void)removeAuthData
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SinaWeiboAuthData"];
//}
//
//- (void)storeAuthData
//{
//    NSDictionary *authData = [NSDictionary dictionaryWithObjectsAndKeys:
//                              _sinaOAuth.accessToken, @"AccessTokenKey",
//                              _sinaOAuth.expirationDate, @"ExpirationDateKey",
//                              _sinaOAuth.userID, @"UserIDKey",
//                              _sinaOAuth.refreshToken, @"refresh_token", nil];
//    [[NSUserDefaults standardUserDefaults] setObject:authData forKey:@"SinaWeiboAuthData"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//#pragma mark - Tencent login callback
//
////登录成功回调
//- (void)onSuccessLogin
//{
//    //    [indicatorView stopAnimating];
//    [self handleFriendWithTencent];
//}
//
////登录失败回调
//- (void)onFailureLogin:(NSError *)error
//{
//    //    [indicatorView stopAnimating];
//    NSString *message = [[NSString alloc] initWithFormat:@"%@",[NSNumber numberWithInteger:[error code]]];
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[error domain]
//                                                        message:message
//                                                       delegate:self
//                                              cancelButtonTitle:@"确定"
//                                              otherButtonTitles:nil];
//    [alertView show];
//}
//
////授权成功回调
//- (void)onSuccessAuthorizeLogin
//{
//    //    [indicatorView stopAnimating];
//}
//
//#pragma mark - Tencent callback
//- (void)successCallBack:(id)result{
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
//                                                        message:[NSString stringWithFormat:@"Post status \"%@\" Success!", @"测试我爱卡"]
//                                                       delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//    [alertView show];
//}
//
//- (void)failureCallBack:(NSError *)error{
//    NSLog(@"error: %@", error);
//}
//
//#pragma mark - SinaWeibo Delegate
//- (void)sinaweiboDidLogIn:(SinaWeibo *)sinaweibo
//{
//    NSLog(@"sinaweiboDidLogIn userID = %@ accesstoken = %@ expirationDate = %@ refresh_token = %@", sinaweibo.userID, sinaweibo.accessToken, sinaweibo.expirationDate,sinaweibo.refreshToken);
//    _sinaOAuth = sinaweibo;
//    [self storeAuthData];
//    
//    [self handleFriendWithSina];
//}
//
//- (void)sinaweiboDidLogOut:(SinaWeibo *)sinaweibo
//{
//    NSLog(@"sinaweiboDidLogOut");
//    [self removeAuthData];
//}
//
//- (void)sinaweiboLogInDidCancel:(SinaWeibo *)sinaweibo
//{
//    NSLog(@"sinaweiboLogInDidCancel");
//}
//
//- (void)sinaweibo:(SinaWeibo *)sinaweibo logInDidFailWithError:(NSError *)error
//{
//    NSLog(@"sinaweibo logInDidFailWithError %@", error);
//}
//
//- (void)sinaweibo:(SinaWeibo *)sinaweibo accessTokenInvalidOrExpired:(NSError *)error
//{
//    NSLog(@"sinaweiboAccessTokenInvalidOrExpired %@", error);
//    [self removeAuthData];
//}
//
//#pragma mark - SinaWeiboRequest Delegate
//
//- (void)request:(SinaWeiboRequest *)request didFailWithError:(NSError *)error
//{
//    NSLog(@"error = %@", error);
//    if ([request.url hasSuffix:@"friendships/create.json"])
//    {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
//                                                            message:[NSString stringWithFormat:@"你已关注此用户！"]
//                                                           delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//        [alertView show];
//        
//        NSLog(@"Post status failed with error : %@", error);
//    }
//}
//
//- (void)request:(SinaWeiboRequest *)request didFinishLoadingWithResult:(id)result
//{
//    if ([request.url hasSuffix:@"friendships/create.json"])
//    {
////        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
////                                                            message:[NSString stringWithFormat:@"Post status \"%@\" succeed!", [result objectForKey:@"text"]]
////                                                           delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//        
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
//                                                            message:[NSString stringWithFormat:@"关注成功！"]
//                                                           delegate:nil
//                                                  cancelButtonTitle:nil
//                                                  otherButtonTitles:@"OK", nil];
//        [alertView show];
//    }
//}

@end
