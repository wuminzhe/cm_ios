//
//  AboutUsViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-27.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"
#import <MessageUI/MessageUI.h>

@interface AboutUsViewController : BaseNavigationController<MFMailComposeViewControllerDelegate, UIAlertViewDelegate>

- (IBAction)sendEmail:(id)sender;
- (IBAction)makeCall:(id)sender;

@end
