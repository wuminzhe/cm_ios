//
//  AppDelegate.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-24.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "JBTabBarController.h"
#import "SplashViewController.h"
#import "GuideViewController.h"
#import "GEOInfo.h"
#import "WXApi.h"

#define MAX_SPLASH_TIMEOUT 1.0f

#define USER_CITY_GET_NEW  @"USER_CITY_GET_NEW"

@class CMEngine;

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate, WXApiDelegate> {
    CMEngine *engine;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *code;
@property (strong, nonatomic) JBTabBarController *tabBarController;
@property (nonatomic, strong) SplashViewController *splashViewController;
@property (nonatomic, strong) GuideViewController *guideViewController;
@property (nonatomic, strong) CLLocation *userLocation;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) GEOInfo *geoInfo;


@end
