//
//  BankService.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-28.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BankService : NSObject

@property (nonatomic, strong) NSMutableArray *bankList;

@end
