//
//  BillViewController.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-1.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"
#import <MessageUI/MessageUI.h>

@class BillEngine;
@class CreditCard;
@class SMS;

@interface BillViewController : BaseNavigationController<MFMessageComposeViewControllerDelegate, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
    
    __unsafe_unretained IBOutlet UIButton *billQueryButton;
    __unsafe_unretained IBOutlet UIButton *moneyQueryButton;
    __unsafe_unretained IBOutlet UILabel *advLabel;
    __unsafe_unretained IBOutlet UITextField *passwordTextField;
    __unsafe_unretained IBOutlet UILabel *currencyLabel;
    __unsafe_unretained IBOutlet UILabel *smsInfoLabel;
    __unsafe_unretained IBOutlet UILabel *smsContentLabel;
    __unsafe_unretained IBOutlet UIButton *currencyButton;
    __unsafe_unretained IBOutlet UIButton *sendButton;
    
    __unsafe_unretained IBOutlet UIView *contentView;
    __unsafe_unretained IBOutlet UIScrollView *theScrollView;
    
    __unsafe_unretained IBOutlet UIPickerView *mPickerView;
    __unsafe_unretained IBOutlet UIView *chooseContentView;
    
    NSString *bankID;
    BillEngine *engine;
    SMS *sms;
}

@property (nonatomic, strong) NSString *cardNo;
@property (nonatomic, strong) CreditCard *card;

- (IBAction)backgroundTap:(id)sender;

- (IBAction)changeTab:(id)sender;

- (IBAction)sendSMS:(id)sender;

- (IBAction)showPicker:(id)sender;
- (IBAction)hidePicker:(id)sender;
- (IBAction)ok:(id)sender;

@end
