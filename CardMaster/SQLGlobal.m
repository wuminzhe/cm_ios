//
//  SQLGlobal.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-17.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "SQLGlobal.h"
#import "GlobalHeader.h"
#import "CreditCard.h"
#import "Reminder.h"
#import "Rate.h"
#import "Bank.h"
#import "SMS.h"
#import "CMUtil.h"
#import "Fee.h"
#import "Discount.h"
#import "MZBubbleData.h"

static SQLGlobal *sharedInstance = nil;

@implementation SQLGlobal

#pragma mark - SQLGlobal instance methods
- (id)init
{
    if (self = [super init]) {
        db = nil;
    }
    
    return self;
}

/***********************************************
 * 函数名称：attachDatabase
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：创建数据库
 * 输入参数：database：数据库名称
 * 返回值:无
 * 修改备注：无
 ***********************************************/
- (void)attachDatabase:(NSString *)database
{
    // 如果已经创建了数据库,则获取数据库句柄返回
    if ([[NSFileManager defaultManager] fileExistsAtPath:MTLP(database)]) {
        db = [FMDatabase databaseWithPath:MTLP(database)];
        return;
    } else
        db = [FMDatabase databaseWithPath:MTLP(database)];
    
    // 创建数据库表
    [db open];
    
    /* 表名：用户表user
     * 创建人：lver
     * 日期：2012-11-17
     * 字段：code:客户端标识
     * 功能描述：保存客户端标识，最基本的用户概念
     * 修改日期：
     * 修改备注：
     */
    [db executeUpdate:@"create table user ('code' VARCHAR)"];
    
    /* 表名：信用卡表cards
     * 创建人：lver
     * 日期：2012-11-17
     * 字段：id : 信用卡Id
     *      card_number : 信用卡卡号
     *      reminder_day : 信用卡还款日期
     *      product_id : 信用卡产品ID
     *      product : 信用卡产品名称
     *      bank_id : 信用卡所属银行ID
     *      bank : 信用卡所属银行名称
     *      owner_name : 信用卡持有人姓名
     *      pic : 信用卡图片名称
     *      nick : 用户自己输入的卡产品信息
     *      remotePic: 服务器端图片路径
     *      status : 是否成功同步到服务器端
     *      source : 卡片添加来源(1:非账单，2:账单)
     *      bill_day : 账单日
     *      code : 
     * 功能描述：保存客户端标识，最基本的用户概念
     * 修改日期：
     * 修改备注：
     */
    [db executeUpdate:@"create table cards ('id' VARCHAR,                   \
                                            'card_number' VARCHAR,          \
                                            'reminder_day' VARCHAR,         \
                                            'product_id' VARCHAR,           \
                                            'bankIcon' VARCHAR,             \
                                            'product' VARCHAR,              \
                                            'bank_id' VARCHAR,              \
                                            'bank' VARCHAR,                 \
                                            'owner_name' VARCHAR,           \
                                            'pic' VARCHAR,                  \
                                            'nick' VARCHAR,                 \
                                            'remotePic' VARCHAR,            \
                                            'status' BOOL,                  \
                                            'source' VARCHAR,               \
                                            'bill_day' VARCHAR,             \
                                            'code' VARCHAR)"];
    
    
    /* 表名：还款提醒表reminders
     * 创建人：lver
     * 日期：2012-11-17
     * 字段：card_id : 信用卡id,
     *      reminder_day : 每月的哪一天
     *      reminder_hour : 几点
     *      pre_day : 提前几天提醒
     *      status : 是否提醒
     *      code : 
     * 功能描述：信用卡还款提醒，可以设置提醒时间
     * 修改日期：
     * 修改备注：
     */
    [db executeUpdate:@"create table reminders ('card_id' VARCHAR,          \
                                                'reminder_day' VARCHAR,     \
                                                'reminder_hour' VARCHAR,    \
                                                'pre_day' VARCHAR,          \
                                                'status' VARCHAR,           \
                                                'code' VARCHAR)"];
    
    /* 表名：银行贷款利率表rates
     * 创建人：lver
     * 日期：2012-11-17
     * 字段：id :费率id
     *      bank_id : 银行id,
     *      stage : 分期,
     *      type : 1：一次性，2：每期,
     *      rate : 利率,
     *      min_amount : 最小额度,
     *      max_amount : 最大额度,
     *      create_time : 创建时间,
     *      update_time : 更新时间
     *      code :
     * 功能描述：信用卡还款提醒，可以设置提醒时间
     * 修改日期：
     * 修改备注：
     */
    [db executeUpdate:@"create table rates ('id' VARCHAR,                           \
                                            'bank_id' VARCHAR,                      \
                                            'stage' VARCHAR,                        \
                                            'type' VARCHAR,                         \
                                            'rate' VARCHAR,                         \
                                            'min_amount' VARCHAR,                   \
                                            'max_amount' VARCHAR,                   \
                                            'create_time' VARCHAR,                  \
                                            'update_time' VARCHAR,                  \
                                            'code' VARCHAR)"];
    
    /* 表名：创建银行表banks
     * 创建人：lver
     * 日期：2012-11-17
     * 字段：id : Bank_ID,
     *      name : 银行名称,
     *      icon : 图标,
     *      pic : 图片,
     *      description : 介绍,
     *      create_time : 创建时间,
     *      update_time : 更新时间
     *      fee : 免年费政策
     *      discounts : 免年费活动
     * 功能描述：信用卡还款提醒，可以设置提醒时间
     * 修改日期：
     * 修改备注：
     */
    [db executeUpdate:@"create table banks ('id' VARCHAR,                           \
                                            'name' VARCHAR,                         \
                                            'icon' VARCHAR,                         \
                                            'pic' VARCHAR,                          \
                                            'description' VARCHAR,                  \
                                            'create_time' VARCHAR,                  \
                                            'update_time' VARCHAR,                  \
                                            'fee' VARCHAR,                          \
                                            'discounts' VARCHAR)"];
    
    /* 表名：创建银行表banks
     * 创建人：lver
     * 日期：2012-11-17
     * 字段：id : ID,
     *      bank_id : 银行模版,
     *      bill_template : 账单模版，{CARD}卡号，{CARD4}卡号后4，{PASSWORD}短信密码，{CODE}货币,
     *      rem_template :余额模板，
     *      mobile : 移动,
     *      mobile_unicom : 联通,
     *      mobile_telecom : 电信,
     *      create_time : 创建时间,
     *      update_time : 更新时间
     * 功能描述：信用卡还款提醒，可以设置提醒时间
     * 修改日期：
     * 修改备注：
     */
    [db executeUpdate:@"create table sms (  'id' VARCHAR,                           \
                                            'bank_id' VARCHAR,                      \
                                            'bill_template' VARCHAR,                \
                                            'rem_template' VARCHAR,                 \
                                            'mobile' VARCHAR,                       \
                                            'mobile_unicom' VARCHAR,                \
                                            'mobile_telecom' VARCHAR,               \
                                            'create_time' VARCHAR,                  \
                                            'update_time' VARCHAR)"];
    
    /* 表名：创建免年费表fee
     * 创建人：lver
     * 日期：2012-11-17
     * 字段：card_number : 信用卡卡号,
     *      effective : 信用卡有效期,
     *      count : 已消费次数，
     *      reminder : 提醒时间,
     *      status : 是否打开免年费提醒
     * 功能描述：保存免年费提醒信息
     * 修改日期：
     * 修改备注：
     */
    [db executeUpdate:@"create table fee (  'card_number' VARCHAR,                  \
                                            'effective' VARCHAR,                    \
                                            'count' VARCHAR,                        \
                                            'reminder' VARCHAR,                     \
                                            'status' VARCHAR)"];
    
    /* 表名：创建优惠收藏表discount_favorite
     * 创建人：lver
     * 日期：2012-12-7
     * id : 优惠编号
     * type : 优惠类型
     * bank_id : 银行ID
     * title : 优惠标题、名称
     * description : 优惠详细信息
     * rate : 折扣（0.8/null），null表示这个优惠不是关于折扣的
     * shop_id : 商家标识（shop_id/null），null表示这个优惠没有关联商家 // 不用
     * hot : 热度,                      // 不用
     * actived : "1" 有效活动标识,       // 不用
     * begin_time : 开始时间,
     * end_time : 结束时间,
     * pic : 银行活动海报,               // 不用
     * create_time : 上架时间,          // 不用
     * update_time : 更新时间,          // 不用
     * bank_name : 银行名称,            // 不用
     * bank_icon : 银行图标,            // 不用
     * bank_pic : 银行图片,             // 不用
     * bank_desc : 银行介绍,            // 不用
     * shop_type : 商户类型id,          // 不用
     * shop_type_name : 商户类别中文,    // 不用
     * shop_name : 商户名称,
     * shop_desc : 商户信息、详细描述,
     * shop_pic : 商户图片（相对地址）,
     * shop_address : 商户地址,
     * shop_lat : 纬度,
     * shop_lon : 经度,
     * shop_phone_number : 商户电话,
     * shop_city : 商户所在城市,         // 不用
     * shop_average_consumption : 商户平均消费，
     * shop_bookmark_times : 商家收藏次数
     * code : 客户端标识
     * 功能描述：保存优惠信息，收藏用
     * 修改日期：
     * 修改备注：
     */
    
    [db executeUpdate:@"create table discounts ('id' VARCHAR, 'type' VARCHAR, 'bank_id' VARCHAR, 'title' VARCHAR, 'pic' VARCHAR, 'description' VARCHAR,'bank_name' VARCHAR, 'rate' VARCHAR, 'begin_time' VARCHAR, 'end_time' VARCHAR, 'shop_name' VARCHAR, 'shop_desc' VARCHAR, 'shop_pic' VARCHAR, 'shop_address' VARCHAR, 'shop_lat' VARCHAR, 'shop_lon' VARCHAR, 'shop_phone_number' VARCHAR, 'shop_average_consumption' VARCHAR, 'shop_bookmark_times' VARCHAR, 'code' VARCHAR)"];
    
//    [db executeUpdate:@"create table discounts ('id' VARCHAR, 'type' VARCHAR, 'bank_id' VARCHAR, 'title' VARCHAR, 'description' VARCHAR, 'rate' VARCHAR, 'shop_id' VARCHAR, 'begin_time' VARCHAR, 'end_time' VARCHAR, 'pic' VARCHAR, 'shop_type' VARCHAR, 'shop_type_name' VARCHAR, 'shop_name' VARCHAR, 'shop_desc' VARCHAR, 'shop_pic' VARCHAR, 'shop_address' VARCHAR, 'shop_lat' VARCHAR, 'shop_lon' VARCHAR, 'shop_phone_number' VARCHAR, 'shop_city' VARCHAR, 'shop_average_consumption' VARCHAR, 'shop_bookmark_times' VARCHAR )"];
    
    /* 表名：创建意见建议表suggest
     * 创建人：lver
     * 日期：2012-11-17
     * 字段：content : 建议内容,
     *      type : 消息发送方, 1,自己  2,官方
     *      create_at : 消息发送时间,
     *      code : 客户端标识
     * 功能描述：保存消息
     * 修改日期：
     * 修改备注：
     */
    [db executeUpdate:@"create table suggest (  'content' VARCHAR,                  \
                                                'type' VARCHAR,                     \
                                                'create_at' VARCHAR,                \
                                                'code' VARCHAR)"];
    
    /* 表名：创建意见城市表cities
     * 创建人：lver
     * 日期：2012-01-06
     * 字段：id : 城市编号,
     *      city : 城市名称
     *      group : 分组,
     * 功能描述：保存消息
     * 修改日期：
     * 修改备注：
     */
    [db executeUpdate:@"create table cities (   'id' VARCHAR,                       \
                                                'city' VARCHAR,                     \
                                                'firstLetter' VARCHAR)"];
    
    [db close];
}

- (void)initializeCity
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"cities" ofType:@"sql"];
    
//    NSLog(@"filePath = %@", filePath);
    NSError *error;
    NSArray *cities = [[NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error] componentsSeparatedByString:@";"];
    
    BOOL status = NO;
    
    [db open];
    for (int i = 0; i < cities.count; i++) {
        NSString *sql = cities[i];
        
        if (sql.length == 0)
            continue;
        
        sql = [sql stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        status = [db executeUpdate:sql];
//        NSLog(@"sql = %@ | length = %d | status = %d", sql, sql.length, status);
    }
    
    [db close];
    
//    NSLog(@"status = %d", status);
    
//    return status;
}

- (NSMutableDictionary *)getCities
{
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    
    [db open];
    
    FMResultSet *set = [db executeQuery:@"select city, firstLetter from cities"];
    
    while ([set next]) {
        NSString *city = [set stringForColumn:@"city"];
        NSString *firstLetter = [set stringForColumn:@"firstLetter"];
        
        NSMutableArray *cities = result[firstLetter];
        
        if (!cities) {
            cities = [[NSMutableArray alloc] init];
        }
        
        [cities addObject:city];
        
        result[firstLetter] = cities;
    }
    
    [db close];
    
    return result;
}

- (NSMutableArray *)getCities:(NSString *)text
{
    NSMutableArray *result = nil;
    
    [db open];
    
//    NSLog(@"%d", text.length);
    NSString *sql = [NSString stringWithFormat:@"select city from cities where city like '%@%%'", text];
    FMResultSet *set = [db executeQuery:sql];
    
    while ([set next]) {
        if (!result)
            result = [[NSMutableArray alloc] init];
        
        [result addObject:[set stringForColumn:@"city"]];
    }
    
    [db close];

    return result;
}

/***********************************************
 * 函数名称：updateUser:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：客户端第一次启动，初始化表User，存储客户端标识符code
 * 输入参数：record 客户端标识
 * 返回值： 无
 * 修改备注：无
 ***********************************************/
- (BOOL)updateUser:(NSString *)record
{
    BOOL status = NO;
    
    [db open];
    
    NSString *sql = @"insert into user (code) values (?)";
    
    // 清空user表
    [db executeUpdate:@"delete from user"];
    
    // 插入数据
    status = [db executeUpdate:sql, record];
    
    [db close];
    return status;
}

/***********************************************
 * 函数名称：getClientCode
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：判断客户端是否第一次启动
 * 输入参数：
 * 返回值：如果是第一次启动，返回为nil，不然返回客户端标识
 * 修改备注：无
 ***********************************************/
- (NSString *)getClientCode
{
    NSString *result = nil;
    
    [db open];
    
    FMResultSet *set = [db executeQuery:@"select code from user"];
    
    if ([set next]) {
        result = [set stringForColumn:@"code"];
    }
    
    [db close];
    
    return result;
}

/***********************************************
 * 函数名称：getNumberOfCard
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：获取一共多少张信用卡
 * 输入参数：
 * 返回值：返回信用卡总数
 * 修改备注：无
 ***********************************************/
- (NSInteger)getNumberOfCard
{
    NSInteger count = 0;
    
    [db open];
    
    FMResultSet *set = [db executeQuery:@"select count(product_id) from cards"];
    
    if ([set next])
        count = [[set stringForColumnIndex:0] integerValue];
    
    [db close];
    
    return count;
}

///***********************************************
// * 函数名称：updateCard:
// * 作者：lver
// * 日期：2012-10-22
// * 功能描述：更新一张卡片信息，插入or更新
// * 输入参数：card:卡片信息
// * 返回值：返回更新是否成功
// * 修改备注：无
// ***********************************************/
//- (BOOL)updateCard:(CreditCard *)card
//{
//    BOOL status = NO;
//    
//    [db open];
//    
//    // 卡片检索由card_number变为id
//    
//    NSString *querySQL = @"select card_number, reminder_day, product_id, product, pic, remotePic, owner_name, bank_id, bank, status from cards where card_number = ? and code = ?";
//    FMResultSet *set = [db executeQuery:querySQL, card.cardNumber, [CMUtil getCode]];
//    
//    if ([set next]) {
//        // 如果有数据，则更新
//        NSString *updateSQL = @"update cards set reminder_day = ?, product_id = ?, product = ?, bank_id = ?, bank = ?, pic = ?, remotePic, owner_name = ?, status = ? where card_number = ? and code = ?";
//        
//        status = [db executeUpdate:updateSQL,card.reminderDay, card.productID, card.productName, card.bankID, card.bankName, card.cardPic, card.remoteCardPic, card.ownerName, card.status ? @"YES" : @"NO", card.cardNumber, [CMUtil getCode]];
//    } else {
//        NSString *insertSQL = @"insert into cards (card_number, reminder_day, product_id, product, pic, remotePic, owner_name, bank_id, bank, status, code) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
//        
//        status = [db executeUpdate:insertSQL, card.cardNumber, card.reminderDay, card.productID, card.productName, card.cardPic, card.remoteCardPic, card.ownerName, card.bankID, card.bankName, card.status ? @"YES" : @"NO", [CMUtil getCode]];
//    }
//
//    [db close];
//    
//    return status;
//}


/***********************************************
 * 函数名称：updateCard:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：更新一张卡片信息，插入or更新
 * 输入参数：card:卡片信息
 * 返回值：返回更新是否成功
 * 修改备注：卡片检索由card_number变为id 2013-1-10
 ***********************************************/
- (BOOL)updateCard:(CreditCard *)card
{
    BOOL status = NO;
    
    [db open];
    
    NSString *querySQL = @"select card_number, reminder_day, product_id, product, pic, remotePic, owner_name, bank_id, bank, status from cards where id = ? and code = ?";
    FMResultSet *set = [db executeQuery:querySQL, card.cardID, [CMUtil getCode]];
    
    if ([set next]) {
        // 如果有数据，则更新
        NSString *updateSQL = @"update cards set card_number = ?, reminder_day = ?, product_id = ?, bankIcon = ?, product = ?, bank_id = ?, bank = ?, pic = ?, remotePic = ?, owner_name = ?, status = ?, nick = ?, source = ?, bill_day = ? where id = ? and code = ?";
        
        status = [db executeUpdate:updateSQL, card.cardNumber, card.reminderDay, card.productID, card.bankIcon, card.productName, card.bankID, card.bankName, card.cardPic, card.remoteCardPic, card.ownerName, card.status ? @"YES" : @"NO", card.nick, card.source, card.billDay, card.cardID, [CMUtil getCode]];
    } else {
        NSString *insertSQL = @"insert into cards (id, card_number, reminder_day, product_id, bankIcon, product, pic, nick, remotePic, owner_name, bank_id, bank, status, source, bill_day, code) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        status = [db executeUpdate:insertSQL, card.cardID, card.cardNumber, card.reminderDay, card.productID, card.bankIcon, card.productName, card.cardPic, card.nick, card.remoteCardPic, card.ownerName, card.bankID, card.bankName, card.status ? @"YES" : @"NO", card.source, card.billDay, [CMUtil getCode]];
    }
    
    [db close];
    
    return status;
}

///***********************************************
// * 函数名称：deleteCard:
// * 作者：lver
// * 日期：2012-10-22
// * 功能描述：根据卡号删除卡片
// * 输入参数：cardNumber:信用卡卡号
// * 返回值：返回更新是否成功
// * 修改备注：无
// ***********************************************/
//- (BOOL)deleteCard:(NSString *)cardNumber
//{
//    BOOL status = NO;
//    
//    [db open];
//    
//    status = [db executeUpdate:@"delete from cards where card_number = ? and code = ?", cardNumber, [CMUtil getCode]];
//    
//    [db close];
//    
//    return status;
//}

/***********************************************
 * 函数名称：deleteCard:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：根据卡号删除卡片
 * 输入参数：cardNumber:信用卡卡号
 * 返回值：返回更新是否成功
 * 修改备注：根据卡id删除卡片
 ***********************************************/
- (BOOL)deleteCard:(NSString *)cardNumber
{
    BOOL status = NO;
    
    [db open];
    
    status = [db executeUpdate:@"delete from cards where id = ? and code = ?", cardNumber, [CMUtil getCode]];
    
    [db close];
    
    return status;
}

- (NSMutableArray *)getLocalCards
{
    return [self getLocalCards:nil];
}

/***********************************************
 * 函数名称：getLocalCards:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：返回所有信用卡卡片
 * 输入参数：无
 * 返回值：返回所有信用卡卡片数组
 * 修改备注：无
 ***********************************************/
- (NSMutableArray *)getLocalCards:(NSString *)code
{
    NSMutableArray *result = nil;
    
    [db open];
    
    NSString *sql = @"select id, card_number, reminder_day, product_id, bankIcon, product, pic, nick, remotePic, owner_name, bank_id, bank, status, source, bill_day from cards where code = ?";
    
    FMResultSet *set = [db executeQuery:sql, code == nil ? [CMUtil getCode] : code];
    
    while ([set next]) {
        if (!result)
            result = [NSMutableArray array];
        
        CreditCard *card = [[CreditCard alloc] init];
        card.cardID = [set stringForColumn:@"id"];
        card.productID = [set stringForColumn:@"product_id"];
        card.bankIcon = [set stringForColumn:@"bankIcon"];
        card.reminderDay = [NSNumber numberWithInteger:[[set stringForColumn:@"reminder_day"] integerValue]];
        card.productName = [set stringForColumn:@"product"];
        card.cardNumber = [set stringForColumn:@"card_number"];
        card.cardPic = [set stringForColumn:@"pic"];
        card.nick = [set stringForColumn:@"nick"];
        card.remoteCardPic = [set stringForColumn:@"remotePic"];
        card.ownerName = [set stringForColumn:@"owner_name"];
        card.bankID = [set stringForColumn:@"bank_id"];
        card.bankName = [set stringForColumn:@"bank"];
        card.status = [[set stringForColumn:@"status"] boolValue];
        card.source = [set stringForColumn:@"source"];
        card.billDay = [set stringForColumn:@"bill_day"];
        
        [result addObject:card];
        card = nil;
    }
    
    [db close];
    
    return result;
}

///***********************************************
// * 函数名称：getCreditCard:
// * 作者：lver
// * 日期：2012-10-22
// * 功能描述：通过卡号获取信用卡信息
// * 输入参数：cardID : 卡号
// * 返回值：返回信用卡提醒信息
// * 修改备注：无
// ***********************************************/
//- (CreditCard *)getCreditCard:(NSString *)cardNumber
//{
//    CreditCard *card = nil;
//    
//    [db open];
//    
//    FMResultSet *set = [db executeQuery:@"select card_number, reminder_day, product_id, product, pic, remotePic, owner_name, bank_id, bank, status from cards where card_number = ? and code = ?", cardNumber, [CMUtil getCode]];
//    
//    if ([set next]) {
//        card = [[CreditCard alloc] init];
//        
//        card.productID = [set stringForColumn:@"product_id"];
//        card.reminderDay = [NSNumber numberWithInteger:[[set stringForColumn:@"reminder_day"] integerValue]];
//        card.productName = [set stringForColumn:@"product"];
//        card.cardNumber = [set stringForColumn:@"card_number"];
//        card.cardPic = [set stringForColumn:@"pic"];
//        card.remoteCardPic = [set stringForColumn:@"remotePic"];
//        card.ownerName = [set stringForColumn:@"owner_name"];
//        card.bankID = [set stringForColumn:@"bank_id"];
//        card.bankName = [set stringForColumn:@"bank"];
//        card.status = [[set stringForColumn:@"status"] boolValue];
//    }
//    
//    [db close];
//    
//    return card;
//}

/***********************************************
 * 函数名称：getCreditCard:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：通过卡号获取信用卡信息
 * 输入参数：cardID : 卡号
 * 返回值：返回信用卡提醒信息
 * 修改备注：根据卡id获取卡信息
 ***********************************************/
- (CreditCard *)getCreditCard:(NSString *)cardNumber
{
    CreditCard *card = nil;
    
    [db open];
    
    FMResultSet *set = [db executeQuery:@"select id, card_number, reminder_day, product_id, product, pic, nick, remotePic, owner_name, bank_id, bank, status, source, bill_day from cards where id = ? and code = ?", cardNumber, [CMUtil getCode]];
    
    if ([set next]) {
        card = [[CreditCard alloc] init];
        card.cardID = [set stringForColumn:@"id"];
        card.productID = [set stringForColumn:@"product_id"];
        card.reminderDay = [NSNumber numberWithInteger:[[set stringForColumn:@"reminder_day"] integerValue]];
        card.productName = [set stringForColumn:@"product"];
        card.cardNumber = [set stringForColumn:@"card_number"];
        card.cardPic = [set stringForColumn:@"pic"];
        card.nick = [set stringForColumn:@"nick"];
        card.remoteCardPic = [set stringForColumn:@"remotePic"];
        card.ownerName = [set stringForColumn:@"owner_name"];
        card.bankID = [set stringForColumn:@"bank_id"];
        card.bankName = [set stringForColumn:@"bank"];
        card.status = [[set stringForColumn:@"status"] boolValue];
        card.source = [set stringForColumn:@"source"];
        card.billDay = [set stringForColumn:@"bill_day"];
    }
    
    [db close];
    
    return card;
}

/***********************************************
 * 函数名称：getReminder:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：通过卡号获取信用卡的提醒信息
 * 输入参数：cardID : 卡号
 * 返回值：返回信用卡提醒信息
 * 修改备注：无
 ***********************************************/
- (Reminder *)getReminder:(NSString *)cardID
{
    Reminder *reminer = nil;
    
    [db open];
    
    NSString *sql = @"select * from reminders where card_id = ? and code = ?";
    
    FMResultSet *set = [db executeQuery:sql, cardID, [CMUtil getCode]];
    
    if ([set next]) {
        reminer = [[Reminder alloc] init];
        
        reminer.cardID = [set stringForColumn:@"card_id"];
        reminer.reminderDay = [set stringForColumn:@"reminder_day"];
        reminer.reminderHour = [set stringForColumn:@"reminder_hour"];
        reminer.preDay = [set stringForColumn:@"pre_day"];
        reminer.on = [set stringForColumn:@"status"];
    }
    
    [db close];
    
    return reminer;
}

- (BOOL)deleteReminder:(NSString *)cardID
{
    BOOL status = NO;
    
    [db open];
    
    status = [db executeUpdate:@"delete from reminders where card_id = ? and code = ?", cardID, [CMUtil getCode]];
    [db close];
    
    return status;
}


/***********************************************
 * 函数名称：updateReminder:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：更新or插入新的信用卡还款提醒
 * 输入参数：reminder : 还款提醒model
 * 返回值：更新or插入是否成功
 * 修改备注：无
 ***********************************************/
- (BOOL)updateReminder:(Reminder *)reminder
{
    BOOL status = NO;
    
    [db open];
    
    // 查询是否已有此卡的提醒信息
    NSString *sql = @"select * from reminders where card_id = ? and code = ?";
    
    FMResultSet *set = [db executeQuery:sql, reminder.cardID, [CMUtil getCode]];
    
    if ([set next]) {
        NSLog(@"有此卡提醒信息！");
        NSString *updateSQL = @"update reminders set    reminder_day = ?,                   \
                                                        reminder_hour = ?,                  \
                                                        pre_day = ?,                        \
                                                        status = ?                          \
                                                        where card_id = ? and code = ?";
        
        status = [db executeUpdate:updateSQL, reminder.reminderDay, reminder.reminderHour, reminder.preDay, reminder.on, reminder.cardID, [CMUtil getCode]];
    } else {
        NSLog(@"没有此卡提醒信息！");
        NSString *insertSQL = @"insert into reminders ( card_id,                        \
                                                        reminder_day,                   \
                                                        reminder_hour,                  \
                                                        pre_day,                        \
                                                        status,                         \
                                                        code)                           \
                                                        values (?, ?, ?, ?, ?, ?)";
        status = [db executeUpdate:insertSQL, reminder.cardID, reminder.reminderDay, reminder.reminderHour, reminder.preDay, reminder.on, [CMUtil getCode]];
    }
    
    [db close];
    
    return status;
}

/***********************************************************
 * 函数名称：getBankLastUpdateTime
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：获取银行表的最后更新时间
 * 输入参数：无
 * 返回值：如果为nil则还没获取银行表，否则返回银行表最后一次更新时间
 * 修改备注：无
 ***********************************************************/
- (NSString *)getBankLastUpdateTime
{
    NSString *result = nil;
    
    [db open];
    
    FMResultSet *set = [db executeQuery:@"select max(update_time) as update_time from banks"];
    
    if ([set next]) {
        result = [set stringForColumn:@"update_time"];
    }
    
    [db close];
    
    return result;
}

/***********************************************************
 * 函数名称：getRateLastUpdateTime
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：获取利率表的最后更新时间
 * 输入参数：无
 * 返回值：如果为nil则还没获取利率表，否则返回利率表最后一次更新时间
 * 修改备注：无
 ***********************************************************/
- (NSString *)getRateLastUpdateTime
{
    NSString *result = nil;
    
    [db open];
    
    FMResultSet *set = [db executeQuery:@"select max(update_time) as update_time from rates"];
    
    if ([set next]) {
        result = [set stringForColumn:@"update_time"];
    }
    
    [db close];
    
    return result;
}


- (BOOL)updateBanks:(NSMutableArray *)records
{
    BOOL status = NO;
    [db open];
    [db beginTransaction];
    
    NSString *queryBankString = @"select * from banks where id = ?";
    
    NSString *updateBankString = @"update banks set name = ?,                       \
                                                    icon = ?,                       \
                                                    pic = ?,                        \
                                                    description = ?,                \
                                                    create_time = ?,                \
                                                    update_time = ?,                \
                                                    fee = ?,                        \
                                                    discounts = ?,                  \
                                                    where id = ?";
    
    NSString *insertBankString = @"insert into banks (  name,                       \
                                                        icon,                       \
                                                        pic,                        \
                                                        description,                \
                                                        create_time,                \
                                                        update_time,                \
                                                        id,                         \
                                                        fee,                        \
                                                        discounts)                  \
                                                        values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    
    for (Bank *bank in records) {
        FMResultSet *set = [db executeQuery:queryBankString, bank.bankID];
        NSString *sql = nil;
        // 如果有数据，则更新操作
        if ([set next]) {
            sql = updateBankString;
            // 如果没有，则执行插入操作
        } else {
            sql = insertBankString;
        }
        
        status = [db executeUpdate:sql, bank.name, bank.icon, bank.pic, bank.bankDesc, bank.createTime, bank.updateTime, bank.bankID, bank.fee, bank.discounts];
    }
    
    if (![db commit])
        [db rollback];
    
    [db close];
    
    return status;
}

/***********************************************
 * 函数名称：updateRates:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：更新银行利率表
 * 输入参数：records : 利率表数据以及银行表数据
 * 返回值：更新or插入是否成功
 * 修改备注：无
 ***********************************************/
- (BOOL)updateRates:(NSDictionary *)records
{
    BOOL status = NO;
    
    [db open];
    
    [db beginTransaction];
    
    NSString *queryBankString = @"select * from banks where id = ?";
    
    NSString *updateBankString = @"update banks set name = ?,                       \
                                                    icon = ?,                       \
                                                    pic = ?,                        \
                                                    description = ?,                \
                                                    create_time = ?,                \
                                                    update_time = ?                 \
                                                    where id = ?";
    
    NSString *insertBankString = @"insert into banks (  name,                       \
                                                        icon,                       \
                                                        pic,                        \
                                                        description,                \
                                                        create_time,                \
                                                        update_time,                \
                                                        id)                         \
                                                        values (?, ?, ?, ?, ?, ?, ?)";
    
    NSString *queryRateString = @"select * from rates where id = ?";
    
    NSString *updateRateString = @"update rates set stage = ?,                      \
                                                    type = ?,                       \
                                                    rate = ?,                       \
                                                    min_amount = ?,                 \
                                                    max_amount = ?,                 \
                                                    create_time = ?,                \
                                                    update_time = ?                 \
                                                    where id = ?";
    
    NSString *insertRateString = @"insert into rates (  bank_id,                    \
                                                        stage,                      \
                                                        type,                       \
                                                        rate,                       \
                                                        min_amount,                 \
                                                        max_amount,                 \
                                                        create_time,                \
                                                        update_time,                \
                                                        id  )                       \
                                                        values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    
    for (Bank *bank in records[@"bank"]) {
        FMResultSet *set = [db executeQuery:queryBankString, bank.bankID];
        NSString *sql = nil;
        // 如果有数据，则更新操作
        if ([set next]) {
            sql = updateBankString;
            // 如果没有，则执行插入操作
        } else {
            sql = insertBankString;
        }
        
        status = [db executeUpdate:sql, bank.name, bank.icon, bank.pic, bank.bankDesc, bank.createTime, bank.updateTime, bank.bankID];
    }
    
    for (Rate *rate in records[@"rate"]) {
        FMResultSet *set = [db executeQuery:queryRateString,rate.rateID];
        NSString *sql = nil;
        // 如果有数据，则更新操作
        if ([set next]) {
            sql = updateRateString;
            // 如果没有，则执行插入操作
        } else {
            sql = insertRateString;
        }
        
        status = [db executeUpdate:sql, rate.bankID, rate.stage, rate.type, rate.rate, rate.minAmount, rate.maxAmount, rate.createTime, rate.updateTime, rate.rateID];
    }
    
    if (![db commit])
        [db rollback];
    
    [db close];
    
    return status;
}

/***********************************************
 * 函数名称：getBank：
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：获取银行信息
 * 输入参数：银行ID
 * 返回值：银行
 * 修改备注：无
 ***********************************************/
- (Bank *)getBank:(NSString *)bankID
{
    Bank *bank = nil;
    [db open];
    
    NSString *sql = @"select id, name, icon, pic, description, create_time, update_time,        \
                             fee, discounts from banks where id = ?";
    
    FMResultSet *set = [db executeQuery:sql, bankID];
    
    if ([set next]) {
        bank = [[Bank alloc] init];
        bank.bankID = [set stringForColumn:@"id"];
        bank.name = [set stringForColumn:@"name"];
        bank.icon = [set stringForColumn:@"icon"];
        bank.pic = [set stringForColumn:@"pic"];
        bank.bankDesc = [set stringForColumn:@"description"];
        bank.createTime = [set stringForColumn:@"create_time"];
        bank.updateTime = [set stringForColumn:@"update_time"];
        bank.fee = [set stringForColumn:@"fee"];
        bank.discounts = [set stringForColumn:@"discounts"];
    }
    
    [db close];
    
    return bank;
}

/***********************************************
 * 函数名称：getBanks
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：获取所有银行列表
 * 输入参数：无
 * 返回值：银行列表
 * 修改备注：无
 ***********************************************/
- (NSMutableArray *)getBanks
{
    NSMutableArray *result = nil;
    
    [db open];
    
    FMResultSet *set = [db executeQuery:@"select id, name, icon, pic, description,      \
                                                 create_time, update_time from banks"];
    
    while ([set next]) {
        if (!result)
            result = [NSMutableArray array];
        
        Bank *bank = [[Bank alloc] init];
        bank.bankID = [set stringForColumn:@"id"];
        bank.name = [set stringForColumn:@"name"];
        bank.icon = [set stringForColumn:@"icon"];
        bank.pic = [set stringForColumn:@"pic"];
        bank.bankDesc = [set stringForColumn:@"description"];
        bank.createTime = [set stringForColumn:@"create_time"];
        bank.updateTime = [set stringForColumn:@"update_time"];
        
        [result addObject:bank];
        
        bank = nil;
    }
    
    [db close];
    
    return result;
}

/***********************************************
 * 函数名称：getRatesWithBankID:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：根据银行ID获取利率表数据
 * 输入参数：bankID : 银行ID
 * 返回值：利率表数据
 * 修改备注：无
 ***********************************************/
- (NSMutableArray *)getRatesWithBankID:(NSString *)bankID
{
    NSMutableArray *result = nil;
    
    [db open];
    
    FMResultSet *set = [db executeQuery:@"select id, bank_id, stage, type,                      \
                        rate, min_amount, max_amount, create_time, update_time  \
                        from rates where bank_id = ? order by bank_id, stage", bankID];
    while ([set next]) {
        if (!result)
            result = [NSMutableArray array];
        
        Rate *rate = [[Rate alloc] init];
        rate.rateID = [set stringForColumn:@"id"];
        rate.bankID = [set stringForColumn:@"bank_id"];
        rate.stage = [set stringForColumn:@"stage"];
        rate.type = [set stringForColumn:@"type"];
        rate.rate = [set stringForColumn:@"rate"];
        rate.minAmount = [set stringForColumn:@"min_amount"];
        rate.maxAmount = [set stringForColumn:@"max_amount"];
        rate.createTime = [set stringForColumn:@"create_time"];
        rate.updateTime = [set stringForColumn:@"update_time"];
        
        [result addObject:rate];
        
        rate = nil;
    }
    
    [db close];
    
    return result;
}

/***********************************************
 * 函数名称：updateSMS:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：更新短信模板表
 * 输入参数：result : 短信模板数据
 * 返回值：更新是否成功
 * 修改备注：无
 ***********************************************/
- (BOOL)updateSMS:(NSMutableArray *)result
{
    BOOL status = NO;
    
    [db open];
    
    // 每一次更更新都清空表数据
    [db executeUpdate:@"delete from sms"];
    
    NSString *sql = @"insert into sms ( id,                                     \
                                        bank_id,                                \
                                        bill_template,                          \
                                        rem_template,                           \
                                        mobile,                                 \
                                        mobile_unicom,                          \
                                        mobile_telecom,                         \
                                        create_time,                            \
                                        update_time)                            \
                                        values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    
    [db beginTransaction];
    
    for (SMS *sms in result) {
        [db executeUpdate:sql, sms.smsID, sms.bankID, sms.billTemplate, sms.remTemplate, sms.mobile, sms.mobileUnicom, sms.mobileTelecom, sms.createTime, sms.updateTime];
    }
    
    status = [db commit];
    
    if (!status)
        [db rollback];
    
    [db close];
    
    return status;
}

/***********************************************
 * 函数名称：getSMSLastUpdateTime
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：获取最后一次短信模板更新时间
 * 输入参数：无
 * 返回值：返回最后一次更新时间，如果没有数据，则返回nil
 * 修改备注：无
 ***********************************************/
- (NSString *)getSMSLastUpdateTime
{
    NSString *result = nil;
    
    [db open];
    
    FMResultSet *set = [db executeQuery:@"select max(update_time) as update_time from sms"];
    
    if ([set next]) {
        result = [set stringForColumn:@"update_time"];
    }
    
    [db close];
    
    return result;
}

/***********************************************
 * 函数名称：getSMSWithBankID:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：根据银行ID获取短信模板
 * 输入参数：bankID:银行ID
 * 返回值：短信模板
 * 修改备注：无
 ***********************************************/
- (SMS *)getSMSWithBankID:(NSString *)bankID
{
    SMS *sms = nil;
    
    [db open];
    
    FMResultSet *set = [db executeQuery:@"select id, bank_id, bill_template, rem_template,  \
                        mobile, mobile_unicom, mobile_telecom, create_time, update_time     \
                        from sms where bank_id = ?", bankID];
    
    if ([set next]) {
        sms = [[SMS alloc] init];
        sms.smsID = [set stringForColumn:@"id"];
        sms.bankID = [set stringForColumn:@"bank_id"];
        sms.billTemplate = [set stringForColumn:@"bill_template"];
        sms.remTemplate = [set stringForColumn:@"rem_template"];
        sms.mobile = [set stringForColumn:@"mobile"];
        sms.mobileUnicom = [set stringForColumn:@"mobile_unicom"];
        sms.mobileTelecom = [set stringForColumn:@"mobile_telecom"];
        sms.createTime = [set stringForColumn:@"create_time"];
        sms.updateTime = [set stringForColumn:@"update_time"];
    }
    
    [db close];
    
    return sms;
}

- (BOOL)updateFee:(Fee *)record
{
    BOOL status = NO;
    [db open];
    
    NSString *querySQL = @"select card_number from fee where card_number = ?";
    
    FMResultSet *set = [db executeQuery:querySQL, record.cardNumber];
    
    if ([set next]) {
        status = [db executeUpdate:@"update fee set effective = ?, count = ?, reminder = ?, status = ? where card_number = ?", record.effective, record.count, record.reminder, record.status ? @"1" : @"0", record.cardNumber];
    } else {
        status = [db executeUpdate:@"insert into fee (card_number, effective, count, reminder, status) values (?, ?, ?, ?, ?)", record.cardNumber, record.effective, record.count, record.reminder, record.status ? @"1" : @"0"];
    }
    
    [db close];
    return status;
}

- (Fee *)getFee:(NSString *)cardNumber
{
    Fee *fee = nil;
    [db open];
    
    FMResultSet *set = [db executeQuery:@"select card_number, effective, count, reminder, status from fee where card_number = ?", cardNumber];
    
    if ([set next]) {
        fee = [[Fee alloc] init];
        
        fee.cardNumber = [set stringForColumn:@"card_number"];
        fee.effective = [set stringForColumn:@"effective"];
        fee.count = [set stringForColumn:@"count"];
        fee.reminder = [set stringForColumn:@"reminder"];
        fee.status = [[set stringForColumn:@"status"] boolValue];
    }
    
    [db close];
    return fee;
}

// 更新or插入优惠数据
- (BOOL)updateDiscount:(Discount *)discount
{    
    BOOL status = NO;
    [db open];
    
    NSString *querySQL = @"select count(id) from discounts where id = ? and code = ?";
    
    FMResultSet *set = [db executeQuery:querySQL, discount.discountID, [CMUtil getCode]];
    
    int count = 0;
    
    if ([set next]) {
        count = [[set stringForColumnIndex:0] integerValue];
    }
    
    [db beginTransaction];
    
    // 先执行删除
    if (count > 0) {
        [db executeUpdate:@"delete from discounts where id = ? and code = ?", discount.discountID, [CMUtil getCode]];
    }
    
    NSString *insertSQL = @"insert into discounts('id', 'type', 'bank_id', 'title', 'pic', 'description', 'bank_name', 'rate', 'begin_time','end_time', 'shop_name', 'shop_desc', 'shop_pic', 'shop_address', 'shop_lat', 'shop_lon', 'shop_phone_number', 'shop_average_consumption', 'shop_bookmark_times', 'code') values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    
    [db executeUpdate:insertSQL, discount.discountID, [NSString stringWithFormat:@"%d", discount.type], discount.bankID, discount.title, discount.pic, discount.discountDescription, discount.bankName, discount.rate, discount.beginTime, discount.endTime, discount.shopName, discount.shopDesc, discount.shopPic, discount.shopAddress, discount.shopLatitude, discount.shopLongitude, discount.shopPhoneNumber, discount.shopAverageConsumption, discount.shopBookmarkTimes, [CMUtil getCode]];
    
    status = [db commit];
    
    [db close];
    return status;
}

//// 根据优惠ID查询本地是否有数据
//- (BOOL)getDiscountStatus:(NSString *)discountID
//{
//    BOOL status = NO;
//    [db open];
//    
//    NSString *querySQL = @"select count(id) from discounts where id = ? and code = ?";
//    
//    FMResultSet *set = [db executeQuery:querySQL, discountID, [CMUtil getCode]];
//    
//    int count = 0;
//    
//    if ([set next]) {
//        count = [[set stringForColumnIndex:0] integerValue];
//    }
//    
//    if (count > 0)
//        status = YES;
//    
//    [db close];
//    return status;
//}

- (BOOL)getDiscountStatus:(Discount *)discount
{
    BOOL status = NO;
    [db open];
    
    NSString *querySQL = @"select count(id) from discounts where id = ? and code = ? and end_time = ?";
    
    FMResultSet *set = [db executeQuery:querySQL, discount.discountID, [CMUtil getCode], discount.endTime];
    
    int count = 0;
    
    if ([set next]) {
        count = [[set stringForColumnIndex:0] integerValue];
    }
    
    if (count > 0)
        status = YES;
    
    [db close];
    return status;
}

// 删除优惠信息
- (BOOL)deleteDiscount:(NSString *)discountID
{
    BOOL status = NO;
    [db open];
    
    status = [db executeUpdate:@"delete from discounts where id = ? and code = ?", discountID, [CMUtil getCode]];
    
    [db close];
    return status;
}

// 获取所有优惠信息
- (NSMutableArray *)getDiscounts
{
    NSMutableArray *result = nil;
    [db open];
    
    NSString *querySQL = @"select id, type, bank_id, title, pic, description, bank_name, rate, begin_time, end_time, shop_name, shop_desc, shop_pic, shop_address, shop_lat, shop_lon, shop_phone_number, shop_average_consumption, shop_bookmark_times from discounts where code = ?";
    
    FMResultSet *set = [db executeQuery:querySQL, [CMUtil getCode]];
    
    while ([set next]) {
        if (!result)
            result = [[NSMutableArray alloc] init];
        
        NSString *lat = [set stringForColumn:@"shop_lat"];
        NSString *lon = [set stringForColumn:@"shop_lon"];
        
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(lat.doubleValue, lon.doubleValue);
        
        Discount *discount = [[Discount alloc] initWithCoordinate:coordinate];
        discount.pic = [set stringForColumn:@"pic"];
        discount.discountID = [set stringForColumn:@"id"];
        discount.type = [set stringForColumn:@"type"].integerValue;
        discount.bankID = [set stringForColumn:@"bank_id"];
        discount.discountTitle = [set stringForColumn:@"title"];
        discount.bankName = [set stringForColumn:@"bank_name"];
        discount.discountDescription = [set stringForColumn:@"description"];
        discount.rate = [set stringForColumn:@"rate"];
        discount.beginTime = [set stringForColumn:@"begin_time"];
        discount.endTime = [set stringForColumn:@"end_time"];
        discount.shopName = [set stringForColumn:@"shop_name"];
        discount.shopDesc = [set stringForColumn:@"shop_desc"];
        discount.shopPic = [set stringForColumn:@"shop_pic"];
        discount.shopAddress = [set stringForColumn:@"shop_address"];
        discount.shopLatitude = [set stringForColumn:@"shop_lat"];
        discount.shopLongitude = [set stringForColumn:@"shop_lon"];
        discount.shopPhoneNumber = [set stringForColumn:@"shop_phone_number"];
        discount.shopAverageConsumption = [set stringForColumn:@"shop_average_consumption"];
        discount.shopBookmarkTimes = [set stringForColumn:@"shop_bookmark_times"];
        
        [result addObject:discount];
        discount = nil;
    }
    
    [db close];
    return result;
}

- (NSMutableArray *)getMessages
{
    NSMutableArray *result = nil;
    [db open];
    
    NSString *sql = @"select content, type, create_at from suggest where code = ?";
    
    FMResultSet *set = [db executeQuery:sql, [CMUtil getCode]];
    
    while ([set next]) {
        if (!result)
            result = [[NSMutableArray alloc] init];
        
        NSString *content = [set stringForColumn:@"content"];
        NSString *dateString = [set stringForColumn:@"create_at"];
        NSString *type = [set stringForColumn:@"type"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [dateFormatter dateFromString:dateString];
        
        MZBubbleData *bubbleData = [[MZBubbleData alloc] initWithText:content andDate:date andType:type.integerValue];
        
        [result addObject:bubbleData];
        
        bubbleData = nil;
    }
    
    [db close];
    return result;
}

- (BOOL)updateMessage:(MZBubbleData *)data
{
    BOOL status = NO;
    [db open];
    
    NSString *insertSQL = @"insert into suggest(content, type, create_at, code) values (?, ?, ?, ?)";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:data.date];
    
    status = [db executeUpdate:insertSQL, data.text, [NSString stringWithFormat:@"%d", data.type], dateString, [CMUtil getCode]];
    
    [db close];
    
    return status;
}

- (BOOL)updateMessages:(NSMutableArray *)result
{
    BOOL status = NO;
    [db open];
    
    [db executeUpdate:@"delete from suggest where code = ?", [CMUtil getCode]];
    
    if (result.count > 0) {
        NSString *insertSQL = @"insert into suggest(content, type, create_at, code) values (?, ?, ?, ?)";
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        for (MZBubbleData *data in result) {
            NSString *dateString = [dateFormatter stringFromDate:data.date];
            
            status = [db executeUpdate:insertSQL, data.text, [NSString stringWithFormat:@"%d", data.type], dateString, [CMUtil getCode]];
        }
    }
    
    [db close];
    
    return status;
}

#pragma mark - SQLGlobal class methods
+ (SQLGlobal *)sharedInstance
{
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        sharedInstance = [[super allocWithZone:nil] init];
    });
    
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedInstance];
}

@end
