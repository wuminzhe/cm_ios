//
//  ChoiceCell.m
//  CardMaster
//
//  Created by wenjun on 13-1-10.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "ChoiceCell.h"

@interface ChoiceCell()
{
    IBOutlet UILabel * label;
    IBOutlet UIImageView * selectedBgView;
}

@end

@implementation ChoiceCell

- (void)loadChoice:(NSString *)choice
{
    label.text = choice;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    if (self.selected != selected)
    {
        [super setSelected:selected animated:animated];
        if (selected)
        {
            selectedBgView.alpha = 1;
        }
        else
        {
            selectedBgView.alpha = 0;
        }
    }
}

+ (ChoiceCell *)getInstance
{
    ChoiceCell * cell = [[[NSBundle mainBundle] loadNibNamed:@"ChoiceCell" owner:nil options:nil] lastObject];
    return cell;
}

@end
