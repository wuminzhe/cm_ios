//
//  Email.h
//  CardMaster
//
//  Created by wenjun on 13-1-15.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Email : NSObject

@property (strong,nonatomic) NSString * emailName;
@property (strong,nonatomic) NSString * password;
@property (strong,nonatomic) NSString * pk;

@end
