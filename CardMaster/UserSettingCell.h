//
//  UserSettingCell.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-26.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserSettingCell : UITableViewCell

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *imageIcon;

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *userInfoLabel;

+ (UserSettingCell *)getInstance;

@end
