//
//  BillViewController.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-1.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BillViewController.h"
#import "GlobalHeader.h"
#import "BillEngine.h"
#import "CMUtil.h"
#import "SQLGlobal.h"
#import "SMS.h"
#import "CMConstants.h"
#import "CreditCard.h"
#import "NSString+Extension.h"

@interface BillViewController () {
    UIButton *selectedButton;
    NSArray *currency;
    
    NSInteger selectedRow;
    
    // NO:显示账单查询, YES:显示余额查询
    BOOL isSearchBill;
    
    NSString *message;
    
    NSString *code;
}

@end

@implementation BillViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"短信账单";
    
    isSearchBill = YES;
    code = @"CNY";
    
    // content view
    CGRect rect = contentView.frame;
    
    rect.origin.y = -120;
    
    contentView.frame = rect;
    
    selectedButton = billQueryButton;
    
    currency = @[ @"人民币（CNY）", @"美元（USD）", @"日元（JPY）"];
    
    CGRect viewRect = self.view.frame;
    
    chooseContentView.frame = CGRectMake(0.0, viewRect.size.height, 320, 260);
    
    [self.view addSubview:chooseContentView];
    
    // 根据卡号判断银行id
    bankID = _card.bankID;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:1];
    params[@"code"] = [CMUtil getCode];
    
    engine = [[BillEngine alloc] initWithHostName:kServerAddr];
    
    [self showHudView:@""];
    [engine getSMSUpdateStatus:params onCompletion:^(BOOL status) {
        
        // 需要更新数据s
        if (status) {
            [engine getSMSData:params onCompletion:^(BOOL status) {
                [self loadSMS];
                [self hideHudView];
            } onError:^(NSError *error) {
                [self loadSMS];
                [self hideHudView];
            }];
        } else {
            [self loadSMS];
            [self hideHudView];
        }
        
    } onError:^(NSError *error) {
        [self loadSMS];
        [self hideHudView];
    }];
}

- (void)viewDidUnload {
    billQueryButton = nil;
    moneyQueryButton = nil;
    advLabel = nil;
    passwordTextField = nil;
    currencyLabel = nil;
    smsInfoLabel = nil;
    smsContentLabel = nil;
    contentView = nil;
    theScrollView = nil;
    chooseContentView = nil;
    mPickerView = nil;
    currencyButton = nil;
    sendButton = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions
/***********************************************
 * 函数功能：加载银行查询信息
 ***********************************************/
- (void)loadSMS
{
    // 根据银行id检索
    sms = [[SQLGlobal sharedInstance] getSMSWithBankID:bankID];
//    sms.billTemplate = @"{CODE}货币代码";
    [self showView];
    
    if (!sms) {
        [self quickAlertView:@"没有此银行的短信模板！"];
    }
    
    NSLog(@"sms = %@ || %@", sms.billTemplate, sms.remTemplate);
}

// 根据sms显示信息
- (void)showView
{
    if (isSearchBill) {
        CGFloat offsetY = [self calculateFrame:sms.billTemplate];

        [UIView animateWithDuration:kFilterAnimtaionDuration animations:^{
            CGRect viewRect = contentView.frame;
            viewRect.origin.y = offsetY;
            contentView.frame = viewRect;
        }];
    } else {
        message = [self replaceSMSTemplate:sms.remTemplate];
        
        CGFloat offsetY = [self calculateFrame:sms.remTemplate];
        
        [UIView animateWithDuration:kFilterAnimtaionDuration animations:^{
            CGRect viewRect = contentView.frame;
            viewRect.origin.y = offsetY;
            contentView.frame = viewRect;
        }];
    }
}

- (CGFloat)calculateFrame:(NSString *)template
{
    CGFloat offsetY = -120;
    
    if ([template rangeOfString:@"{PASSWORD}"].location != NSNotFound) {
        offsetY += 120;
    }
    
    if ([template rangeOfString:@"{CODE}"].location == NSNotFound) {
        
        CGRect viewRect = sendButton.frame;
        viewRect.origin.y = currencyButton.frame.origin.y;
        sendButton.frame = viewRect;
        
        currencyButton.hidden = YES;
        currencyLabel.hidden = YES;
        
        CGRect smsInfoFrame = smsInfoLabel.frame;
        smsInfoFrame.origin.y = viewRect.origin.y + viewRect.size.height + 10;
        smsInfoLabel.frame = smsInfoFrame;
        
        CGRect smsContentFrame = smsContentLabel.frame;
        smsContentFrame.origin.y = smsInfoFrame.origin.y + smsInfoFrame.size.height + 10;
        smsContentLabel.frame = smsContentFrame;
    } else {
        // 如果没有password
        if (offsetY < 0) {
            offsetY += 60;
        }
        
        currencyButton.hidden = NO;
        currencyLabel.hidden = NO;
    }

    return offsetY;
}

- (NSString *)replaceSMSTemplate:(NSString *)templateString
{
    if (!templateString)
        return nil;
    
    NSMutableString *temp = [[NSMutableString alloc] initWithString:templateString];
    
    // 替换货币
    NSRange range = [templateString rangeOfString:@"{CODE}"];
    
    if (range.location != NSNotFound) {
        [temp replaceCharactersInRange:range withString:code];
    }
    
    // 替换卡号
    range = [templateString rangeOfString:@"{CARD}"];
    if (range.location != NSNotFound) {
        [temp replaceCharactersInRange:range withString:_card.cardNumber];
    }
    
    // 替换后4位卡号
    range = [templateString rangeOfString:@"{CARD}"];
    if (range.location != NSNotFound) {
        [temp replaceCharactersInRange:range
                            withString:[_card.cardNumber substringFromIndex:_card.cardNumber.length - 5]];
    }
    
    // 替换密码
    range = [templateString rangeOfString:@"{PASSWORD}"];
    
    if (range.location != NSNotFound) {
        [temp replaceCharactersInRange:range withString:passwordTextField.text];
    }
    
    return temp;
}

- (IBAction)backgroundTap:(id)sender
{
    if ([passwordTextField isFirstResponder]) {
        [passwordTextField resignFirstResponder];
    }
}

- (IBAction)changeTab:(id)sender
{
    if (sender == selectedButton)
        return;
    
    selectedButton = sender;
    
    passwordTextField.text = @"";
    
    if (sender == billQueryButton) {
        isSearchBill = YES;
        [moneyQueryButton setImage:[UIImage imageNamed:@"bill_money_search.png"]
                          forState:UIControlStateNormal];
        
        [moneyQueryButton setImage:[UIImage imageNamed:@"bill_money_search_select.png"]
                          forState:UIControlStateHighlighted];
        
        [billQueryButton setImage:[UIImage imageNamed:@"bill_search_select.png"]
                         forState:UIControlStateNormal];
        
        [billQueryButton setImage:[UIImage imageNamed:@"bill_search_select.png"]
                         forState:UIControlStateHighlighted];
        
        [sendButton setImage:[UIImage imageNamed:@"bill_send.png"]
                    forState:UIControlStateNormal];
        
        [sendButton setImage:[UIImage imageNamed:@"bill_send_click.png"]
                    forState:UIControlStateHighlighted];
        
    } else {
        isSearchBill = NO;
        [billQueryButton setImage:[UIImage imageNamed:@"bill_search.png"]
                          forState:UIControlStateNormal];
        
        [billQueryButton setImage:[UIImage imageNamed:@"bill_search_select.png"]
                          forState:UIControlStateHighlighted];
        
        [moneyQueryButton setImage:[UIImage imageNamed:@"bill_money_search_select.png"]
                          forState:UIControlStateNormal];
        
        [moneyQueryButton setImage:[UIImage imageNamed:@"bill_money_search_select.png"]
                          forState:UIControlStateHighlighted];
        
        [sendButton setImage:[UIImage imageNamed:@"money_send.png"]
                    forState:UIControlStateNormal];
        
        [sendButton setImage:[UIImage imageNamed:@"money_send_click.png"]
                    forState:UIControlStateHighlighted];
    }
    
    [self showView];
}

- (IBAction)showPicker:(id)sender
{
    [self backgroundTap:sender];
    
    CGRect contentViewRect = contentView.frame;
    CGRect viewRect = currencyButton.frame;
    
    [UIView animateWithDuration:0.35 animations:^{
        [theScrollView setContentOffset:CGPointMake(0, viewRect.origin.y + 180 + contentViewRect.origin.y - 6)
                               animated:NO];
        CGRect viewRect = self.view.frame;
        chooseContentView.frame = CGRectMake(0, viewRect.size.height - 260, 320, 260);
    }];
}

- (IBAction)hidePicker:(id)sender
{
    [UIView animateWithDuration:0.35 animations:^{
        [theScrollView setContentOffset:CGPointMake(0, 0) animated:NO];
        CGRect viewRect = self.view.frame;
        chooseContentView.frame = CGRectMake(0, viewRect.size.height, 320, 260);
    }];
}

- (IBAction)ok:(id)sender
{
    [self hidePicker:sender];
    
    selectedRow = [mPickerView selectedRowInComponent:0];
    
    if (selectedRow == 0)
        code = @"CNY";
    else if (selectedRow == 1)
        code = @"USD";
    else if (selectedRow == 2)
        code = @"JPY";
    
    
    currencyLabel.text = currency[selectedRow];
    
    
}

#pragma mark - SMS
- (IBAction)sendSMS:(id)sender
{
    
    NSString *carrierName = [NSString getCarrierName];
    NSLog(@"name = %@", carrierName);
    
    if (isSearchBill)
        message = [self replaceSMSTemplate:sms.billTemplate];
    else
        message = [self replaceSMSTemplate:sms.remTemplate];
    
    [self displaySMSComposerSheet];
}

/***********************************************
 * 函数功能：短信发送
 ***********************************************/
- (void)displaySMSComposerSheet
{
    MFMessageComposeViewController *messageComposeViewController = [[MFMessageComposeViewController alloc] init];
    messageComposeViewController.messageComposeDelegate = self;
    messageComposeViewController.body = message;
    messageComposeViewController.recipients = @[sms.mobile];
    [self presentModalViewController:messageComposeViewController animated:YES];
}

#pragma mark - MFMessageComposeViewControllerDelegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:
            break;
        case MessageComposeResultSent:
            break;
    }
    
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - UITextFieldDelegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [theScrollView setContentOffset:CGPointMake(0, 180) animated:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

#pragma mark - UIPickerViewDataSource methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 3;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return currency[row];
}

@end
