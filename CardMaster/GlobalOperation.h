//
//  GlobalOperation.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-22.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalOperation : NSObject

// 创建缓存文件夹，每次运行程序都会删除，重新创建
+ (BOOL)createCacheDirectory;

// 创建资源文件夹
+ (BOOL)createLocalDirectory;

// 创建保存信用卡图片文件夹
+ (BOOL)createCreditCardImageDirectory;

// 创建保存优惠图片文件夹
+ (BOOL)createDiscountImageDirectory;

@end
