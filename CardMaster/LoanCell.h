//
//  LoanCell.h
//  CardMaster
//
//  Created by wenjun on 13-1-11.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoanProduct.h"

@interface LoanCell : UITableViewCell

+ (LoanCell *)getInstance;
- (void)loadLoan:(LoanProduct *)loan;

@end
