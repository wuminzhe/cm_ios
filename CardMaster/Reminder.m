//
//  Reminder.m
//  cardMaster
//
//  Created by Zhang Ming on 12-10-24.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "Reminder.h"
#import "NSObject+Extension.h"

@implementation Reminder

- (id)init
{
    if (self = [super init]) {
        _cardID = @"";
//        _reminderDay = nil;
        _reminderDay = @"1";
        _reminderHour = @"0";
        _preDay = @"1";
        _on = @"0";
    }
    
    return self;
}

@end
