//
//  DiscountDetailCell.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-18.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "DiscountDetailCell.h"

@implementation DiscountDetailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)changeToMapView:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(changeToMapView:)])
        [_delegate changeToMapView:sender];
}

- (IBAction)takePhoneCall:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(takePhoneCall:)])
        [_delegate takePhoneCall:sender];
}

+ (DiscountDetailCell *)getEffectiveDateCell
{
    DiscountDetailCell *cell =[[[NSBundle mainBundle] loadNibNamed:@"DiscountDetailCell" owner:nil options:nil] objectAtIndex:0];
    
    return cell;
}

+ (DiscountDetailCell *)getBankCell
{
    DiscountDetailCell *cell =[[[NSBundle mainBundle] loadNibNamed:@"DiscountDetailCell" owner:nil options:nil] objectAtIndex:1];
    
    return cell;
}

+ (DiscountDetailCell *)getAddressCell
{
    DiscountDetailCell *cell =[[[NSBundle mainBundle] loadNibNamed:@"DiscountDetailCell" owner:nil options:nil] objectAtIndex:2];
    
    return cell;
}

+ (DiscountDetailCell *)getPhoneCell
{
    DiscountDetailCell *cell =[[[NSBundle mainBundle] loadNibNamed:@"DiscountDetailCell" owner:nil options:nil] objectAtIndex:3];
    
    return cell;
}

+ (DiscountDetailCell *)getDetailCell
{
    DiscountDetailCell *cell =[[[NSBundle mainBundle] loadNibNamed:@"DiscountDetailCell" owner:nil options:nil] objectAtIndex:4];
    
    return cell;
}

@end
