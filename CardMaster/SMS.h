//
//  SMS.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-2.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMS : NSObject

@property (nonatomic, strong) NSString *smsID;
@property (nonatomic, strong) NSString *bankID;
@property (nonatomic, strong) NSString *billTemplate;
@property (nonatomic, strong) NSString *remTemplate;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *mobileUnicom;
@property (nonatomic, strong) NSString *mobileTelecom;
@property (nonatomic, strong) NSString *createTime;
@property (nonatomic, strong) NSString *updateTime;

@end
