//
//  WalletCell.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-16.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class CreditCard;

@interface WalletCell : UITableViewCell {
    __unsafe_unretained IBOutlet UIButton *cardButton;
    __unsafe_unretained IBOutlet UILabel *freeLabel;
    __unsafe_unretained IBOutlet UIActivityIndicatorView *activity;
    __unsafe_unretained IBOutlet UILabel *waitLabel;
}

@property (nonatomic, unsafe_unretained) IBOutlet UIButton *cardButton;
@property (nonatomic, unsafe_unretained) IBOutlet UIImageView *bankLogoView;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *cardInfoLabel;
@property (nonatomic, strong) NSString *freeDays;
@property (nonatomic, assign) BOOL isAddingCard;

- (void)registerClickEventWithTarget:(id)target selector:(SEL)selector;
- (void)loadCard:(CreditCard *)card index:(NSInteger)index;

+ (WalletCell *)getNomalInstance;
+ (WalletCell *)getTopInstance;
+ (WalletCell *)getBottomInstance;
+ (WalletCell *)getHeaderInstance;

@end
