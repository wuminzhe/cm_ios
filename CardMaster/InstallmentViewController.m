//
//  InstallmentViewController.m
//  cardMaster
//
//  Created by Zhang Ming on 12-10-25.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "InstallmentViewController.h"
#import "InstallmentResultViewController.h"
#import "InstallmentEngine.h"
#import "GlobalHeader.h"
#import "CMUtil.h"
#import "SQLGlobal.h"
#import "Bank.h"
#import "Rate.h"

@interface InstallmentViewController () {
    NSMutableArray *banks;
    NSMutableArray *rates;
    
    Bank *selectedBank;
    Rate *selectedRate;
    
    NSInteger selectedBankIndex;                    // pickerView选择的银行列表index
    NSInteger selectedRateIndex;                    // pickerView选择的利率列表index
    NSInteger lastSelectedBankIndex;                // 保存上一次选择银行index
}

@end

@implementation InstallmentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        selectedBank = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"分期计算";
    
    [self.view addSubview:selectedView];
    selectedView.frame = CGRectMake(0, 416, 320, 260);
    
    [theScrollView addSubview:helpView];
    helpView.frame = CGRectMake(192, 78, 119, 67);
    helpView.alpha = 0.0;
    
    selectedBankIndex = 0;
    selectedRateIndex = 0;
    
    engine = [[InstallmentEngine alloc] initWithHostName:kServerAddr];
    
    [self loadData];
}

- (void)viewDidUnload
{
    theScrollView = nil;
    accountTextfield = nil;
    helpView = nil;
    minAmountLabel = nil;
    selectedView = nil;
    pickerView = nil;
    bankNameLabel = nil;
    stageLabel = nil;
    helpButton = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)backgroundTap:(id)sender
{
    [accountTextfield resignFirstResponder];
    [self hidePickerView];
}

- (IBAction)showInfo:(id)sender
{
    // 如果sender为nil,则表明不是点击触发
    if (!sender) {
        double account = accountTextfield.text.doubleValue;
        
        // 如果输入的分期金额小于银行的最低金额，则提示
        if (account < selectedRate.minAmount.doubleValue) {
            selected = YES;
        } else
            selected = NO;
    } else {
        selected = !selected;
    }
    
    if (selected) {
        [helpButton setImage:[UIImage imageNamed:@"btn_help_click.png"]
                    forState:UIControlStateNormal];
        [helpButton setImage:[UIImage imageNamed:@"btn_help_click.png"]
                    forState:UIControlStateHighlighted];
        
        [self showHelpView];
    } else {
        [helpButton setImage:[UIImage imageNamed:@"btn_help.png"]
                    forState:UIControlStateNormal];
        [helpButton setImage:[UIImage imageNamed:@"btn_help_click.png"]
                    forState:UIControlStateHighlighted];
        
        [self hideHelpView];
    }
}

- (void)showHelpView
{
    minAmountLabel.text = [NSString stringWithFormat:@"%.1f", selectedRate.minAmount.doubleValue];
    
    [UIView animateWithDuration:0.25 animations:^{
        helpView.alpha = 1.0;
    }];
}

- (void)hideHelpView
{
    [UIView animateWithDuration:0.25 animations:^{
        helpView.alpha = 0.0;
    }];
}

- (IBAction)showPickerView:(id)sender
{
    int tag = ((UIButton *)sender).tag;
    choose = tag;
    
    [accountTextfield resignFirstResponder];
    
    [UIView animateWithDuration:0.35 animations:^{
        
        if (tag == 1001) {
            [theScrollView setContentOffset:CGPointMake(0, 100) animated:YES];
            pickerData = banks;
            [pickerView reloadAllComponents];
            [pickerView selectRow:selectedBankIndex inComponent:0 animated:YES];
        } else {
            [theScrollView setContentOffset:CGPointMake(0, 160) animated:YES];
            pickerData = rates;
            [pickerView reloadAllComponents];
            [pickerView selectRow:selectedRateIndex inComponent:0 animated:YES];
        }
        
        selectedView.frame = CGRectMake(0, 416 - 260, 320, 260);
    }];
}

- (IBAction)hidePickerView
{
    [UIView animateWithDuration:0.35 animations:^{
        [theScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        selectedView.frame = CGRectMake(0, 416, 320, 260);
    }];
}

- (IBAction)ok:(id)sender
{
    NSInteger index = [pickerView selectedRowInComponent:0];
    
    // 如果选择银行，则进行利率加载
    if (choose == 1001) {
        
        selectedBankIndex = index;
        
        // 如果连续两次选择是同一家银行，则不用重新加载利率表数据
        if (lastSelectedBankIndex == selectedBankIndex) {
            [self hidePickerView];
            return;
        }
        
        lastSelectedBankIndex = selectedBankIndex;
        selectedBank = banks[selectedBankIndex];
        
        rates = nil;
        
        rates = [[SQLGlobal sharedInstance] getRatesWithBankID:selectedBank.bankID];
        bankNameLabel.text = selectedBank.name;
        
        if (rates)
            selectedRate = rates[0];
        else
            stageLabel.text = @"";
    } else {
        selectedRateIndex = index;
    }
    
    if (!rates) {
        [self hidePickerView];
        return;
    }
    
    // 获得选中的利率
    selectedRate = rates[selectedRateIndex];
    
    // 比较输入的金额是否大于利率的最小金额度
    [self showInfo:nil];
    
    if (selectedRate)
        stageLabel.text = [NSString stringWithFormat:@"%@期", selectedRate.stage];
    
    [self hidePickerView];
}

- (IBAction)calculate:(id)sender
{
    if (!selectedRate)
        return;
    
    if ([accountTextfield.text doubleValue] < selectedRate.minAmount.doubleValue) {
        [self showInfo:nil];
        return;
    }
    
    InstallmentResultViewController *installmentResultViewController = [[InstallmentResultViewController alloc] initWithNibName:@"InstallmentResultViewController" bundle:nil];
    
    installmentResultViewController.rate = selectedRate;
    installmentResultViewController.account = [accountTextfield.text doubleValue];
    
    [self.navigationController pushViewController:installmentResultViewController animated:YES];
}

// 获得banks之后设置数据
- (void)settingData
{
    
    for (Bank *bank in banks) {
        if ([bank.bankID isEqualToString:_bankId]) {
            selectedBank = bank;
            break;
        }
    }
    
    if (!selectedBank)
        selectedBank = banks[0];

    bankNameLabel.text = selectedBank.name;
    [self loadRateData];
}

- (void)loadData
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"code"] = [CMUtil getCode];
    
    [engine getInstallmentUpdateStatus:params onCompletion:^(BOOL status) {
        // 如果返回YES，则进行更新操作
        if (status) {
            [engine updateInstallmentRate:params onCompletion:^(NSMutableArray *result) {
                banks = result;
                [self settingData];
            } onError:^(NSError *error) {
                banks = [[SQLGlobal sharedInstance] getBanks];
                [self settingData];
            }];
        } else {
            banks = [[SQLGlobal sharedInstance] getBanks];
            [self settingData];
        }
    } onError:^(NSError *error) {
        banks = [[SQLGlobal sharedInstance] getBanks];
        [self settingData];
    }];
}

- (void)loadRateData
{
    rates = [[SQLGlobal sharedInstance] getRatesWithBankID:selectedBank.bankID];

    if (rates)
        selectedRate = rates[0];
    
    if (selectedRate)
        stageLabel.text = [NSString stringWithFormat:@"%@期", selectedRate.stage];
}

#pragma mark - UIPickerViewDataSource methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerData.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (choose == 1001) {
        Bank *bank = pickerData[row];
        
        return bank.name;
    }
    
    Rate *rate = pickerData[row];
    
    return rate.stage;
}

#pragma mark - UITextFieldDelegate methods
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self showInfo:nil];
}

@end
