//
//  LoginEngine.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-22.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "LoginEngine.h"
#import "NSDictionary+Extension.h"

@implementation LoginEngine

- (MKNetworkOperation *)login:(NSMutableDictionary *)params
                 onCompletion:(LoginResponseBlock)completionBlock
                      onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/login" params:params httpMethod:@"POST"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        BOOL status = [[response stringForKey:@"status"] boolValue];
        if (status) {
            NSDictionary *data = [response objectForKey:@"data"];
            NSString *code = [data stringForKey:@"code"];
            
            completionBlock(code);
        } else 
            completionBlock(@"FAIL");
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)loginWithWeibo:(NSMutableDictionary *)params
                          onCompletion:(LoginWithWeiboResponseBlock)completionBlock
                               onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/weibo_login" params:params httpMethod:@"POST"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        BOOL status = [[response stringForKey:@"status"] boolValue];
        NSString *code = nil;
        
        if (status) {
            code = [response stringForKey:@"code"];
            
            completionBlock(code);
        } else {
            completionBlock(@"NO");
        }
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)registerUser:(NSMutableDictionary *)params
                        onCompletion:(RegisterResponseBlock)completionBlock
                             onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/register" params:params httpMethod:@"POST"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        NSDictionary *data = [response objectForKey:@"data"];
        
        BOOL status = [[response stringForKey:@"status"] boolValue];
        
        if (status) {
            completionBlock([data stringForKey:@"code"]);
        } else
            completionBlock(nil);
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)getCode:(NSDictionary *)params
                   onCompletion:(GetCodeResponseBlock)completionBlock
                        onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/init"
                                              params:nil
                                          httpMethod:@"POST"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        NSDictionary *data = response[@"data"];
        
        completionBlock([data stringForKey:@"code"]);
    } onError:^(NSError *error) {
        DLog("error:%@", error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

@end
