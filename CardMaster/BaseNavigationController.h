//
//  BaseNavigationController.h
//  cardMaster
//
//  Created by Zhang Ming on 12-12-3.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseNavigationController : BaseViewController

@property (nonatomic, strong) UIButton *backButton;

@end
