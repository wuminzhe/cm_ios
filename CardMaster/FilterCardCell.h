//
//  FilterCardCell.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-16.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterCardCell : UITableViewCell

@property (nonatomic, unsafe_unretained) IBOutlet UILabel *cardNameLabel;

+ (FilterCardCell *)getFilterCardCell;

@end
