//
//  BillImportViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 13-1-8.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BillImportViewController.h"
#import "CheckBox.h"
#import "RegexKitLite.h"
#import "LvToast.h"
#import "GTMUtil.h"
#import "CMUtil.h"
#import "JSONKit.h"
#import "CMConstants.h"
#import "NSDictionary+Extension.h"

@interface BillImportViewController () <UIPickerViewDelegate,UIPickerViewDataSource,UIAlertViewDelegate,UITextFieldDelegate>
{
    IBOutlet UITextField * emailField;
    IBOutlet UITextField * passwordField;
    IBOutlet UIButton * emailSelectBtn;
    IBOutlet CheckBox * checkBox;
    IBOutlet UILabel * protocolLabel;
    IBOutlet UIPickerView * picker;
    IBOutlet UIView * tapView;
    IBOutlet UIView * protocolPart;
    
    IBOutlet UIView *chooseView;
}

- (IBAction)next;
- (IBAction)textDone;
- (IBAction)changeEmail;
- (IBAction)submit;

@property (strong,nonatomic) NSMutableArray * emailChoices;

@end

@implementation BillImportViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
//    picker.hidden = YES;
    [self hidePickerView:nil];
    return YES;
}

- (IBAction)next
{
    [passwordField becomeFirstResponder];
}

- (void)resign
{
    [emailField resignFirstResponder];
    [passwordField resignFirstResponder];
}

- (IBAction)textDone
{
    [self resign];
}

- (IBAction)changeEmail
{
    [self resign];
//    picker.hidden = NO;
    [self showPickerView:nil];
}

- (void)bgTap
{
    [self resign];
    [self hidePickerView:nil];
//    picker.hidden = YES;
}

- (NSString *)checkForm
{
    if (emailField.text.length == 0)
    {
        return @"邮箱不能为空";
    }
    NSString * email = [NSString stringWithFormat:@"%@%@",emailField.text,emailSelectBtn.titleLabel.text];
    if (![self isEmail:email])
    {
        return @"邮箱格式不正确";
    }
    if (passwordField.text.length == 0)
    {
        return @"密码不能为空";
    }
    if (!checkBox.checked)
    {
        return @"请阅读安全保障协议并同意";
    }
    return nil;
}

- (IBAction)submit
{
    NSString * msg = [self checkForm];
    if (msg.length > 0)
    {
        [LvToast showWithText:msg];
    }
    else
    {
        [self showHudView:@"正在处理中..."];
        if (self.email)
        {
            NSDictionary * params = @{@"code" : [CMUtil getCode], @"email_id" : self.email.pk, @"email" : [NSString stringWithFormat:@"%@%@",emailField.text,emailSelectBtn.titleLabel.text], @"password" : passwordField.text};
            GTMHTTPFetcher * fetcher = [GTMUtil fetcherWithURLString:[NSString stringWithFormat:@"http://%@/api/email_update",kServerAddr] params:params HTTPType:@"post" urlEncode:NO];
            [fetcher beginFetchWithDelegate:self didFinishSelector:@selector(updateEmailFetcher:data:error:)];
        }
        else
        {            
            NSDictionary * params = @{@"code" : [CMUtil getCode],@"email" : [NSString stringWithFormat:@"%@%@",emailField.text,emailSelectBtn.titleLabel.text],@"password" : passwordField.text};
            GTMHTTPFetcher * fetcher = [GTMUtil fetcherWithURLString:[NSString stringWithFormat:@"http://%@/api/add_email",kServerAddr] params:params HTTPType:@"post" urlEncode:NO];
            [fetcher beginFetchWithDelegate:self didFinishSelector:@selector(addEmailFetcher:data:error:)];
        }

    }
}

- (void)updateEmailFetcher:(GTMHTTPFetcher *)fetcher data:(NSData *)data error:(NSError *)error
{
    [self hideHudView];
    
    BOOL success = NO;
    
    if (fetcher.statusCode == 200)
    {
        NSDictionary * jsonDict = [data objectFromJSONData];
        if ([[jsonDict objectForKey:@"status"] intValue] == 1)
        {
            success = YES;
        }
    }
    if (success)
    {
        self.email.emailName = [NSString stringWithFormat:@"%@%@",emailField.text,emailSelectBtn.titleLabel.text];
        self.email.password = passwordField.text;
        [LvToast showWithText:@"修改邮箱成功" duration:1];
        [self performSelector:@selector(backAction) withObject:nil afterDelay:.5];
    }
    else
    {
        [LvToast showWithText:@"修改邮箱失败，请重试" duration:1];
    }
}

- (void)addEmailFetcher:(GTMHTTPFetcher *)fetcher data:(NSData *)data error:(NSError *)error
{
    [self hideHudView];
    
    BOOL success = NO;
    
    if (fetcher.statusCode == 200)
    {
        NSDictionary * jsonDict = [data objectFromJSONData];
        if ([[jsonDict stringForKey:@"status"] intValue] == 1)
        {
            success = YES;
        }
        else
        {
            NSString * msg = [jsonDict stringForKey:@"msg"];
            if (msg)
            {
                [LvToast showWithText:msg duration:1];
            }
        }
    } else if (fetcher.statusCode == 400) {
        NSDictionary * jsonDict = [data objectFromJSONData];
        
        NSString * msg = [jsonDict stringForKey:@"msg"];
        if (msg)
        {
            [LvToast showWithText:msg duration:1];
        }
    }
    
    if (success)
    {
        NSDictionary * jsonDict = [data objectFromJSONData];
        NSString *emailId = [jsonDict stringForKey:@"email_id"];
        
        NSMutableArray *emailList = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:IMPORT_BILL]];
        if (!emailList)
            emailList = [[NSMutableArray alloc] init];
        
        [emailList addObject:emailId];
        
        [[NSUserDefaults standardUserDefaults] setObject:emailList forKey:IMPORT_BILL];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [LvToast showWithText:@"添加邮件成功" duration:1];
        [self performSelector:@selector(backAction) withObject:nil afterDelay:.5];
    }
}

- (void)protocol
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"安全保障协议" message:@"我爱卡在信用卡在线申请方面已经积累了的多年的技术和经验，并得到众多用户和合作银行的全权信任。保护您的隐私信息安全，是我们的能力所及，也是我们的责任所至。\n我爱卡管家郑重承诺：\n1.隐私保护:您的个人信息、信用卡信息以及银行卡信息都仅用于您在我爱卡管家中使用的各项服务。未经您同意和确认，我们不会把您的任何个人信息提供给无关的第三方（除国家法律、地方法规和政府规章规定之外）。\n2.信息安全:我们采用行业标准以维护您的个人资料的隐私性，并用合理的安全技术手段保护已存储的个人信息，对您所提供的个人资料进行严格的管理和保护，防止个人资料丢失、被盗用或遭窜改。\n3.免责事项:用户出现下列情况时，我们不承担任何责任：\n一、用户主动告知他人密码、个人信息或共享帐户，而导致的任何个人资料泄露； \n二、由于用户计算机遭受第三方的攻击或病毒侵入、政府管制、其他任何影响正常经营之不可抗力而造成之个人资料丢失、盗用、篡改、泄露等。" delegate:self cancelButtonTitle:@"不同意" otherButtonTitles:@"同意", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    checkBox.checked = buttonIndex == 1;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"导入账单";
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bgTap)];
    [tapView addGestureRecognizer:tapGesture];
    
    [checkBox setCheckedImage:[UIImage imageNamed:@"sellect2"]];
    [checkBox setUnCheckedImage:[UIImage imageNamed:@"unsellect2"]];
    checkBox.checked = NO;
    
    UITapGestureRecognizer * protocolGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(protocol)];
    [protocolLabel addGestureRecognizer:protocolGesture];
    
    self.emailChoices = [NSMutableArray array];
    
//    picker.hidden = YES;
    
    emailField.delegate = self;
    passwordField.delegate = self;
    
    [self.view addSubview:chooseView];
    chooseView.frame = CGRectMake(0, self.view.frame.size.height, 320, 260);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.email && [self isEmail:self.email.emailName])
    {
        emailField.text = [[self.email.emailName componentsSeparatedByString:@"@"] objectAtIndex:0];
        passwordField.text = self.email.password;
        NSString *suffix = [NSString stringWithFormat:@"@%@", [[self.email.emailName componentsSeparatedByString:@"@"] lastObject]];
        
        [emailSelectBtn setTitle:suffix forState:UIControlStateNormal];
        [emailSelectBtn setTitle:suffix forState:UIControlStateHighlighted];
    }
    if (self.emailChoices.count == 0)
    {
        [self requestEmailChoices];
    }
}

- (void)requestEmailChoices
{
    //todo
    [_emailChoices addObject:@"@163.com"];
    [_emailChoices addObject:@"@qq.com"];
    
    [self requestEmailOver];
}

- (void)requestEmailOver
{
    //todo 更新emailchoices列表
    [picker reloadAllComponents];
    [picker selectedRowInComponent:[self.emailChoices indexOfObject:emailSelectBtn.titleLabel.text]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[CMUtil getAppDelegate].tabBarController.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)isEmail:(NSString *)email
{
    return [[email stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isMatchedByRegex:@"[\\w-]+@([\\w-]+\\.)+[\\w-]+"];
}

#pragma mark - Actions
- (void)showPickerView:(id)sender
{
    [UIView animateWithDuration:0.25 animations:^{
        chooseView.frame = CGRectMake(0, self.view.frame.size.height - 260, 320, 260);
    }];
}

- (IBAction)choose:(id)sender
{
    [self hidePickerView:nil];
}

- (IBAction)hidePickerView:(id)sender
{
    [UIView animateWithDuration:0.25 animations:^{
        chooseView.frame = CGRectMake(0, self.view.frame.size.height, 320, 260);
    }];
}

#pragma picker

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.emailChoices.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self.emailChoices objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSString * choice = [self.emailChoices objectAtIndex:row];
    [emailSelectBtn setTitle:choice forState:UIControlStateNormal];
    [emailSelectBtn setTitle:choice forState:UIControlStateHighlighted];
    
//    [self hidePickerView:nil];
//    picker.hidden = YES;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


#pragma mark - Private methods
- (void)backAction
{
    self.email = nil;
    picker.hidden = YES;
    [self hidePickerView:nil];
    [self resign];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
