//
//  EmailCell.m
//  CardMaster
//
//  Created by wenjun on 13-1-15.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "EmailCell.h"

@interface EmailCell ()
{
    IBOutlet UILabel * nameLabel;
}

@end

@implementation EmailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadEmail:(Email *)email
{
    nameLabel.text = email.emailName;
}

+ (EmailCell *)getInstance
{
    return [[[NSBundle mainBundle] loadNibNamed:@"EmailCell" owner:nil options:nil] lastObject];
}

@end
