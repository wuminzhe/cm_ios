//
//  CMUtil.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-17.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface CMUtil : NSObject

+ (AppDelegate *)getAppDelegate;

+ (NSString *)getCode;

+ (NSString *)getUsername;

+ (UIImage *)getNumberImage:(NSInteger)num;

+ (UIImageView *)getNumberImageView:(NSInteger)num;

+ (UIImage *)walletImgFromCardImg:(UIImage *)cardImg;

+ (UIImage *)getBankLogo:(NSString *)bankID;

@end
