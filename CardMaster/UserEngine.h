//
//  UserEngine.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-27.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MKNetworkEngine.h"

@interface UserEngine : MKNetworkEngine

typedef void (^GetUserInfoResponse) (NSMutableArray *results);
typedef void (^GetImageResponse) (BOOL status);

- (MKNetworkOperation *)getAllCards:(NSMutableDictionary *)params
                       onCompletion:(GetUserInfoResponse)completionBlock
                            onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)getAllReminders:(NSMutableDictionary *)params
                           onCompletion:(GetUserInfoResponse)completionBlock
                                onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation* )downloadFatAssFileFrom:(NSString*)remoteURL
                                       toFile:(NSString*)fileName;

- (MKNetworkOperation *)getFavoriteDiscounts:(NSMutableDictionary *)params
                                onCompletion:(GetUserInfoResponse)completionBlock
                                     onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)downloadImage:(NSString *)path
                             destPath:(NSString *)destPath
                         onCompletion:(GetImageResponse)completionBlock
                              onError:(MKNKErrorBlock)errorBlock;


@end
