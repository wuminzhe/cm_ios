//
//  GEOInfo.m
//  NearbyShops
//
//  Created by wenjun on 12-12-20.
//
//

#import "GEOInfo.h"

@implementation GEOInfo

- (id)initWithGEODict:(NSDictionary *)geoDict
{
    self = [super init];
    if (self)
    {
        _contry = [geoDict objectForKey:@"country"];
        _province = [geoDict objectForKey:@"administrative_area_level_1"];
        _city = [geoDict objectForKey:@"locality"];
        _district = [geoDict objectForKey:@"sublocality"];
        _street = [geoDict objectForKey:@"route"];
        _streetNumber = [geoDict objectForKey:@"street_number"];
        _postCode = [geoDict objectForKey:@"postal_code"];
        NSDictionary * location = [geoDict objectForKey:@"location"];
        _coordinate = CLLocationCoordinate2DMake([[location objectForKey:@"lat"] doubleValue], [[location objectForKey:@"lng"] doubleValue]);
    }
    return self;
}

@end
