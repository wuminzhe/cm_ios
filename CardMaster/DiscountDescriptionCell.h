//
//  DiscountDescriptionCell.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-21.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"

#define kTypeWidth                  63

@protocol DiscountDescriptionCellDelegate <NSObject>

@optional

- (void)handleBookmark:(id)sender;
- (void)handleFeedback:(id)sender;
- (void)handleShare:(id)sender;

@end

@interface DiscountDescriptionCell : UITableViewCell

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *discountNameLabel;

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *averageTitleLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *averageLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *sheepLabel;

@property (unsafe_unretained, nonatomic) IBOutlet EGOImageView *discountIconView;

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *bookmarkTitleLabel;

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *bookmarkLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *bookmarkIconView;

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *discountLabel;

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *discountTextLabel;

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *discountTitleLabel;

@property (nonatomic) id<DiscountDescriptionCellDelegate> delegate;

@property (nonatomic) NSInteger type;

@property (nonatomic, strong) NSString *rate;

- (IBAction)handleBookmark:(id)sender;
- (IBAction)handleFeedback:(id)sender;
- (IBAction)handleShare:(id)sender;

+ (DiscountDescriptionCell *)getInstance;
@end
