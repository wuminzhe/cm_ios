//
//  RefundRemindEngine.m
//  cardMaster
//
//  Created by Zhang Ming on 12-10-23.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "RefundRemindEngine.h"
#import "NSDictionary+Extension.h"

@implementation RefundRemindEngine

- (MKNetworkOperation *)addReminder:(NSMutableDictionary *)params
                       onCompletion:(AddReminderResponseBlock)completionBlock
                            onError:(MKNKErrorBlock)errorBlock
{ 
    MKNetworkOperation *op = [self operationWithPath:@"api/add_reminder"
                                              params:params
                                          httpMethod:@"POST"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        NSLog(@"response = %@", response);
        
        BOOL status = [[response stringForKey:@"status"] boolValue];
        
        completionBlock(status);
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

@end
