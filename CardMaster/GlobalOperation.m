//
//  GlobalOperation.m
//  cardMaster
//
//  Created by Zhang Ming on 12-10-22.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "GlobalOperation.h"

@implementation GlobalOperation

+ (BOOL)createCacheDirectory
{
    // 创建临时缓存文件夹(程序打开即删除再新建)
    BOOL bHaveCacheDir = [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/Documents/cache",NSHomeDirectory()] isDirectory:nil];
    
    if (bHaveCacheDir)
    {
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/Documents/cache",NSHomeDirectory()] error:nil];
    }
    
    return [[NSFileManager defaultManager] createDirectoryAtPath:[NSString stringWithFormat:@"%@/Documents/cache",NSHomeDirectory()] withIntermediateDirectories:YES attributes:nil error:nil];
}

+ (BOOL)createLocalDirectory
{
    BOOL bHaveLocalDir = [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/Documents/local",NSHomeDirectory()] isDirectory:nil];
    
    if (!bHaveLocalDir)
    {
        return [[NSFileManager defaultManager] createDirectoryAtPath:[NSString stringWithFormat:@"%@/Documents/local",NSHomeDirectory()] withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    return  YES;
}

+ (BOOL)createCreditCardImageDirectory
{
    BOOL bHaveCreditCardDir = [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/Documents/local/card",NSHomeDirectory()] isDirectory:nil];
    
    if (!bHaveCreditCardDir)
    {
        return [[NSFileManager defaultManager] createDirectoryAtPath:[NSString stringWithFormat:@"%@/Documents/local/card",NSHomeDirectory()] withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    return  YES;
}

+ (BOOL)createDiscountImageDirectory
{
    BOOL bHaveDiscountDir = [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/Documents/local/discount",NSHomeDirectory()] isDirectory:nil];
    
    if (!bHaveDiscountDir)
    {
        return [[NSFileManager defaultManager] createDirectoryAtPath:[NSString stringWithFormat:@"%@/Documents/local/discount",NSHomeDirectory()] withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    return  YES;
}

@end
