//
//  BaseViewController.h
//  cardMaster
//
//  Created by Zhang Ming on 12-12-3.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "LvToast.h"

/* BaseViewController
 * 设置通用方法
 */

@interface BaseViewController : UIViewController<MBProgressHUDDelegate> {
    MBProgressHUD *hud;
}

- (void)quickAlertView:(NSString *)message;

- (void)showHudView:(NSString *)text;

- (void)showHudViewOnWindow:(NSString *)text;

- (void)hideHudView;

- (void)showNoResultOnView:(UIView *)subView;

- (void)hideNoResult;

@end
