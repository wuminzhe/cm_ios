//
//  SuggestViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 12-12-26.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "SuggestViewController.h"
#import "MZBubbleData.h"
#import "SQLGlobal.h"
#import "SuggestEngine.h"
#import "CMConstants.h"
#import "CMUtil.h"
#import "LvToast.h"

@interface SuggestViewController ()

@end

@implementation SuggestViewController
{
    NSMutableArray *bubbleData;
    SuggestEngine *engine;
    NSMutableDictionary *sendParams;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 5.0) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        bubbleData = nil;
        engine = nil;
        sendParams = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"意见建议";
    
    bubbleTableView.bubbleDataSource = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 5.0) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    }
    
    if (!engine)
        engine = [[SuggestEngine alloc] initWithHostName:kServerAddr];
    
    sendParams = [[NSMutableDictionary alloc] initWithCapacity:2];
    sendParams[@"code"] = [CMUtil getCode];
    
//    bubbleData = [[NSMutableArray alloc] initWithObjects:
//                  [MZBubbleData dataWithText:@"Marge, there's something that I want to ask you, but I'm afraid, because if you say no, it will destroy me and make me a criminal." andDate:[NSDate dateWithTimeIntervalSinceNow:-300] andType:BubbleTypeMine],
//                  [MZBubbleData dataWithText:@"Well, I haven't said no to you yet, have I?" andDate:[NSDate dateWithTimeIntervalSinceNow:-280] andType:BubbleTypeSomeoneElse],
//                  [MZBubbleData dataWithText:@"Marge... Oh, damn it." andDate:[NSDate dateWithTimeIntervalSinceNow:0] andType:BubbleTypeMine],
//                  [MZBubbleData dataWithText:@"What's wrong?" andDate:[NSDate dateWithTimeIntervalSinceNow:300]  andType:BubbleTypeSomeoneElse],
//                  [MZBubbleData dataWithText:@"Ohn I wrote down what I wanted to say on a card.." andDate:[NSDate dateWithTimeIntervalSinceNow:395]  andType:BubbleTypeMine],
//                  [MZBubbleData dataWithText:@"The stupid thing must have fallen out of my pocket." andDate:[NSDate dateWithTimeIntervalSinceNow:400]  andType:BubbleTypeMine],
//                  nil];
    
    bubbleData = [[SQLGlobal sharedInstance] getMessages];
    
    
    if (!bubbleData)
        bubbleData = [[NSMutableArray alloc] init];
    
    [self loadMessages];
}

- (void)viewDidUnload
{
    bubbleTableView = nil;
    chatInputView = nil;
    messageTextField = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods
- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.userInfo;
    
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [value CGRectValue];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    CGRect viewRect = self.view.frame;
    CGFloat tableViewHeight = viewRect.size.height - chatInputView.frame.size.height - keyboardRect.size.height;
    
    bubbleTableView.frame = CGRectMake(0, 0, 320, tableViewHeight);
    [bubbleTableView scrollToEnd];
    
    [UIView animateWithDuration:animationDuration animations:^{
        chatInputView.frame = CGRectMake(0, tableViewHeight, 320, chatInputView.frame.size.height);
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.userInfo;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    CGRect viewRect = self.view.frame;
    CGFloat tableViewHeight = viewRect.size.height - chatInputView.frame.size.height;
    
    [UIView animateWithDuration:animationDuration animations:^{
        bubbleTableView.frame = CGRectMake(0, 0, 320, tableViewHeight);
        chatInputView.frame = CGRectMake(0, tableViewHeight, 320, chatInputView.frame.size.height);
    } completion:^(BOOL finished) {
        [bubbleTableView scrollToEnd];
    }];
}

#pragma mark - Actions
- (IBAction)sendMessage:(id)sender
{
    [messageTextField resignFirstResponder];
    
    NSString *content = messageTextField.text;
    
    if (content.length == 0) return;
    
    sendParams[@"content"] = content;
    
    [engine sendMessage:sendParams onCompletion:^(BOOL status) {
        if (status) {
            messageTextField.text = @"";
            MZBubbleData *data = [[MZBubbleData alloc] initWithText:content andDate:[NSDate date] andType:BubbleTypeMine];
            
            [bubbleData addObject:data];
            
            [bubbleTableView reloadData];
        } else {
            [LvToast showWithText:@"发送失败！"];
        }
    } onError:^(NSError *error) {
        [LvToast showWithText:@"发送失败！"];
    }];
}

#pragma mark - Custom methods
- (void)loadMessages
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:2];
    params[@"code"] = [CMUtil getCode];
    params[@"page"] = @"1";
    
    [engine getMessages:params onCompletion:^(NSMutableArray *result) {
        if (result.count != bubbleData.count) {
            [bubbleData removeAllObjects];
            [bubbleData addObjectsFromArray:result];
            
            [[SQLGlobal sharedInstance] updateMessages:result];
            
            [bubbleTableView reloadData];
        }
    } onError:^(NSError *error) {
        [LvToast showWithText:@"获取回复失败！"];
    }];
}

#pragma mark - MZBubbleTableViewDataSource methods
- (NSInteger)rowsForBubbleTable:(MZBubbleTableView *)tableView
{
    return [bubbleData count];
}

- (MZBubbleData *)bubbleTableView:(MZBubbleTableView *)tableView dataForRow:(NSInteger)row
{
    return [bubbleData objectAtIndex:row];
}

#pragma mark - UITextFieldDelegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
//    NSLog(@"size1 = %@", NSStringFromCGSize(bubbleTableView.contentSize));
//    bubbleTableView.frame = CGRectMake(0, 0, 320, 200);
//    NSLog(@"size2 = %@", NSStringFromCGSize(bubbleTableView.contentSize));
//    [bubbleTableView setContentOffset:CGPointMake(0, bubbleTableView.contentSize.height - 200) animated:YES];
}

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"111");
}

@end
