//
//  RefundRemindViewController.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-19.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"

@class Reminder;
@class CreditCard;
@class RefundRemindEngine;

@interface RefundRemindViewController : BaseNavigationController<UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIAlertViewDelegate> {
    __unsafe_unretained IBOutlet UIScrollView *theScrollView;
    __unsafe_unretained IBOutlet UIPickerView *pickerView;
    __unsafe_unretained IBOutlet UIView *contentView;
    __unsafe_unretained IBOutlet UITableView *mTableView;
    __unsafe_unretained IBOutlet UIImageView *leftFirstDayImageView;
    __unsafe_unretained IBOutlet UIImageView *leftSecondDayImageView;
    __unsafe_unretained IBOutlet UIImageView *leftFirstHourImageView;
    __unsafe_unretained IBOutlet UIImageView *leftSecondHourImageView;
    
    UISwitch *switchON;
    
    Reminder *reminder;
    
    RefundRemindEngine *engine;
}

@property (nonatomic, strong) CreditCard *card;                 // 卡片ID

- (IBAction)ok:(id)sender;
- (IBAction)cancel:(id)sender;

@end
