//
//  FreeAnnualFeeViewController.m
//  cardMaster
//
//  Created by Zhang Ming on 12-12-3.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "FreeAnnualFeeViewController.h"
#import "SQLGlobal.h"
#import "CreditCard.h"
#import "Bank.h"
#import "FreeAnnualFeeEngine.h"
#import "GlobalHeader.h"
#import "CMUtil.h"
#import "CMConstants.h"
#import "Fee.h"

@interface FreeAnnualFeeViewController () {
    Bank *bank;
    
    NSString *feePolicy;
    
    NSMutableArray *years;
    NSMutableArray *months;
    
    NSString *effective;
    NSString *count;
    NSString *reminderDay;
}

@end

@implementation FreeAnnualFeeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"免年费";
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveButton setImage:[UIImage imageNamed:@"save.png"]
                forState:UIControlStateNormal];
    [saveButton setImage:[UIImage imageNamed:@"save_click.png"]
                forState:UIControlStateHighlighted];
    
    [saveButton addTarget:self
                    action:@selector(saveFee)
         forControlEvents:UIControlEventTouchUpInside];
    
    saveButton.frame = CGRectMake(0, 0, 44, 25);
    
    UIBarButtonItem *saveBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
    
    self.navigationItem.rightBarButtonItem = saveBarButtonItem;
    
    
    NSLog(@"product id = %@, bank id = %@", self.card.productID, self.card.bankID);
    
    bank = [[SQLGlobal sharedInstance] getBank:self.card.bankID];
    
    [self.view addSubview:dateView];
    dateView.frame = CGRectMake(0,
                                self.view.frame.size.height,
                                320,
                                dateView.frame.size.height);
    
    years = [[NSMutableArray alloc] initWithCapacity:50];
    months = [[NSMutableArray alloc] initWithCapacity:12];
    
    for (int i = 0; i < 50; i++) {
        [years addObject:[NSString stringWithFormat:@"%d", 2012+i]];
    }
    
    for (int i = 0; i < 12; i++) {
        [months addObject:[NSString stringWithFormat:@"%d", i + 1]];
    }
    
    contentTextField = nil;
    
    effective = nil;
    count = nil;
    reminderDay = nil;
    
    iv1 = nil;
    iv2 = nil;
    iv3 = nil;
    
    Fee *fee = [[SQLGlobal sharedInstance] getFee:self.card.cardNumber];
    effective = fee.effective;
    count = fee.count;
    reminderDay = fee.reminder;
    [mTableView reloadData];
    
    if (_card.nick) {
        cardNameLabel.text = _card.nick;
    } else {
        cardNameLabel.text = _card.productName;
    }
    
    if (fee.effective.length > 0) {
        NSString *yearString = [fee.effective substringToIndex:4];
        NSString *monthString = [fee.effective substringWithRange:NSMakeRange(5, fee.effective.length - 5 - 1)];
        
        [mPickerView selectRow:yearString.integerValue - 2012 inComponent:0 animated:YES];
        [mPickerView selectRow:monthString.integerValue - 1 inComponent:1 animated:YES];
    }
    
    mSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(220, 8, 79, 21)];
    mSwitch.on = fee.status;
    
    if (!count || count.length == 0) {
        count = @"0";
    }
    
    if (!reminderDay || reminderDay.length == 0) {
        reminderDay = @"0";
    }
    
    [self showNumberImageView];
    
    engine = [[FreeAnnualFeeEngine alloc] initWithHostName:kServerAddr];
    if (!self.card.productID) {
        feePolicy = bank.fee;
    } else {
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:2];
        params[@"code"] = [CMUtil getCode];
        params[@"product_id"] = self.card.productID;
        [self showHudView:@"加载中..."];
        [engine getProductFee:params onCompletion:^(NSString *result) {
            [self hideHudView];
            if (result) {
                feePolicy = result;
            } else {
                feePolicy = bank.fee;
            }
            
            [self calculateFrame];
        } onError:^(NSError *error) {
            feePolicy = bank.fee;
            [self calculateFrame];
            [self hideHudView];
        }];
    }
}

- (void)viewDidUnload
{
    theScrollView = nil;
    cardNameLabel = nil;
    bankIcon = nil;
    mTableView = nil;
    feeLabel = nil;
    discountTitleLabel = nil;
    discountLabel = nil;
    dateView = nil;
    mPickerView = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods
- (void)calculateFrame
{
    feeLabel.text = feePolicy;
    discountLabel.text = bank.discounts;
    
    CGRect feeRect = feeLabel.frame;
    CGSize feeSize = feeRect.size;
    
    feeSize = [feePolicy sizeWithFont:[UIFont systemFontOfSize:13.0]
          constrainedToSize:CGSizeMake(feeSize.width, 2000)
              lineBreakMode:UILineBreakModeWordWrap];
    
    feeRect.size = feeSize;
    
    feeLabel.frame = feeRect;
    
    CGRect discountsRect = discountLabel.frame;
    CGSize discountsSize = discountsRect.size;
    
    discountsSize = [bank.discounts sizeWithFont:[UIFont systemFontOfSize:13.0]
                               constrainedToSize:CGSizeMake(discountsSize.width, 2000)
                                   lineBreakMode:UILineBreakModeWordWrap];
    
    CGRect titleRect = discountTitleLabel.frame;
    titleRect.origin.y = feeLabel.frame.origin.y + feeLabel.frame.size.height + 40;
    discountTitleLabel.frame = titleRect;
    
    discountsRect.origin.y = titleRect.origin.y + 21;
    
    discountLabel.frame = discountsRect;
    
    theScrollView.contentSize = CGSizeMake(320, discountsRect.origin.y + discountsRect.size.height);
}

- (void)showNumberImageView
{
    if (!iv1)
        iv1 = [[UIImageView alloc] init];
//        iv1 = [CMUtil getNumberImageView:[[reminderDay substringToIndex:1] integerValue]];
    
    if (!iv2)
        iv2 = [[UIImageView alloc] init];
//        iv2 = [CMUtil getNumberImageView:[reminderDay substringWithRange:NSMakeRange(1, 1)].integerValue];
    
    if (!iv3)
        iv3 = [[UIImageView alloc] init];
//        iv3 = [CMUtil getNumberImageView:[reminderDay substringWithRange:NSMakeRange(2, 1)].integerValue];
    
    if (!iv1.superview)
        [theScrollView addSubview:iv1];
    
    if (reminderDay.length == 3) {
        
        iv1.frame = CGRectMake(105, 76, 16, 21);
        iv1.image = [CMUtil getNumberImage:[[reminderDay substringToIndex:1] integerValue]];
        
        iv2.frame = CGRectMake(122, 76, 16, 21);
        iv2.image = [CMUtil getNumberImage:[reminderDay substringWithRange:NSMakeRange(1, 1)].integerValue];
        
        if (!iv2.superview)
            [theScrollView addSubview:iv2];

        iv3.frame = CGRectMake(139, 76, 16, 21);
        iv3.image = [CMUtil getNumberImage:[reminderDay substringWithRange:NSMakeRange(2, 1)].integerValue];
        
        if (!iv3.superview)
            [theScrollView addSubview:iv3];
    } else if (reminderDay.length == 2) {
        iv1.frame = CGRectMake(114, 76, 16, 21);
        iv1.image = [CMUtil getNumberImage:[[reminderDay substringToIndex:1] integerValue]];
        
        if (!iv1.superview)
            [theScrollView addSubview:iv1];
        
        iv2.frame = CGRectMake(131, 76, 16, 21);
        iv2.image = [CMUtil getNumberImage:[reminderDay substringWithRange:NSMakeRange(1, 1)].integerValue];
        
        if (!iv2.superview)
            [theScrollView addSubview:iv2];
        
        if (iv3.superview)
            [iv3 removeFromSuperview];
    } else if (reminderDay.length == 1) {
        iv1.image = [CMUtil getNumberImage:[[reminderDay substringToIndex:1] integerValue]];
        iv1.frame = CGRectMake(123, 76, 16, 21);
        
        if (!iv1.superview)
            [theScrollView addSubview:iv1];
        
        if (iv2.superview)
            [iv2 removeFromSuperview];
        
        if (iv3.superview)
            [iv3 removeFromSuperview];
    }
}

#pragma mark - Actions
- (void)showPickerView
{
    [UIView animateWithDuration:0.25 animations:^{
        theScrollView.contentOffset = CGPointMake(0, 120);
        dateView.frame = CGRectMake(0, self.view.frame.size.height - 260, 320, 260);
    }];
}

- (IBAction)hidePickerView:(id)sender
{
    [UIView animateWithDuration:0.25 animations:^{
        if (sender)
            theScrollView.contentOffset = CGPointMake(0, 0);
        dateView.frame = CGRectMake(0, self.view.frame.size.height, 320, 260);
    }];
}

- (IBAction)chooseDate:(id)sender
{
    NSString *year = years[[mPickerView selectedRowInComponent:0]];
    NSString *month = months[[mPickerView selectedRowInComponent:1]];
    
    effective = [NSString stringWithFormat:@"%@年%@月", year, month];
    [mTableView reloadData];
    
    [self hidePickerView:sender];
}

- (void)saveFee
{
    Fee *fee = [[Fee alloc] init];
    fee.cardNumber = self.card.cardNumber;
    fee.effective = effective;
    fee.count = count;
    fee.reminder = reminderDay;
    fee.status = mSwitch.on;
    
    // 设置提醒时间
    if (fee.status) {
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.alertBody = @"免年费提醒测试";
        localNotification.repeatInterval = NSYearCalendarUnit;
        localNotification.userInfo = @{ @"cardNumber" : fee.cardNumber };
        
        // 提前多少天提醒
        NSInteger preDay = 365 - reminderDay.integerValue + 1;
        
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *comps = [gregorian components:NSYearCalendarUnit fromDate:[NSDate date]];
        comps.day = preDay;
        
        // 等待设置时间
        NSDate *date = [gregorian dateFromComponents:comps];
        localNotification.fireDate = date;
//        NSLog(@"date = %@", date);
    // 取消以前设置的notification
    } else {
        // 如果有通知，则删除
        NSArray *notifications = [UIApplication sharedApplication].scheduledLocalNotifications;
        for (UILocalNotification *notification in notifications)
        {
            if ([notification.userInfo[@"cardNumber"] isEqualToString:self.card.cardNumber]) {
                [[UIApplication sharedApplication] cancelLocalNotification:notification];
            }
        }
    }
    
    [[SQLGlobal sharedInstance] updateFee:fee];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"信息"
                                                        message:@"设置成功"
                                                       delegate:self
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles: nil];
    [alertView show];
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"FreeAnnualFee";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if (indexPath.row == 0) {
        cell.textLabel.text = @"信用卡有效期";
        cell.detailTextLabel.text = effective;
        
    } else if (indexPath.row == 1) {
        cell.textLabel.text = @"已消费次数";
        cell.detailTextLabel.text = count;
        
    } else if (indexPath.row == 2) {
        cell.textLabel.text = @"提醒时间";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@天", reminderDay];
        
    } else {
        cell.textLabel.text = @"是否打开免年费提醒";        
        [cell addSubview:mSwitch];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        [self showPickerView];
    } else {
        [self hidePickerView:nil];
        
        if (indexPath.row == 1) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请填写已消费次数"
                                                                message:nil
                                                               delegate:self
                                                      cancelButtonTitle:@"确定"
                                                      otherButtonTitles:@"取消", nil];
            
            alertView.tag = 1000;
            [alertView show];
        } else if (indexPath.row == 2) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请填写提醒时间（0-365）"
                                                                message:nil
                                                               delegate:self
                                                      cancelButtonTitle:@"确定"
                                                      otherButtonTitles:@"取消", nil];
            
            alertView.tag = 1001;
            [alertView show];
        }
    }
}

#pragma mark - UIPickerViewDataSource methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return years.count;
    }

    return 12;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        return years[row];
    }

    return months[row];
}

#pragma mark - UIAlertViewDelegate methods
- (void)willPresentAlertView:(UIAlertView *)alertView
{
    if (alertView.tag == 0)
        return;
    
    CGRect frame = alertView.frame;
    frame.size.height += 40;
    frame.origin.y -= 40;
    alertView.frame = frame;
    
    for (UIView *view in alertView.subviews) {
        if (view.tag == 1) {
            CGRect viewRect = view.frame;
            viewRect.origin.y += 40;
            view.frame = viewRect;
        } else if (view.tag == 2) {
            CGRect viewRect = view.frame;
            viewRect.origin.y += 40;
            view.frame = viewRect;
        }
    }
    
    if (!contentTextField) {
        contentTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, 50, 244, 31)];
        contentTextField.borderStyle = UITextBorderStyleRoundedRect;
        contentTextField.keyboardType = UIKeyboardTypeNumberPad;
        contentTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    }
    
    contentTextField.text = @"";
    
    [alertView addSubview:contentTextField];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        if (alertView.tag == 0) {
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
        
        if (alertView.tag == 1000) {
            count = contentTextField.text;
        } else if (alertView.tag == 1001) {
            reminderDay = contentTextField.text;
            if (reminderDay.integerValue >= 365)
                reminderDay = @"364";
            else if (reminderDay.integerValue <= 0)
                reminderDay = @"0";
            [self showNumberImageView];
        }
        
        [mTableView reloadData];
    }
}

@end
