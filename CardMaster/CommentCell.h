//
//  CommentCell.h
//  LifeShanghai
//
//  Created by wenjun on 12-11-30.
//
//

#import <UIKit/UIKit.h>
#import "Comment.h"

@interface CommentCell : UITableViewCell

+ (CommentCell *)getInstance;
- (void)loadComment:(Comment *)comment;
+ (NSInteger)heightForComment:(Comment *)comment;

@end
