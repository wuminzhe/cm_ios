//
//  FilterCell.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-27.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterCell : UITableViewCell

@property (nonatomic, unsafe_unretained) IBOutlet UILabel *filterLabel;

+ (FilterCell *)getFilterCell;

@end
