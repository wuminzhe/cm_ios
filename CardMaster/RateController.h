//
//  RateController.h
//  CardMaster
//
//  Created by wenjun on 13-1-9.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseNavigationController.h"
#import "Product.h"

@interface RateController : BaseNavigationController

@property (retain,nonatomic) Product * product;

@end
