//
//  BankServiceViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 12-12-26.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BankServiceViewController.h"
#import "SingleBankViewController.h"
#import "UserSettingCell.h"
#import "BankService.h"
#import "BankServiceParser.h"
#import "BankModel.h"

@interface BankServiceViewController ()

@end

@implementation BankServiceViewController {
    BankService *bankService;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        bankService = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"银行服务";
    
    bankService = [BankServiceParser loadBankService];
    
    [mTableView reloadData];
}

- (void)viewDidUnload
{
    mTableView = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return bankService.bankList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserSettingCell"];
    
    if (!cell) {
        cell = [UserSettingCell getInstance];
        cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"setting_bg_select.png"]];
    }
    
    BankModel *bankModel = bankService.bankList[indexPath.row];
    
    cell.imageIcon.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", bankModel.bankID]];
    cell.userInfoLabel.text = bankModel.bankName;
    
    return cell;
}

#pragma mark - UITableViewDelegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SingleBankViewController *singleBankViewController = [[SingleBankViewController alloc] initWithNibName:@"SingleBankViewController" bundle:nil];
    BankModel *bankModel = bankService.bankList[indexPath.row];
    singleBankViewController.bankModel = bankModel;
    [self.navigationController pushViewController:singleBankViewController animated:YES];
}

@end
