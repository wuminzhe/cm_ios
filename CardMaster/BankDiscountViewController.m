//
//  ShopDiscountViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 12-12-24.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BankDiscountViewController.h"
#import "ShopDiscountViewController.h"
#import "JBTabBarController.h"
#import "BankDiscountDetailViewController.h"
#import "UINavigationController+Extension.h"
#import "UserSettingViewController.h"
#import "CMConstants.h"
#import "BankCell.h"
#import "SQLGlobal.h"
#import "Bank.h"
#import "CMUtil.h"
#import "CreditCard.h"
#import "FilterCell.h"
#import "LoadMoreCell.h"
#import "DiscountCell.h"
#import "BankDiscountCell.h"
#import "DiscountEngine.h"
#import "LvToast.h"
#import "Discount.h"
#import "AppDelegate.h"
#import "GEOInfo.h"
#import "FilterTitle.h"

@interface BankDiscountViewController () <FilterTitleDelegate>
{
    FilterTitle * bankFilterTitle;
    FilterTitle * categoryFilterTitle;
}

@end

@implementation BankDiscountViewController
{
    UIView *filterView;
    UIView *bankFilterView;
    UIView *categoryFilterView;
    UIView *sortFilterView;
    
    UITableView *bankTableView;
    UITableView *categoryTableView;
    UITableView *sortTableView;
    
    NSArray *categoryList;                  // 分类
    NSArray *sortList;                      // 排序
    
    NSMutableArray *allBankList;            // 所有银行
    NSMutableArray *myBankList;             // 我的卡片所属银行
    NSMutableArray *myCards;                // 我的卡片
    NSMutableArray *selectedBanks;
    
    NSMutableDictionary *params;            // 查询优惠参数
    NSMutableDictionary *filterParams;      // 筛选参数
    
    int chooseFilter;
    
    BOOL selectAll;                         // 选中全部银行
    
    BOOL isLoadNearBy;
    BOOL isLoadMore;
    BOOL isSearch;
    
    NSInteger currentPage;                  // 当前加载页数
    NSInteger totalPages;                   // 优惠总页数
    
    NSString *filterText;                   // 筛选文本
    
    DiscountEngine *engine;
    
    NSTimer *timer;
    
    BOOL isLoaded;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:USER_CITY_GET_NEW object:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        filterView = nil;
        bankFilterView = nil;
        categoryFilterView = nil;
        sortFilterView = nil;
        bankTableView = nil;
        categoryTableView = nil;
        sortTableView = nil;
        
        categoryList = @[ @"全部", @"餐饮美食", @"旅游酒店", @"休闲娱乐", @"生活服务", @"时尚购物" ];
        sortList = @[ @"全部", @"打开率", @"折扣", @"截止时间" ];
        
        allBankList = nil;
        myBankList = nil;
        myCards = nil;
        selectedBanks = [[NSMutableArray alloc] init];;
        
        params = [[NSMutableDictionary alloc] init];
        filterParams = [[NSMutableDictionary alloc] init];
        
        chooseFilter = 0;
        
        selectAll = YES;
        
        currentPage = 1;
        totalPages = 1;
        
        timer = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabBarItem.image = [UIImage imageNamed:@"tabbar_bank.png"];
    self.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_bank_select.png"];

    categoryLabel.text = categoryList[0];
//    sortLabel.text = sortList[0];
    
    // code: 客户端唯一标识
    // type:1:银行活动 2:商户优惠
    // bank_id:银行id，逗号分隔（需要用户卡添加成功，从卡表中选择卡产品） -可选
    // shop_type: 商户类型（餐饮美食、旅游酒店、休闲娱乐、生活服务、时尚购物），如果是null或者没有此参数，不参与查询 -可选
    // sort: 排序类型, -可选
    // 0/null：默认，1：根据热度高靠前，2：根据折扣率空或低靠前，3：截至时间近的靠前
    // other_cards: 1，-可选，其他卡（除card_number输入的其他卡产品）
    // page: 第几页（每页10条） -可选
    params[@"type"] = @"1";
    params[@"code"] = [CMUtil getCode];
    
    engine = [[DiscountEngine alloc] initWithHostName:kServerAddr];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadCurrentCity) name:USER_CITY_GET_NEW object:nil];
    
    bankFilterTitle = [FilterTitle getInstance];
    bankFilterTitle.text = @"全部银行";
    bankFilterTitle.delegate = self;
    categoryFilterTitle = [FilterTitle getInstance];
    categoryFilterTitle.text = categoryList[0];
    categoryFilterTitle.delegate = self;
    
    bankFilterTitle.frame = CGRectMake(31, 87, bankFilterTitle.frame.size.width, bankFilterTitle.frame.size.height);
    [theScrollView addSubview:bankFilterTitle];
    
    categoryFilterTitle.frame = CGRectMake(219, 87, categoryFilterTitle.frame.size.width, categoryFilterTitle.frame.size.height);
    [theScrollView addSubview:categoryFilterTitle];
    
    bankFilterTitle.tag = kBankFilter;
    categoryFilterTitle.tag = kCategoryFilter;
}

- (void)shouldShowChoices:(FilterTitle *)filterTitle
{
    [self filterDiscounts:filterTitle];
}

- (void)shouldHideChoices:(FilterTitle *)filterTitle
{
    [self selectFilterDiscount:filterTitle];
}

- (void)viewDidUnload
{
    theScrollView = nil;
    mTableView = nil;
    
    bankLabel = nil;
    categoryLabel = nil;
    sortLabel = nil;
    
    bankButton = nil;
    categoryButton = nil;
    sortButton = nil;
    
    settingButton = nil;
    searchButton = nil;
    
    searchView = nil;
    mSearchBar = nil;
    
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self calculateScrollViewContentOffset];
    
    [self loadBankData];
    
    if (!isLoaded) {
        [self loadDiscounts:NO];
    }

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods
- (void)createFilterView
{
    if (filterView == nil) {
        filterView = [[UIView alloc] initWithFrame:CGRectMake(0, 119, 320, 343)];
        filterView.backgroundColor = [UIColor clearColor];
        filterView.clipsToBounds = YES;
        
        UIView *contentView = [[UIView alloc] initWithFrame:filterView.bounds];
        contentView.backgroundColor = [UIColor blackColor];
        contentView.alpha = 0.6;
        
        [filterView addSubview:contentView];
        
    }
}

- (void)createBankFilterView
{
    if (bankFilterView == nil) {
        bankFilterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 262)];
        UIImageView *bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"filter_bg_small_0.png"]];
        bgView.frame = CGRectMake(14, 0, 181, 256);
        [bankFilterView addSubview:bgView];
    }
    
    if (bankTableView == nil) {
        bankTableView = [[UITableView alloc] initWithFrame:CGRectMake(15, 0, 180, 256)];
        bankTableView.backgroundColor = [UIColor clearColor];
        bankTableView.showsHorizontalScrollIndicator = NO;
        bankTableView.showsVerticalScrollIndicator = NO;
        bankTableView.dataSource = self;
        bankTableView.delegate = self;
        [bankFilterView addSubview:bankTableView];
        [bankTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
}

- (void)createCategoryFilterView
{
    if (categoryFilterView == nil) {
        categoryFilterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 262)];
        UIImageView *bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"filter_bg_small_2.png"]];
        bgView.frame = CGRectMake(210, -2, 97, 211);
        [categoryFilterView addSubview:bgView];
    }
    
    if (categoryTableView == nil) {
        categoryTableView = [[UITableView alloc] initWithFrame:CGRectMake(210, 0, 97, 204)];
        categoryTableView.backgroundColor = [UIColor clearColor];
        categoryTableView.showsHorizontalScrollIndicator = NO;
        categoryTableView.showsVerticalScrollIndicator = NO;
        categoryTableView.dataSource = self;
        categoryTableView.delegate = self;
        [categoryFilterView addSubview:categoryTableView];
        [categoryTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
}

- (void)createSortFilterView
{
    if (sortFilterView == nil) {
        sortFilterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 262)];
        UIImageView *bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"filter_bg_small_1.png"]];
        bgView.frame = CGRectMake(210, 0, 97, 175);
        [sortFilterView addSubview:bgView];
    }
    
    if (sortTableView == nil) {
        sortTableView = [[UITableView alloc] initWithFrame:CGRectMake(210, 0, 97, 170)];
        
        sortTableView.backgroundColor = [UIColor clearColor];
        sortTableView.showsHorizontalScrollIndicator = NO;
        sortTableView.showsVerticalScrollIndicator = NO;
        sortTableView.dataSource = self;
        sortTableView.delegate = self;
        sortTableView.scrollEnabled = NO;
        [sortFilterView addSubview:sortTableView];
        
        [sortTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
}

- (void)showFilterView:(kChooseFilter)tag
{
    [self createFilterView];
    
    if (filterView.superview == nil)
    {
        [theScrollView insertSubview:filterView aboveSubview:mTableView];
    }
    
    UIView *view = nil;
    UIButton *button = nil;
    UILabel *label = nil;
    
    if (tag == kBankFilter) {
        button = bankButton,   label = bankLabel,  view = bankFilterView;
    } else if (tag == kCategoryFilter) {
        button = categoryButton,   label = categoryLabel,  view = categoryFilterView;
    } else {
        button = sortButton,   label = sortLabel,  view = sortFilterView;
    }
    
    [filterView addSubview:view];
    CGRect viewRect = view.frame;
    view.frame = CGRectMake(viewRect.origin.x,
                            -viewRect.size.height,
                            viewRect.size.width,
                            viewRect.size.height);
    
    [UIView animateWithDuration:kFilterAnimtaionDuration animations:^{
        
//        label.hidden = YES;
//        
//        [button setImage:[UIImage imageNamed:@"filter_btn_ok.png"]
//                forState:UIControlStateNormal];
//        
//        [button removeTarget:self
//                      action:@selector(filterDiscounts:)
//            forControlEvents:UIControlEventTouchUpInside];
//        
//        [button addTarget:self
//                   action:@selector(selectFilterDiscount:)
//         forControlEvents:UIControlEventTouchUpInside];
        
        CGRect viewRect = view.frame;
        view.frame = CGRectMake(viewRect.origin.x,
                                        0,
                                        viewRect.size.width,
                                        viewRect.size.height);
    }];
}

- (void)hideFilterView:(kChooseFilter)tag
{
    UIButton *button = nil;
    UILabel *label = nil;
    UIView *view = nil;
    
    if (tag == kBankFilter) {
        button = bankButton,   label = bankLabel,  view = bankFilterView;
        [bankFilterTitle hide];
    } else if (tag == kCategoryFilter) {
        button = categoryButton,   label = categoryLabel,  view = categoryFilterView;
        [categoryFilterTitle hide];
    }
    
    [UIView animateWithDuration:kFilterAnimtaionDuration animations:^{
//        label.hidden = NO;
//        
//        [button setImage:[UIImage imageNamed:@"filter_btn.png"]
//                forState:UIControlStateNormal];
//        
//        [button removeTarget:self
//                      action:@selector(selectFilterDiscount:)
//            forControlEvents:UIControlEventTouchUpInside];
//        
//        [button addTarget:self
//                   action:@selector(filterDiscounts:)
//         forControlEvents:UIControlEventTouchUpInside];
        
        CGRect viewRect = view.frame;
        view.frame = CGRectMake(viewRect.origin.x,
                                -viewRect.size.height,
                                viewRect.size.width,
                                viewRect.size.height);
        
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
        
        if (chooseFilter == 0)
            [filterView removeFromSuperview];
    }];
}

// 点击确认按钮
- (void)selectFilterDiscount:(id)sender
{
    int tag = [sender tag];
    chooseFilter = 0;
    [self hideFilterView:tag];
    
    if (tag == kBankFilter) {
        if (!selectAll) {
            NSString *bankIDString = @"";
            
            for (int i = 0; i < selectedBanks.count; i++) {
                Bank *bank = selectedBanks[i];
                
                if (i < selectedBanks.count - 1)
                    bankIDString = [bankIDString stringByAppendingFormat:@"'%@',", bank.bankID];
                else
                    bankIDString = [bankIDString stringByAppendingFormat:@"'%@'", bank.bankID];
            }
            
            if (selectedBanks.count != 0)
                params[@"bank_id"] = bankIDString;
            else
                [params removeObjectForKey:@"bank_id"];
        } else
            [params removeObjectForKey:@"bank_id"];
        
        
        currentPage = 1;
        params[@"page"] = [NSString stringWithFormat:@"%d", currentPage];
        
        [_discountList removeAllObjects];
        [self loadDiscounts:isLoadNearBy];
    }
}

- (void)loadBankData
{
    // 加载所有银行
    allBankList = [[SQLGlobal sharedInstance] getBanks];
    // 加载我的卡片
    myCards = [[SQLGlobal sharedInstance] getLocalCards];
    
    if (myBankList == nil)
        myBankList = [[NSMutableArray alloc] init];
    
    [myBankList removeAllObjects];
    
    for (CreditCard *card in myCards) {
        for (int i = 0; i < allBankList.count; i++) {
            Bank *bank = allBankList[i];
            
            if ([card.bankID isEqualToString:bank.bankID]) {
                [myBankList addObject:bank];
                [allBankList removeObject:bank];
                break;
            }
        }
    }
}

- (void)loadDiscounts:(BOOL)nearBy
{
    isLoaded = YES;
    
    isLoadNearBy = nearBy;
    isSearch = NO;
    
    self.discountList = nil;
    
    [mTableView reloadData];
    
    [self showHudView:@"正在加载..."];
    [engine getDiscountList:params nearbyFlag:nearBy onCompletion:^(NSMutableDictionary *results) {
    
        self.discountList = results[@"list"];
        
        if (self.discountList.count == 0) {
            [self showNoResultOnView:mTableView];
        } else {
            [self hideNoResult];
        }
        
        totalPages = [results[@"pages"] integerValue];
        
        NSLog(@"pages = %d", totalPages);
        
        [mTableView reloadData];
        
        [self hideHudView];
    } onError:^(NSError *error) {
        [mTableView reloadData];
        [self hideHudView];
        if (self.discountList.count == 0) {
            [self showNoResultOnView:mTableView];
        } else {
            [self hideNoResult];
        }
        
        [LvToast showWithText:@"加载失败" duration:1.0];
    }];
}

- (void)loadMoreDiscounts
{
    currentPage++;
    
    if (currentPage > totalPages)
        return;
    
    if (isSearch) {
        return;
    }
    
    params[@"page"] = [NSString stringWithFormat:@"%d", currentPage];
    
    [engine getDiscountList:params nearbyFlag:isLoadNearBy onCompletion:^(NSMutableDictionary *results) {
        
        [self.discountList addObjectsFromArray:results[@"list"]];
        
        totalPages = [results[@"pages"] integerValue];
        
        [mTableView reloadData];
        
        isLoadMore = NO;
    } onError:^(NSError *error) {
        isLoadMore = NO;
        
        currentPage -= 1;
    }];
}

- (void)loadFilterDiscount:(BOOL)loadMore
{
    filterParams[@"code"] = [CMUtil getCode];
    filterParams[@"key"] = filterText;
    filterParams[@"page"] = [NSString stringWithFormat:@"%d", currentPage];
    
    [engine searchDiscountList:filterParams onCompletion:^(NSMutableDictionary *results) {
        if (!_discountList)
            _discountList = results[@"list"];
        else
            [_discountList addObjectsFromArray:results[@"list"]];
        
        if (self.discountList.count == 0) {
            [self showNoResultOnView:mTableView];
        } else {
            [self hideNoResult];
        }
        
        totalPages = [results[@"pages"] integerValue];
        
        [mTableView reloadData];
        
        if (loadMore)
            isLoadMore = NO;
        
        if (searchView.superview)
            [searchView removeFromSuperview];
    } onError:^(NSError *error) {
        if (self.discountList.count == 0) {
            [self showNoResultOnView:mTableView];
        } else {
            [self hideNoResult];
        }
    }];
}

- (void)calculateScrollViewContentOffset
{
    NSLog(@"%f",mTableView.contentOffset.y);
    CGPoint contentOffset = mTableView.contentOffset;
    
    if (contentOffset.y <= 0) {
        theScrollView.contentOffset = CGPointMake(0, 0);
        settingButton.frame = CGRectMake(17, 10, 32, 32);
        searchButton.frame = CGRectMake(271, 10, 32, 32);
    } else if (contentOffset.y <= 63 * 2) {
        
        theScrollView.contentOffset = CGPointMake(mTableView.contentOffset.x / 2.0,
                                                  mTableView.contentOffset.y / 2.0);
        
        CGRect settingButtonFrame = settingButton.frame;
        
        settingButtonFrame.origin.x = 17 - ((int)mTableView.contentOffset.y * 18) / 126;
        
        settingButton.frame = settingButtonFrame;
        
        CGRect searchButtonFrame= searchButton.frame;
        
        searchButtonFrame.origin.x = 271 + ((int)mTableView.contentOffset.y * 18) / 126;
        
        searchButton.frame = searchButtonFrame;
    } else {
        theScrollView.contentOffset = CGPointMake(0, 63);
    }

    NSLog(@"%@",NSStringFromCGRect(settingButton.frame));
}

- (void)loadCurrentCity
{
    if ([sortLabel.text isEqualToString:@"全部城市"]) {
        sortLabel.text = [CMUtil getAppDelegate].geoInfo.city;
        
        if (sortLabel.text.length != 0)
            params[@"city_name"] = sortLabel.text;
    }
}

#pragma mark - Actions
- (IBAction)filterDiscounts:(id)sender
{
    int tag = [sender tag];
    
    if (tag == kBankFilter) {
        [self createBankFilterView];
    } else if (tag == kCategoryFilter) {
        [self createCategoryFilterView];
    }
    
    // 首先判定是否已经显示筛选页面, 0的时候不显示
    if (chooseFilter > 0)
        [self hideFilterView:chooseFilter];     // 隐藏之前的筛选页面
    
    // 显示要得筛选页面
    chooseFilter = tag;
    
    [self showFilterView:tag];
}

- (IBAction)searchDiscounts:(id)sender
{
    [UIView animateWithDuration:kFilterAnimtaionDuration animations:^{
        [self.view addSubview:searchView];
        [mSearchBar becomeFirstResponder];
    }];
}

- (IBAction)selectCity:(id)sender
{
    int tag = chooseFilter;
    
    if (tag > 0) {
        chooseFilter = 0;
        
        [self hideFilterView:tag];
    }
    
    CityViewController *cityViewController = [[CityViewController alloc] initWithNibName:@"CityViewController" bundle:nil];
    cityViewController.delegate = self;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:cityViewController];
    
    [navigationController setNavigationBarBackgroundImage:[UIImage imageNamed:@"nav.png"]];
    
    navigationController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    [[CMUtil getAppDelegate].tabBarController presentModalViewController:navigationController animated:YES];
}

- (IBAction)gotoUserSettings:(id)sender
{
    UserSettingViewController *userSettingViewController = [[UserSettingViewController alloc] initWithNibName:@"UserSettingViewController" bundle:nil];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:userSettingViewController];
    
    [navigationController setNavigationBarBackgroundImage:[UIImage imageNamed:@"nav.png"]];
    
    [[CMUtil getAppDelegate].tabBarController presentModalViewController:navigationController animated:YES];
    
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == bankTableView) {
        if (myBankList.count == 0)
            return 2;
        
        return 3;
    }

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == mTableView) {
        if (_discountList.count <= 4) {
            tableView.scrollEnabled = NO;
            return 0;
        }
        
        tableView.scrollEnabled = YES;
        if (currentPage == totalPages)
            return _discountList.count;
        
        return _discountList.count + 1;
    } else if (tableView == bankTableView) {
        if (section == 0)   return 1;
        
        if (myBankList.count == 0) {
            if (section == 1)   return allBankList.count;
        } else {
            if (section == 1)   return myBankList.count;
            else    return allBankList.count;
        }
        
    } else if (tableView == categoryTableView) {
        return categoryList.count;
    } 
    
    return sortList.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == bankTableView) {        
        if (section == 0)
            return nil;
        
        if (myBankList.count == 0) {
            if (section == 1)
                return @"其他银行";
        } else {
            if (section == 1)
                return @"我的银行";
            else
                return @"其他银行";
        }
    }
    
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == mTableView) {
        
        if (indexPath.row == _discountList.count) {
            NSLog(@"111");
            
            LoadMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LoadMoreCell"];
            
            if (!cell)
                cell = [LoadMoreCell getInstance];
            
            return cell;
        }
        
        BankDiscountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BankDiscountCell"];
        
        if (!cell) {
            cell = [BankDiscountCell getInstance];
            cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bank_discount_bg"]];
            cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bank_discount_selected_bg.png"]];
        }
        
        Discount *discount = _discountList[indexPath.row];
        
        NSString *urlString = [discount.pic stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSLog(@"pic = %@", urlString);
        cell.bankImageView.imageURL = [NSURL URLWithString:urlString];
        
        return cell;
    } else if (tableView == bankTableView) {
        BankCell *bankCell = (indexPath.section == 0 && indexPath.row == 0) ? [BankCell getAllBankCell] : [BankCell getBankCell];
        
        if (indexPath.section == 0 && indexPath.row == 0) {
            bankCell.selectImageView.hidden = !selectAll;
            
            return bankCell;
        }
        
        Bank *bank = nil;
        
        // 无我的卡片信息
        if (myBankList.count == 0) {
            if (indexPath.section == 1)
                bank = allBankList[indexPath.row];
        } else {
            if (indexPath.section == 1)
                bank = myBankList[indexPath.row];
            else 
                bank = allBankList[indexPath.row];
        }
        
        bankCell.bankImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"bank_%@.png", bank.bankID]];//[CMUtil getBankLogo:bank.bankID];
        bankCell.bankNameLabel.text = bank.name;
        bankCell.selectImageView.hidden = !bank.check;
        
        return bankCell;
    } else {
        FilterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FilterCell"];
        
        if (!cell) {
            cell = [FilterCell getFilterCell];
        }
        
        cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"filter_cell_select.png"]];
        
        NSString *content = nil;
        
        if (tableView == categoryTableView) {
            content = categoryList[indexPath.row];
        } else {
            content = sortList[indexPath.row];
        }
        
        cell.filterLabel.text = content;
        
        return cell;
    }
}

#pragma mark - UITableViewDelegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == mTableView) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        BankDiscountDetailViewController *bankDiscountDetailViewController = [[BankDiscountDetailViewController alloc] initWithNibName:@"BankDiscountDetailViewController" bundle:nil];
        bankDiscountDetailViewController.discount = _discountList[indexPath.row];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:bankDiscountDetailViewController];
        
        [navigationController setNavigationBarBackgroundImage:[UIImage imageNamed:@"nav.png"]];
        
        navigationController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        
        [[CMUtil getAppDelegate].tabBarController presentModalViewController:navigationController animated:YES];
        
    } else if (tableView == bankTableView) {
        if (indexPath.section  == 0) {
            selectAll = !selectAll;
            
            [selectedBanks removeAllObjects];
            
            for (Bank *bank in myBankList) {
                bank.check = NO;
            }
            
            for (Bank *bank in allBankList) {
                bank.check = NO;
            }
            
            bankLabel.text = @"全部银行";
            bankFilterTitle.text = @"全部银行";
        } else {
            selectAll = NO;
            Bank *bank;
            if (myBankList.count == 0) {
                bank = allBankList[indexPath.row];
            } else {
                if (indexPath.section == 1) {
                    bank = myBankList[indexPath.row];
                } else
                    bank = allBankList[indexPath.row];
            }
            
            bank.check = !bank.check;
            
            if (bank.check) {
                [selectedBanks addObject:bank];
            } else {
                if ([selectedBanks containsObject:bank])
                    [selectedBanks removeObject:bank];
            }
            
            if (selectedBanks.count > 0)
            {
                bankLabel.text = ((Bank *)selectedBanks[0]).name;
                bankFilterTitle.text = ((Bank *)selectedBanks[0]).name;
            }
            else {
                bankLabel.text = @"全部银行";
                bankFilterTitle.text = @"全部银行";
                selectAll = YES;
            }
        }
        
        [bankTableView reloadData];
    } else if (tableView == categoryTableView) {
        chooseFilter = 0;
        [self hideFilterView:kCategoryFilter];
        categoryLabel.text = categoryList[indexPath.row];
        categoryFilterTitle.text = categoryList[indexPath.row];
        
        if (indexPath.row == 0) {
            [params removeObjectForKey:@"shop_type"];
        } else {
            params[@"shop_type"] = [NSString stringWithFormat:@"%d", indexPath.row];
        }
        
        [self loadDiscounts:NO];
        
    }
//    else {
//        chooseFilter = 0;
//        [self hideFilterView:kSortFilter];
//        sortLabel.text = sortList[indexPath.row];
//        
////        params[@"sort"] = [NSString stringWithFormat:@"%d", indexPath.row];
//        
//        [self loadDiscounts:NO];
//    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == mTableView)  {
        if (indexPath.row == _discountList.count)
            return 44.0f;
        
        return 135.0f;
    } else if (tableView == bankTableView)
        return 36.0f;
    else if (tableView == categoryTableView)
        return 34.0f;
    else
        return 34.0f;
}

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView != mTableView)
        return;
    NSLog(@"%f",scrollView.contentOffset.y);
    CGPoint contentOffset = scrollView.contentOffset;
    
    if (contentOffset.y <= 0) {
        scrollView.contentOffset = CGPointMake(0, 0);
        theScrollView.contentOffset = mTableView.contentOffset;
        settingButton.frame = CGRectMake(17, 10, 32, 32);
        searchButton.frame = CGRectMake(271, 10, 32, 32);
    } else if (contentOffset.y <= 63 * 2) {
        
        [UIView beginAnimations:NULL context:NULL];
        
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDuration:0.2];
        [UIView setAnimationRepeatAutoreverses:NO];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        theScrollView.contentOffset = CGPointMake(mTableView.contentOffset.x / 2.0,
                                                  mTableView.contentOffset.y / 2.0);
        
        CGRect settingButtonFrame = settingButton.frame;
        
        settingButtonFrame.origin.x = 17 - ((int)mTableView.contentOffset.y * 18) / 126;
        
        settingButton.frame = settingButtonFrame;
        
        CGRect searchButtonFrame= searchButton.frame;
        
        searchButtonFrame.origin.x = 271 + ((int)mTableView.contentOffset.y * 18) / 126;
        
        searchButton.frame = searchButtonFrame;
        
        [UIView commitAnimations];
        
    } else {
        theScrollView.contentOffset = CGPointMake(0, 63);
    }

    // 如果当前页是总页数，则没有加载更多
    if (currentPage == totalPages)
        return;
    
    // 加载更多
    if (contentOffset.y >= mTableView.contentSize.height - 44.0 - mTableView.frame.size.height) {
        if (!isLoadMore) {
            isLoadMore = !isLoadMore;
            [self loadMoreDiscounts];
        }
    }
    NSLog(@"%@",NSStringFromCGRect(settingButton.frame));
}

#pragma mark - UISearchBarDelegate methods
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
    if (searchView.superview)
        [searchView removeFromSuperview];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    isSearch = YES;
    currentPage = 1;
    
    filterText = searchBar.text;
    
    _discountList = nil;
    [self loadFilterDiscount:NO];
}

#pragma mark - CitySelectDelegate methods
- (void)didSelectCity:(NSString *)cityName
{
    sortLabel.text = cityName;
    params[@"city_name"] = cityName;
    
    [self loadDiscounts:NO];
}
@end
