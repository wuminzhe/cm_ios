//
//  LoanListController.m
//  CardMaster
//
//  Created by wenjun on 13-1-10.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "LoanListController.h"
#import "RefreshList.h"
#import "LoadingView.h"
#import "FilterView.h"
#import "LoanProduct.h"
#import "LoanCell.h"
#import "RefreshCell.h"
#import "LoanController.h"
#import "LvToast.h"
#import "GTMUtil.h"
#import "CMUtil.h"
#import "JSONKit.h"
#import "NSDictionary+Extension.h"

@interface LoanListController () <RefreshListDelegate,FilterViewDelegate>
{
    IBOutlet RefreshList * loanList;
    IBOutlet LoadingView * loadingView;
    FilterView * filterView;
}

@property (strong,nonatomic) NSMutableArray * loans;
@property (strong,nonatomic) NSArray * choices;
@property (unsafe_unretained,nonatomic) GTMHTTPFetcher * fetcher;

@end

@implementation LoanListController
{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"申请贷款";
    loanList.delegate = self;
    loanList.topRefreshEnabled = NO;
    loanList.bottomRefreshEnabled = YES;
    
    [loadingView setText:@"正在加载"];
    
    self.choices = @[@"全部贷款",@"个人贷款",@"车贷",@"信用贷款"];
    filterView = [FilterView getInstance];
    filterView.delegate = self;
    [self.view addSubview:filterView];
    [filterView reload];
    filterView.seletedIndex = 0;
    
    self.loans = [NSMutableArray array];
}


#pragma RefreshList
- (NSInteger)refreshList:(RefreshList *)refreshList heightForRowAtIndex:(NSInteger)index
{
    return 98;
}

- (UITableViewCell *)refreshList:(RefreshList *)refreshList cellForRowAtIndex:(NSInteger)index
{
    LoanProduct * loan = [self.loans objectAtIndex:index];
    LoanCell * cell = (LoanCell *)[refreshList dequeueReusableCellWithIdentifier:@"LoanCell"];
    if (!cell)
    {
        cell = [LoanCell getInstance];
    }
    [cell loadLoan:loan];
//    if (!loan.image && !loan.isLoadingImg)
//    {
//        [self downloadImgForLoan:loan];
//    }
    return cell;
}

- (UITableViewCell *)refreshCellForRefreshList:(RefreshList *)refreshList
{
    RefreshCell * cell = (RefreshCell *)[refreshList dequeueReusableCellWithIdentifier:@"RefreshCell"];
    if (!cell)
    {
        cell = [RefreshCell getInstance];
    }
    [cell animate];
    return cell;
}

- (void)refreshList:(RefreshList *)refreshList didSelectRowAtIndex:(NSInteger)index
{
    LoanController * loanController = [[LoanController alloc] init];
    loanController.loan = [self.loans objectAtIndex:index];
    [self.navigationController pushViewController:loanController animated:YES];
}

- (void)bottomRefreshInRefreshList:(RefreshList *)refreshList
{
    [self requestLoansAtPage:refreshList.page + 1 type:filterView.seletedIndex];
}

- (NSInteger)numberOfRowsInRefreshList:(RefreshList *)refreshList
{
    return self.loans.count;
}

- (void)topDragTriggered
{
    if (loanList.page == 0 && !loanList.bottomOver)
    {
        [self refresh];
    }
}

- (void)requestFirstPage
{
    [self requestLoansAtPage:1 type:filterView.seletedIndex];
}

- (void)requestLoansOverWithPage:(NSInteger)page loans:(NSArray *)newLoans success:(BOOL)success
{
    if (page == 1)
    {
        if (success)
        {
            [self.loans addObjectsFromArray:newLoans];
            [loanList topOverWithNumber:newLoans.count finished:YES];
            if (self.loans.count == 0)
            {
//                [LvToast showWithText:@"暂无数据" duration:1];
                [self showNoResultOnView:loanList.innerTable];
            }
            else
            {
                [self hideNoResult];
            }
        }
        else
        {
            [loanList topOverWithNumber:0 finished:NO];
            [LvToast showWithText:@"请求失败" duration:1];
        }
    }
    else
    {
        if (success)
        {
            [self.loans addObjectsFromArray:newLoans];
            [loanList bottomOverWithNumber:newLoans.count finished:YES];
        }
        else
        {
            
            [loanList bottomOverWithNumber:0 finished:NO];
        }
    }
    [loadingView stopWithAnimation];
    [loanList setHidden:NO];
}

//请求贷款产品列表
- (void)requestLoansAtPage:(NSInteger)page type:(NSInteger)type
{
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithCapacity:3];
    params[@"code"] = [CMUtil getCode];
    if (filterView.seletedIndex > 0)
    {
        params[@"loan_category_id"] = [NSString stringWithFormat:@"%d",type];
    }
    params[@"page"] = [NSString stringWithFormat:@"%d",page];
    
    self.fetcher = [GTMUtil fetcherWithURLString:[NSString stringWithFormat:@"http://%@/api/loan_products",kServerAddr] params:params HTTPType:@"get" urlEncode:NO];
    [_fetcher setProperty:[NSString stringWithFormat:@"%d",page] forKey:@"page"];
    [self.fetcher beginFetchWithDelegate:self didFinishSelector:@selector(loanFetcher:data:error:)];
}

- (void)loanFetcher:(GTMHTTPFetcher *)fetcher data:(NSData *)data error:(NSError *)error
{
    BOOL success = NO;
    NSMutableArray * newLoans = nil;
    if (fetcher.statusCode == 200)
    {
        NSArray * jsonArray = [[[data objectFromJSONData] objectForKey:@"data"] objectForKey:@"loan_products"];
        newLoans = [NSMutableArray arrayWithCapacity:jsonArray.count];
        for (NSDictionary * dict in jsonArray)
        {
            LoanProduct * loan = [[LoanProduct alloc] init];
            loan.pk = [dict stringForKey:@"id"];
            loan.name = [dict stringForKey:@"name"];
            loan.imageName = [dict stringForKey:@"pic"];
            loan.amount = [dict stringForKey:@"max_amount"];
            loan.rate = [dict stringForKey:@"rate"];
            loan.suitableUsers = [dict stringForKey:@"apply_to"];
            loan.applyDuration = [dict stringForKey:@"apply_time"];
            loan.condition = [dict stringForKey:@"requirement"];
            loan.deadLine = [dict stringForKey:@"end_time"];
            loan.applyCount = [dict stringForKey:@"apply_people"];
            loan.suggestURL = [dict stringForKey:@"suggest_url"];
            [newLoans addObject:loan];
        }
        success = YES;
    }
    [self requestLoansOverWithPage:[[fetcher propertyForKey:@"page"] intValue] loans:newLoans success:success];
    self.fetcher = nil;
}

- (void)cancelAllRequests
{
    [self.fetcher stopFetching];
    self.fetcher = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//FilterView
- (void)filterView:(FilterView *)filterView didSelectRow:(NSInteger)row choice:(NSString *)choice
{
    [loadingView start];
    [self.loans removeAllObjects];
    [loanList clear];
    [loanList setHidden:YES];
    [self cancelAllRequests];
    [self performSelector:@selector(requestFirstPage) withObject:nil afterDelay:.5];
}

- (NSArray *)choicesInFilterView:(FilterView *)filterView
{
    return self.choices;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (loanList.page == 0 && !loanList.bottomOver)
    {
        [loadingView start];
        [loanList setHidden:YES];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (loanList.page == 0 && !loanList.bottomOver)
    {
        [self performSelector:@selector(requestFirstPage) withObject:nil afterDelay:.5];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)backAction
{
    [self cancelAllRequests];
    [loanList clear];
    self.loans = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)refresh
{
    [self cancelAllRequests];
    [loadingView start];
    [loanList setHidden:YES];
    [loanList clear];
    [self performSelector:@selector(requestFirstPage) withObject:nil afterDelay:.5];
}

@end
