//
//  CheckBox.m
//  LifeShanghai
//
//  Created by wenjun on 12-12-2.
//
//

#import "CheckBox.h"

@interface CheckBox()
{
    SEL _selector;
}

@property (strong,nonatomic) UIButton * btn;
@property (unsafe_unretained,nonatomic) id target;

@end

@implementation CheckBox

- (void)initialize
{
    self.backgroundColor = [UIColor clearColor];
    self.btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [self addSubview:self.btn];
    [self.btn addTarget:self action:@selector(clicked) forControlEvents:UIControlEventTouchUpInside];
    _checkedImage = [UIImage imageNamed:@"checked"];
    _unCheckedImage = [UIImage imageNamed:@"unchecked"];
    self.canDeselect = YES;
    [self updateLook];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initialize];
    }
    return self;
}

- (void)addTarget:(id)target valueChangedSelector:(SEL)selector
{
    self.target = target;
    _selector = selector;
}

- (void)clicked
{
    if (self.checked && !self.canDeselect)
    {
        return;
    }
    self.checked = !self.checked;
    [self.target performSelector:_selector withObject:self];
}

- (void)loadUnCheckedImage:(UIImage *)unCheckedImage checkedImage:(UIImage *)checkedImage
{
    self.checkedImage = checkedImage;
    self.unCheckedImage = unCheckedImage;
}

- (void)setUnCheckedImage:(UIImage *)unCheckedImage
{
    _unCheckedImage = unCheckedImage;
    if (!self.checked)
    {
        [self updateLook];
    }
}

- (void)setEnabled:(BOOL)enabled
{
    [self.btn setEnabled:enabled];
}

- (BOOL)isEnabled
{
    return self.btn.isEnabled;
}

- (void)setCheckedImage:(UIImage *)checkedImage
{
    _checkedImage = checkedImage;
    if (self.checked)
    {
        [self updateLook];
    }
}

- (void)setChecked:(BOOL)checked
{
    if (_checked != checked)
    {
        _checked = checked;
        [self updateLook];
    }
}

- (void)updateLook
{
    UIImage * image = self.checked ? self.checkedImage : self.unCheckedImage;
    [self.btn setImage:image forState:UIControlStateNormal];
    [self.btn setImage:image forState:UIControlStateHighlighted];
}

@end
