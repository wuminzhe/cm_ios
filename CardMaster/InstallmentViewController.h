//
//  InstallmentViewController.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-25.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"

@class InstallmentEngine;

@interface InstallmentViewController : BaseNavigationController<UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate> {
    InstallmentEngine *engine;
    
    __unsafe_unretained IBOutlet UIScrollView *theScrollView;
    
    __unsafe_unretained IBOutlet UIButton *helpButton;
    
    __unsafe_unretained IBOutlet UITextField *accountTextfield;
    __unsafe_unretained IBOutlet UIView *helpView;
    __unsafe_unretained IBOutlet UILabel *minAmountLabel;
    
    __unsafe_unretained IBOutlet UILabel *bankNameLabel;
    __unsafe_unretained IBOutlet UILabel *stageLabel;
    
    __unsafe_unretained IBOutlet UIView *selectedView;
    __unsafe_unretained IBOutlet UIPickerView *pickerView;
    
    BOOL selected;                      // DEFAULT NO
    
    NSInteger choose;
    
    NSMutableArray *pickerData;
}

@property (nonatomic, strong) NSString *bankId;         // 银行Id

- (IBAction)backgroundTap:(id)sender;
- (IBAction)showInfo:(id)sender;
- (IBAction)showPickerView:(id)sender;
- (IBAction)hidePickerView;
- (IBAction)ok:(id)sender;
- (IBAction)calculate:(id)sender;

@end
