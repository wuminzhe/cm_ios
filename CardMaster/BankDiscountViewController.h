//
//  ShopDiscountViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-24.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CityViewController.h"

@interface BankDiscountViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, CitySelectDelegate> {
    __unsafe_unretained IBOutlet UIScrollView *theScrollView;
    __unsafe_unretained IBOutlet UITableView *mTableView;
    
    __unsafe_unretained IBOutlet UILabel *bankLabel;
    __unsafe_unretained IBOutlet UILabel *categoryLabel;
    __unsafe_unretained IBOutlet UILabel *sortLabel;
    
    __unsafe_unretained IBOutlet UIButton *bankButton;
    __unsafe_unretained IBOutlet UIButton *categoryButton;
    __unsafe_unretained IBOutlet UIButton *sortButton;
    
    __unsafe_unretained IBOutlet UIButton *settingButton;
    __unsafe_unretained IBOutlet UIButton *searchButton;
    
    IBOutlet UIView *searchView;
    
    IBOutlet UISearchBar *mSearchBar;
}

@property (nonatomic, strong) NSMutableArray *discountList;

- (IBAction)filterDiscounts:(id)sender;

// 搜索优惠信息
- (IBAction)searchDiscounts:(id)sender;
- (IBAction)gotoUserSettings:(id)sender;

- (IBAction)selectCity:(id)sender;

@end
