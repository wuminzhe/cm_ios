//
//  FilterTitle.h
//  CardMaster
//
//  Created by wenjun on 13-1-23.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FilterTitle;
@protocol FilterTitleDelegate

- (void)shouldShowChoices:(FilterTitle *)filterTitle;
- (void)shouldHideChoices:(FilterTitle *)filterTitle;

@end

@interface FilterTitle : UIView

@property (unsafe_unretained,nonatomic) id<FilterTitleDelegate> delegate;

@property (nonatomic) NSString * text;

+ (FilterTitle *)getInstance;

- (void)show;
- (void)hide;

@end
