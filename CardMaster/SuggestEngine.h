//
//  SuggestEngine.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-27.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MKNetworkEngine.h"

@interface SuggestEngine : MKNetworkEngine

typedef void (^SendMessageResponseBlock) (BOOL status);
typedef void (^GetMessagesResponseBlock) (NSMutableArray *result);

- (MKNetworkOperation *)sendMessage:(NSMutableDictionary *)params
                       onCompletion:(SendMessageResponseBlock)completionBlock
                            onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)getMessages:(NSMutableDictionary *)params
                        onCompletion:(GetMessagesResponseBlock)completionBlock
                                onError:(MKNKErrorBlock)errorBlock;

@end
