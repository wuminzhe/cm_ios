//
//  PointView.m
//  NearbyShops
//
//  Created by wenjun on 12-12-26.
//
//

#import "PointView.h"

@interface PointView()
{
    CGSize imgSize;
    CGSize pointSize;
    NSInteger count;
}

@property (strong,nonatomic) UIImage * darkImage;
@property (strong,nonatomic) UIImage * lightImage;
@property (strong,nonatomic) UIView * lightView;
@property (strong,nonatomic) UITapGestureRecognizer * tapGesture;

@end

@implementation PointView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
    }
    return self;
}

- (id)initWithDarkImage:(UIImage *)darkImage lightImage:(UIImage *)lightImage imageSize:(CGSize)imageSize space:(NSInteger)space pointCount:(NSInteger)pointCount
{
    self = [super initWithFrame:CGRectMake(0, 0, (imageSize.width + space) * pointCount, imageSize.height)];
    if (self)
    {
        count = pointCount;
        imgSize = imageSize;
        pointSize = CGSizeMake(imageSize.width + space, imageSize.height);
        self.darkImage = darkImage;
        self.lightImage = lightImage;
        self.lightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, imageSize.height)];
        self.lightView.clipsToBounds = YES;
        for (int i = 0; i < pointCount; i++)
        {
            UIImageView * darkImgView = [[UIImageView alloc] initWithFrame:CGRectMake(i * pointSize.width, 0, imgSize.width, imgSize.height)];
            darkImgView.image = self.darkImage;
            [self addSubview:darkImgView];
            UIImageView * lightImgView = [[UIImageView alloc] initWithFrame:darkImgView.frame];
            lightImgView.image = self.lightImage;
            [self.lightView addSubview:lightImgView];
        }
        [self addSubview:self.lightView];
    }
    return self;
}

- (UITapGestureRecognizer *)tapGesture
{
    if (!_tapGesture)
    {
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    }
    return _tapGesture;
}


- (void)setPoints:(CGFloat)points
{
    points = fmaxf(0,fminf(count,points));
    if (points != self.points)
    {
        [self willChangeValueForKey:@"points"];
        _points = points;
        self.lightView.frame = CGRectMake(0, 0, pointSize.width * points, pointSize.height);
        [self didChangeValueForKey:@"points"];
        if ([self.delegate respondsToSelector:@selector(pointView:pointsChanged:)])
        {
            [self.delegate pointView:self pointsChanged:_points];
        }
    }
}

- (void)setGestureEnabled:(BOOL)gestureEnabled
{
    if (_gestureEnabled != gestureEnabled)
    {
        _gestureEnabled = gestureEnabled;
        if (gestureEnabled)
        {
            [self addGestureRecognizer:self.tapGesture];
        }
        else
        {
            [self removeGestureRecognizer:self.tapGesture];
        }
    }
}

- (void)tap:(UITapGestureRecognizer *)gesture
{
    [self setPoints:(NSInteger)([gesture locationInView:self].x / pointSize.width) + 1];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.gestureEnabled)
    {
        if ([self.delegate respondsToSelector:@selector(gestureStarted:)])
        {
            [self.delegate gestureStarted:self];
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self setPoints:[[touches anyObject] locationInView:self].x / pointSize.width];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.gestureEnabled)
    {
        if ([self.delegate respondsToSelector:@selector(gestureStopped:)])
        {
            [self.delegate gestureStarted:self];
        }
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.gestureEnabled)
    {
        if ([self.delegate respondsToSelector:@selector(gestureStopped:)])
        {
            [self.delegate gestureStarted:self];
        }
    }
}


@end
