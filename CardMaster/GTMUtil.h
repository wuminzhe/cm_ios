//
//  GTMUtil.h
//  CardMaster
//
//  Created by wenjun on 13-1-14.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTMHTTPFetcher.h"

@interface GTMUtil : NSObject

+ (GTMHTTPFetcher *)fetcherWithURLString:(NSString *)urlString params:(NSDictionary *)params HTTPType:(NSString *)httpType urlEncode:(BOOL)encode;

+ (NSURLRequest *)requestWithURLString:(NSString *)urlString params:(NSDictionary *)params HTTPType:(NSString *)httpType urlEncode:(BOOL)encode;


@end
