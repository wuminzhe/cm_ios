//
//  CreditCard.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-16.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

/* CreditCard
 * 信用卡
 */

@interface CreditCard : NSObject

@property (nonatomic, strong) NSString *cardID;         // 信用卡ID
@property (nonatomic, strong) NSString *cardNumber;     // 信用卡卡号
@property (nonatomic, strong) NSNumber *reminderDay;    // 每月还款日
@property (nonatomic, strong) NSString *billDay;        // 账单日
@property (nonatomic, strong) NSString *productID;      // 信用卡产品ID
@property (nonatomic, strong) NSString *productName;    // 信用卡产品名称
@property (nonatomic, strong) NSString *bankID;         // 信用卡所属银行ID
@property (nonatomic, strong) NSString *bankName;       // 信用卡所属银行名称
@property (nonatomic, strong) NSString *ownerName;      // 信用卡持有人姓名
@property (nonatomic, strong) NSString *cardPic;        // 信用卡图片名称
@property (nonatomic, strong) NSString *remoteCardPic;  // 服务器端图片地址
@property (nonatomic, strong) NSString *nick;           // 用户自己输入卡产品名称
@property (nonatomic, assign) BOOL status;              // 信用卡数据是否成功同步到服务器端

@property (nonatomic, strong) UIImage *cardImage;       // 信用卡图片
@property (nonatomic, strong) UIImage *walletImage;     // 信用卡图片（钱包位置）

@property (nonatomic, assign) BOOL addingCard;          // 表明卡片处于添加状态

@property (nonatomic, strong) NSString *bankIcon;       // 银行标识
@property (nonatomic, strong) NSString *source;         // 卡片来源（1:非账单， 2:账单）

@property (nonatomic) BOOL swiped;

@end
