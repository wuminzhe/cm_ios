//
//  InstallmentResultViewController.m
//  cardMaster
//
//  Created by Zhang Ming on 12-10-30.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "InstallmentResultViewController.h"
#import "InstallmentResultCell.h"
#import "Rate.h"

@interface InstallmentResultViewController () {
    NSArray *data;
}

@end

@implementation InstallmentResultViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"分期计算结果";
    
    stageLabel.text = [NSString stringWithFormat:@"%@期", _rate.stage];
    accountLabel.text = [NSString stringWithFormat:@"￥%.1f", _account];
    
    double accountPerStage = _account / _rate.stage.integerValue;           // 每期还款金额
    double allRate = 0.0;
    
    if ([_rate.type isEqualToString:@"1"]) {
        allRate = _account * _rate.rate.integerValue;
        data = @[ [NSString stringWithFormat:@"%.1f", accountPerStage + allRate],
                  [NSString stringWithFormat:@"%.1f", accountPerStage]];
    } else if ([_rate.type isEqualToString:@"2"]) {
        double ratePerStage = _account * _rate.rate.integerValue;
        allRate = ratePerStage * _rate.stage.integerValue;
        data = @[ [NSString stringWithFormat:@"%.1f", accountPerStage + ratePerStage] ];
    }
    
    allRateLabel.text = [NSString stringWithFormat:@"￥%.1f", allRate];
    allAccountLabel.text = [NSString stringWithFormat:@"￥%.1f", _account + allRate];
    
    mTableView.tableFooterView = footView;
}

- (void)viewDidUnload
{
    accountLabel = nil;
    stageLabel = nil;
    allAccountLabel = nil;
    allRateLabel = nil;
    footView = nil;
    mTableView = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)loan:(id)sender
{
    [self quickAlertView:@"功能还没完善，敬请期待！"];
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_rate.stage integerValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    InstallmentResultCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InstallmentResultCell"];
    
    if (!cell) {
        cell = [InstallmentResultCell getInstance];
    }
    
    cell.stageLabel.text = [NSString stringWithFormat:@"第%d期", indexPath.row + 1];
    
    if ([_rate.type isEqualToString:@"1"]) {
        if (indexPath.row == 0) {
            cell.installmentLabel.text = data[0];
        } else {
            cell.installmentLabel.text = data[1];
        }
    } else {
        cell.installmentLabel.text = data[0];
    }
    
    return cell;
}

@end
