//
//  BankServiceCell.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-29.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BankServiceDataInternal;

@protocol BankServiceCellDelegate <NSObject>

- (void)handleButtonClick:(BankServiceDataInternal *)dataInternal;
- (void)handleFirstResponder:(UITextField *)textField;

@end

@interface BankServiceCell : UITableViewCell<UITextFieldDelegate> {
    UILabel *infoLabel;
    UIButton *sendButton;
}
@property (nonatomic, strong) BankServiceDataInternal *dataInternal;
@property (nonatomic, assign) id<BankServiceCellDelegate> delegate;

@end
