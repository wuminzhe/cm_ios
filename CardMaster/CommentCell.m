//
//  CommentCell.m
//  LifeShanghai
//
//  Created by wenjun on 12-11-30.
//
//

#import "CommentCell.h"
#import "PointView.h"

@interface CommentCell()
{
    IBOutlet UILabel * nameLabel;
    IBOutlet UILabel * contentLabel;
//    IBOutlet UILabel * timeLabel;
    PointView * pointView;
}

@end

@implementation CommentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)initialize
{
    pointView = [[PointView alloc] initWithDarkImage:[UIImage imageNamed:@"carddetail_star_gree_"] lightImage:[UIImage imageNamed:@"carddetail_star_yellow_"] imageSize:CGSizeMake(16, 16) space:1 pointCount:5];
    CGRect frame = pointView.frame;
    frame.origin.x = 135;
    frame.origin.y = 10;
    pointView.frame = frame;
    pointView.gestureEnabled = NO;
    [self.contentView addSubview:pointView];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

+ (CommentCell *)getInstance
{
    CommentCell * cell = [[[NSBundle mainBundle] loadNibNamed:@"CommentCell" owner:nil options:nil] lastObject];
    [cell initialize];
    return cell;
}

- (void)loadComment:(Comment *)comment
{
    nameLabel.text = comment.username;
    [[self class] reSizeLabel:contentLabel withText:comment.content rowHeight:18];
    [pointView setPoints:[comment.score floatValue]];
//    timeLabel.text = [CommonUtil descriptionFromDate:comment.date now:nil];
}

+ (NSInteger)heightForComment:(Comment *)comment
{
   NSInteger textHeight = [self heightForText:comment.content font:[UIFont systemFontOfSize:14] width:280 rowHeight:18];
    return 63 + (textHeight - 18);
}

+ (NSInteger)heightForText:(NSString *)text font:(UIFont *)font width:(NSInteger)width rowHeight:(NSInteger)rowHeight
{
    if (text.length == 0)
    {
        return rowHeight;
    }
    CGSize size = [text sizeWithFont:font constrainedToSize:CGSizeMake(width, 1000)];
    return roundf(size.height / rowHeight) * rowHeight;
}

+ (void)reSizeLabel:(UILabel *)label withText:(NSString *)text rowHeight:(NSInteger)rowHeight
{
    label.text = text;
    if (text.length == 0)
    {
        label.frame = CGRectMake(label.frame.origin.x, label.frame.origin.y, label.bounds.size.width,rowHeight);
        return;
    }
    CGSize size = [text sizeWithFont:label.font constrainedToSize:CGSizeMake(label.bounds.size.width, 1000) lineBreakMode:label.lineBreakMode];
    label.frame = CGRectMake(label.frame.origin.x, label.frame.origin.y, label.bounds.size.width, roundf(size.height / rowHeight) * rowHeight);
}

@end
