//
//  Product.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-22.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 * 信用卡产品
 */

@interface Product : NSObject

@property (nonatomic, strong) NSString *productID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *pic;
@property (nonatomic, strong) NSString *bankID;
@property (nonatomic, strong) NSString *bankName;
@property (nonatomic, strong) NSString *appliable;
@property (nonatomic, strong) NSString *categoryID;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSString *grade;
@property (nonatomic, strong) NSString *gradeName;
@property (nonatomic, strong) NSString *org;
@property (nonatomic, strong) NSString *fee;
@property (nonatomic, strong) NSString *feePolicy;
@property (nonatomic, strong) NSString *cashRate;
@property (nonatomic, strong) NSString *special;
@property (nonatomic, strong) NSString *addOn;
@property (nonatomic, strong) NSString *bookmarkTimes;
@property (nonatomic, strong) NSString *applications;
@property (nonatomic, strong) NSString *totalPoints;
@property (nonatomic, strong) NSString *totalNumber;
@property (nonatomic, strong) NSURL *applyURL;
@property (nonatomic, strong) NSString *commentCount;
@property (nonatomic, strong) NSString *score;

@end
