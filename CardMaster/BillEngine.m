//
//  BillEngine.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-2.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BillEngine.h"
#import "NSDictionary+Extension.h"
#import "SQLGlobal.h"
#import "SMS.h"

@implementation BillEngine

- (MKNetworkOperation *)getSMSUpdateStatus:(NSMutableDictionary *)params
                              onCompletion:(SMSUpdateStatusResponseBlock)completionBlock
                                   onError:(MKNKErrorBlock)errorBlock;
{
    MKNetworkOperation *op = [self operationWithPath:@"api/update_sms" params:params];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        DLog(@"%@", response);
        NSString *updateTime = response[@"data"][@"update_time"];
        
        NSString *lastUpdateTime = [[SQLGlobal sharedInstance] getSMSLastUpdateTime];
        
        completionBlock(![lastUpdateTime isEqualToString:updateTime]);
        
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)getSMSData:(NSMutableDictionary *)params
                      onCompletion:(SMSResponseBlock)completionBlock
                           onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/sms" params:params];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        DLog(@"%@", response);
        
        NSArray *data = response[@"data"];
        
        NSMutableArray *result = [NSMutableArray arrayWithCapacity:data.count];
        
        for (NSDictionary *record in data) {
            SMS *sms = [[SMS alloc] init];
            
            sms.smsID = [record stringForKey:@"id"];
            sms.bankID = [record stringForKey:@"bank_id"];
            sms.billTemplate = [record stringForKey:@"bill_template"];
            sms.remTemplate = [record stringForKey:@"rem_template"];
            sms.mobile = [record stringForKey:@"mobile"];
            sms.mobileUnicom = [record stringForKey:@"mobile_unicom"];
            sms.mobileTelecom = [record stringForKey:@"mobile_telecom"];
            sms.createTime = [record stringForKey:@"create_time"];
            sms.updateTime = [record stringForKey:@"update_time"];
            
            [result addObject:sms];
            
            sms = nil;
        }
        
        completionBlock([[SQLGlobal sharedInstance] updateSMS:result]);
        
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

@end
