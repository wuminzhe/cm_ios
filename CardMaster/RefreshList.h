//
//  RefreshList.h
//  LifeShanghai
//
//  Created by wenjun on 12-11-30.
//
//

#import <UIKit/UIKit.h>
@class RefreshList;

@protocol RefreshListDelegate <NSObject>

//不算refresh cell
@optional
- (NSInteger)refreshList:(RefreshList *)refreshList heightForRowAtIndex:(NSInteger)index;
- (UITableViewCell *)refreshList:(RefreshList *)refreshList cellForRowAtIndex:(NSInteger)index;
- (UITableViewCell *)refreshCellForRefreshList:(RefreshList *)refreshList;
- (void)refreshList:(RefreshList *)refreshList didSelectRowAtIndex:(NSInteger)index;

- (void)topRefreshInRefreshList:(RefreshList *)refreshList;
- (void)bottomRefreshInRefreshList:(RefreshList *)refreshList;
- (void)topDragTriggered;

@required
- (NSInteger)numberOfRowsInRefreshList:(RefreshList *)refreshList;

@end

@interface RefreshList : UIView

@property (assign,nonatomic) id<RefreshListDelegate> delegate;

@property (nonatomic) BOOL topRefreshEnabled;
@property (nonatomic) BOOL bottomRefreshEnabled;

@property (assign,nonatomic) UIImage * backgroundImage;
@property (nonatomic) BOOL firstPageNeedAnimation;
@property (nonatomic) UITableViewRowAnimation firstPageAnimation;

@property (readonly,nonatomic) UITableView * innerTable;

@property (readonly,nonatomic) BOOL bottomOver;
//从1开始,0表示没有数据
@property (readonly,nonatomic) NSInteger page;

- (void)topOverWithNumber:(NSUInteger)number finished:(BOOL)finished;
- (void)bottomOverWithNumber:(NSUInteger)number finished:(BOOL)finished;

- (void)clear;
- (UITableViewCell *)dequeueReusableCellWithIdentifier:(NSString *)identifier;

- (void)reloadRowsAtIndexes:(NSArray *)indexes withRowAnimation:(UITableViewRowAnimation)animation;
- (void)insertRowsAtIndexes:(NSArray *)indexes withRowAnimation:(UITableViewRowAnimation)animation;
- (void)deleteRowsAtIndexes:(NSArray *)indexes withRowAnimation:(UITableViewRowAnimation)animation;
- (void)reloadVisibleRows;
- (void)reloadData;

@end
