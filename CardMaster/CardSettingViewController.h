//
//  CardSettingViewController.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-16.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"

@class CreditCard;
@class CardEngine;

//@protocol CardSettingViewControllerDelegate <NSObject>
//
//- (void)deleteCard;
//- (void)modifyCard:(CreditCard *)newCard withOldCard:(CreditCard *)oldCard;
//
//@end

#define DELETE_CARD             @"DELETE_CARD"
#define MODIFY_CARD             @"MODIFY_CARD"

@interface CardSettingViewController : BaseNavigationController<UIAlertViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate> {
    __unsafe_unretained IBOutlet UIScrollView *theMainScrollView;
    __unsafe_unretained IBOutlet UIScrollView *theScrollView;
    __unsafe_unretained UIButton *editButton;
    __unsafe_unretained IBOutlet UIButton *detailButton;
    __unsafe_unretained IBOutlet UIButton *bussinessButton;
    __unsafe_unretained IBOutlet UIImageView *cardImageView;
    
    __unsafe_unretained IBOutlet UILabel *refundTotalMoneyLabel;
    __unsafe_unretained IBOutlet UILabel *refundMiniMoneyLabel;
    __unsafe_unretained IBOutlet UILabel *refundDayLabel;
    
    UIBarButtonItem *editBarButtonItem;
    
    // detail view
    IBOutlet UIView *detailView;
    IBOutlet UILabel *cardNumberLabel;
    IBOutlet UIImageView *bankImageView;
    IBOutlet UILabel *cardOfBankLabel;
    IBOutlet UILabel *cardNameLabel;
    IBOutlet UILabel *reminderLabel;
    
    // buiness view
    IBOutlet UIView *bussinessView;
    
    // edit view
    IBOutlet UIView *editView;
    IBOutlet UITextField *cardNumberTextField;
    IBOutlet UITextField *cardNameTextField;
    IBOutlet UILabel *editBankNameLabel;
    IBOutlet UIImageView *editBankIconView;
    
    // picker view
    IBOutlet UIView *chooseView;
    IBOutlet UIPickerView *mPickerView;
    IBOutlet UIBarButtonItem *doneButton;
    IBOutlet UIButton *reminderDayButton;
    
    CardEngine *engine;
    
    NSInteger choose;                       // default 1000
    BOOL detailEditing;                     // default NO
}

@property (nonatomic, strong) CreditCard *card;
//@property (nonatomic, assign) id<CardSettingViewControllerDelegate> delegate;

- (IBAction)changeInfo:(id)sender;
- (IBAction)refundRemind:(id)sender;
- (IBAction)refundImmediate:(id)sender;
- (IBAction)searchDebt:(id)sender;
- (IBAction)installment:(id)sender;
- (IBAction)freeAnnualFee:(id)sender;
- (IBAction)deleteCard:(id)sender;
- (IBAction)searchEDebt:(id)sender;
- (IBAction)applyLoan:(id)sender;

// 银行，还款日选择
- (IBAction)showPickerView:(id)sender;
- (IBAction)chooseDone:(id)sender;

@end
