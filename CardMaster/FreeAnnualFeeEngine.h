//
//  FreeAnnualFeeEngine.h
//  cardMaster
//
//  Created by Zhang Ming on 12-12-3.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MKNetworkEngine.h"

@interface FreeAnnualFeeEngine : MKNetworkEngine

//typedef void (^getBanksResponseBlock) (NSMutableArray *results);
typedef void (^getProductFeeResponseBlock) (NSString *result);

//- (MKNetworkOperation *)getBanks:(NSMutableDictionary *)params
//                    onCompletion:(getBanksResponseBlock)completionBlock
//                         onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)getProductFee:(NSMutableDictionary *)params
                         onCompletion:(getProductFeeResponseBlock)completionBlock
                              onError:(MKNKErrorBlock)errorBlock;

@end
