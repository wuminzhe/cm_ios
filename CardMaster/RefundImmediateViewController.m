//
//  RefundImmediateViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 13-1-11.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "RefundImmediateViewController.h"

@interface RefundImmediateViewController ()

@end

@implementation RefundImmediateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"立即还款";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)refundImmediate:(id)sender
{
    [self quickAlertView:@"功能还没完善，敬请期待！"];
}

- (IBAction)loan:(id)sender
{
    [self quickAlertView:@"功能还没完善，敬请期待！"];
}

@end
