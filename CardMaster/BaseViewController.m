//
//  BaseViewController.m
//  cardMaster
//
//  Created by Zhang Ming on 12-12-3.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()
{
    UILabel * noResultLabel;
}

@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    hud = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Actions

- (void)quickAlertView:(NSString *)message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"信息"
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
    [alertView show];
}

- (void)showHudView:(NSString *)text
{
    if (hud == nil) {
        hud = [[MBProgressHUD alloc] initWithView:self.view];
        hud.labelText = text;
        [self.view addSubview:hud];
        hud.delegate = self;
    }
    
    [hud show:YES];
}

- (void)showHudViewOnWindow:(NSString *)text
{
    if (hud == nil) {
        hud = [[MBProgressHUD alloc] initWithView:self.view];
        hud.labelText = text;
        [self.view.window addSubview:hud];
        hud.delegate = self;
    }
    
    [hud show:YES];
}

- (void)hideHudView
{
    [hud hide:YES];
}

- (void)showNoResultOnView:(UIView *)subView
{
    if (!noResultLabel)
    {
        noResultLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 21)];
        noResultLabel.font = [UIFont boldSystemFontOfSize:17];
        noResultLabel.backgroundColor = [UIColor clearColor];
        noResultLabel.textAlignment = UITextAlignmentCenter;
        noResultLabel.textColor = [UIColor lightGrayColor];
        noResultLabel.text = @"没有数据";
    }
    CGRect frame = CGRectMake(roundf((subView.bounds.size.width - noResultLabel.bounds.size.width)/2), roundf((subView.bounds.size.height * .4 - 10)), noResultLabel.bounds.size.width, noResultLabel.bounds.size.height);
    noResultLabel.frame = frame;
    noResultLabel.alpha = 0;
    [subView addSubview:noResultLabel];
    [UIView animateWithDuration:.25 animations:^{
        noResultLabel.alpha = 1;
    }];
}

- (void)hideNoResult
{
    [UIView animateWithDuration:.25 animations:^{
        noResultLabel.alpha = 0;
    } completion:^(BOOL finished) {
        [noResultLabel removeFromSuperview];
    }];
}

@end
