//
//  Fee.h
//  cardMaster
//
//  Created by Zhang Ming on 12-12-5.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fee : NSObject

@property (nonatomic, strong) NSString *cardNumber;
@property (nonatomic, strong) NSString *effective;
@property (nonatomic, strong) NSString *count;
@property (nonatomic, strong) NSString *reminder;
@property (nonatomic, assign) BOOL status;

@end
