//
//  DiscountDetailViewController.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-18.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"
#import "DiscountDetailCell.h"
#import "DiscountDescriptionCell.h"
//#import "SinaWeibo.h"
//#import "TCWBEngine.h"
#import "MBProgressHUD.h"
#import "WXApiObject.h"
#import "WXApi.h"
#import "DiscountEngine.h"

@class Discount;

//@interface DiscountDetailViewController : BaseNavigationController<UITableViewDataSource, UITableViewDelegate, DiscountDetailCellDelegate, UIAlertViewDelegate, DiscountDescriptionCellDelegate,SinaWeiboDelegate,SinaWeiboRequestDelegate,MBProgressHUDDelegate, UIActionSheetDelegate> {
//    SinaWeibo *sinaWeibo;
//    TCWBEngine *weiboEngine;
//    
//    DiscountEngine *engine;
//}

#import "SinaWBEngine.h"
#import "TencentWBEngine.h"

@interface DiscountDetailViewController : BaseNavigationController<UITableViewDataSource, UITableViewDelegate, DiscountDetailCellDelegate, UIAlertViewDelegate, DiscountDescriptionCellDelegate,MBProgressHUDDelegate, UIActionSheetDelegate, OAuthClientDelegate> {
    
    DiscountEngine *engine;
}


@property (nonatomic, strong) Discount *discount;


@end
