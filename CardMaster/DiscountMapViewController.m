//
//  DiscountMapViewController.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-21.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "DiscountMapViewController.h"
#import "Discount.h"

@interface DiscountMapViewController ()

@end

@implementation DiscountMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"地图";
    
    // 显示当前设备位置
    mMapView.showsUserLocation = YES;
    
    // 添加优惠商铺信息
    [mMapView addAnnotation:_discount];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(_discount.shopLatitude.doubleValue, _discount.shopLongitude.doubleValue);
    
    [mMapView setRegion:MKCoordinateRegionMakeWithDistance(coordinate, 5000, 5000)
              animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MKMapViewDelegate methods
- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    CGRect visibleRect = [mapView annotationVisibleRect];
    
    for (MKAnnotationView *view in views) {
        if ([view.annotation isKindOfClass:[Discount class]]) {
            CGRect endFrame = view.frame;
            CGRect startFrame = endFrame;
            startFrame.origin.y = visibleRect.origin.y - startFrame.size.height;
            view.frame = startFrame;
            
            [UIView animateWithDuration:0.6 animations:^{
                view.frame = endFrame;
            }];
        } else {
            
        }
    }
}

@end
