//
//  InstallmentResultCell.m
//  cardMaster
//
//  Created by Zhang Ming on 12-10-31.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "InstallmentResultCell.h"

@implementation InstallmentResultCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (InstallmentResultCell *)getInstance
{
    return  [[[NSBundle mainBundle] loadNibNamed:@"InstallmentResultCell" owner:nil options:nil] lastObject];
}

@end
