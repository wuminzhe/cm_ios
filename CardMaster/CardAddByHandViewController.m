//
//  CardAddByHandViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 13-1-8.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "CardAddByHandViewController.h"
#import "CMConstants.h"
#import "CMUtil.h"
#import "CardEngine.h"
#import "CreditCard.h"
#import "SQLGlobal.h"
#import "NSDictionary+Extension.h"
#import "NSString+Extension.h"
#import "JSONKit.h"

#import "UserEngine.h"

@interface CardAddByHandViewController ()

@end

@implementation CardAddByHandViewController
{
    NSMutableArray *pickerData;
    CardEngine *engine;
    UserEngine *userEngine;
    
    int selectedRow;
    
    int count;
    
    BOOL finish;
    
    int waitTime;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        pickerData = nil;
        engine = nil;
        userEngine = nil;
        
        selectedRow = 0;
        count = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!self.title)
        self.title = @"手工卡号添加";
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    [self.view addSubview:chooseView];
    CGRect viewRect = self.view.frame;
    chooseView.frame = CGRectMake(0,
                                  viewRect.size.height,
                                  viewRect.size.width,
                                  260);
    
    pickerData = [[NSMutableArray alloc] init];
    for (int i = 1; i <= kNumberOfDays; i++) {
        [pickerData addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    cardNumberTextField.text = _cardNumber;
    
    engine = [[CardEngine alloc] initWithHostName:kServerAddr];
}

- (void)viewDidUnload
{
    cardNumberTextField = nil;
    chooseView = nil;
    mPickerView = nil;
    
    [super viewDidUnload];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[CMUtil getAppDelegate].tabBarController.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods
// 保存信用卡图片到本地
- (NSString *)saveCardImage
{
    if (!_cardImage) return NO;
    
    NSData *imageData = UIImageJPEGRepresentation(_cardImage, 0.1);
    
    // 图片本地名称，未上传之前
    NSString *imageName = [NSString stringWithFormat:@"card%@.jpg", [NSString getDateString]];
    
    // 图片存储位置
    NSString *imagePath = [MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:imageName];
    
    if (![imageData writeToFile:imagePath atomically:YES]) {
        NSLog(@"保存图片不成功！");
        return nil;
    }
    
    return imagePath;
}

// 更改本地图片名称
- (BOOL)renameLocalCardImage:(NSString *)localImagePath remoteImagePath:(NSString *)remoteImagePath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL status = [fileManager moveItemAtPath:localImagePath toPath:remoteImagePath error:NULL];
    
    return status;
}

- (void)downloadImage:(NSString *)imageName
{
    if (!imageName) return;
    
    count++;
    
    if (!userEngine)
        userEngine = [[UserEngine alloc] initWithHostName:kServerAddr];
    
    //    NSString *downloadURLString = [NSString stringWithFormat:@"http://%@/assets/upload/%@", kServerAddr, imageName];
    
    NSString *downloadURLString = [NSString stringWithFormat:@"assets/upload/%@", imageName];
    
    NSString *imagePath = [MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:[imageName lastPathComponent]];
    
    [self downloadImage:downloadURLString destPath:imagePath];
}

- (void)downloadImage:(NSString *)srcString destPath:(NSString *)destPath
{
    [userEngine downloadImage:srcString destPath:destPath onCompletion:^(BOOL status) {
        finish = status;
    } onError:^(NSError *error) {
        if (count < 2) {
            [self downloadImage:srcString destPath:destPath];
        } else
            finish = YES;
    }];
}

#pragma mark - Actions
- (IBAction)hidePickerView:(id)sender
{
    [UIView animateWithDuration:kFilterAnimtaionDuration animations:^{
        chooseView.frame = CGRectMake(0,
                                      self.view.frame.size.height,
                                      self.view.frame.size.width,
                                      260);
    }];
}


- (IBAction)showPickerView:(id)sender
{
    [UIView animateWithDuration:kFilterAnimtaionDuration animations:^{
        
        chooseView.frame = CGRectMake(0,
                                      self.view.frame.size.height - 260,
                                      self.view.frame.size.width,
                                      260);
        
        [cardNumberTextField resignFirstResponder];
    }];
}

- (IBAction)choose:(id)sender
{
    if ([sender isKindOfClass:[UITextField class]]) {
        [self showPickerView:nil];
    } else {
        [self hidePickerView:nil];
        
        selectedRow = [mPickerView selectedRowInComponent:0];
        [reminderDayButton setTitle:pickerData[selectedRow]
                           forState:UIControlStateNormal];
    }
}

- (IBAction)addCard:(id)sender
{
    CreditCard *needAddCard = [[CreditCard alloc] init];
    needAddCard.cardNumber = cardNumberTextField.text;
    needAddCard.ownerName = [CMUtil getUsername];
    needAddCard.billDay = pickerData[selectedRow];
    
    [self showHudView:@"正在添加..."];
    
    
    if (_cardImage) {
        [self handleCreditCard:needAddCard];
        
        return;
    }
    
    __block id weakSelf = self;
    
    // 获取卡片信息参数
    NSMutableDictionary *cardInfoParams = [NSMutableDictionary dictionaryWithCapacity:2];
    cardInfoParams[@"code"] = [CMUtil getCode];
    cardInfoParams[@"card_number"] = needAddCard.cardNumber;
    
    [engine getCardInfo:cardInfoParams onCompletion:^(CreditCard *card) {
        if (!card) {
            [self quickAlertView:@"获取不到卡信息！"];
        }
        
        needAddCard.bankID = card.bankID;
        needAddCard.bankName = card.bankName;
        needAddCard.productID = card.productID;
        needAddCard.productName = card.productName;
        needAddCard.bankIcon = card.bankIcon;
        
        [self downloadImage:needAddCard.bankIcon];
        
        // 上传卡片到服务器
        NSMutableDictionary *addCardParams = [NSMutableDictionary dictionary];
        addCardParams[@"code"] = [CMUtil getCode];
        addCardParams[@"card_number"] = needAddCard.cardNumber;
        addCardParams[@"pic"] = @"";
        addCardParams[@"owner_name"] = [CMUtil getUsername];
        if (needAddCard.bankID)
            addCardParams[@"bank_id" ] = needAddCard.bankID;
        if (needAddCard.productID)
            addCardParams[@"creditcard_product_id" ] = needAddCard.productID;
        if (needAddCard.billDay) {
//            addCardParams[@"pay_day"] = needAddCard.reminderDay;
            addCardParams[@"state_day"] = needAddCard.billDay;
        }
        else {
//            addCardParams[@"pay_day"] = @"1";
            addCardParams[@"state_day"] = @"1";
            needAddCard.billDay = @"1";
        }
        
        __block id weakDelegate = _delegate;
        __block BOOL weakFinish = finish;
        __block int weakWaitTime = waitTime;
        
        [engine addCreditCard:addCardParams onCompletion:^(NSDictionary *response) {
            needAddCard.status = [[response stringForKey:@"status"] boolValue];
            needAddCard.cardID = [response stringForKey:@"card_id"];
            
            [[SQLGlobal sharedInstance] updateCard:needAddCard];
            
            [weakSelf hideHudView];
            
            while (!weakFinish) {
                if (weakWaitTime < 2) {
                    [NSThread sleepForTimeInterval:2.0];
                    weakWaitTime += 2;
                } else {
                    [self.navigationController popViewControllerAnimated:YES];
                    
                    if (weakDelegate && [weakDelegate respondsToSelector:@selector(addCreditCardByHand:)])
                        [weakDelegate addCreditCardByHand:needAddCard];
                    
                    weakDelegate = nil;
                    weakSelf = nil;
                    break;
                }
            }
            
        } onError:^(NSError *error) {
            [weakSelf hideHudView];
        }];
        
    } onError:^(NSError *error) {
        [weakSelf hideHudView];
        NSLog(@"*** = %@", error);
    }];
}

// 处理卡片
- (void)handleCreditCard:(CreditCard *)needAddCard
{
    NSString *imagePath = [self saveCardImage];
    
    if (imagePath) {
        // 上传图片到服务器
        NSMutableDictionary *uploadPicParams = [[NSMutableDictionary alloc] initWithCapacity:1];
        uploadPicParams[@"code"] = [CMUtil getCode];
        
        if (!engine)
            engine = [[CardEngine alloc] initWithHostName:kServerAddr];
        
        [engine uploadCreditCardImageFromFile:imagePath params:uploadPicParams onCompletion:^(NSString *urlString) {
            if ([urlString isEqualToString:Fail]) {
                [self quickAlertView:@"上传信用卡图片失败！"];
                return ;
            }
            
            // 上传图片成功
            needAddCard.remoteCardPic = urlString;
            // 更改本地图片名字，使得和服务器保存的一致
            NSString *remoteImageName = [urlString lastPathComponent];
            NSString *remoteImagePath = [MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:remoteImageName];
            if ([self renameLocalCardImage:imagePath remoteImagePath:remoteImagePath]) {
                needAddCard.cardPic = remoteImageName;
            } else {
                needAddCard.cardPic = [imagePath lastPathComponent];
            }
            
            // 获取卡片信息参数
            NSMutableDictionary *cardInfoParams = [NSMutableDictionary dictionaryWithCapacity:2];
            cardInfoParams[@"code"] = [CMUtil getCode];
            cardInfoParams[@"card_number"] = needAddCard.cardNumber;
            
            [engine getCardInfo:cardInfoParams onCompletion:^(CreditCard *card) {
                if (!card) {
                    [self quickAlertView:@"获取不到卡信息！"];
                }
                
                needAddCard.bankID = card.bankID;
                needAddCard.bankName = card.bankName;
                needAddCard.productID = card.productID;
                needAddCard.productName = card.productName;
                needAddCard.bankIcon = card.bankIcon;
                
                [self downloadImage:needAddCard.bankIcon];
                
                // 上传卡片到服务器
                NSMutableDictionary *addCardParams = [NSMutableDictionary dictionary];
                addCardParams[@"code"] = [CMUtil getCode];
                addCardParams[@"card_number"] = needAddCard.cardNumber;
                addCardParams[@"pic"] = urlString;
                addCardParams[@"owner_name"] = [CMUtil getUsername];
                if (needAddCard.bankID)
                    addCardParams[@"bank_id" ] = needAddCard.bankID;
                if (needAddCard.productID)
                    addCardParams[@"creditcard_product_id" ] = needAddCard.productID;
                if (needAddCard.billDay) {
//                    addCardParams[@"pay_day"] = needAddCard.reminderDay;
                    addCardParams[@"state_day"] = needAddCard.billDay;
                }
                else {
//                    addCardParams[@"pay_day"] = @"1";
                    addCardParams[@"state_day"] = @"1";
                    needAddCard.billDay = @"1";
                }
                __block id weakDelegate = _delegate;
                __block id weakSelf = self;
                __block BOOL weakFinish = finish;
                __block int weakWaitTime = waitTime;
                
                [engine addCreditCard:addCardParams onCompletion:^(NSDictionary *response) {
                    needAddCard.status = [[response stringForKey:@"status"] boolValue];
                    needAddCard.cardID = [response stringForKey:@"card_id"];
                    
                    [[SQLGlobal sharedInstance] updateCard:needAddCard];
                    
                    [weakSelf hideHudView];
                    
                    while (!weakFinish) {
                        if (weakWaitTime < 2) {
                            [NSThread sleepForTimeInterval:2.0];
                            weakWaitTime += 2;
                        } else {
                            [self.navigationController popViewControllerAnimated:YES];
                            
                            if (weakDelegate && [weakDelegate respondsToSelector:@selector(addCreditCardByHand:)])
                                [weakDelegate addCreditCardByHand:needAddCard];
                            
                            weakDelegate = nil;
                            weakSelf = nil;
                            break;
                        }
                    }
                } onError:^(NSError *error) {                    
                    [weakSelf hideHudView];
                }];
                
            } onError:^(NSError *error) {
                NSLog(@"error = %@", error);
                [self hideHudView];
            }];
            
        } onError:^(NSError *error) {
            NSLog(@"error = %@", error);
            [self hideHudView];
        }];
    }
}

#pragma mark - UIPickerViewDataSource methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerData.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return pickerData[row];
}

@end
