//
//  Discount.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-12.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface Discount : NSObject<MKAnnotation>

@property (nonatomic, strong) NSString *discountID;                     // 优惠编号
@property (nonatomic, assign) NSInteger type;                           // 优惠类型（1.银行活动、2.商家优惠）
@property (nonatomic, strong) NSString *creditcardProductID;            // 信用卡产品ID
@property (nonatomic, strong) NSString *bankID;                         // 银行ID
@property (nonatomic, strong) NSString *bankDescription;                // 银行详细信息
@property (nonatomic, strong) NSString *bankIcon;                       // 银行图标
@property (nonatomic, strong) NSString *bankName;                       // 银行名称
@property (nonatomic, strong) NSString *bankPic;                        // 银行图片
@property (nonatomic, strong) NSString *discountTitle;                  // 优惠标题、名称
@property (nonatomic, strong) NSString *discountDescription;            // 优惠详细信息
@property (nonatomic, strong) NSString *rate;                           // 折扣（0.8/null）, null则表示不是折扣
@property (nonatomic, strong) NSString *shopID;                         // 商家标识, null表示此优惠没有关联商家
@property (nonatomic, strong) NSString *hot;                            // 热度
@property (nonatomic, strong) NSString *activated;                      // "1", 有效活动标识
@property (nonatomic, strong) NSString *beginTime;                      // 开始时间
@property (nonatomic, strong) NSString *endTime;                        // 结束时间
@property (nonatomic, strong) NSString *pic;                            // 银行活动海报
@property (nonatomic, strong) NSString *createTime;                     // 商家时间
@property (nonatomic, strong) NSString *updateTime;                     // 更新时间
@property (nonatomic, strong) NSString *shopType;                       // 商户类型（餐饮美食、旅游酒店、休闲娱乐、
                                                                        // 生活服务、时尚购物）
@property (nonatomic, strong) NSString *shopName;                       // 商家名称
@property (nonatomic, strong) NSString *shopDesc;                       // 商户信息，详细描述
@property (nonatomic, strong) NSString *shopPic;                        // 商户图片
@property (nonatomic, strong) NSString *shopTypeName;                   // 商户类型名称
@property (nonatomic, strong) NSString *shopAddress;                    // 商户地址
@property (nonatomic, strong) NSString *shopLatitude;                   // 纬度
@property (nonatomic, strong) NSString *shopLongitude;                  // 经度
@property (nonatomic, strong) NSString *shopPhoneNumber;                // 商户电话
@property (nonatomic, strong) NSString *shopCity;                       // 商户所在城市
@property (nonatomic, strong) NSString *shopAverageConsumption;         // 商户平均消费
@property (nonatomic, strong) NSString *shopBookmarkTimes;              // 商家收藏次数
@property (nonatomic, strong) NSString *distance;

// MKAnnotaion properties

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly, copy) NSString *title;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate;

@end
