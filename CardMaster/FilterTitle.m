//
//  FilterTitle.m
//  CardMaster
//
//  Created by wenjun on 13-1-23.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "FilterTitle.h"

@interface FilterTitle()
{
    IBOutlet UILabel * titleLabel;
    IBOutlet UIButton * okBtn;
    IBOutlet UIButton * titleBtn;
    BOOL show;
}

- (IBAction)showChoices;
- (IBAction)hideChoices;

@end

@implementation FilterTitle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setText:(NSString *)text
{
    titleLabel.text = text;
}

- (NSString *)text
{
    return titleLabel.text;
}

- (CGRect)changeFrame:(CGRect)frame originY:(CGFloat)originY
{
    frame.origin.y = originY;
    return frame;
}

- (IBAction)showChoices
{
    [self show];
    [self.delegate shouldShowChoices:self];
}

- (IBAction)hideChoices
{
    [self hide];
    [self.delegate shouldHideChoices:self];
}

- (void)show
{
    if (show)
    {
        return;
    }
    show = !show;
    okBtn.userInteractionEnabled = YES;
    titleBtn.userInteractionEnabled = NO;
    [self bringSubviewToFront:okBtn];
    [UIView beginAnimations:nil context:nil];
    okBtn.frame = [self changeFrame:okBtn.frame originY:okBtn.frame.origin.y + self.bounds.size.height];
    titleLabel.frame = [self changeFrame:titleLabel.frame originY:titleLabel.frame.origin.y - self.bounds.size.height];
    titleBtn.frame = [self changeFrame:titleBtn.frame originY:titleBtn.frame.origin.y - self.bounds.size.height];
    [UIView commitAnimations];

}

- (void)hide
{
    if (!show)
    {
        return;
    }
    show = !show;
    okBtn.userInteractionEnabled = NO;
    titleBtn.userInteractionEnabled = YES;
    [self sendSubviewToBack:okBtn];
    [UIView beginAnimations:nil context:nil];
    okBtn.frame = [self changeFrame:okBtn.frame originY:okBtn.frame.origin.y - self.bounds.size.height];
    titleLabel.frame = [self changeFrame:titleLabel.frame originY:titleLabel.frame.origin.y + self.bounds.size.height];
    titleBtn.frame = [self changeFrame:titleBtn.frame originY:titleBtn.frame.origin.y + self.bounds.size.height];
    [UIView commitAnimations];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)initialize
{
    self.clipsToBounds = YES;
    okBtn.userInteractionEnabled = NO;
    titleBtn.userInteractionEnabled = YES;
}

+ (FilterTitle *)getInstance
{
    FilterTitle * filterTitle = [[[NSBundle mainBundle] loadNibNamed:@"FilterTitle" owner:nil options:nil] lastObject];
    [filterTitle initialize];
    return filterTitle;
}


@end
