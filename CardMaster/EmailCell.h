//
//  EmailCell.h
//  CardMaster
//
//  Created by wenjun on 13-1-15.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Email.h"

@interface EmailCell : UITableViewCell

+ (EmailCell *)getInstance;

- (void)loadEmail:(Email *)email;

@end
