//
//  SQLGlobal.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-17.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"

@class CreditCard;
@class Discount;
@class Reminder;
@class SMS;
@class Bank;
@class Fee;
@class MZBubbleData;

@interface SQLGlobal : NSObject {
    FMDatabase *db;
}

// 创建数据库
- (void)attachDatabase:(NSString *)database;

// 插入城市数据
- (void)initializeCity;

// 获得城市数据
- (NSMutableDictionary *)getCities;

- (NSMutableArray *)getCities:(NSString *)text;

// 返回不为nil，表明程序初始化过
- (NSString *)getClientCode;

// 更新user表
- (BOOL)updateUser:(NSString *)record;

// 获取数据库中的卡片总数
- (NSInteger)getNumberOfCard;

- (NSMutableArray *)getLocalCards;
// 获取所有卡片信息
- (NSMutableArray *)getLocalCards:(NSString *)code;

// 根据卡号获取卡片
- (CreditCard *)getCreditCard:(NSString *)cardNumber;

// 保存卡片信息
- (BOOL)updateCard:(CreditCard *)card;

// 删除卡片
- (BOOL)deleteCard:(NSString *)cardNumber;

// 获得提醒信息
- (Reminder *)getReminder:(NSString *)cardID;

// 更新提醒信息
- (BOOL)updateReminder:(Reminder *)reminder;

- (BOOL)deleteReminder:(NSString *)cardID;

// 获取银行表的最后更新时间
- (NSString *)getBankLastUpdateTime;

// 获取利率表的最后更新时间
- (NSString *)getRateLastUpdateTime;

// 更新银行利率表
- (BOOL)updateRates:(NSDictionary *)records;

// 更新银行表
- (BOOL)updateBanks:(NSMutableArray *)records;

// 获取Bank信息
- (Bank *)getBank:(NSString *)bankID;

// 获得银行列表
- (NSMutableArray *)getBanks;

// 根据bankId获取分期利率信息
- (NSMutableArray *)getRatesWithBankID:(NSString *)bankID;

// 存储账单查询资料
- (BOOL)updateSMS:(NSMutableArray *)result;

// 根据银行id查询账单查询资料
- (SMS *)getSMSWithBankID:(NSString *)bankID;

// 获取上一次账单更新时间
- (NSString *)getSMSLastUpdateTime;

// 更新免年费信息
- (BOOL)updateFee:(Fee *)record;

// 获取免年费信息
- (Fee *)getFee:(NSString *)cardNumber;

// 更新or插入优惠数据
- (BOOL)updateDiscount:(Discount *)record;

//// 根据优惠ID查询本地是否有数据
//- (BOOL)getDiscountStatus:(NSString *)discountID;

// 根据优惠ID查询本地是否有数据
- (BOOL)getDiscountStatus:(Discount *)discount;

// 删除优惠信息
- (BOOL)deleteDiscount:(NSString *)discountID;

// 获取所有优惠信息
- (NSMutableArray *)getDiscounts;

- (NSMutableArray *)getMessages;

- (BOOL)updateMessage:(MZBubbleData *)data;

- (BOOL)updateMessages:(NSMutableArray *)result;

+ (SQLGlobal *)sharedInstance;
@end
