//
//  FilterCardCell.m
//  cardMaster
//
//  Created by Zhang Ming on 12-10-16.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "FilterCardCell.h"

@implementation FilterCardCell
@synthesize cardNameLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (FilterCardCell *)getFilterCardCell
{
    return [[[NSBundle mainBundle] loadNibNamed:@"FilterCardCell" owner:nil options:NULL] lastObject];
}

@end
