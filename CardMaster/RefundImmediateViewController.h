//
//  RefundImmediateViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 13-1-11.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"

@interface RefundImmediateViewController : BaseNavigationController

- (IBAction)refundImmediate:(id)sender;

- (IBAction)loan:(id)sender;

@end
