//
//  CheckBox.h
//  LifeShanghai
//
//  Created by wenjun on 12-12-2.
//
//

#import <UIKit/UIKit.h>

@interface CheckBox : UIView

@property (nonatomic) BOOL checked;
@property (strong,nonatomic) UIImage * checkedImage;
@property (strong,nonatomic) UIImage * unCheckedImage;
@property (nonatomic) BOOL enabled;

@property (nonatomic) BOOL canDeselect;

- (void)loadUnCheckedImage:(UIImage *)unCheckedImage checkedImage:(UIImage *)checkedImage;

- (void)addTarget:(id)target valueChangedSelector:(SEL)selector;

@end
