//
//  BankDiscountDetailViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 13-1-5.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"
#import "DiscountDetailCell.h"
#import "BankDiscountDescriptionCell.h"
//#import "SinaWeibo.h"
//#import "TCWBEngine.h"
#import "MBProgressHUD.h"
#import "WXApiObject.h"
#import "WXApi.h"

#import "SinaWBEngine.h"
#import "TencentWBEngine.h"

@class Discount;

//@interface BankDiscountDetailViewController : BaseNavigationController<UITableViewDataSource, UITableViewDelegate, BankDiscountDescriptionCellDelegate, SinaWeiboDelegate,SinaWeiboRequestDelegate,MBProgressHUDDelegate, UIActionSheetDelegate, DiscountDetailCellDelegate>

@interface BankDiscountDetailViewController : BaseNavigationController<UITableViewDataSource, UITableViewDelegate, BankDiscountDescriptionCellDelegate, OAuthClientDelegate, MBProgressHUDDelegate, UIActionSheetDelegate, DiscountDetailCellDelegate>

@property (nonatomic, strong) Discount *discount;

@end
