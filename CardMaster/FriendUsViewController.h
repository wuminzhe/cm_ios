//
//  FriendUsViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 13-1-15.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"
#import "TencentOAuth.h"
//#import "SinaWeibo.h"
//#import "TCWBEngine.h"
#import "WXApi.h"
#import "SinaWBEngine.h"
#import "TencentWBEngine.h"

//@interface FriendUsViewController : BaseNavigationController<TencentSessionDelegate,SinaWeiboDelegate,SinaWeiboRequestDelegate> {
//    /*
//     *  新浪和腾讯应用高度集成到“我爱卡”，所以其他项目不能用
//     */
//    TencentOAuth *_tencentOAuth;
//    NSMutableArray *_permissions;
//    TCWBEngine *weiboEngine;
//    SinaWeibo *_sinaOAuth;
//}


@interface FriendUsViewController : BaseNavigationController<OAuthClientDelegate> {
    
}

- (IBAction)friendWithSina:(id)sender;
- (IBAction)friendWithTencent:(id)sender;
- (IBAction)friendWithWeChat:(id)sender;

@end
