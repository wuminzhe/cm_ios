//
//  CardDetailViewController.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-20.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"
//#import "TencentOAuth.h"
//#import "SinaWeibo.h"
#import "TCWBEngine.h"
#import "WXApi.h"

#import "SinaWBEngine.h"
#import "TencentWBEngine.h"

@class Product;

//@interface CardDetailViewController : BaseNavigationController<UITableViewDataSource,UITabBarDelegate, UIActionSheetDelegate, TencentSessionDelegate,SinaWeiboDelegate,SinaWeiboRequestDelegate>
//{
//    IBOutlet UIView * rightBtn;
//    /*
//     *  新浪和腾讯应用高度集成到“我爱卡”，所以其他项目不能用
//     */
//    TencentOAuth *_tencentOAuth;
//    NSMutableArray *_permissions;
//    TCWBEngine *weiboEngine;
//    SinaWeibo *_sinaOAuth;
//}

@interface CardDetailViewController : BaseNavigationController<UITableViewDataSource,UITabBarDelegate, UIActionSheetDelegate, OAuthClientDelegate>
{
    IBOutlet UIView * rightBtn;
}


@property (unsafe_unretained, nonatomic) IBOutlet UITableView *contentTableView;
@property (nonatomic, strong) Product *product;

@end
