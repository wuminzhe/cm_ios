//
//  DiscountFeedbackViewController.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-29.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"
#import "LvCheckBox.h"
#import "DiscountEngine.h"

@class Discount;

@interface DiscountFeedbackViewController : BaseNavigationController<UITextViewDelegate, UIAlertViewDelegate> {
    IBOutlet UITextView *feedTextView;
    IBOutlet UIScrollView *theScrollView;
    IBOutlet UILabel *nameLabel;
    
    LvCheckBox *discountCheckBox;
    LvCheckBox *addressCheckBox;
    LvCheckBox *phoneCheckBox;
    
    DiscountEngine *engine;
}

@property (nonatomic, strong) Discount *discount;

- (IBAction)backgroundTap:(id)sender;
- (IBAction)send:(id)sender;

@end
