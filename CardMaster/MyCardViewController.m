//
//  MyCardViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 12-12-24.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MyCardViewController.h"
#import "CMUtil.h"
#import "SQLGlobal.h"
#import "WalletCell.h"
#import "CardEngine.h"
#import "CreditCard.h"
#import "GlobalHeader.h"
#import "CMConstants.h"
#import "NSString+Extension.h"
#import "JBTabBarController.h"
#import "CardIOPaymentViewController.h"
#import "CardSettingViewController.h"
#import "UserSettingViewController.h"
#import "objc/runtime.h"
#import "UINavigationController+Extension.h"
#import "BillImportViewController.h"
#import "NSDictionary+Extension.h"
#import "GTMUtil.h"
#import "JSONKit.h"
#import "Email.h"
#import "GTMHTTPFetcherService.h"
#import "UserEngine.h"

@interface MyCardViewController ()

@property (strong,nonatomic) GTMHTTPFetcherService * fetcherService;

@end

@implementation MyCardViewController
{
    UIImage *cardImage;             // 拍照得到的卡片
    UIView *noCardView;             // 没有卡片时候显示的页面
    UIButton *importEBillButton;
    UIButton *addCardByHandButton;
    CardIOPaymentViewController *_scanViewController;
    
    NSMutableArray *needAddCards;   // 等待添加卡片列表
    NSInteger selectedCardIndex;
    
//    CreditCard *needAddCard;        // 需要添加的卡片
//    BOOL addingCard;                // 表示是否正在添加卡片
    BOOL needTimer;
    
    UserEngine *userEngine;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:DELETE_CARD object:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        cardImage = nil;
        noCardView = nil;
        importEBillButton = nil;
        addCardByHandButton = nil;
        _scanViewController = nil;
//        needAddCard = nil;
        engine = nil;
        
        selectedCardIndex = -1;
        
        needAddCards = [[NSMutableArray alloc] init];
        userEngine = nil;
        
//        addingCard = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabBarItem.image = [UIImage imageNamed:@"tabbar_card.png"];
    self.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_card_select.png"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDeleteCard:)
                                                 name:DELETE_CARD
                                               object:nil];
    
    self.fetcherService = [[GTMHTTPFetcherService alloc] init];
    
    self.cards = [[SQLGlobal sharedInstance] getLocalCards];
    
    if (!_cards)
        _cards = [[NSMutableArray alloc] init];
    
    if (_cards.count == 0 && needAddCards.count == 0)
        [self showNoCardView];
    else
        [self hideNoCardView];
    mTableView.decelerationRate = .8;
}

- (void)viewDidUnload
{
    mTableView = nil;
    
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    BOOL reloaded = NO;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"NeedReload"]) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"NeedReload"];
        self.cards = [[SQLGlobal sharedInstance] getLocalCards];
        [mTableView reloadData];
        reloaded = YES;
    }
    
    // 轮询
    needTimer = YES;
    NSMutableArray *emailList = [[NSUserDefaults standardUserDefaults] objectForKey:IMPORT_BILL];
    
    if (emailList.count > 0) {
        
        [needAddCards removeAllObjects];
        
        for (NSString *emailId in emailList) {
            Email *email = [[Email alloc] init];
            email.pk = emailId;
            [self checkEmail:email];
            
            CreditCard *card = [[CreditCard alloc] init];
            card.addingCard = YES;
            
            [needAddCards addObject:card];
        }
        
        [mTableView reloadData];
        reloaded = YES;
    }
    
    if (!reloaded && self.cards.count > 0)
    {
        [mTableView reloadRowsAtIndexPaths:[mTableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [self loadNoneCardView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    needTimer = NO;
    [self.fetcherService stopAllFetchers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods
- (void)loadNoneCardView
{
    if (_cards.count == 0 && needAddCards.count == 0) {
        [self showNoCardView];
        mTableView.scrollEnabled = NO;
    } else {
        [self hideNoCardView];
        mTableView.scrollEnabled = YES;
    }
}

// 如果没有卡片显示的信息页面
- (void)showNoCardView
{
    if (noCardView == nil) {
        noCardView = [[UIView alloc] initWithFrame:CGRectMake(0, 63, 320, 338)];
        noCardView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
        
        UIImageView *noCardImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nocard_bg.png"]];
        
        [noCardView addSubview:noCardImageView];
        
        UIImageView *noCardImageView1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nocard_kuang_1.png"]];
        noCardImageView1.frame = CGRectMake(25, 25, 270, 67);
        
        [noCardView addSubview:noCardImageView1];
        
        UIImageView *noCardImageView2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nocard_kuang_2.png"]];
        noCardImageView2.frame = CGRectMake(25, 94, 270, 81);
        
        [noCardView addSubview:noCardImageView2];
        
        UIImageView *noCardImageView3 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nocard_kuang_3.png"]];
        noCardImageView3.frame = CGRectMake(25, 177, 270, 94);
        
        [noCardView addSubview:noCardImageView3];
        
        // 合作伙伴
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(35, 188, 250, 72)];
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.showsVerticalScrollIndicator = NO;
        [noCardView addSubview:scrollView];
        
        UIImageView *logo1 = [[UIImageView alloc] initWithFrame:CGRectMake(12, 30, 39, 39)];
        logo1.image = [UIImage imageNamed:@"logo1.png"];
        
        [scrollView addSubview:logo1];
        
        UIImageView *logo2 = [[UIImageView alloc] initWithFrame:CGRectMake(79, 30, 39, 39)];
        logo2.image = [UIImage imageNamed:@"logo2.png"];
        
        [scrollView addSubview:logo2];
        
        UIImageView *logo3 = [[UIImageView alloc] initWithFrame:CGRectMake(146, 30, 39, 39)];
        logo3.image = [UIImage imageNamed:@"logo3.png"];
        
        [scrollView addSubview:logo3];
        
        UIImageView *logo4 = [[UIImageView alloc] initWithFrame:CGRectMake(213, 30, 39, 39)];
        logo4.image = [UIImage imageNamed:@"logo4.png"];
        
        [scrollView addSubview:logo4];
        
        UIImageView *logo5 = [[UIImageView alloc] initWithFrame:CGRectMake(280, 30, 39, 39)];
        logo5.image = [UIImage imageNamed:@"logo5.png"];
        
        [scrollView addSubview:logo5];
        
        scrollView.contentSize = CGSizeMake(320, 72);
        
        UIButton *addCardButton = [UIButton buttonWithType:UIButtonTypeCustom];
        addCardButton.frame = CGRectMake(35, 279, 250, 40);
        [addCardButton setImage:[UIImage imageNamed:@"nocard_add.png"]
                       forState:UIControlStateNormal];
        [addCardButton setImage:[UIImage imageNamed:@"nocard_add_click.png"]
                       forState:UIControlStateHighlighted];
        [addCardButton addTarget:self
                          action:@selector(addCreditCard:)
                forControlEvents:UIControlEventTouchUpInside];
        
        [noCardView addSubview:addCardButton];
    }
    
    if (noCardView.superview == nil) {
        [self.view addSubview:noCardView];
    }
}

- (void)hideNoCardView
{
    if (noCardView && noCardView.superview)
        [noCardView removeFromSuperview];
}

- (void)addCardByHand:(id)sender
{
    [self removeCameraButtons];
    [self showAddCardByHandView];
    [_scanViewController dismissModalViewControllerAnimated:YES];
}

- (void)importEBill:(id)sender
{
    [self removeCameraButtons];
    [self showImportView];
    [_scanViewController dismissModalViewControllerAnimated:YES ];
}

- (void)showBillImportView
{
    BillImportViewController *billImportViewController = [[BillImportViewController alloc] initWithNibName:@"BillImportViewController" bundle:nil];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:billImportViewController];
    
    [navigationController setNavigationBarBackgroundImage:[UIImage imageNamed:@"nav.png"]];
    
    [[CMUtil getAppDelegate].tabBarController presentModalViewController:navigationController animated:YES];
}

- (void)showAddCardByHandView
{
    CardAddByHandViewController *cardAddByHandViewController = [[CardAddByHandViewController alloc] initWithNibName:@"CardAddByHandViewController" bundle:nil];
    cardAddByHandViewController.delegate = self;
    
    [[CMUtil getAppDelegate].tabBarController.navigationController pushViewController:cardAddByHandViewController animated:YES];
    
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:cardAddByHandViewController];
//    
//    [navigationController setNavigationBarBackgroundImage:[UIImage imageNamed:@"nav.png"]];
    
//    [[CMUtil getAppDelegate].tabBarController presentModalViewController:navigationController animated:YES];
}

- (void)showImportView
{
    BillImportViewController *billImportViewController = [[BillImportViewController alloc] initWithNibName:@"BillImportViewController" bundle:nil];
    
    [[CMUtil getAppDelegate].tabBarController.navigationController pushViewController:billImportViewController animated:YES];
}

// 保存信用卡图片到本地
- (NSString *)saveCardImage
{
    if (!cardImage) return NO;
    
    NSData *imageData = UIImageJPEGRepresentation(cardImage, 0.1);//UIImagePNGRepresentation(cardImage);
    
    // 图片本地名称，未上传之前
    NSString *imageName = [NSString stringWithFormat:@"card%@.jpg", [NSString getDateString]];
    
    // 图片存储位置
    NSString *imagePath = [MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:imageName];
    
    if (![imageData writeToFile:imagePath atomically:YES]) {
        NSLog(@"保存图片不成功！");
        return nil;
    }
    
    return imagePath;
}

// 更改本地图片名称
- (BOOL)renameLocalCardImage:(NSString *)localImagePath remoteImagePath:(NSString *)remoteImagePath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL status = [fileManager moveItemAtPath:localImagePath toPath:remoteImagePath error:NULL];
    
    return status;
}

// 处理卡片
- (void)handleCreditCard:(CreditCard *)needAddCard
{
    NSString *imagePath = [self saveCardImage];
    
    if (imagePath) {
        // 上传图片到服务器
        NSMutableDictionary *uploadPicParams = [[NSMutableDictionary alloc] initWithCapacity:1];
        uploadPicParams[@"code"] = [CMUtil getCode];
     
        if (!engine)
            engine = [[CardEngine alloc] initWithHostName:kServerAddr];
        
        [engine uploadCreditCardImageFromFile:imagePath params:uploadPicParams onCompletion:^(NSString *urlString) {
            if ([urlString isEqualToString:Fail]) {
                [self quickAlertView:@"上传信用卡图片失败！"];
                return ;
            }
            
            // 上传图片成功
            needAddCard.remoteCardPic = urlString;
            // 更改本地图片名字，使得和服务器保存的一致
            NSString *remoteImageName = [urlString lastPathComponent];
            NSString *remoteImagePath = [MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:remoteImageName];
            if ([self renameLocalCardImage:imagePath remoteImagePath:remoteImagePath]) {
                needAddCard.cardPic = remoteImageName;
            } else {
                needAddCard.cardPic = [imagePath lastPathComponent];
            }
            
            // 获取卡片信息参数
            NSMutableDictionary *cardInfoParams = [NSMutableDictionary dictionaryWithCapacity:2];
            cardInfoParams[@"code"] = [CMUtil getCode];
            cardInfoParams[@"card_number"] = needAddCard.cardNumber;
            
            [engine getCardInfo:cardInfoParams onCompletion:^(CreditCard *card) {
                if (!card) {
                    [self quickAlertView:@"获取不到卡信息！"];
                }
                
                needAddCard.bankID = card.bankID;
                needAddCard.bankName = card.bankName;
                needAddCard.productID = card.productID;
                needAddCard.productName = card.productName;
                
                // 上传卡片到服务器
                NSMutableDictionary *addCardParams = [NSMutableDictionary dictionary];
                addCardParams[@"code"] = [CMUtil getCode];
                addCardParams[@"card_number"] = needAddCard.cardNumber;
                addCardParams[@"pic"] = urlString;
                addCardParams[@"owner_name"] = [CMUtil getUsername];
                if (needAddCard.bankID)
                    addCardParams[@"bank_id" ] = needAddCard.bankID;
                if (needAddCard.productID)
                    addCardParams[@"creditcard_product_id" ] = needAddCard.productID;
                if (needAddCard.reminderDay)
                    addCardParams[@"pay_day"] = needAddCard.reminderDay;
                else {
                    addCardParams[@"pay_day"] = @"1";
                    needAddCard.reminderDay = @1;
                }
                
                needAddCard.addingCard = NO;
                
                // fix retain cycle
                __block NSMutableArray *weakAddCards = needAddCards;
                __block UITableView *weakTableView = mTableView;
                __block NSMutableArray *weakCards = _cards;
                
                [engine addCreditCard:addCardParams onCompletion:^(NSDictionary *response) {
                    needAddCard.status = [[response stringForKey:@"status"] boolValue];
                    needAddCard.cardID = [response stringForKey:@"card_id"];
                    
                    // 添加卡片
                    [[SQLGlobal sharedInstance] updateCard:needAddCard];
                    [weakCards addObject:needAddCard];
                    [weakAddCards removeObject:needAddCard];
                    
                    [weakTableView reloadData];
                    weakAddCards = nil;
                    weakTableView = nil;
                } onError:^(NSError *error) {
                    [weakAddCards removeObject:needAddCard];
                    [weakTableView reloadData];
                    weakAddCards = nil;
                    weakTableView = nil;
                }];
                
            } onError:^(NSError *error) {
                [needAddCards removeObject:needAddCard];
                [mTableView reloadData];
            }];
            
        } onError:^(NSError *error) {
            
            NSLog(@"error = %@", error);
            [needAddCards removeObject:needAddCard];
            [mTableView reloadData];
        }];
    }
}

#pragma mark - Actions
- (IBAction)gotoUserSettings:(id)sender
{
    UserSettingViewController *userSettingViewController = [[UserSettingViewController alloc] initWithNibName:@"UserSettingViewController" bundle:nil];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:userSettingViewController];
    
    [navigationController setNavigationBarBackgroundImage:[UIImage imageNamed:@"nav.png"]];
    
    [[CMUtil getAppDelegate].tabBarController presentModalViewController:navigationController animated:YES];

}

- (IBAction)addCreditCard:(id)sender
{
    _scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    [_scanViewController setShowsFirstUseAlert:NO];
    _scanViewController.disableManualEntryButtons = YES;
    _scanViewController.collectCVV = NO;
    _scanViewController.collectExpiry = NO;
    _scanViewController.appToken = @"bf5aba62bc8443a8ad991cf4ee1ab61c";
    [[CMUtil getAppDelegate].tabBarController presentModalViewController:_scanViewController animated:YES];
    
    // 添加2个按钮
    if (importEBillButton == nil) {
        importEBillButton = [UIButton buttonWithType:UIButtonTypeCustom];
        importEBillButton.titleLabel.font = [UIFont systemFontOfSize:13.0];
        importEBillButton.frame = CGRectMake(98, 437, 107, 31);
        [importEBillButton setBackgroundImage:[UIImage imageNamed:@"import_btn.png"]
                           forState:UIControlStateNormal];
        [importEBillButton setTitle:@"电子账单添加"
                           forState:UIControlStateNormal];
        [importEBillButton addTarget:self
                              action:@selector(importEBill:)
                    forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (importEBillButton.superview == nil)
        [self.view.window addSubview:importEBillButton];
    
    if (addCardByHandButton == nil) {
        addCardByHandButton = [UIButton buttonWithType:UIButtonTypeCustom];
        addCardByHandButton.titleLabel.font = [UIFont systemFontOfSize:13.0];
        addCardByHandButton.frame = CGRectMake(209, 437, 107, 31);
        [addCardByHandButton setBackgroundImage:[UIImage imageNamed:@"import_btn.png"]
                                     forState:UIControlStateNormal];
        [addCardByHandButton setTitle:@"手工卡号添加"
                           forState:UIControlStateNormal];
        [addCardByHandButton addTarget:self
                                action:@selector(addCardByHand:)
                      forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (addCardByHandButton.superview == nil)
        [self.view.window addSubview:addCardByHandButton];
}

#pragma mark - Custom
- (void)removeCameraButtons
{
    if (importEBillButton.superview && importEBillButton) {
        [importEBillButton removeFromSuperview];
    }
    
    if (addCardByHandButton.superview && addCardByHandButton) {
        [addCardByHandButton removeFromSuperview];
    }
}

- (void)clickCreditCard:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    int index = btn.tag;
    
    if (index > _cards.count - 1)
        return;
    
    CardSettingViewController *cardSettingViewController = [[CardSettingViewController alloc] initWithNibName:@"CardSettingViewController" bundle:nil];
    cardSettingViewController.card = _cards[index];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:cardSettingViewController];
    
    selectedCardIndex = index;
    
    [navigationController setNavigationBarBackgroundImage:[UIImage imageNamed:@"nav.png"]];
    
    [[CMUtil getAppDelegate].tabBarController presentModalViewController:navigationController animated:YES];
}

#pragma mark - UITableViewDataSource methods
/****************************************************
 * 卡片算法
 * 3张本地卡片，3张正在添加卡片
 * tableview布局
 * 0: 标题
 * 1: 本地卡片1
 * 2: 本地卡片2
 * 3: 本地卡片3
 * 4: 正在添加卡片1
 * 5: 正在添加卡片2
 * 6: 正在添加卡片3
 * 7: 空位置，留待添加卡片
 * 8: 隐藏的位置，用于显示附加信息
 ****************************************************/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // 留一个位置显示空的卡片，留一个位置显示信息
    // 卡片个数由卡片和正在添加中的卡片个数组成
    return MAX(_cards.count + needAddCards.count + OTHER_CELL_NUMBER, MIN_CARD_NUMBER + OTHER_CELL_NUMBER);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WalletCell *cell = nil;
    
    // 添加信息
    if (_cards.count < MIN_CARD_NUMBER + OTHER_CELL_NUMBER - 2) {
        if (indexPath.row == MIN_CARD_NUMBER + OTHER_CELL_NUMBER - 1) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"BottomWalletCell"];
            
            if (!cell)
                cell = [WalletCell getBottomInstance];
            
            return cell;
        }
    } else {
        if (indexPath.row == _cards.count + OTHER_CELL_NUMBER - 1) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"BottomWalletCell"];
            
            if (!cell)
                cell = [WalletCell getBottomInstance];
            
            return cell;
        }
    }
    
    // 添加标题
    if (indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"HeaderWalletCell"];
        
        if (!cell)
            cell = [WalletCell getHeaderInstance];
        
        return cell;
    }
    
    cell = [tableView dequeueReusableCellWithIdentifier:indexPath.row == 1 ? @"TopWalletCell" : @"WalletCell"];
    
    if (!cell) {
        cell = (indexPath.row == 1 ? [WalletCell getTopInstance] : [WalletCell getNomalInstance]);
        
        [cell registerClickEventWithTarget:self selector:@selector(clickCreditCard:)];
    }
    
    CreditCard *card = nil;
    // 卡片加载由两部分组成：1.本地卡片，2.正在添加中的卡片
    if (_cards.count > indexPath.row - 1)
        card = _cards[indexPath.row - 1];
    else if (needAddCards.count > indexPath.row - 1 - _cards.count)
        card = needAddCards[indexPath.row - 1 - _cards.count];
    
    NSString *imagePath = [MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:[card.bankIcon lastPathComponent]];
    cell.bankLogoView.image = [UIImage imageWithContentsOfFile:imagePath];
    
    [cell loadCard:card index:indexPath.row - 1];
    
    return cell;
}

#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return 42.0f;
    
    if (_cards.count < MIN_CARD_NUMBER + OTHER_CELL_NUMBER - 2) {
        if (indexPath.row == MIN_CARD_NUMBER + OTHER_CELL_NUMBER - 1)
            return 201.0f;
        
        return indexPath.row == 1 ? 106.0f : 89.0f;
    } else {
        if (indexPath.row == _cards.count + OTHER_CELL_NUMBER - 1)
            return 201.0f;
        
        return indexPath.row == 1 ? 106.0f : 89.0f;
    }
}

#pragma mark - CardIOPaymentViewControllerDelegate methods
- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)scanViewController
{
    NSLog(@"User canceled payment info");
    [self removeCameraButtons];
    [scanViewController dismissModalViewControllerAnimated:YES];
    _scanViewController = nil;
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)scanViewController
{
    NSLog(@"Received card info. Number: %@, expiry: %02i/%i, cvv: %@.", info.cardNumber, info.expiryMonth, info.expiryYear, info.cvv);
    
    unsigned int count = 0;
    Ivar *ivars = class_copyIvarList([[scanViewController.viewControllers lastObject] class], &count);
    for (int i = 0; i < count; i++) {
        const char *cname = ivar_getName(ivars[i]);
        NSString *name = [[NSString alloc] initWithCString:cname encoding:NSUTF8StringEncoding];
        if ([name isEqualToString:@"cardImage"]) {
            cardImage = object_getIvar([scanViewController.viewControllers lastObject], ivars[i]);
            break;
        }
    }
    [self removeCameraButtons];
    
//    BOOL findCard = NO;
//    
//    for (CreditCard *card in self.cards) {
//        if ([card.cardNumber isEqualToString:info.cardNumber]) {
//            findCard = YES;
//            break;
//        }
//    }
//    
//    if (!findCard) {
//        for (CreditCard *card in needAddCards) {
//            if ([card.cardNumber isEqualToString:info.cardNumber]) {
//                findCard = YES;
//                break;
//            }
//        }
//    }
//
//    if (!findCard) {
//        // 处理添加卡片操作
//        CreditCard *needAddCard = [[CreditCard alloc] init];
//        needAddCard.cardNumber = info.cardNumber;
//        needAddCard.addingCard = YES;
//        
//        // 添加卡片到待添加卡片数组
//        [needAddCards addObject:needAddCard];
//        
//        // 刷新tableview
//        [mTableView reloadData];
//        
//        [self handleCreditCard:needAddCard];
//        needAddCard = nil;
//    } else
//        [self quickAlertView:@"此卡片已添加"];
    
    
//    // 处理添加卡片操作
//    CreditCard *needAddCard = [[CreditCard alloc] init];
//    needAddCard.cardNumber = info.cardNumber;
//    needAddCard.addingCard = YES;
//    
//    // 添加卡片到待添加卡片数组
//    [needAddCards addObject:needAddCard];
//    
//    // 刷新tableview
//    [mTableView reloadData];
//    
//    [self handleCreditCard:needAddCard];
//    needAddCard = nil;

    CardAddByHandViewController *cardAddByHandViewController = [[CardAddByHandViewController alloc] initWithNibName:@"CardAddByHandViewController" bundle:nil];
    cardAddByHandViewController.delegate = self;
    cardAddByHandViewController.cardImage = cardImage;
    cardAddByHandViewController.cardNumber = info.cardNumber;
    cardAddByHandViewController.title = @"信息完善";
    
    [[CMUtil getAppDelegate].tabBarController.navigationController pushViewController:cardAddByHandViewController animated:NO];
    
    [scanViewController dismissModalViewControllerAnimated:YES];
    _scanViewController = nil;
}

#pragma mark - CardAddByHandViewControllerDelegate methods
- (void)addCreditCardByHand:(CreditCard *)card
{
    if (!_cards)
        _cards = [[NSMutableArray alloc] init];
    
    [_cards addObject:card];
    
    [mTableView reloadData];

    [self loadNoneCardView];
}

#pragma mark - 
- (void)handleDeleteCard:(NSNotification *)notification
{
    [self.cards removeObjectAtIndex:selectedCardIndex];
    
    [mTableView reloadData];
    
    [self loadNoneCardView];
}

- (void)checkEmail:(id)email
{
    static CGFloat interval = 5;
    if ([email isKindOfClass:[Email class]])
    {
        NSTimer * timer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(checkEmail:) userInfo:email repeats:YES];
        [timer fire];
    }
    else 
    {
        NSTimer * timer = (NSTimer *)email;
        if (!needTimer)
        {
            [timer invalidate];
            return;
        }
        Email * mail = [timer userInfo];
        NSDictionary * params = @{@"code" : [CMUtil getCode], @"email_id" : mail.pk};
        NSURLRequest * request = [GTMUtil requestWithURLString:[NSString stringWithFormat:@"http://%@/api/email_check",kServerAddr] params:params HTTPType:@"get" urlEncode:NO];
        GTMHTTPFetcher * fetcher = [self.fetcherService fetcherWithRequest:request];
        
        NSMutableDictionary *properties = [[NSMutableDictionary alloc] init];
        properties[@"emailId"] = mail.pk;
        [fetcher setProperties:properties];
        [fetcher beginFetchWithDelegate:self didFinishSelector:@selector(checkFetcher:data:error:)];
        fetcher.userData = timer;
    }
}

- (void)checkFetcher:(GTMHTTPFetcher *)fetcher data:(NSData *)data error:(NSError *)error
{
    NSDictionary * jsonDict = [data objectFromJSONData];
    if ([[jsonDict stringForKey:@"status"] intValue] != 1)
    {
        NSLog(@"参数验证不通过报错");
        [fetcher.userData invalidate];
        [LvToast showWithText:@"邮箱设置不支持"];
    }
    else if ([[jsonDict stringForKey:@"data"] intValue] == 1)
    {
        NSLog(@"扫描完了");
        //扫描成功；
        [fetcher.userData invalidate];
        
        // 获取卡片
        if (!userEngine)
            userEngine = [[UserEngine alloc] initWithHostName:kServerAddr];
        
        NSMutableDictionary *userParams = [[NSMutableDictionary alloc] init];
        userParams[@"code"] = [CMUtil getCode];
        
        NSString *emailId = [fetcher propertyForKey:@"emailId"];
        NSMutableArray *emailList = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:IMPORT_BILL]];
        
        if (emailList) {
            [emailList removeObject:emailId];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:emailList forKey:IMPORT_BILL];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [userEngine getAllCards:userParams onCompletion:^(NSMutableArray *results) {
            
            for (CreditCard *card in results) {
                [[SQLGlobal sharedInstance] updateCard:card];
                
                if (card.cardPic) {
                    // 卡片图片
                    NSString *downloadURLString = [NSString stringWithFormat:@"assets/upload/cards/%@", card.cardPic];
                    NSString *imagePath = [MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:card.cardPic];
                    // download image
                    [userEngine downloadImage:downloadURLString destPath:imagePath onCompletion:^(BOOL status) {
                        [mTableView reloadData];
                    } onError:^(NSError *error) {
                    }];
                }
                
                if (card.bankIcon) {
                    // 卡片Icon
                    NSString *downloadIconURLString = [NSString stringWithFormat:@"assets/upload/%@", card.bankIcon];
                    
                    NSString *iconPath = [MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:[card.bankIcon lastPathComponent]];
                    [userEngine downloadImage:downloadIconURLString destPath:iconPath onCompletion:^(BOOL status) {
                        [mTableView reloadData];
                    } onError:^(NSError *error) {
                    }];
                }
            }
            
            self.cards = [[SQLGlobal sharedInstance] getLocalCards];
            
            if (needAddCards.count > 0)
                [needAddCards removeObjectAtIndex:0];
            
            [mTableView reloadData];
            
            [self loadNoneCardView];
            
        } onError:^(NSError *error) {
            NSLog(@"error = %@", error);
        }];
    }
    else
    {
        NSLog(@"没扫描到");
    }
}

@end
