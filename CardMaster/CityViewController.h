//
//  CityViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 13-1-7.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"

@protocol CitySelectDelegate <NSObject>

- (void)didSelectCity:(NSString *)cityName;

@end

@interface CityViewController : BaseNavigationController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate> {
    IBOutlet UITableView *mTableView;
    IBOutlet UISearchBar *mSearchBar;
}

@property (nonatomic, assign) id<CitySelectDelegate> delegate;

@end
