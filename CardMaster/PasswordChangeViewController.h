//
//  PasswordChangeViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 13-1-9.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"

@interface PasswordChangeViewController : BaseNavigationController<UIAlertViewDelegate> {
    __unsafe_unretained IBOutlet UITextField *oldPasswordTextField;
    __unsafe_unretained IBOutlet UITextField *passwordTextField;
    __unsafe_unretained IBOutlet UITextField *passwordAgainTextField;
    
}

- (IBAction)resetPassword:(id)sender;

@end
