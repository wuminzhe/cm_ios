//
//  DiscountDetailCell.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-18.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kAddressLabelWidth                  166
#define kDetailLabelWidth                   274

@protocol DiscountDetailCellDelegate <NSObject>

@optional
- (void)changeToMapView:(id)sender;
- (void)takePhoneCall:(id)sender;

@end

@interface DiscountDetailCell : UITableViewCell

@property (nonatomic, unsafe_unretained) IBOutlet UILabel *discountTitleLabel;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *discountTitle2Label;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *discountTextLabel;
@property (nonatomic, unsafe_unretained) IBOutlet UIImageView *bankIconView;
@property (nonatomic, unsafe_unretained) IBOutlet UIImageView *separatorImageView;

@property (nonatomic, assign) id <DiscountDetailCellDelegate> delegate;

- (IBAction)changeToMapView:(id)sender;
- (IBAction)takePhoneCall:(id)sender;

+ (DiscountDetailCell *)getEffectiveDateCell;
+ (DiscountDetailCell *)getBankCell;
+ (DiscountDetailCell *)getAddressCell;
+ (DiscountDetailCell *)getPhoneCell;
+ (DiscountDetailCell *)getDetailCell;

@end
