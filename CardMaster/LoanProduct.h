//
//  LoanProduct.h
//  CardMaster
//
//  Created by wenjun on 13-1-10.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoanProduct : NSObject

@property (strong,nonatomic) NSString * pk;
@property (strong,nonatomic) NSString * name;
@property (strong,nonatomic) NSString * amount;//额度
@property (strong,nonatomic) NSString * rate;//利率
@property (strong,nonatomic) NSString * applyCount;
@property (strong,nonatomic) NSString * deadLine;//期限
@property (strong,nonatomic) NSString * applyDuration;
@property (strong,nonatomic) NSString * suitableUsers;
@property (strong,nonatomic) NSString * condition;//条件
@property (strong,nonatomic) NSString * suggestURL;

@property (strong,nonatomic) NSString * imageName;
@property (strong,nonatomic) UIImage * image;
@property (nonatomic,getter = isLoadingImg) BOOL loadingImg;

@end
