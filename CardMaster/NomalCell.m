//
//  NomalCell.m
//  CardMaster
//
//  Created by wenjun on 13-1-10.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "NomalCell.h"

@interface NomalCell()
{
    IBOutlet UILabel * label1;
    IBOutlet UILabel * label2;
}
@end

@implementation NomalCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (NomalCell *)getInstance
{
    return [[[NSBundle mainBundle] loadNibNamed:@"NomalCell" owner:nil options:nil] lastObject];
}

- (void)setTitle:(NSString *)title description:(NSString *)desc
{
    label1.text = title;
    label2.text = desc;
}

@end
