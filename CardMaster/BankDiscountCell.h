//
//  BankDiscountCell.h
//  CardMaster
//
//  Created by Zhang Ming on 13-1-6.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"

@interface BankDiscountCell : UITableViewCell

@property (nonatomic, strong) IBOutlet EGOImageView *bankImageView;

@property (nonatomic, unsafe_unretained) IBOutlet UIButton *deleteButton;

+ (BankDiscountCell *)getInstance;

@end
