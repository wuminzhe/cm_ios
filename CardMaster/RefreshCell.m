//
//  RefreshCell.m
//  LoveShopping
//
//  Created by wenjun on 12-11-15.
//
//

#import "RefreshCell.h"

@implementation RefreshCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (RefreshCell *)getInstance
{
    return [[[NSBundle mainBundle] loadNibNamed:@"RefreshCell" owner:nil options:nil] lastObject];
}

- (void)animate
{
    [indicator setHidesWhenStopped:YES];
    [indicator setHidden:NO];
    [indicator startAnimating];
}

@end
