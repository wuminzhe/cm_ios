//
//  SwitchCard.m
//  CardMaster
//
//  Created by wenjun on 13-1-18.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "SwitchCard.h"
#import <QuartzCore/QuartzCore.h>
#import "CMConstants.h"

@interface SwitchCard() <UIScrollViewDelegate>
{
    IBOutlet UIScrollView * scroll;
    IBOutlet UIView * frontView;
    IBOutlet UIView * backView;
    IBOutlet UIView * backPart;
}

@property (unsafe_unretained,nonatomic) CreditCard * card;

@end

@implementation SwitchCard

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)initialize
{
    scroll.contentSize = CGSizeMake(scroll.frame.size.width * 2 - 30, scroll.frame.size.height);
//    scroll.layer.cornerRadius = 13;
//    scroll.clipsToBounds = YES;
//    
//    backPart.layer.cornerRadius = 13;
//    backPart.clipsToBounds = YES;
    self.backBtn.userInteractionEnabled = NO;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    backPart.frame = CGRectMake(1, 2, backPart.frame.size.width, backPart.frame.size.height);
    [self insertSubview:backPart atIndex:0];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    backPart.frame = CGRectMake(scrollView.contentOffset.x, 0, backPart.frame.size.width, backPart.frame.size.height);
    [scrollView insertSubview:backPart atIndex:0];
    self.backBtn.userInteractionEnabled = scrollView.contentOffset.x > 0;
    self.frontBtn.userInteractionEnabled = scrollView.contentOffset.x == 0;
    self.card.swiped = scroll.contentOffset.x != 0;
}

+ (SwitchCard *)getInstance
{
    SwitchCard * switchCard = [[[NSBundle mainBundle] loadNibNamed:@"SwitchCard" owner:nil options:nil] lastObject];
    [switchCard initialize];
    return switchCard;
}

- (void)loadCard:(CreditCard *)card
{
    self.card = card;
    
    if (!card.billDay)
    {
        self.hidden = YES;
        return;
    }
    self.hidden = NO;
    UIImage * backImg = nil;
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd"];
    NSInteger day = [[formatter stringFromDate:[NSDate date]] intValue];
    NSInteger billDay = [card.billDay intValue];
    if (day < billDay)
    {
        day += 30;
    }
    if (day < billDay + 10)
    {
        backImg = [UIImage imageNamed:@"green_bg"];
    }
    else if (day < billDay + 20)
    {
        backImg = [UIImage imageNamed:@"yellow_bg"];
    }
    else
    {
        backImg = [UIImage imageNamed:@"red_bg"];
    }
    [self.backBtn setImage:backImg forState:UIControlStateNormal];
    
    if ([card.bankID isEqualToString:@"cmbchina"] || [card.bankID isEqualToString:@"boc"]) {
        [self.frontBtn setImage:[UIImage imageNamed:@"cmbchina.png"]
                    forState:UIControlStateNormal];
    } else if ([card.bankID isEqualToString:@"ccb"]) {
        [self.frontBtn setImage:[UIImage imageNamed:@"ccb.png"]
                    forState:UIControlStateNormal];
    } else if ([card.bankID isEqualToString:@"abchina"]) {
        [self.frontBtn setImage:[UIImage imageNamed:@"abchina.png"]
                    forState:UIControlStateNormal];
    } else if ([card.bankID isEqualToString:@"bankcomm"]) {
        [self.frontBtn setImage:[UIImage imageNamed:@"bankcomm.png"]
                    forState:UIControlStateNormal];
    } else {
        [self.frontBtn setImage:[UIImage imageNamed:@"bank_default.png"]
                    forState:UIControlStateNormal];
    }
    
    NSString *imagePath = [MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:[card.bankIcon lastPathComponent]];
    self.frontIcon.image = [UIImage imageWithContentsOfFile:imagePath];
    
    if (card.productName.length > 0)
    {
        self.frontLabel.text = card.productName;
    }
    else
    {
        self.frontLabel.text = [NSString stringWithFormat:@"%@    %@", card.bankName, [card.cardNumber substringFromIndex:card.cardNumber.length - 4]];
    }
    
    scroll.contentOffset = card.swiped ? CGPointMake(scroll.contentSize.width - scroll.bounds.size.width, 0) : CGPointZero;
    [self scrollViewDidEndDecelerating:scroll];
}

+ (SwitchCard *)getNormalInstance
{
    SwitchCard * switchCard = [[[NSBundle mainBundle] loadNibNamed:@"SwitchCard" owner:nil options:nil] lastObject];
    [switchCard initialize];
    UIImageView * imgView = [[UIImageView alloc] initWithFrame:CGRectMake(roundf((switchCard.bounds.size.width - 320) / 2) , switchCard.bounds.size.height - 89, 320, 89)];
    imgView.image = [UIImage imageNamed:@"mycard_wallet_empty"];
    [switchCard addSubview:imgView];
    return switchCard;
}

+ (SwitchCard *)getTopInstance
{
    SwitchCard * switchCard = [[[NSBundle mainBundle] loadNibNamed:@"SwitchCard" owner:nil options:nil] lastObject];
    [switchCard initialize];
    UIImageView * imgView = [[UIImageView alloc] initWithFrame:CGRectMake(roundf((switchCard.bounds.size.width - 320) / 2) , switchCard.bounds.size.height - 106, 320, 106)];
    imgView.image = [UIImage imageNamed:@"mycard_wallet_top1"];
    [switchCard addSubview:imgView];
    return switchCard;
}

@end
