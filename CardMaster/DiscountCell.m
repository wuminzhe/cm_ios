//
//  DiscountCell.m
//  CardMaster
//
//  Created by Zhang Ming on 12-12-27.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "DiscountCell.h"
#import "Discount.h"
#import "CMConstants.h"
#import "CMUtil.h"


@implementation DiscountCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Properties methods
- (void)setDiscount:(Discount *)discount
{
    _discount = discount;
    
    [self setupDiscount];
}

#pragma mark - Private methods
- (void)setupDiscount
{
    nameLabel.text = _discount.title;
    averageTextLabel.text = _discount.shopAverageConsumption;
    
    if (averageTextLabel.text.length == 0) {
        averageTextLabel.text = @"不详";
        averageImageView.hidden = YES;
    } else {
        averageImageView.hidden = NO;
    }
    
    effectiveLabel.text = _discount.endTime;
    
    NSInteger hot = [_discount.hot integerValue];
    
    if (hot > 1000)
        favoriteTextLabel.text = [NSString stringWithFormat:@"%.1fk", hot / 1000.0];
    else
        favoriteTextLabel.text = [NSString stringWithFormat:@"%d", hot];
    
    if (_discount.rate) {
        discount1Label.hidden = NO;
        discount2Label.hidden = NO;
        discountTextLabel.hidden = YES;
        discount1Label.text = _discount.rate;
    } else {
        discount1Label.hidden = YES;
        discount2Label.hidden = YES;
        discountTextLabel.hidden = NO;
    }
    
    NSString *bankIcon = [NSString stringWithFormat:@"bank_%@.png", _discount.bankID];
    bankIconView.image = [UIImage imageNamed:bankIcon];
    
    NSString *urlString = [_discount.pic stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    // 添加用于收藏页面
    NSString *imageName = [NSString stringWithFormat:@"%@_%@", _discount.bankID, [_discount.pic lastPathComponent]];
    
    NSString *imagePath = [MTLP(DISCOUNT_IMAGE_FILE) stringByAppendingPathComponent:imageName];
    
    UIImage *img = [UIImage imageWithContentsOfFile:imagePath];
    
    if (img) {
        discountImageView.image = img;
    } else {
    
        discountImageView.placeholderImage = [UIImage imageNamed:@"shop_discount_none.png"];
        discountImageView.imageURL = [NSURL URLWithString:urlString];
    }
    
    CLLocationDistance distance = [_discount.distance doubleValue];
    
    if (distance == 0) {
        distanceTextLabel.hidden = YES;
        distanceTitleLabel.hidden = YES;
        distanceImageView.hidden = YES;
    } else {
        distanceTextLabel.hidden = NO;
        distanceTitleLabel.hidden = NO;
        distanceImageView.hidden = NO;
        
        
        if (distance > 1000)
            distanceTextLabel.text = [NSString stringWithFormat:@"%.1f千米", distance / 1000];
        else
            distanceTextLabel.text = [NSString stringWithFormat:@"%d米", (int)distance];
    }
}

#pragma mark - methods
+ (DiscountCell *)getInstance
{
    DiscountCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"DiscountCell" owner:nil options:nil] lastObject];
    
    
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:
                                   [UIImage imageNamed:@"discount_cell_selected.png"]];
    return cell;
}

@end
