//
//  BankDiscountCell.m
//  CardMaster
//
//  Created by Zhang Ming on 13-1-6.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BankDiscountCell.h"

@implementation BankDiscountCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (BankDiscountCell *)getInstance
{
    BankDiscountCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"BankDiscountCell" owner:nil options:NULL] lastObject];
    cell.bankImageView.placeholderImage = [UIImage imageNamed:@"bank_discount_none.png"];
    
    return cell;
}

@end
