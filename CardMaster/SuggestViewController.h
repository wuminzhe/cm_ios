//
//  SuggestViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-26.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"
#import "MZBubbleTableView.h"

@interface SuggestViewController : BaseNavigationController<MZBubbleTableViewDataSource, UITextFieldDelegate, UIScrollViewDelegate> {
    IBOutlet MZBubbleTableView *bubbleTableView;
    IBOutlet UIView *chatInputView;
    IBOutlet UITextField *messageTextField;
}

- (IBAction)sendMessage:(id)sender;

@end
