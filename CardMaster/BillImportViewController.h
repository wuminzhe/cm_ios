//
//  BillImportViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 13-1-8.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"
#import "Email.h"

//todo 修改邮箱按钮图片
@interface BillImportViewController : BaseNavigationController

@property (strong,nonatomic) NSString * cardID;
@property (strong,nonatomic) Email * email;

- (void)showPickerView:(id)sender;

- (IBAction)choose:(id)sender;
- (IBAction)hidePickerView:(id)sender;

@end
