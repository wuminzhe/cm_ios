//
//  LastCell.m
//  CardMaster
//
//  Created by wenjun on 13-1-10.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "LastCell.h"

@interface LastCell()
{
    IBOutlet UILabel * topLabel;
    IBOutlet UILabel * bottomLabel;
}

@end

@implementation LastCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setTitle:(NSString *)title description:(NSString *)desc
{
    topLabel.text = title;
    [[self class] reSizeLabel:bottomLabel withText:desc rowHeight:18];
}

+ (LastCell *)getInstance
{
    LastCell * cell = [[[NSBundle mainBundle] loadNibNamed:@"LastCell" owner:nil options:nil] lastObject];
    return cell;
}

+ (NSInteger)cellHeightForDesc:(NSString *)desc
{
    return 54 + [[self class] heightForText:desc font:[UIFont systemFontOfSize:14] width:300 rowHeight:18];
}

+ (NSInteger)heightForText:(NSString *)text font:(UIFont *)font width:(NSInteger)width rowHeight:(NSInteger)rowHeight
{
    if (text.length == 0)
    {
        return rowHeight;
    }
    CGSize size = [text sizeWithFont:font constrainedToSize:CGSizeMake(width, 1000)];
    return roundf(size.height / rowHeight) * rowHeight;
}

+ (void)reSizeLabel:(UILabel *)label withText:(NSString *)text rowHeight:(NSInteger)rowHeight
{
    label.text = text;
    if (text.length == 0)
    {
        label.frame = CGRectMake(label.frame.origin.x, label.frame.origin.y, label.bounds.size.width,rowHeight);
        return;
    }
    CGSize size = [text sizeWithFont:label.font constrainedToSize:CGSizeMake(label.bounds.size.width, 1000) lineBreakMode:label.lineBreakMode];
    label.frame = CGRectMake(label.frame.origin.x, label.frame.origin.y, label.bounds.size.width, roundf(size.height / rowHeight) * rowHeight);
}

@end
