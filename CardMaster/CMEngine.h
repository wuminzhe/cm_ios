//
//  CMEngine.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-23.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MKNetworkEngine.h"

@class GEOInfo;

@interface CMEngine : MKNetworkEngine

typedef void (^InitializeResponseBlock) (BOOL status);
typedef void (^GetBanksUpdateResponseBlock) (BOOL status);
typedef void (^GetBanksResponseBlock) (NSMutableArray *results);
typedef void (^GetGeoResponseBlock) (GEOInfo *result);

- (void)initializeClient:(NSDictionary *)params
            onCompletion:(InitializeResponseBlock)completionBlock
                 onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)getBanksUpdateStatus:(NSMutableDictionary *)params
                                onCompletion:(GetBanksUpdateResponseBlock)completionBlock
                                     onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)getBanks:(NSMutableDictionary *)params
                    onCompletion:(GetBanksResponseBlock)completionBlock
                         onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)getGeoInfo:(NSMutableDictionary *)params
                      onCompletion:(GetGeoResponseBlock)completionBlock
                           onError:(MKNKErrorBlock)errorBlock;

@end
