//
//  BillEngine.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-2.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MKNetworkEngine.h"

@interface BillEngine : MKNetworkEngine

typedef void (^SMSUpdateStatusResponseBlock) (BOOL status);
typedef void (^SMSResponseBlock) (BOOL status);

- (MKNetworkOperation *)getSMSUpdateStatus:(NSMutableDictionary *)params
                              onCompletion:(SMSUpdateStatusResponseBlock)completionBlock
                                   onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)getSMSData:(NSMutableDictionary *)params
                      onCompletion:(SMSResponseBlock)completionBlock
                           onError:(MKNKErrorBlock)errorBlock;

@end
