//
//  EmailListController.m
//  CardMaster
//
//  Created by wenjun on 13-1-15.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "EmailListController.h"
#import "LoadingView.h"
#import "CMUtil.h"
#import "LvToast.h"
#import "GTMUtil.h"
#import "JSONKit.h"
#import "Email.h"
#import "NSDictionary+Extension.h"
#import "EmailCell.h"
#import "BillImportViewController.h"
#import "GTMHTTPFetcherService.h"

@interface EmailListController () <UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    IBOutlet UITableView * table;
    IBOutlet LoadingView * loadingView;
}

@property (strong,nonatomic) NSMutableArray * emails;
//@property (unsafe_unretained,nonatomic) GTMHTTPFetcher * fetcher;
@property (strong,nonatomic) GTMHTTPFetcherService * fetcherService;

@end

@implementation EmailListController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"电子账单";
    
    [loadingView setText:@"正在加载"];
    
    self.emails = [NSMutableArray array];
    
    self.fetcherService = [[GTMHTTPFetcherService alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.emails.count == 0)
    {
        [loadingView start];
        table.hidden = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.emails.count == 0)
    {
        [self performSelector:@selector(fetchEmails) withObject:nil afterDelay:.5];
    }
}

- (void)fetchEmails
{
    GTMHTTPFetcher * fetcher = [self.fetcherService fetcherWithRequest:[GTMUtil requestWithURLString:[NSString stringWithFormat:@"http://%@/api/email_list",kServerAddr] params:@{@"code" : [CMUtil getCode]} HTTPType:@"get" urlEncode:NO]];
    [fetcher beginFetchWithDelegate:self didFinishSelector:@selector(emailsFetcher:data:error:)];
}

- (void)emailsFetcher:(GTMHTTPFetcher *)fetcher data:(NSData *)data error:(NSError *)error
{
    BOOL success = NO;
    if (fetcher.statusCode == 200)
    {
        [self.emails removeAllObjects];
        NSArray * jsonArray = [[[data objectFromJSONData] objectForKey:@"data"] objectForKey:@"emails"];
        for (NSDictionary * dict in jsonArray)
        {
            Email * email = [[Email alloc] init];
            email.emailName = [dict stringForKey:@"email"];
            email.pk = [dict stringForKey:@"id"];
            email.password = [dict stringForKey:@"password"];
            [self.emails addObject:email];
        }
        success = YES;
    }
    if (success)
    {
        [table reloadData];
        if (self.emails.count == 0)
        {
            [LvToast showWithText:@"没有数据" duration:1];
        }
    }
    else
    {

        [LvToast showWithText:@"请求失败!" duration:1];
    }
    [loadingView stopWithAnimation];
    table.hidden = NO;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.emails.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EmailCell * cell = (EmailCell *)[tableView dequeueReusableCellWithIdentifier:@"EmailCell"];
    if (!cell)
    {
        cell = [EmailCell getInstance];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    [cell loadEmail:[self.emails objectAtIndex:indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Email * email = [self.emails objectAtIndex:indexPath.row];
    BillImportViewController * updateEmailController = [[BillImportViewController alloc] init];
    updateEmailController.email = email;
    [self.navigationController pushViewController:updateEmailController animated:YES];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle==UITableViewCellEditingStyleDelete)
    {
        Email * email = [self.emails objectAtIndex:indexPath.row];
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"删除邮箱" message:[NSString stringWithFormat:@"确认删除‘%@’",email.emailName] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"删除", nil];
        alert.tag = indexPath.row;
        [alert show];
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [self deleteEmailAtIndex:alertView.tag];
    }
}

- (void)deleteEmailAtIndex:(NSInteger)index
{
    Email * email = [self.emails objectAtIndex:index];
    GTMHTTPFetcher * fetcher = [self.fetcherService fetcherWithRequest:[GTMUtil requestWithURLString:[NSString stringWithFormat:@"http://%@/api/email_delete",kServerAddr] params:@{@"code" : [CMUtil getCode], @"email_id" : email.pk} HTTPType:@"post" urlEncode:NO]];
//    NSLog(@"%@",[CMUtil getCode]);
//    NSLog(@"%@",email.pk);
    [fetcher setProperty:[NSString stringWithFormat:@"%d",index] forKey:@"index"];
    [fetcher beginFetchWithDelegate:self didFinishSelector:@selector(deleteFetcher:data:error:)];
}

- (void)deleteFetcher:(GTMHTTPFetcher *)fetcher data:(NSData *)data error:(NSError *)error
{
    BOOL success = NO;
//    NSLog(@"%@",[NSString stringWithUTF8String:data.bytes]);
    if (fetcher.statusCode == 200)
    {
        NSDictionary * jsonDict = [data objectFromJSONData];
        if ([[jsonDict stringForKey:@"status"] intValue] == 1)
        {
            success = YES;
        }
    }
    NSLog(@"%@",fetcher.mutableRequest.URL.absoluteString);
    NSLog(@"%d",fetcher.statusCode);
    if (success)
    {
        NSInteger index = [[fetcher propertyForKey:@"index"] intValue];
        Email *email = [self.emails objectAtIndex:index];
        [self.emails removeObjectAtIndex:index];
        
        NSMutableArray *emailList = [NSMutableArray  arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:IMPORT_BILL]];
        [emailList removeObject:email.pk];
        
        [[NSUserDefaults standardUserDefaults] setObject:emailList forKey:IMPORT_BILL];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [table deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        [LvToast showWithText:@"删除失败!" duration:1];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)backAction
{
    [self.fetcherService stopAllFetchers];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)checkEmail:(Email *)email
{
    static CGFloat interval = 3;
    static BOOL hasTimer = NO;
    if (!hasTimer)
    {
        NSTimer * timer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(checkEmail:) userInfo:email repeats:YES];
        hasTimer = YES;
        [timer fire];
    }
    else
    {
        NSTimer * timer = (NSTimer *)email;
        Email * mail = [timer userInfo];
        NSDictionary * params = @{@"code" : [CMUtil getCode], @"email_id" : mail.pk};
        GTMHTTPFetcher * fetcher = [GTMUtil fetcherWithURLString:[NSString stringWithFormat:@"http://%@/api/email_check",kServerAddr] params:params HTTPType:@"get" urlEncode:NO];
        [fetcher beginFetchWithCompletionHandler:^(NSData *data, NSError *error){
            NSDictionary * jsonDict = [data objectFromJSONData];
            if ([[jsonDict stringForKey:@"status"] intValue] != 1)
            {
                NSLog(@"参数验证不通过报错");
                [timer invalidate];
                [LvToast showWithText:@"邮箱设置不支持" duration:1];
                hasTimer = NO;
            }
            else if ([[jsonDict stringForKey:@"data"] intValue] == 1)
            {
                NSLog(@"扫描完了");
                //扫描成功；
                [timer invalidate];
                hasTimer = NO;
            }
            else
            {
                NSLog(@"没扫描到");
            }
        }];
    }
}

@end
