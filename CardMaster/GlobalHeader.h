//
//  GlobalHeader.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-19.
//  Copyright (c) 2012年 lver. All rights reserved.
//

// 服务器地址
#define kServerAddr         @"client.51ika.com"//@"client.51ika.com" //@"192.168.0.70"


// 软件下载地址
#define kAppURL             @"http://aaaa"

// 新浪微博
//#define kSinaAppKey         @"3801945043"
//#define kSinaAppSecret      @"53d6ad49b51880efd70e99a4fbb299fe"
#define kSinaAppRedirect    @"http://www.51ika.com"

// 腾讯微博
//#define kTencentAppKey      @"801184473"
//#define kTencentAppSecret   @"2319fbee0ae70653e2277a3369c7a58c"
#define kTencentAppRedirect @"http://www.51ika.com"


/************************************/
#define Fail                @"Fail"
#define SUCCESS             @"SUCCESS"
#define USER_TEST           0

#define IMPORT_BILL         @"IMPORT_BILL"

// 本地资源文件夹
#define MergeToLocalPath(name) [NSString stringWithFormat:@"%@/Documents/local/%@",NSHomeDirectory(),name]
#define MTLP MergeToLocalPath

// 缓存资源文件夹
#define MergeToCachePath(name) [NSString stringWithFormat:@"%@/Documents/cache/%@",NSHomeDirectory(),name]
#define MTCP MergeToCachePath

static BOOL copyCacheToLocal(NSString *name)
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:MTCP(name)])
        return NO;
    
    return [[NSFileManager defaultManager] copyItemAtPath:MTCP(name) toPath:MTLP(name) error:nil];
}

#define CCTL CopyCacheToLocal