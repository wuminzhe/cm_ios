//
//  RefreshList.m
//  LifeShanghai
//
//  Created by wenjun on 12-11-30.
//
//

#import "RefreshList.h"
#import "EGORefreshTableHeaderView.h"

@interface RefreshList() <UITableViewDataSource,UITableViewDelegate,EGORefreshTableHeaderDelegate>
{
    NSUInteger totalCount;
    BOOL topReloading;
    BOOL bottomReloading;
    
    BOOL animating;
    NSInteger animateIndex;
}

@property (strong,nonatomic) UITableView * table;
@property (strong,nonatomic) EGORefreshTableHeaderView * ego;
@property (strong,nonatomic) UIImageView * backgroundImageView;
@property (strong,nonatomic) NSDate * updateTime;

@property (strong,nonatomic) NSTimer * animateTimer;

@end

@implementation RefreshList

- (void)initialize
{
    self.table = [[UITableView alloc] initWithFrame:self.bounds];
    self.table.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.table.delegate = self;
    self.table.dataSource = self;
    
    [self addSubview:self.table];
    self.ego = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.bounds.size.height, self.frame.size.width, self.bounds.size.height)];
    self.ego.delegate = self;
    [self.table addSubview:self.ego];
    
    _table.backgroundColor = self.backgroundColor;
    _ego.backgroundColor = self.backgroundColor;
    _topRefreshEnabled = YES;
    _bottomRefreshEnabled = YES;
    _firstPageNeedAnimation =YES;
    _firstPageAnimation = UITableViewRowAnimationRight;
}

- (UITableView *)innerTable
{
    return self.table;
}

#pragma EGO

- (void)egoDown
{
	[self.ego egoRefreshScrollViewDataSourceDidFinishedLoading:self.table];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.topRefreshEnabled)
    {
        [self.ego egoRefreshScrollViewDidScroll:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (self.topRefreshEnabled)
    {
        [self.ego egoRefreshScrollViewDidEndDragging:scrollView];
    }
    else if (scrollView.contentOffset.y < - 65)
    {
        if ([self.delegate respondsToSelector:@selector(topDragTriggered)])
        {
            [self.delegate topDragTriggered];
        }
    }
}

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
    if (!topReloading)
    {
        if (!bottomReloading && !animating)
        {
            topReloading = YES;
            [(id)self.delegate performSelector:@selector(topRefreshInRefreshList:) withObject:self afterDelay:.5];
        }
        else
        {
            [self egoDown];
        }
    }
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
    return topReloading;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
	return self.updateTime;
}

#pragma table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.page == 0)
    {
        return 0;
    }
    if (animating)
    {
        return animateIndex + 1;
    }
    if (self.bottomRefreshEnabled && !self.bottomOver)
    {
        return totalCount + 1;
    }
    return totalCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < totalCount)
    {
        return [self.delegate refreshList:self cellForRowAtIndex:indexPath.row];
    }
    else
    {
        if (self.bottomRefreshEnabled && !bottomReloading && !topReloading && !animating)
        {
            bottomReloading = YES;
            [(id)self.delegate performSelector:@selector(bottomRefreshInRefreshList:) withObject:self afterDelay:.5];
        }
        return [self.delegate refreshCellForRefreshList:self];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < totalCount)
    {
        if ([self.delegate respondsToSelector:@selector(refreshList:didSelectRowAtIndex:)])
        {
            [self.delegate refreshList:self didSelectRowAtIndex:indexPath.row];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < totalCount && [self.delegate respondsToSelector:@selector(refreshList:heightForRowAtIndex:)])
    {
        return [self.delegate refreshList:self heightForRowAtIndex:indexPath.row];
    }
    return 44;
}

#pragma update

- (void)firstPageAnimate
{
    if (animating)
    {
        if (animateIndex == totalCount - 1 || self.table.contentSize.height > self.frame.size.height)
        {
            if (self.bottomRefreshEnabled && self.table.contentSize.height < self.frame.size.height)
            {
                animating = NO;
                [self.table insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:totalCount inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
            }
            [self stopAnimatingFirstPage];
            return;
        }
        animateIndex ++;
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:animateIndex inSection:0];
        [self.table insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:self.firstPageAnimation];
    }
    else
    {
        [self stopAnimatingFirstPage];
    }
}


- (void)stopAnimatingFirstPage
{
    [self.animateTimer invalidate];
    self.animateTimer = nil;
    if (animating)
    {
        animating = NO;
        [self.table reloadData];
    }
}


- (void)topOverWithNumber:(NSUInteger)number finished:(BOOL)finished
{
    [self egoDown];
    if (finished)
    {
        self.updateTime = [NSDate date];
        [self.ego refreshLastUpdatedDate];
        if (self.page == 0)
        {
            _bottomOver = (number == 0);
        }
        if (number > 0)
        {
            totalCount = [self.delegate numberOfRowsInRefreshList:self];
            if (self.page == 0)
            {
                _page = 1;
                if (self.firstPageNeedAnimation)
                {
                    animating = YES;
                    animateIndex = -1;
                    self.animateTimer = [NSTimer scheduledTimerWithTimeInterval:.21 target:self selector:@selector(firstPageAnimate) userInfo:nil repeats:YES];
                }
                else
                {
                    [self.table reloadData];
                }
            }
            else
            {
                NSRange range = NSMakeRange(0, number);
                NSArray * indexPaths = [[self class] indexPathsFromRange:range];
                [self.table insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }
    }
    topReloading = NO;
}

+ (NSMutableArray *)indexPathsFromRange:(NSRange)range
{
    if (range.length > 0)
    {
        NSMutableArray * indexPaths = [NSMutableArray arrayWithCapacity:range.length];
        for (int i = 0; i < range.length; i++)
        {
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:range.location + i inSection:0];
            [indexPaths addObject:indexPath];
        }
        return indexPaths;
    }
    return nil;
}

- (void)overBottom
{
    _bottomOver = YES;
    if ([self.table numberOfRowsInSection:0] > totalCount)
    {
        [self.table deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:totalCount inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)bottomOverWithNumber:(NSUInteger)number finished:(BOOL)finished
{
    if (finished)
    {
        if (number > 0)
        {
            totalCount = [self.delegate numberOfRowsInRefreshList:self];
            _page ++;
            NSArray * indexPaths = [[self class] indexPathsFromRange:NSMakeRange(totalCount - number, number)];
            [self.table insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        }
        else 
        {
            [self overBottom];
        }
    }
    bottomReloading = NO;
}


- (void)setTopRefreshEnabled:(BOOL)topRefreshEnabled
{
    _topRefreshEnabled = topRefreshEnabled;
    self.ego.hidden = !_topRefreshEnabled;
}

- (void)setBottomRefreshEnabled:(BOOL)bottomRefreshEnabled
{
    _bottomRefreshEnabled = bottomRefreshEnabled;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initialize];
    }
    return self;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:backgroundColor];
    [self.table setBackgroundColor:backgroundColor];
    [self.ego setBackgroundColor:backgroundColor];
}

- (void)setBackgroundImage:(UIImage *)backgroundImage
{
    self.backgroundImageView.image = backgroundImage;
}

- (void)reloadRowsAtIndexes:(NSArray *)indexes withRowAnimation:(UITableViewRowAnimation)animation
{
    [_table reloadRowsAtIndexPaths:[self indexPaths:indexes] withRowAnimation:animation];
}

- (void)insertRowsAtIndexes:(NSArray *)indexes withRowAnimation:(UITableViewRowAnimation)animation
{
    [_table insertRowsAtIndexPaths:[self indexPaths:indexes] withRowAnimation:animation];
}

- (void)deleteRowsAtIndexes:(NSArray *)indexes withRowAnimation:(UITableViewRowAnimation)animation
{
    [_table deleteRowsAtIndexPaths:[self indexPaths:indexes] withRowAnimation:animation];
}

- (void)reloadVisibleRows
{
    [_table reloadRowsAtIndexPaths:_table.indexPathsForVisibleRows withRowAnimation:UITableViewRowAnimationNone];
}

- (void)reloadData
{
    totalCount = [self.delegate numberOfRowsInRefreshList:self];
    [_table reloadData];
}

- (NSArray *)indexPaths:(NSArray *)indexes
{
    NSMutableArray * indexpaths = nil;
    if (indexes.count > 0)
    {
        indexpaths = [NSMutableArray arrayWithCapacity:indexes.count];
        for (NSString * indexStr in indexes)
        {
            [indexpaths addObject:[NSIndexPath indexPathForRow:[indexStr intValue] inSection:0]];
        }
    }
    return indexpaths;
}

- (UITableViewCell *)dequeueReusableCellWithIdentifier:(NSString *)identifier
{
    return [self.table dequeueReusableCellWithIdentifier:identifier];
}

- (void)clear
{
    _page = 0;
    totalCount = 0;
    [self stopAnimatingFirstPage];
    [self egoDown];
    animateIndex = -1;
    _bottomOver = NO;
    topReloading = NO;
    bottomReloading = NO;
    self.updateTime = nil;
    [_table reloadData];
}

@end
