//
//  UserEngine.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-27.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "UserEngine.h"
#import "NSDictionary+Extension.h"
#import "CreditCard.h"
#import "Reminder.h"
#import "Discount.h"

@implementation UserEngine

- (MKNetworkOperation *)getAllCards:(NSMutableDictionary *)params
                       onCompletion:(GetUserInfoResponse)completionBlock
                            onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/cards" params:params];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        NSMutableArray *data = [response objectForKey:@"data"];
        
        NSMutableArray *list = nil;
        
        for (NSDictionary *record in data) {
            if (!list)
                list = [NSMutableArray array];
            CreditCard *card = [[CreditCard alloc] init];
            card.cardID = [record stringForKey:@"id"];
            card.productID = [record stringForKey:@"creditcard_product_id"];
            card.productName = [record stringForKey:@"creditcard_product"];
            card.bankID = [record stringForKey:@"bank_id"];
            card.nick = [record stringForKey:@"nick"];
            card.bankName = [record stringForKey:@"bank"];
            card.cardPic = [[record stringForKey:@"pic"] lastPathComponent];
            card.cardNumber = [record stringForKey:@"card_number"];
            card.ownerName = [record stringForKey:@"owner_name"];
            card.bankIcon = [record stringForKey:@"bank_icon"];
            card.status = YES;
            card.billDay = [record stringForKey:@"state_day"];
            
            if (!card.billDay)
                card.billDay = @"1";
            
            [list addObject:card];
            card = nil;
        }
        
        completionBlock(list);
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)getAllReminders:(NSMutableDictionary *)params
                           onCompletion:(GetUserInfoResponse)completionBlock
                                onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/reminders" params:params];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        NSDictionary *data = [response objectForKey:@"data"];
        
        NSMutableArray *records = [data objectForKey:@"reminders"];
        
        
        NSMutableArray *results = nil;
        
        for (NSDictionary *record in records) {
            if (!results) {
                results = [NSMutableArray arrayWithCapacity:records.count];
            }
            
            Reminder *reminder = [[Reminder alloc] init];
            reminder.cardID = [record stringForKey:@"card_id"];
            reminder.reminderDay = [record stringForKey:@"reminder_day"];
            reminder.reminderHour = [record stringForKey:@"reminder_hour"];
            reminder.preDay = [record stringForKey:@"pre_day"];
            
            [results addObject:reminder];
            reminder = nil;
        }
        
        completionBlock(results);
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

-(MKNetworkOperation*) downloadFatAssFileFrom:(NSString*)remoteURL
                                       toFile:(NSString*)fileName
{
    MKNetworkOperation *op = [self operationWithURLString:remoteURL
                                                   params:nil
                                               httpMethod:@"GET"];
    
    [op addDownloadStream:[NSOutputStream outputStreamToFileAtPath:fileName
                                                            append:YES]];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)getFavoriteDiscounts:(NSMutableDictionary *)params
                                onCompletion:(GetUserInfoResponse)completionBlock
                                     onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/favoritediscounts"
                                              params:params];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        NSArray *discounts = response[@"data"];
        NSMutableArray *result = [NSMutableArray arrayWithCapacity:discounts.count];
        
        for (int i = 0; i < discounts.count; i++) {
            NSDictionary *record = [discounts objectAtIndex:i];
            
            // 解析coordinate
            CLLocationDegrees latitude = [[record stringForKey:@"shop_lat"] doubleValue];
            CLLocationDegrees longitude = [[record stringForKey:@"shop_lon"] doubleValue];
            CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
            
            Discount *d = [[Discount alloc] initWithCoordinate:coordinate];
            
            d.activated = [record stringForKey:@"actived"];
            d.bankDescription = [record stringForKey:@"bank_desc"];
            d.bankIcon = [record stringForKey:@"bank_icon"];
            d.bankID = [record stringForKey:@"bank_id"];
            d.bankName = [record stringForKey:@"bank_name"];
            d.bankPic = [record stringForKey:@"bank_pic"];
            d.beginTime = [record stringForKey:@"begin_time"];
            d.createTime = [record stringForKey:@"create_time"];
            d.creditcardProductID = [record stringForKey:@"creditcard_product_id"];
            d.discountDescription = [record stringForKey:@"description"];
            d.endTime = [record stringForKey:@"end_time"];
            d.hot = [record stringForKey:@"hot"];
            d.discountID = [record stringForKey:@"id"];
            d.pic = [record stringForKey:@"pic"];
            d.rate = [record stringForKey:@"rate"];
            d.shopAddress = [record stringForKey:@"shop_address"];
            d.shopAverageConsumption = [record stringForKey:@"shop_average_consumption"];
            d.shopBookmarkTimes = [record stringForKey:@"shop_bookmark_times"];
            d.shopCity = [record stringForKey:@"shop_city"];
            d.shopDesc = [record stringForKey:@"shop_description"];
            d.shopID = [record stringForKey:@"shop_id"];
            d.shopLatitude = [record stringForKey:@"shop_lat"];
            d.shopLongitude = [record stringForKey:@"shop_lon"];
            d.shopName = [record stringForKey:@"shop_name"];
            d.shopPhoneNumber = [record stringForKey:@"shop_phone_number"];
            d.shopPic = [record stringForKey:@"shop_pic"];
            d.shopType = [record stringForKey:@"shop_type"];
            d.shopTypeName = [record stringForKey:@"shop_type_name"];
            d.discountTitle = [record stringForKey:@"title"];
            d.type = [[record stringForKey:@"type"] integerValue];
            d.updateTime = [record stringForKey:@"update_time"];
            
            [result addObject:d];
            d = nil;
        }

        completionBlock(result);
        
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)downloadImage:(NSString *)path
                             destPath:(NSString *)destPath
                         onCompletion:(GetImageResponse)completionBlock
                              onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:path];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSData *responseData = [completedOperation responseData];

//        NSLog(@"data = %@", responseData);
        if (responseData) {
            [responseData writeToFile:destPath atomically:YES];
            completionBlock(YES);
        } else
            completionBlock(NO);
        
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

@end
