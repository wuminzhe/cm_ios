//
//  InstallmentResultCell.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-31.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InstallmentResultCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *stageLabel;
@property (nonatomic, strong) IBOutlet UILabel *installmentLabel;

+ (InstallmentResultCell *)getInstance;

@end
