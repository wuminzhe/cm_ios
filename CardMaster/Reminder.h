//
//  Reminder.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-24.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Reminder : NSObject

@property (nonatomic, strong) NSString *cardID;
@property (nonatomic, strong) NSString *reminderDay;
@property (nonatomic, strong) NSString *reminderHour;
@property (nonatomic, strong) NSString *preDay;
@property (nonatomic, strong) NSString *on;

@end
