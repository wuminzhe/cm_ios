//
//  NomalCell.h
//  CardMaster
//
//  Created by wenjun on 13-1-10.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NomalCell : UITableViewCell

- (void)setTitle:(NSString *)title description:(NSString *)desc;

+ (NomalCell *)getInstance;

@end
