//
//  MyFavoriteViewController.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-16.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"

@interface MyFavoriteViewController : BaseNavigationController<UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UITableView *mTableView;
}

@property (nonatomic, strong) NSMutableArray *favorites;

@end
