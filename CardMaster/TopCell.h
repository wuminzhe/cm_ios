//
//  TopCell.h
//  CardMaster
//
//  Created by wenjun on 13-1-10.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoanProduct.h"

@interface TopCell : UITableViewCell

- (void)addTarget:(id)target selecotr:(SEL)selector;

- (void)loadLoan:(LoanProduct *)loan;

+ (TopCell *)getInstance;

@end
