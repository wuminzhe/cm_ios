//
//  BankServiceDataInternal.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-29.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BankServiceDataInternal : NSObject

@property (nonatomic, copy) NSString *tip;                      // 显示内容
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *mobile;                   // 电话号码
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) NSInteger type;                   // 0.电话服务, 1:短信服务
@property (nonatomic, strong) NSMutableArray *list;             // 需要替换的code列表
@property (nonatomic, strong) NSMutableArray *values;           // code对应的值
@property (nonatomic, strong) NSMutableDictionary *codeList;

@end
