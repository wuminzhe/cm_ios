//
//  LoanProduct.m
//  CardMaster
//
//  Created by wenjun on 13-1-10.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "LoanProduct.h"

@implementation LoanProduct

- (BOOL)isEqual:(id)object
{
    if ([object isKindOfClass:[LoanProduct class]])
    {
        return [[object pk] isEqualToString:self.pk];
    }
    return NO;
}

@end
