//
//  InstallmentResultViewController.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-30.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"

@class Rate;

@interface InstallmentResultViewController : BaseNavigationController<UITableViewDataSource, UITableViewDelegate>
{
    __unsafe_unretained IBOutlet UILabel *accountLabel;
    __unsafe_unretained IBOutlet UILabel *stageLabel;
    __unsafe_unretained IBOutlet UILabel *allAccountLabel;
    __unsafe_unretained IBOutlet UILabel *allRateLabel;
    
    __unsafe_unretained IBOutlet UITableView *mTableView;
    __unsafe_unretained IBOutlet UIView *footView;
}

@property (nonatomic, strong) Rate *rate;           // 利率
@property (nonatomic) double account;               // 分期金额

- (IBAction)loan:(id)sender;
        
@end
