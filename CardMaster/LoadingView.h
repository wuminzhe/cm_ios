//
//  LoadingView.h
//  LifeShanghai
//
//  Created by wenjun on 12-11-27.
//
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

@property (readonly,nonatomic) UIActivityIndicatorView * indicator;
@property (readonly,nonatomic) UILabel * label;

@property (strong,nonatomic) NSString * text;
@property (nonatomic) NSUInteger height;

- (void)start;
- (void)stop;
- (void)stopWithAnimation;

@end
