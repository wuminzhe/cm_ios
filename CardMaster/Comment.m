//
//  Comment.m
//  LifeShanghai
//
//  Created by wenjun on 12-11-30.
//
//

#import "Comment.h"

@implementation Comment

- (NSComparisonResult)dateComparetor:(Comment *)other
{
    return [other.createAt compare:self.createAt];
}

@end
