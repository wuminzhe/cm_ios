//
//  InstallmentEngine.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-25.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MKNetworkEngine.h"

@interface InstallmentEngine : MKNetworkEngine

typedef void (^InstallmentUpdateStatusResponseBlock) (BOOL status);
typedef void (^InstallmentResponseBlock) (NSMutableArray *result);

- (MKNetworkOperation *)getInstallmentUpdateStatus:(NSMutableDictionary *)params
                                   onCompletion:(InstallmentUpdateStatusResponseBlock)completionBlock
                                        onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)updateInstallmentRate:(NSMutableDictionary *)params
                                 onCompletion:(InstallmentResponseBlock)completionBlock
                                      onError:(MKNKErrorBlock)errorBlock;

@end
