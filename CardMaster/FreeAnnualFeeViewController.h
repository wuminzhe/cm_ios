//
//  FreeAnnualFeeViewController.h
//  cardMaster
//
//  Created by Zhang Ming on 12-12-3.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"

@class CreditCard;
@class FreeAnnualFeeEngine;

@interface FreeAnnualFeeViewController : BaseNavigationController<UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIAlertViewDelegate> {
    __unsafe_unretained IBOutlet UIScrollView *theScrollView;
    __unsafe_unretained IBOutlet UILabel *cardNameLabel;
    __unsafe_unretained IBOutlet UIImageView *bankIcon;
    __unsafe_unretained IBOutlet UITableView *mTableView;
    __unsafe_unretained IBOutlet UILabel *feeLabel;
    __unsafe_unretained IBOutlet UILabel *discountTitleLabel;
    __unsafe_unretained IBOutlet UILabel *discountLabel;
    
    UIImageView *iv1;
    UIImageView *iv2;
    UIImageView *iv3;
    
    UITextField *contentTextField;
    
    IBOutlet UIView *dateView;
    
    IBOutlet UIPickerView *mPickerView;
    
    UISwitch *mSwitch;
    
    FreeAnnualFeeEngine *engine;
}

@property (nonatomic, strong) CreditCard *card;

- (IBAction)hidePickerView:(id)sender;
- (IBAction)chooseDate:(id)sender;

@end
