//
//  UserSettingCell.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-26.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "UserSettingCell.h"

@implementation UserSettingCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (UserSettingCell *)getInstance
{
    return [[[NSBundle mainBundle] loadNibNamed:@"UserSettingCell" owner:nil options:NULL] lastObject];
}

@end
