//
//  RecommendViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 13-1-9.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"

@interface RecommendViewController : BaseNavigationController<UIWebViewDelegate>
{
    __unsafe_unretained IBOutlet UIWebView *webView;
}

@property (nonatomic, strong) NSURL *applyURL;
@property (nonatomic, strong) NSString *titleString;

@end
