//
//  LoanController.m
//  CardMaster
//
//  Created by wenjun on 13-1-10.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "LoanController.h"
#import "TopCell.h"
#import "LastCell.h"
#import "NomalCell.h"
#import "RecommendViewController.h"

@interface LoanController () <UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView * table;
}

@end

@implementation LoanController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"贷款详情";
    table.showsVerticalScrollIndicator = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [table reloadData];
}

- (void)apply
{
    //申请贷款
    RecommendViewController *vc = [[RecommendViewController alloc] initWithNibName:@"RecommendViewController" bundle:nil];
    vc.titleString = @"贷款申请";
    
    NSString *urlString = [_loan.suggestURL stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    vc.applyURL = [NSURL URLWithString:urlString];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * identifier = @"NomalCell";
    if (indexPath.row == 0)
    {
        identifier = @"TopCell";
    }
    else if (indexPath.row == 4)
    {
        identifier = @"LastCell";
    }
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        if (indexPath.row == 0)
        {
            cell = [TopCell getInstance];
            [(TopCell *)cell addTarget:self selecotr:@selector(apply)];
        }
        else if (indexPath.row == 4)
        {
            cell = [LastCell getInstance];
        }
        else
        {
            cell = [NomalCell getInstance];
        }
    }
    if (indexPath.row == 0)
    {
        [(id)cell loadLoan:self.loan];
    }
    else if (indexPath.row == 4)
    {
        [(id)cell setTitle:@"贷款条件" description:self.loan.condition];
    }
    else if (indexPath.row == 1)
    {
        [(id)cell setTitle:@"期       限" description:self.loan.deadLine];
    }
    else if (indexPath.row == 2)
    {
        [(id)cell setTitle:@"办理时间" description:self.loan.applyDuration];
    }
    else if (indexPath.row == 3)
    {
        [(id)cell setTitle:@"适用人群" description:self.loan.suitableUsers];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return 194;
    }
    else if (indexPath.row == 4)
    {
        return [LastCell cellHeightForDesc:self.loan.condition];
    }
    else
    {
        return 44;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
