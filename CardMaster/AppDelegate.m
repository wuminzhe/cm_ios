//
//  AppDelegate.m
//  CardMaster
//
//  Created by Zhang Ming on 12-12-24.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "AppDelegate.h"
#import "CMEngine.h"
#import "SQLGlobal.h"
#import "GlobalHeader.h"
#import "GlobalOperation.h"
#import "MyCardViewController.h"
#import "ShopDiscountViewController.h"
#import "BankDiscountViewController.h"
#import "RecommendCardViewController.h"
#import "UINavigationController+Extension.h"

@implementation AppDelegate {
    CLLocation *usedLocation;
    BOOL firstLaunch;
    ShopDiscountViewController *shopDiscountViewController;
}

- (void)buildTabBarController
{
    MyCardViewController *myCardViewController = [[MyCardViewController alloc] initWithNibName:@"MyCardViewController" bundle:nil];
    
    shopDiscountViewController = [[ShopDiscountViewController alloc] initWithNibName:@"ShopDiscountViewController" bundle:nil];
    
    BankDiscountViewController *bankDiscountViewController = [[BankDiscountViewController alloc] initWithNibName:@"BankDiscountViewController" bundle:nil];
    
    RecommendCardViewController *recommendCardViewController = [[RecommendCardViewController alloc] initWithNibName:@"RecommendCardViewController" bundle:nil];
    
    self.tabBarController = [[JBTabBarController alloc] init];
    
    self.tabBarController.viewControllers = @[  myCardViewController,
                                                shopDiscountViewController,
                                                bankDiscountViewController,
                                                recommendCardViewController
                                                ];
    self.tabBarController.tabBar.maximumTabWidth = 64.0f;
    self.tabBarController.tabBar.layoutStrategy = JBTabBarLayoutStrategyFill;

    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_tabBarController];
    
    [nav setNavigationBarBackgroundImage:[UIImage imageNamed:@"nav.png"]];
    [nav setNavigationBarHidden:YES];
    self.window.rootViewController = nav;
//    self.window.rootViewController = _tabBarController;
}

- (void)initialize
{
    // 创建文件夹
    [GlobalOperation createCacheDirectory];
    [GlobalOperation createLocalDirectory];
    [GlobalOperation createCreditCardImageDirectory];
    [GlobalOperation createDiscountImageDirectory];
    
    engine = [[CMEngine alloc] initWithHostName:kServerAddr];
    
    // 创建数据库
    [[SQLGlobal sharedInstance] attachDatabase:@"cm.sqlite"];
    
    firstLaunch = ([[SQLGlobal sharedInstance] getClientCode] == nil ? YES : NO);
    
    //判断程序是否初始化过
    if (firstLaunch) {
        // 添加城市列表
        [[SQLGlobal sharedInstance] initializeCity];
        
        [engine initializeClient:nil onCompletion:^(BOOL status) {
            if (status) {
                _code = [[SQLGlobal sharedInstance] getClientCode];
                
                [self initializeWithCode];
            }
        } onError:^(NSError *error) {
            ;
        }];
    } else {
        self.code = [[SQLGlobal sharedInstance] getClientCode];
        
        NSLog(@"code = %@", _code);
        [self initializeWithCode];
    }
}

- (void)initializeWithCode
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:1];
    params[@"code"] = self.code;
    
    [engine getBanksUpdateStatus:params onCompletion:^(BOOL status) {
        // 如果需要更新银行表
        if (status) {
            // 更新银行表
            [engine getBanks:params onCompletion:^(NSMutableArray *results) {
                ;
            } onError:^(NSError *error) {
                ;
            }];
        }
    } onError:^(NSError *error) {
        ;
    }];
    
    [self performSelector:@selector(handleFadeOut:) withObject:nil afterDelay:MAX_SPLASH_TIMEOUT];
}

- (void)initializeLocationManager
{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    _locationManager.distanceFilter = kCLLocationAccuracyHundredMeters;
    [_locationManager startUpdatingLocation];
}

- (void)handleFadeOut:(id)sender
{
    [self.splashViewController.view removeFromSuperview];
    
    [self buildTabBarController];
    
//    if (firstLaunch) {
//        _guideViewController = [[GuideViewController alloc] initWithNibName:@"GuideViewController" bundle:nil];
//        
//        [self.window addSubview:_guideViewController.view];
//    }
    
    [self initializeLocationManager];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [WXApi registerApp:@"wx3933f5e362fc02a0"];
    usedLocation = nil;
    
    self.splashViewController = [[SplashViewController alloc] initWithNibName:@"SplashViewController" bundle:nil];
    
    [self.window addSubview:self.splashViewController.view];
    
    [self initialize];
    self.window.backgroundColor = [UIColor blackColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [self.locationManager stopUpdatingLocation];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    [self.locationManager startUpdatingLocation];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [WXApi handleOpenURL:url delegate:self];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [WXApi handleOpenURL:url delegate:self];
}

#pragma mark - CLLocationManagerDelegate methods
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if (error.code == kCLErrorDenied) {
        [self.locationManager stopUpdatingLocation];
//        [self.locationManager startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if (newLocation.horizontalAccuracy > 0 && [newLocation.timestamp timeIntervalSinceNow] <= 20) {
        self.userLocation = newLocation;
        
        if (!usedLocation || ABS([usedLocation.timestamp timeIntervalSinceDate:newLocation.timestamp]) > (4 * 60 * 60)) {
            usedLocation = newLocation;
            
            [shopDiscountViewController startLoadDiscounts];
            
            NSMutableDictionary *geoParams = [[NSMutableDictionary alloc] init];
            geoParams[@"lat"] = [NSString stringWithFormat:@"%f", self.userLocation.coordinate.latitude];
            geoParams[@"lon"] = [NSString stringWithFormat:@"%f", self.userLocation.coordinate.longitude];
            
            CMEngine *geoEngine = [[CMEngine alloc] initWithHostName:@"maps.googleapis.com"];
            
            [geoEngine getGeoInfo:geoParams onCompletion:^(GEOInfo *result) {
                
                if ([self.geoInfo.city isEqualToString:result.city])
                    return;
            
                self.geoInfo = result;
                
                
                [[NSNotificationCenter defaultCenter] postNotificationName:USER_CITY_GET_NEW object:nil];
                
            } onError:^(NSError *error) {
                ;
            }];
        }
        
        NSLog(@"userloaction = %@", newLocation);
    }
}

#pragma mark - WXApiDelegate methods
-(void)onReq:(BaseReq*)req
{
    
}

-(void)onResp:(BaseResp*)resp
{
    
}

@end
