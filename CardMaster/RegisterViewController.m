//
//  RegisterViewController.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-23.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "RegisterViewController.h"
#import "LoginEngine.h"
#import "NSString+Verify.h"
#import "CMUtil.h"
#import "GlobalHeader.h"
#import "UserAccountViewController.h"
#import "SQLGlobal.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"注册";
    
    engine = [[LoginEngine alloc] initWithHostName:kServerAddr];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    usernameTextField = nil;
    passwordTextField = nil;
    passwordAgainTextField = nil;
    
    [super viewDidUnload];
}

#pragma mark - Actions
- (IBAction)backgroundTap:(id)sender
{
    if ([usernameTextField isFirstResponder])
        [usernameTextField resignFirstResponder];
    
    if ([passwordTextField isFirstResponder])
        [passwordTextField resignFirstResponder];
    
    if ([passwordAgainTextField isFirstResponder])
        [passwordAgainTextField resignFirstResponder];
    
    [theScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (IBAction)chooseNext:(id)sender
{
    if (sender == usernameTextField) {
        [sender resignFirstResponder];
        [passwordTextField becomeFirstResponder];
    } else if (sender == passwordTextField) {
        [sender resignFirstResponder];
        [passwordAgainTextField becomeFirstResponder];
    } else if (sender == passwordAgainTextField) {
        [self commit:sender];
    }
}

- (IBAction)commit:(id)sender
{    
    NSString *userName = usernameTextField.text;
    NSString *password = passwordTextField.text;
    NSString *passwordAgain = passwordAgainTextField.text;
    
    if (!([userName isValidEmail] || [userName isValidPhoneNumber])) {
        [self quickAlertView:@"请输入正确的用户名！"];
        return;
    }
    
    if (password.length < 6) {
        [self quickAlertView:@"密码长度不得小于6位 ！"];
        return;
    }
    
    if (![password isEqualToString:passwordAgain]) {
        [self quickAlertView:@"两次密码输入不一致！"];
        return;
    }
    [self backgroundTap:nil];
    // 提交操作
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    params[@"username"] = userName;
    params[@"password"] = password;
    params[@"repassword"] = passwordAgain;
    
    [self showHudView:@"注册中..."];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"LoginSuccess"]) {
        [engine getCode:nil onCompletion:^(NSString *code) {
            params[@"code"] = code;
            
            // 注册用户
            [engine registerUser:params onCompletion:^(NSString *rightCode) {
                // 注册成功，修改信息
                if (rightCode) {
                    [self hideHudView];
                    [CMUtil getAppDelegate].code = rightCode;
                    [[SQLGlobal sharedInstance] updateUser:rightCode];
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"信息"
                                                                        message:@"注册成功！"
                                                                       delegate:self
                                                              cancelButtonTitle:@"确定"
                                                              otherButtonTitles:nil];
                    
                    [alertView show];
                } else {
                    [self hideHudView];
                    [self quickAlertView:@"注册失败！"];
                }
            } onError:^(NSError *error) {
                [self hideHudView];
            }];

            
        } onError:^(NSError *error) {
            [self hideHudView];
        }];
    } else {
        params[@"code"] = [CMUtil getCode];
        [engine registerUser:params onCompletion:^(NSString *rightCode) {
            if (rightCode) {
                [self hideHudView];
                [CMUtil getAppDelegate].code = rightCode;
                [[SQLGlobal sharedInstance] updateUser:rightCode];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"信息"
                                                                    message:@"注册成功！"
                                                                   delegate:self
                                                          cancelButtonTitle:@"确定"
                                                          otherButtonTitles:nil];
                
                [alertView show];
            } else {
                [self quickAlertView:@"注册失败！"];
            }
        } onError:^(NSError *error) {
            [self hideHudView];
        }];

    }
}

#pragma mark - UITextFieldDelegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == usernameTextField)
        return;
    
    [theScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y - 80) animated:YES];
}

#pragma mark - UIAlertViewDelegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UserAccountViewController *userAccountViewController = [[UserAccountViewController alloc] initWithNibName:@"UserAccountViewController" bundle:nil];
    [[NSUserDefaults standardUserDefaults] setValue:usernameTextField.text forKey:@"LoginWay"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LoginSuccess"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"NewUser"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController pushViewController:userAccountViewController animated:YES];
}
@end
