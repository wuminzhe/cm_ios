//
//  UserAccountViewController.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-26.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"
#import "TCWBEngine.h"
#import "SinaWeibo.h"
#import "MBProgressHUD.h"
#import "TencentOAuth.h"

@class UserEngine;

@interface UserAccountViewController : BaseNavigationController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, SinaWeiboDelegate, SinaWeiboRequestDelegate, UIActionSheetDelegate, TencentSessionDelegate> {
    IBOutlet UITableView *mTableView;
    TCWBEngine *weiboEngine;
    SinaWeibo *_sinaOAuth;
    
    UIActivityIndicatorView     *indicatorView;
    
    UserEngine *engine;
}

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *code;

@end
