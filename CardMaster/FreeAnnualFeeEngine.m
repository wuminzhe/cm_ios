//
//  FreeAnnualFeeEngine.m
//  cardMaster
//
//  Created by Zhang Ming on 12-12-3.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "FreeAnnualFeeEngine.h"
#import "NSDictionary+Extension.h"

@implementation FreeAnnualFeeEngine

//- (MKNetworkOperation *)getBanks:(NSMutableDictionary *)params
//                    onCompletion:(getBanksResponseBlock)completionBlock
//                         onError:(MKNKErrorBlock)errorBlock
//{
//    MKNetworkOperation *op = [self operationWithPath:@"api/banks" params:params];
//    
//    [op onCompletion:^(MKNetworkOperation *completedOperation) {
//        NSDictionary *response = [completedOperation responseJSON];
//        
////        BOOL status = [[response stringForKey:@"status"] boolValue];
////        
////        NSArray *data = [response objectForKey:@"data"];
//        
//        
//    } onError:^(NSError *error) {
//        errorBlock(error);
//    }];
//    
//    [self enqueueOperation:op];
//    
//    return op;
//}

- (MKNetworkOperation *)getProductFee:(NSMutableDictionary *)params
                         onCompletion:(getProductFeeResponseBlock)completionBlock
                              onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/product_fee" params:params];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        
        NSDictionary *response = [completedOperation responseJSON];
        DLog(@"%@", response);
        BOOL status = [[response stringForKey:@"status"] boolValue];
        
        NSArray *data = [response objectForKey:@"data"];
        
        if (data.count == 0) {
            completionBlock(nil);
            return ;
        }
        if (status) {
            NSString *feePolicy = [[response objectForKey:@"data"] stringForKey:@"fee_policy"];
            
            completionBlock(feePolicy);
        } else {
            completionBlock(nil);
        }
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

@end
