//
//  Rate.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-25.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Rate : NSObject

@property (nonatomic, strong) NSString *rateID;                     // 利率ID
@property (nonatomic, strong) NSString *bankID;                     // 关联银行ID
@property (nonatomic, strong) NSString *maxAmount;                  // 最大额度
@property (nonatomic, strong) NSString *minAmount;                  // 最小额度
@property (nonatomic, strong) NSString *rate;                       // 利率
@property (nonatomic, strong) NSString *stage;                      // 分期
@property (nonatomic, strong) NSString *type;                       // 1：一次性，2：每期
@property (nonatomic, strong) NSString *createTime;                 
@property (nonatomic, strong) NSString *updateTime;

@end
