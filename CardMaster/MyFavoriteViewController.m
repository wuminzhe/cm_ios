//
//  MyFavoriteViewController.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-16.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MyFavoriteViewController.h"
#import "DiscountCell.h"
#import "BankDiscountCell.h"
#import "Discount.h"
#import "SQLGlobal.h"
#import "DiscountDetailViewController.h"

@interface MyFavoriteViewController () {
    BOOL edit;
}

@end

@implementation MyFavoriteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"我的收藏";
    
    UIButton *editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    editButton.frame = CGRectMake(0, 0, 44, 25);
    
    [editButton setImage:[UIImage imageNamed:@"edit.png"]
                forState:UIControlStateNormal];
    
    [editButton setImage:[UIImage imageNamed:@"edit_click.png"]
                forState:UIControlStateSelected];
    
    [editButton addTarget:self
                   action:@selector(edit:)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *editBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:editButton];
    
    self.navigationItem.rightBarButtonItem = editBarButtonItem;
    
    [self loadFavoriteDiscount];
}

- (void)viewDidUnload
{
    mTableView = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods
- (void)backAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)edit:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    [button setImage:[UIImage imageNamed:@"save.png"]
            forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"save_click.png"]
            forState:UIControlStateHighlighted];
    
    [button removeTarget:self
                  action:@selector(edit:)
        forControlEvents:UIControlEventTouchUpInside];
    
    [button addTarget:self
               action:@selector(save:)
     forControlEvents:UIControlEventTouchUpInside];
    
    edit = YES;
    
    [mTableView reloadData];
}

- (void)save:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    [button setImage:[UIImage imageNamed:@"edit.png"]
            forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"edit_click.png"]
            forState:UIControlStateHighlighted];
    
    [button removeTarget:self
                  action:@selector(save:)
        forControlEvents:UIControlEventTouchUpInside];
    
    [button addTarget:self
               action:@selector(edit:)
     forControlEvents:UIControlEventTouchUpInside];
}

- (void)del:(id)sender
{
    int tag = ((UIButton *)sender).tag;
    
    Discount *discount = self.favorites[tag];
    
    [[SQLGlobal sharedInstance] deleteDiscount:discount.discountID];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:tag inSection:0];
    
    [self.favorites removeObjectAtIndex:tag];
    [mTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                      withRowAnimation:UITableViewRowAnimationNone];
    
    [mTableView reloadData];
}

- (void)loadFavoriteDiscount
{
    self.favorites = [[SQLGlobal sharedInstance] getDiscounts];
    
    [mTableView reloadData];
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return 1;
    return self.favorites.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Discount *discount = _favorites[indexPath.row];
    
    // 银行
    if (discount.type == 1) {
        BankDiscountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BankDiscountCell"];
        
        if (!cell) {
            cell = [BankDiscountCell getInstance];
            cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bank_discount_bg.png"]];
            cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bank_discount_selected_bg.png"]];
            [cell.deleteButton addTarget:self
                                  action:@selector(del:)
                        forControlEvents:UIControlEventTouchUpInside];
        }
        
        cell.deleteButton.tag = indexPath.row;
        if (edit) {
            cell.deleteButton.hidden = NO;
        } else {
            cell.deleteButton.hidden = YES;
        }
        
        NSString *urlString = [discount.pic stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSLog(@"pic = %@", urlString);
        cell.bankImageView.imageURL = [NSURL URLWithString:urlString];
        
        return cell;
    } else {
        DiscountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DISCOUNT_CELL"];
        
        if (!cell) {
            cell = [DiscountCell getInstance];
            
            [cell.deleteButton addTarget:self
                                  action:@selector(del:)
                        forControlEvents:UIControlEventTouchUpInside];
        
            cell.backgroundView = [[UIImageView alloc] initWithImage:
                                   [UIImage imageNamed:@"discount_cell_bg.png"]];
            
            cell.maskImageView.image = [UIImage imageNamed:@"discount_icon.png"];
        }
        
        cell.deleteButton.tag = indexPath.row;
        if (edit) {
            cell.discountView.hidden = YES;
            cell.deleteButton.hidden = NO;
        } else {
            cell.discountView.hidden = NO;
            cell.deleteButton.hidden = YES;
        }
        
        cell.discount = discount;
        
        return cell;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%s", __FUNCTION__);
    return NO;
}

#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Discount *discount = _favorites[indexPath.row];
    
    if (discount.type == 1)
        return 135.0;
    
    return 96.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    if (!edit) {
//        DiscountDetailViewController *discountDetailViewController = [[DiscountDetailViewController alloc] initWithNibName:@"DiscountDetailViewController" bundle:nil];
//        discountDetailViewController.discount = self.favorites[indexPath.row];
//        
//        [self.navigationController pushViewController:discountDetailViewController animated:YES];
//    }
}
@end
