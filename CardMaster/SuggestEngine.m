//
//  SuggestEngine.m
//  CardMaster
//
//  Created by Zhang Ming on 12-12-27.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "SuggestEngine.h"
#import "MZBubbleData.h"
#import "NSDictionary+Extension.h"

@implementation SuggestEngine

- (MKNetworkOperation *)sendMessage:(NSMutableDictionary *)params
                       onCompletion:(SendMessageResponseBlock)completionBlock
                            onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/sys_feedback"
                                              params:params
                                          httpMethod:@"POST"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        BOOL status = [[response stringForKey:@"status"] boolValue];
        
        completionBlock(status);
        
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)getMessages:(NSMutableDictionary *)params
                       onCompletion:(GetMessagesResponseBlock)completionBlock
                            onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/sys_feedback_list"
                                              params:params
                                          httpMethod:@"GET"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        BOOL status = [[response stringForKey:@"status"] boolValue];
        NSMutableArray *result = nil;
        if (status) {
            NSArray *comments = [[response objectForKey:@"data"] objectForKey:@"comments"];
            
            if (comments.count > 0) {
                if (!result)
                    result = [[NSMutableArray alloc] init];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                for (NSDictionary *record in comments) {
                    NSString *text = [record stringForKey:@"content"];
                    NSString *type = [record stringForKey:@"type"];
                    NSString *createAt = [record stringForKey:@"created_at"];
                    
                    NSDate *date = [dateFormatter dateFromString:createAt];
                    
                    MZBubbleData *bubbleData = [[MZBubbleData alloc] initWithText:text andDate:date andType:type.integerValue];
                    
                    [result addObject:bubbleData];
                    
                    bubbleData = nil;
                }
            }
            
            completionBlock(result);
            
        } else {
            completionBlock(result);
        }
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

@end
