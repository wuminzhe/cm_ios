//
//  RecommendEngine.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-22.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "RecommendEngine.h"
#import "NSDictionary+Extension.h"
#import "Product.h"
#import "Comment.h"

@implementation RecommendEngine

- (MKNetworkOperation *)getCreditCardProductList:(NSMutableDictionary *)params
                                    onCompletion:(GetCreditCardProductResponse)completionBlock
                                         onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/creditcard_suggests" params:params];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        NSArray *records = response[@"data"][@"creditcard_products"];
        
        NSMutableArray *products = [NSMutableArray array];
        
        for (NSDictionary *record in records) {
            Product *product = [[Product alloc] init];
            product.addOn = [record stringForKey:@"add_on"];
            product.appliable = [record stringForKey:@"appliable"];
            product.applications = [record stringForKey:@"applications"];
            product.bankID = [record stringForKey:@"bank_id"];
            product.bankName = [record stringForKey:@"bank_name"];
            product.bookmarkTimes = [record stringForKey:@"bookmark_times"];
            product.categoryID = [record stringForKey:@"creditcard_category_id"];
            product.desc = [record stringForKey:@"description"];
            product.fee = [record stringForKey:@"fee"];
            product.feePolicy = [record stringForKey:@"fee_policy"];
            product.grade = [record stringForKey:@"grade"];
            product.gradeName = [record stringForKey:@"grade_name"];
            product.productID = [record stringForKey:@"auto_id"];
            product.name = [record stringForKey:@"name"];
            product.org = [record stringForKey:@"org"];
            product.pic = [record stringForKey:@"pic"];
            product.special = [record stringForKey:@"special"];
            product.totalNumber = [record stringForKey:@"total_number"];
            product.totalPoints = [record stringForKey:@"total_points"];
            product.type = [record stringForKey:@"type"];
            product.commentCount = [record stringForKey:@"comment_count"];
            
            NSString *urlString = [[record stringForKey:@"suggest_url"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            product.applyURL = [NSURL URLWithString:urlString];
            
            // 添加评分字段
            product.score = [record stringForKey:@"score"];
            
            [products addObject:product];
            product = nil;
        }
        
//        DLog(@"%@", response);
        
        NSString *pageCount = [response[@"data"] stringForKey:@"page_count"];
        
        NSMutableDictionary *results = [NSMutableDictionary dictionaryWithCapacity:2];
        results[@"list"] = products;
        results[@"pages"] = pageCount;
        completionBlock(results);
    } onError:^(NSError *error) {
        ;
    }];
 
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)addProductComment:(NSMutableDictionary *)params
                             onCompletion:(AddProductCommentReponse)completionBlock
                                  onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/product_comment" params:params httpMethod:@"POST"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        BOOL status = [[response stringForKey:@"status"] boolValue];
        
        completionBlock(status);
    } onError:^(NSError *error) {
        DLog(@"")
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)getProductCommentList:(NSMutableDictionary *)params
                                 onCompletion:(GetCommentListResponse)completionBlock
                                      onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/product_comment_list" params:params];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        BOOL status = [[response stringForKey:@"status"] boolValue];
        
        if (status) {
            NSMutableArray *results = nil;
            
            NSDictionary *data = [response objectForKey:@"data"];
            
            NSString *pageCount = [data stringForKey:@"page_count"];
            
            NSArray *comments = [data objectForKey:@"comments"];
            
            if (comments.count > 0) {
                results = [[NSMutableArray alloc] initWithCapacity:comments.count];
                
                for (NSDictionary *record in comments) {
                    Comment *comment = [[Comment alloc] init];
                    comment.cid = [record stringForKey:@"id"];
                    comment.productId = [record stringForKey:@"creditcard_product_id"];
                    comment.userId = [record stringForKey:@"user_id"];
                    comment.score = [record stringForKey:@"score"];
                    comment.content = [record stringForKey:@"content"];
                    
                    NSString *dateString = [record stringForKey:@"created_at"];
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
                    comment.createAt = [dateFormatter dateFromString:dateString];
                
                    comment.username = [record stringForKey:@"username"];
                    
                    [results addObject:comment];
                    
                    comment = nil;
                }
                
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:2];
                dict[@"list"] = results;
                dict[@"totalPages"] = pageCount;
     
                completionBlock(dict);
     
            } else
                completionBlock(nil);
            
        } else
            completionBlock(nil);
    } onError:^(NSError *error) {
        DLog(@"error = %@", error);
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

@end
