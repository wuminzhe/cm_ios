//
//  CardEngine.m
//  CardMaster
//
//  Created by Zhang Ming on 12-12-25.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "CardEngine.h"
#import "CreditCard.h"
#import "NSDictionary+Extension.h"

@implementation CardEngine

/*****************************************************************************************
 * 函数名称：uploadCreditCardImageFromFile:params:onCompletion:onError:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：上传信用卡图片到服务器
 * 输入参数：file : 图片路径
 *         params : POST提交参数
 *         completionBlock : 正确请求后执行的代码块
 *         errorBlock : 错误后执行的代码块
 * 返回值：MKNetworkOperation
 * 修改备注：无
 *****************************************************************************************/
- (MKNetworkOperation *)uploadCreditCardImageFromFile:(NSString *)file
                                               params:(NSMutableDictionary *)params
                                         onCompletion:(UploadCreditCardImageResponseBlock)completionBlock
                                              onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/upload_pic" params:params httpMethod:@"POST"];
    
    [op addFile:file forKey:@"Filedata"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        BOOL status = [[response stringForKey:@"status"] boolValue];
        
        if (status) {
            NSString *urlString = [response stringForKey:@"url"];
            completionBlock(urlString);
        } else {
            completionBlock(Fail);
        }
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

/*****************************************************************************************
 * 函数名称：getCardInfo:onCompletion:onError:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：通过卡号解析信息
 * 输入参数：params : POST提交参数
 *         completionBlock : 正确请求后执行的代码块
 *         errorBlock : 错误后执行的代码块
 * 返回值：MKNetworkOperation
 * 修改备注：无
 *****************************************************************************************/
- (MKNetworkOperation *)getCardInfo:(NSMutableDictionary *)params
                       onCompletion:(GetCardInfoResponseBlock)completionBlock
                            onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/creditcard_product" params:params];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        BOOL status = [[response stringForKey:@"status"] boolValue];
        
        if (status) {
            NSDictionary *product = [[response objectForKey:@"data"] objectForKey:@"product"];
            
            CreditCard *card = [[CreditCard alloc] init];
            card.bankIcon = [product stringForKey:@"bank_icon"];
            card.productID = [product stringForKey:@"product_id"];
            card.productName = [product stringForKey:@"product"];
            card.bankID = [product stringForKey:@"bank_id"];
            card.bankName = [product stringForKey:@"bank"];
            
            completionBlock(card);
        } else
            completionBlock(nil);
        
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

/*****************************************************************************************
 * 函数名称：addCreditCard:onCompletion:onError:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：添加卡片信息到服务器
 * 输入参数：params : POST提交参数
 *         completionBlock : 正确请求后执行的代码块
 *         errorBlock : 错误后执行的代码块
 * 返回值：MKNetworkOperation
 * 修改备注：无
 *****************************************************************************************/
- (MKNetworkOperation *)addCreditCard:(NSMutableDictionary *)params
                         onCompletion:(AddCreditCardResponseBlock)completionBlock
                              onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/add_creditcard" params:params httpMethod:@"POST"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        completionBlock(response);
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

/*****************************************************************************************
 * 函数名称：deleteCreditCard:onCompletion:onError:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：删除卡片信息
 * 输入参数：params : POST提交参数
 *         completionBlock : 正确请求后执行的代码块
 *         errorBlock : 错误后执行的代码块
 * 返回值：MKNetworkOperation
 * 修改备注：无
 *****************************************************************************************/
- (MKNetworkOperation *)deleteCreditCard:(NSMutableDictionary *)params
                            onCompletion:(DeleteCardResponseBlock)completionBlock
                                 onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/delete_creditcard" params:params httpMethod:@"POST"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        BOOL status = [[response stringForKey:@"status"] boolValue];
        
        completionBlock(status);
        
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)updateCreditCard:(NSMutableDictionary *)params
                            onCompletion:(UpdateCardResponseBlock)completionBlock
                                 onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/update_creditcard" params:params httpMethod:@"POST"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        BOOL status = [[response stringForKey:@"status"] boolValue];
        
        if (status) {
            completionBlock(SUCCESS);
        } else {
            NSString *errorMsg = [response stringForKey:@"msg"];
            completionBlock(errorMsg);
        }
        
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

@end
