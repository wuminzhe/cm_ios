//
//  RecommendCardCell.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-15.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"

@interface RecommendCardCell : UITableViewCell {
    __unsafe_unretained IBOutlet UILabel *peopleTitleLabel;
}
@property (unsafe_unretained, nonatomic) IBOutlet EGOImageView *cardImageView;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *bankImageView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *cardNameLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *cardDescriptionLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *rateLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *numberOfApplyLabel;

+ (RecommendCardCell *)getRecommendCardTopCell;
+ (RecommendCardCell *)getRecommendCardCell;

@end
