//
//  GTMUtil.m
//  CardMaster
//
//  Created by wenjun on 13-1-14.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "GTMUtil.h"
#import "NSString+URLEncoding.h"

@implementation GTMUtil

+ (NSMutableDictionary *)encodeParams:(NSDictionary *)dict
{
    NSMutableDictionary * params = nil;
    if (dict.count > 0)
    {
        params = [[NSMutableDictionary alloc] initWithCapacity:dict.count];
        for (NSString * key in dict.allKeys)
        {
            [params setObject:[[dict objectForKey:key] URLEncodedString] forKey:key];
        }
    }
    return params;
}

+ (NSString *)paramString:(NSDictionary *)dict
{
    NSMutableString * str = nil;
    if (dict.count > 0)
    {
        str = [[NSMutableString alloc] init];
        NSString * key;
        NSEnumerator * enumerator = [dict keyEnumerator];
        while (key = [enumerator nextObject])
        {
            [str appendFormat:@"&%@=%@",key,[dict objectForKey:key]];
        }
        return [str substringFromIndex:1];
    }
    return str;
}

+ (GTMHTTPFetcher *)fetcherWithURLString:(NSString *)urlString params:(NSDictionary *)params HTTPType:(NSString *)httpType urlEncode:(BOOL)encode
{
    GTMHTTPFetcher * fetcher = nil;
    if (params.count > 0)
    {
        if (encode)
        {
            params = [[self class] encodeParams:params];
        }
        NSString * paramString = [[self class] paramString:params];
        if ([[httpType uppercaseString] isEqualToString:@"GET"])
        {
            fetcher = [GTMHTTPFetcher fetcherWithURLString:[NSString stringWithFormat:@"%@?%@",urlString,paramString]];
        }
        else
        {
            fetcher = [GTMHTTPFetcher fetcherWithURLString:urlString];
            [fetcher setPostData:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    else
    {
        fetcher = [GTMHTTPFetcher fetcherWithURLString:urlString];
    }
    fetcher.mutableRequest.timeoutInterval = 20;
    return fetcher;
}

+ (NSURLRequest *)requestWithURLString:(NSString *)urlString params:(NSDictionary *)params HTTPType:(NSString *)httpType urlEncode:(BOOL)encode
{
    NSMutableURLRequest * request = nil;
    if (params.count > 0)
    {
        if (encode)
        {
            params = [[self class] encodeParams:params];
        }
        NSString * paramString = [[self class] paramString:params];
        if ([[httpType uppercaseString] isEqualToString:@"GET"])
        {
            request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?%@",urlString,paramString]]];
            
        }
        else
        {
            request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    else
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    }
    
    request.timeoutInterval = 20;
    return request;
}

@end
