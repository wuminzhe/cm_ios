//
//  CardDetailViewController.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-20.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "CardDetailViewController.h"
#import "Product.h"
#import "CommentListController.h"
#import "LoanListController.h"
#import "EGOImageView.h"
#import "RecommendViewController.h"
#import "RateController.h"

@interface CardDetailViewController ()
{
    IBOutlet UILabel * commentCountLabel;
    NSInteger additionalServiceCellHeight;
    
    NSString *message;
}

@property (nonatomic, strong) SinaWBEngine *sinaEngine;
@property (nonatomic, strong) TencentWBEngine *tencentEngine;

- (IBAction)comment;

@end

@implementation CardDetailViewController

+ (NSInteger)heightForText:(NSString *)text font:(UIFont *)font width:(NSInteger)width rowHeight:(NSInteger)rowHeight
{
    if (text.length == 0)
    {
        return rowHeight;
    }
    CGSize size = [text sizeWithFont:font constrainedToSize:CGSizeMake(width, 1000)];
    return roundf(size.height / rowHeight) * rowHeight;
}

+ (void)reSizeLabel:(UILabel *)label withText:(NSString *)text rowHeight:(NSInteger)rowHeight
{
    label.text = text;
    if (text.length == 0)
    {
        label.frame = CGRectMake(label.frame.origin.x, label.frame.origin.y, label.bounds.size.width,rowHeight);
        return;
    }
    CGSize size = [text sizeWithFont:label.font constrainedToSize:CGSizeMake(label.bounds.size.width, 1000) lineBreakMode:label.lineBreakMode];
    label.frame = CGRectMake(label.frame.origin.x, label.frame.origin.y, label.bounds.size.width, roundf(size.height / rowHeight) * rowHeight);
}

#pragma mark - View_LifeCircle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"卡详情";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    
//    _sinaOAuth = [[SinaWeibo alloc] initWithAppKey:kSinaAppKey
//                                         appSecret:kSinaAppSecret
//                                    appRedirectURI:kSinaAppRedirect
//                                       andDelegate:self];
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSDictionary *sinaweiboInfo = [defaults objectForKey:@"SinaWeiboAuthData"];
//    if ([sinaweiboInfo objectForKey:@"AccessTokenKey"] && [sinaweiboInfo objectForKey:@"ExpirationDateKey"] && [sinaweiboInfo objectForKey:@"UserIDKey"])
//    {
//        _sinaOAuth.accessToken = [sinaweiboInfo objectForKey:@"AccessTokenKey"];
//        _sinaOAuth.expirationDate = [sinaweiboInfo objectForKey:@"ExpirationDateKey"];
//        _sinaOAuth.userID = [sinaweiboInfo objectForKey:@"UserIDKey"];
//    }
//    
//    weiboEngine = [[TCWBEngine alloc] initWithAppKey:kTencentAppKey andSecret:kTencentAppSecret andRedirectUrl:kTencentAppRedirect];
//    [weiboEngine setRootViewController:self];
    
    message = [NSString stringWithFormat:@"%@%@：%@！那谁，这不就是你一直想办的信用卡么？赶紧办起来！%@", _product.bankName, _product.name, _product.desc, _product.applyURL];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    commentCountLabel.text = self.product.commentCount;
    if (_product.addOn.length == 0)
    {
        additionalServiceCellHeight = 44;
    }
    additionalServiceCellHeight = 54 + [[self class] heightForText:self.product.addOn font:[UIFont systemFontOfSize:14] width:280 rowHeight:18];
}

- (void)viewDidUnload
{
    [self setContentTableView:nil];
    [super viewDidUnload];
}

- (IBAction)comment
{
    CommentListController * commentListController = [[CommentListController alloc] init];
    commentListController.product = _product;
    [self.navigationController pushViewController:commentListController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NavigationBar
- (void)backAction
{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - TableView
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            return 192;
        case 6:
            return additionalServiceCellHeight;
        default:
            return 44;
    }
    return 0;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
/*
 *  第一行子试图索引从100开始
 *  第二行子试图索引从200开始
 *  其他类退
 */
    NSString * identifier = nil;
    if (indexPath.row == 0)
    {
        identifier = @"first";
    }
    else if (indexPath.row == 3)
    {
        identifier = @"third";
    }
    else if (indexPath.row == 6)
    {
        identifier = @"six";
    }
    else
    {
        identifier = @"other";
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIImageView * separator = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,2)];
        separator.image = [UIImage imageNamed:@"line_separator"];
        separator.frame = CGRectMake(0, cell.bounds.size.height - separator.frame.size.height, separator.frame.size.width, separator.frame.size.height);
        [cell addSubview:separator];
        separator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        switch (indexPath.row) {
            case 0:
            {
                // 名称
                UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(20, 15, 300, 40)];
                name.font = [UIFont boldSystemFontOfSize:17];
                name.backgroundColor = [UIColor clearColor];
                name.textColor = [UIColor blackColor];
                name.textAlignment = UITextAlignmentLeft;
                name.tag = 100;
                [cell addSubview:name];
                
                // 卡片图片
                EGOImageView *card = [[EGOImageView alloc] initWithFrame:CGRectMake(23, 53, 117, 74)];
                card.placeholderImage = [UIImage imageNamed:@"recommend_card_none.png"];
                card.tag = 101;
                [cell addSubview:card];
                
                UIImageView *cardforeground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"carddetail_card_"]];
                cardforeground.frame = CGRectMake(20, 50, 123, 84);
                [cell addSubview:cardforeground];
                
                // 等级星星
                for (NSInteger i = 0; i < 5; i++)
                {
                    UIImageView *stargray = [[UIImageView alloc] initWithFrame:CGRectMake(157+i*22, 50, 20, 20)];
                    stargray.image = [UIImage imageNamed:@"carddetail_star_gree_.png"];
                    [cell addSubview:stargray];
                    
                    UIImageView *star = [[UIImageView alloc] initWithFrame:CGRectMake(157+i*22, 50, 20, 20)];
                    star.image = [UIImage imageNamed:@"carddetail_star_yellow_.png"];
                    star.tag = 102+i;
                    [cell addSubview:star];
                }
//                UILabel * label1 = [[UILabel alloc] initWithFrame:CGRectMake(157, 78, 50,20)];
//                label1.textColor = [UIColor lightGrayColor];
//                label1.font = [UIFont systemFontOfSize:13];
//                label1.backgroundColor = [UIColor clearColor];
//                label1.text = @"收藏 ";
//                UILabel * label2 = [[UILabel alloc] initWithFrame:CGRectMake(190, 78, 80, 20)];
//                label2.textColor = [UIColor darkGrayColor];
//                label2.font = [UIFont systemFontOfSize:13];
//                label2.backgroundColor = [UIColor clearColor];
//                label2.tag = 112;
//                UILabel * label3 = [[UILabel alloc] initWithFrame:CGRectMake(218, 78, 50,20)];
//                label3.textColor = [UIColor lightGrayColor];
//                label3.font = [UIFont systemFontOfSize:13];
//                label3.backgroundColor = [UIColor clearColor];
//                label3.text = @"申请数";
//                UILabel * label4 = [[UILabel alloc] initWithFrame:CGRectMake(264, 78, 65,20)];
//                label4.textColor = [UIColor darkGrayColor];
//                label4.font = [UIFont systemFontOfSize:13];
//                label4.backgroundColor = [UIColor clearColor];
//                label4.tag = 114;
//                [cell.contentView addSubview:label1];
//                [cell.contentView addSubview:label2];
                
                
                UILabel * label3 = [[UILabel alloc] initWithFrame:CGRectMake(157, 78, 50,20)];
                label3.textColor = [UIColor lightGrayColor];
                label3.font = [UIFont systemFontOfSize:13];
                label3.backgroundColor = [UIColor clearColor];
                label3.text = @"申请数";
                UILabel * label4 = [[UILabel alloc] initWithFrame:CGRectMake(203, 78, 65,20)];
                label4.textColor = [UIColor darkGrayColor];
                label4.font = [UIFont systemFontOfSize:13];
                label4.backgroundColor = [UIColor clearColor];
                label4.tag = 114;
                [cell.contentView addSubview:label3];
                [cell.contentView addSubview:label4];

//                UIButton * button1 = [[UIButton alloc] initWithFrame:CGRectMake(157,100,30,30)];
//                [button1 setImage:[UIImage imageNamed:@"shoucang_"] forState:UIControlStateNormal];
//                button1.tag = 121;
                UIButton * button2 = [[UIButton alloc] initWithFrame:CGRectMake(157, 100, 30, 30)];
                [button2 setImage:[UIImage imageNamed:@"fenxiang_"] forState:UIControlStateNormal];
                button2.tag = 122;
                UIButton * button3 = [[UIButton alloc] initWithFrame:CGRectMake(211, 100, 30, 30)];
                [button3 setImage:[UIImage imageNamed:@"pingjia_btn"] forState:UIControlStateNormal];
                button3.tag = 123;
//                [button1 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                [button2 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                [button3 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
//                [cell.contentView addSubview:button1];
                [cell.contentView addSubview:button2];
                [cell.contentView addSubview:button3];
                
                
                // 申请按钮
                UIButton *apply = [UIButton buttonWithType:UIButtonTypeCustom];
                apply.frame = CGRectMake(20, 141, 281, 37);
                [apply setBackgroundImage:[UIImage imageNamed:@"carddetail_buttn_.png"] forState:UIControlStateNormal];
                [apply setBackgroundImage:[UIImage imageNamed:@"carddetail_buttn_click_.png"] forState:UIControlStateHighlighted];
                apply.tag = 107;
                [cell addSubview:apply];
                [apply addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                break;
            }
            case 1:
            {
                UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 300, 20)];
                title.text = @"卡种类型 ";
                title.backgroundColor = [UIColor clearColor];
                title.textColor = [UIColor lightGrayColor];
                title.font = [UIFont systemFontOfSize:14];
                [cell addSubview:title];
                
                UILabel *content = [[UILabel alloc] initWithFrame:CGRectMake(90, 10, 300, 20)];
                content.backgroundColor = [UIColor clearColor];
                content.font = [UIFont systemFontOfSize:14];
                content.tag = 200;
                content.textColor = [UIColor darkGrayColor];
                [cell addSubview:content];
                break;
            }
            case 2:
            {
                UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 300, 20)];
                title.text = @"卡种等级 ";
                title.textColor = [UIColor lightGrayColor];
                title.backgroundColor = [UIColor clearColor];
                title.font = [UIFont systemFontOfSize:14];
                [cell addSubview:title];
                
                UILabel *content = [[UILabel alloc] initWithFrame:CGRectMake(90, 10, 300, 20)];
                content.backgroundColor = [UIColor clearColor];
                content.font = [UIFont systemFontOfSize:14];
                content.tag = 300;
                content.textColor = [UIColor darkGrayColor];
                [cell addSubview:content];
                break;
            }
            case 3:
            {
                UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 300, 20)];
                title.text = @"所属银行 ";
                title.backgroundColor = [UIColor clearColor];
                title.textColor = [UIColor lightGrayColor];
                title.font = [UIFont systemFontOfSize:14];
                [cell addSubview:title];
                
                UIImageView *bankico = [[UIImageView alloc] initWithFrame:CGRectMake(90, 12, 15, 15)];
                bankico.tag = 400;
                [cell addSubview:bankico];
                
                UILabel *content = [[UILabel alloc] initWithFrame:CGRectMake(113, 10, 300, 20)];
                content.backgroundColor = [UIColor clearColor];
                content.font = [UIFont systemFontOfSize:14];
                content.tag = 401;
                content.textColor = [UIColor darkGrayColor];
                [cell addSubview:content];
                break;
            }
            case 4:
            {
                UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 300, 20)];
                title.text = @"卡种组织 ";
                title.backgroundColor = [UIColor clearColor];
                title.textColor = [UIColor lightGrayColor];
                title.font = [UIFont systemFontOfSize:14];
                [cell addSubview:title];
                
                UILabel *content = [[UILabel alloc] initWithFrame:CGRectMake(90, 10, 300, 20)];
                content.backgroundColor = [UIColor clearColor];
                content.font = [UIFont systemFontOfSize:14];
                content.tag = 500;
                content.textColor = [UIColor darkGrayColor];
                [cell addSubview:content];
                break;
            }
            case 5:
            {
                UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 300, 20)];
                title.text = @"币       种 ";
                title.backgroundColor = [UIColor clearColor];
                title.textColor = [UIColor lightGrayColor];
                title.font = [UIFont systemFontOfSize:14];
                [cell addSubview:title];
                
                UILabel *content = [[UILabel alloc] initWithFrame:CGRectMake(90, 10, 300, 20)];
                content.backgroundColor = [UIColor clearColor];
                content.font = [UIFont systemFontOfSize:14];
                content.tag = 600;
                content.textColor = [UIColor darkGrayColor];
                [cell addSubview:content];
                break;
            }
            case 6:
            {
                UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 300, 20)];
                title.text = @"增值服务 ";
                title.backgroundColor = [UIColor clearColor];
                title.textColor = [UIColor lightGrayColor];
                title.font = [UIFont systemFontOfSize:14];
                [cell addSubview:title];
                
                UILabel *content = [[UILabel alloc] initWithFrame:CGRectMake(20, 40, 280, 20)];
                content.numberOfLines = 0;
                content.backgroundColor = [UIColor clearColor];
                content.font = [UIFont systemFontOfSize:14];
                content.textColor = [UIColor darkGrayColor];
                content.tag = 700;
                [cell addSubview:content];
                break;
            }
            default:
                break;
        }
    }
    
    // 刷新数据
    switch (indexPath.row) {
        case 0:
        {
            UILabel *name = (UILabel*)[cell viewWithTag:100];
            name.text = _product.name;

            EGOImageView *card = (EGOImageView*)[cell viewWithTag:101];
            
            NSString *imageURLString = [[NSString stringWithFormat:@"http://%@/assets/upload/%@", kServerAddr, _product.pic] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            card.imageURL = [NSURL URLWithString:imageURLString];
            
//            NSInteger level = arc4random()%5;
            NSInteger level = round(_product.score.doubleValue);
            for (NSInteger i = level; i < 5; i++)
            {
                UIImageView *star = (UIImageView*)[cell viewWithTag:102+i];
                [star setHidden:YES];
            }
//            UILabel * collectTimesLabel = (UILabel *)[cell.contentView viewWithTag:112];
            UILabel * applyTimesLabel = (UILabel *)[cell.contentView viewWithTag:114];
//            collectTimesLabel.text = [NSString stringWithFormat:@"%@ ♡",_product.bookmarkTimes ? _product.bookmarkTimes : @""] ;
            applyTimesLabel.text = _product.totalNumber;
            break;
        }
        case 1:
        {
            UILabel *content = (UILabel*)[cell viewWithTag:200];
            content.text = _product.type;
            break;
        }
        case 2:
        {
            UILabel *content = (UILabel*)[cell viewWithTag:300];
            content.text = _product.gradeName;
            break;
        }
        case 3:
        {
            UIImageView *bankico = (UIImageView*)[cell viewWithTag:400];
            bankico.image = [UIImage imageNamed:[NSString stringWithFormat:@"bank_%@", _product.bankID]];
            
            UILabel *content = (UILabel*)[cell viewWithTag:401];
            content.text = _product.bankName;
            break;
        }
        case 4:
        {
            UILabel *content = (UILabel*)[cell viewWithTag:500];
            content.text = _product.org;;
            break;
        }
        case 5:
        {
            UILabel *content = (UILabel*)[cell viewWithTag:600];
            content.text = _product.currency;
            break;
        }
        case 6:
        {
            UILabel *content = (UILabel*)[cell viewWithTag:700];
            [[self class] reSizeLabel:content withText:_product.addOn rowHeight:18];
        }
        default:
            break;
    }
    return cell;
}

- (void)buttonClicked:(UIButton *)button
{
//    if (button.tag == 121)
//    {
//        //收藏
//    }
//    else
    if (button.tag == 122)
    {
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"分享" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"分享到新浪", @"分享到腾讯", @"分享到微信", nil];
        
        [sheet showInView:self.view];
    }
    else if (button.tag == 123)
    {
        //评价
        RateController * rateController = [[RateController alloc] init];
        rateController.product = self.product;
        [self.navigationController pushViewController:rateController animated:YES];
    }
    else if (button.tag == 107)
    {
        RecommendViewController *recommendViewController = [[RecommendViewController alloc] initWithNibName:@"RecommendViewController" bundle:nil];
        recommendViewController.applyURL = _product.applyURL;
        
        [self.navigationController pushViewController:recommendViewController animated:YES];
    }
}

- (void)shareWithSina
{
//    [_sinaOAuth requestWithURL:@"statuses/update.json"
//                       params:[NSMutableDictionary dictionaryWithObjectsAndKeys:message, @"status", nil]
//                   httpMethod:@"POST"
//                     delegate:self];
    
    [self.sinaEngine requestWithPath:@"statuses/update.json" params:@{@"status" : message} httpMethod:@"POST"];
}

- (void)shareWithTencent
{
//    [weiboEngine postTextTweetWithFormat:@"json"
//                                 content:message
//                                clientIP:@"10.10.1.31"
//                               longitude:nil
//                             andLatitude:nil
//                             parReserved:nil
//                                delegate:self
//                               onSuccess:@selector(successCallBack:)
//                               onFailure:@selector(failureCallBack:)];
    
    [self.tencentEngine requestWithPath:@"t/add" params:@{@"content" : message} httpMethod:@"POST"];
}

#pragma mark - UIActionSheetDelegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        if (!self.sinaEngine) {
            self.sinaEngine = [[SinaWBEngine alloc] init];
            self.sinaEngine.rootViewController = self;
            self.sinaEngine.delegate = self;
        }
    
        if ([self.sinaEngine isAuthValid])
            [self shareWithSina];
        else
            [self.sinaEngine logIn];
    }else if (buttonIndex == 1) {
        
        if (!self.tencentEngine) {
            self.tencentEngine = [[TencentWBEngine alloc] init];
            self.tencentEngine.rootViewController = self;
            self.tencentEngine.delegate = self;
        }
        
        if ([self.tencentEngine isAuthValid])
            [self shareWithTencent];
        else
            [self.tencentEngine logIn];
    }else if (buttonIndex == 2) {
        // 分享到微信
        SendMessageToWXReq *request = [[SendMessageToWXReq alloc] init];
        request.scene = WXSceneSession;
        request.bText = YES;
        request.text = message;
        if (![WXApi isWXAppInstalled] || ![WXApi isWXAppSupportApi])
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                             message:@"您的设备没有安装微信客户端或版本过低"
                                                            delegate:self
                                                   cancelButtonTitle:@"确定"
                                                   otherButtonTitles:@"现在下载", nil];
            [alert show];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FromWX"];
            [WXApi sendReq:request];
        }

    }
}


#pragma mark OAuthClientDelegate methods
- (void)oauthClientDidLogIn:(id)wbEngine
{
    if ([wbEngine isKindOfClass:[SinaWBEngine class]]) {
        [self shareWithSina];
    } else {
        [self shareWithTencent];
    }
}

- (void)oauthClientRequestDidSuccess:(id)engine
{
    [self quickAlertView:@"分享成功！"];
}

- (void)oauthClientRequestDidFail:(id)engine errorMsg:(NSString *)errorMsg
{
    [self quickAlertView:errorMsg];
}

//
//#pragma mark - Private methods
//- (void)removeAuthData
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SinaWeiboAuthData"];
//}
//
//- (void)storeAuthData
//{
//    NSDictionary *authData = [NSDictionary dictionaryWithObjectsAndKeys:
//                              _sinaOAuth.accessToken, @"AccessTokenKey",
//                              _sinaOAuth.expirationDate, @"ExpirationDateKey",
//                              _sinaOAuth.userID, @"UserIDKey",
//                              _sinaOAuth.refreshToken, @"refresh_token", nil];
//    [[NSUserDefaults standardUserDefaults] setObject:authData forKey:@"SinaWeiboAuthData"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//#pragma mark - Tencent login callback
//
////登录成功回调
//- (void)onSuccessLogin
//{
//    //    [indicatorView stopAnimating];
//    [self shareWithTencent];
//}
//
////登录失败回调
//- (void)onFailureLogin:(NSError *)error
//{
//    //    [indicatorView stopAnimating];
//    NSString *message1 = [[NSString alloc] initWithFormat:@"%@",[NSNumber numberWithInteger:[error code]]];
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[error domain]
//                                                        message:message1
//                                                       delegate:self
//                                              cancelButtonTitle:@"确定"
//                                              otherButtonTitles:nil];
//    [alertView show];
//}
//
////授权成功回调
//- (void)onSuccessAuthorizeLogin
//{
//    //    [indicatorView stopAnimating];
//}
//
//#pragma mark - Tencent callback
//- (void)successCallBack:(id)result{
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
//                                                        message:[NSString stringWithFormat:@"Post status \"%@\" Success!", @"测试我爱卡"]
//                                                       delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//    [alertView show];
//}
//
//- (void)failureCallBack:(NSError *)error{
//    NSLog(@"error: %@", error);
//}
//
//#pragma mark - SinaWeibo Delegate
//- (void)sinaweiboDidLogIn:(SinaWeibo *)sinaweibo
//{
//    NSLog(@"sinaweiboDidLogIn userID = %@ accesstoken = %@ expirationDate = %@ refresh_token = %@", sinaweibo.userID, sinaweibo.accessToken, sinaweibo.expirationDate,sinaweibo.refreshToken);
//    _sinaOAuth = sinaweibo;
//    [self storeAuthData];
//    
//    [self shareWithSina];
//}
//
//- (void)sinaweiboDidLogOut:(SinaWeibo *)sinaweibo
//{
//    NSLog(@"sinaweiboDidLogOut");
//    [self removeAuthData];
//}
//
//- (void)sinaweiboLogInDidCancel:(SinaWeibo *)sinaweibo
//{
//    NSLog(@"sinaweiboLogInDidCancel");
//}
//
//- (void)sinaweibo:(SinaWeibo *)sinaweibo logInDidFailWithError:(NSError *)error
//{
//    NSLog(@"sinaweibo logInDidFailWithError %@", error);
//}
//
//- (void)sinaweibo:(SinaWeibo *)sinaweibo accessTokenInvalidOrExpired:(NSError *)error
//{
//    NSLog(@"sinaweiboAccessTokenInvalidOrExpired %@", error);
//    [self removeAuthData];
//}
//
//#pragma mark - SinaWeiboRequest Delegate
//
//- (void)request:(SinaWeiboRequest *)request didFailWithError:(NSError *)error
//{
//    NSLog(@"error = %@", error);
//    if ([request.url hasSuffix:@"statuses/update.json"])
//    {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
//                                                            message:[NSString stringWithFormat:@"重复内容！"]
//                                                           delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//        [alertView show];
//        
//        NSLog(@"Post status failed with error : %@", error);
//    }
//}
//
//- (void)request:(SinaWeiboRequest *)request didFinishLoadingWithResult:(id)result
//{
//    if ([request.url hasSuffix:@"statuses/update.json"])
//    {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
//                                                            message:[NSString stringWithFormat:@"Post status \"%@\" succeed!", [result objectForKey:@"text"]]
//                                                           delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//        [alertView show];
//    }
//}

@end

