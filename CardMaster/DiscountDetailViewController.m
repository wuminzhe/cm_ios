//
//  DiscountDetailViewController.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-18.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "DiscountDetailViewController.h"
#import "Discount.h"
#import "NSString+Extension.h"
#import "DiscountMapViewController.h"
#import "DiscountFeedbackViewController.h"
#import "CMUtil.h"
#import "GlobalHeader.h"
#import "SQLGlobal.h"
#import "LvToast.h"
#import "EGOImageView.h"
#import "CMConstants.h"

@interface DiscountDetailViewController ()

@property (nonatomic, strong) SinaWBEngine *sinaEngine;
@property (nonatomic, strong) TencentWBEngine *tencentEngine;

@end

@implementation DiscountDetailViewController {
    IBOutlet UITableView *mTableView;
    DiscountDescriptionCell *descriptionCell;
    NSString *content;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        content = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"优惠详情";
    
//    sinaWeibo = nil;
//    weiboEngine = nil;
    
    engine = [[DiscountEngine alloc] initWithHostName:kServerAddr];
    
    content = [NSString stringWithFormat:@"持%@信用卡，在%@享受%@！那谁，带上你的卡，走起！", _discount.bankName, _discount.shopName, _discount.rate == nil ? @"特惠" : _discount.rate];
    
    // 持【a****】信用卡，在【b****】享受【c****】！那谁，带上你的卡，走起！【d***】
}

- (void)viewDidUnload
{    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (void)backAction
{
    if (self.navigationController.viewControllers.count == 1)
        [self dismissModalViewControllerAnimated:YES];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        descriptionCell = [tableView dequeueReusableCellWithIdentifier:@"DiscountDescriptionCell"];
        
        if (!descriptionCell) {
            descriptionCell = [DiscountDescriptionCell getInstance];
            descriptionCell.delegate = self;
        }
        
        descriptionCell.type = _discount.type;
        
        if (_discount.type == 1) {
            descriptionCell.discountNameLabel.text = _discount.bankName;
        } else {
            descriptionCell.discountNameLabel.text = _discount.shopName;
        }
        
        descriptionCell.rate = _discount.rate;
        
        if (_discount.shopAverageConsumption.length > 0) {
            descriptionCell.averageLabel.text = _discount.shopAverageConsumption;
            descriptionCell.sheepLabel.hidden = NO;
        } else {
            descriptionCell.averageLabel.text = @"不详";
            descriptionCell.sheepLabel.hidden = YES;
        }
        descriptionCell.bookmarkLabel.text = _discount.hot;
        NSString *urlString = [_discount.pic stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        descriptionCell.discountIconView.placeholderImage = [UIImage imageNamed:@"shop_discount_none.png"];
        descriptionCell.discountIconView.imageURL = [NSURL URLWithString:urlString];
        
        return descriptionCell;
    }
    
    DiscountDetailCell *cell = nil;
    if (indexPath.row == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"EffectiveDateCell"];
        
        if (!cell)
            cell = [DiscountDetailCell getEffectiveDateCell];
        cell.discountTextLabel.text = _discount.endTime;
    } else if (indexPath.row == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"BankDetailCell"];
        
        if (!cell)
            cell = [DiscountDetailCell getBankCell];
        
        cell.bankIconView.image = [UIImage imageNamed:[NSString stringWithFormat:@"bank_%@.png", _discount.bankID]];
        
        cell.discountTextLabel.text = _discount.bankName;
    } else if (indexPath.row == 3) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"AddressDetailCell"];
        
        if (!cell) {
            cell = [DiscountDetailCell getAddressCell];
            cell.delegate = self;
        }
        
        cell.discountTitleLabel.text = @"地";
        cell.discountTitle2Label.text = @"址：";
        cell.discountTextLabel.text = _discount.shopAddress;
        
        if (_discount.shopAddress.length > 0 || _discount.shopAddress) {
        
            CGSize size = [_discount.shopAddress calculateTextSize:kAddressLabelWidth andFont:[UIFont systemFontOfSize:14.0]];
        
            CGRect labelRect = cell.discountTextLabel.frame;
            labelRect.size = size;
        
            cell.discountTextLabel.frame = labelRect;
        
            // 计算分割线位置
            CGRect separatorRect = cell.separatorImageView.frame;
            separatorRect.origin.y = labelRect.origin.y + size.height + 9;
            cell.separatorImageView.frame = separatorRect;
        }
    } else if (indexPath.row == 4) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"PhoneDetailCell"];
        
        if (!cell) {
            cell = [DiscountDetailCell getPhoneCell];
            cell.delegate = self;
        }
        
        cell.discountTitleLabel.text = @"电";
        cell.discountTitle2Label.text = @"话：";
        cell.discountTextLabel.text = _discount.shopPhoneNumber;
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"DiscountDetailCell"];
        
        if (!cell)
            cell = [DiscountDetailCell getDetailCell];
        
        if (indexPath.row == 5) {
            cell.discountTitleLabel.text = @"活动详情";
            cell.discountTextLabel.text = _discount.discountDescription;
            
            // 根据内容，计算label高度
            CGSize size = [_discount.discountDescription calculateTextSize:kDetailLabelWidth andFont:[UIFont systemFontOfSize:14.0]];
            
            CGRect labelRect = cell.discountTextLabel.frame;
            labelRect.size = size;
            
            cell.discountTextLabel.frame = labelRect;
            
            // 计算分割线位置
            CGRect separatorRect = cell.separatorImageView.frame;
            separatorRect.origin.y = labelRect.origin.y + size.height + 9;
            
            cell.separatorImageView.frame = separatorRect;
        } else if (indexPath.row == 6) {
            cell.discountTitleLabel.text = @"商铺介绍";
            cell.discountTextLabel.text = _discount.shopDesc;
            
            // 根据内容，计算label高度
            CGSize size = [_discount.shopDesc calculateTextSize:kDetailLabelWidth andFont:[UIFont systemFontOfSize:14.0]];
            
            CGRect labelRect = cell.discountTextLabel.frame;
            labelRect.size = size;
            
            cell.discountTextLabel.frame = labelRect;
            
            // 计算分割线位置
            CGRect separatorRect = cell.separatorImageView.frame;
            separatorRect.origin.y = labelRect.origin.y + size.height + 9;
            cell.separatorImageView.frame = separatorRect;
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return 128.0;
    else if (indexPath.row <= 2 || indexPath.row == 4) {
        return 40.0f;
    } else if (indexPath.row == 3) {
        CGSize size = [_discount.shopAddress calculateTextSize:kAddressLabelWidth
                                                       andFont:[UIFont systemFontOfSize:14.0]];
        return MAX(40, size.height + 18) ;
    }
    else if (indexPath.row == 5) {
        CGSize size = [_discount.discountDescription calculateTextSize:kDetailLabelWidth
                                                               andFont:[UIFont systemFontOfSize:14.0]];
        
        return MAX(58.0, size.height + 29.0 + 9.0);
    } else {
        CGSize size = [_discount.shopDesc calculateTextSize:kDetailLabelWidth
                                                    andFont:[UIFont systemFontOfSize:14.0]];
        
        return MAX(58.0, size.height + 29.0 + 9.0);
    }
}

#pragma mark - DiscountDetailCellDelegate methods
- (void)changeToMapView:(id)sender
{
    DiscountMapViewController *mapViewController = [[DiscountMapViewController alloc] initWithNibName:@"DiscountMapViewController" bundle:nil];
    mapViewController.discount = self.discount;
    [self.navigationController pushViewController:mapViewController animated:YES];
}

- (void)takePhoneCall:(id)sender
{
    if (!_discount.shopPhoneNumber)
        return;
    
    NSString *message = [NSString stringWithFormat:@"是否拨打热线电话:\n%@", _discount.shopPhoneNumber];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示信息"
                                                        message:message                                                       delegate:self
                                              cancelButtonTitle:@"拨打"
                                              otherButtonTitles:@"取消", nil];
    
    [alertView show];
}

#pragma mark - DiscountDescriptionCellDelegate methods
- (void)handleBookmark:(id)sender
{
    if (![[SQLGlobal sharedInstance] getDiscountStatus:_discount]) {
        
        UIImage *image = descriptionCell.discountIconView.image;
        
        if (image) {
            NSData *imageData = UIImageJPEGRepresentation(image, 1);
            NSString *imageName = [NSString stringWithFormat:@"%@_%@", _discount.bankID, [_discount.pic lastPathComponent]];
            
            NSString *imagePath = [MTLP(DISCOUNT_IMAGE_FILE) stringByAppendingPathComponent:imageName];
            
            [imageData writeToFile:imagePath atomically:YES];
        }
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:2];
        params[@"code"] = [CMUtil getCode];
        params[@"discount_id"] = _discount.discountID;
        
        _discount.hot = [NSString stringWithFormat:@"%d", _discount.hot.integerValue + 1];
        
        [engine addFavoriteDiscount:params onCompletion:^(BOOL status) {
            if (status) {
                [LvToast showWithText:@"收藏成功！" bottomOffset:60 duration:0.8];
            }
        } onError:^(NSError *error) {
            [LvToast showWithText:@"收藏失败！" bottomOffset:260 duration:0.8];
        }];
    } else {
        [LvToast showWithText:@"已收藏！" bottomOffset:60 duration:0.8];
    }
    
    // 本地收藏
    [[SQLGlobal sharedInstance] updateDiscount:_discount];
    [mTableView reloadData];
}

- (void)handleFeedback:(id)sender
{
    DiscountFeedbackViewController *discountFeedbackViewController = [[DiscountFeedbackViewController alloc] initWithNibName:@"DiscountFeedbackViewController" bundle:nil];
    
    discountFeedbackViewController.discount = self.discount;
    
    [self.navigationController pushViewController:discountFeedbackViewController animated:YES];
}

- (void)handleShare:(id)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"分享" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"分享到新浪微博", @"分享到腾讯微博", @"分享到微信", nil];
    [sheet showInView:self.view];
}

#pragma mark - UIAlertViewDelegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", _discount.shopPhoneNumber]];
        
        [[UIApplication sharedApplication] openURL:url];
    }
}


#pragma mark - UIActionSheetDelegate methods
//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    switch (buttonIndex) {
//        case 0:
//            // 分享到新浪微博
//            if (!sinaWeibo) {
//                if (!sinaWeibo) {
//                    sinaWeibo = [[SinaWeibo alloc] initWithAppKey:kSinaAppKey
//                                                        appSecret:kSinaAppSecret
//                                                   appRedirectURI:kSinaAppRedirect
//                                                      andDelegate:self];
//                    
//                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//                    NSDictionary *sinaweiboInfo = [defaults objectForKey:@"SinaWeiboAuthData"];
//                    if ([sinaweiboInfo objectForKey:@"AccessTokenKey"] && [sinaweiboInfo objectForKey:@"ExpirationDateKey"] && [sinaweiboInfo objectForKey:@"UserIDKey"])
//                    {
//                        sinaWeibo.accessToken = [sinaweiboInfo objectForKey:@"AccessTokenKey"];
//                        sinaWeibo.expirationDate = [sinaweiboInfo objectForKey:@"ExpirationDateKey"];
//                        sinaWeibo.userID = [sinaweiboInfo objectForKey:@"UserIDKey"];
//                    }
//                }
//                
//                if (!sinaWeibo.isAuthValid) {
//                    [sinaWeibo logIn];
//                } else {
//                    [self shareWithSina];
//                }
//            }
//            break;
//        case 1:
//            // 分享到腾讯微博
//            if (!weiboEngine) {
//                weiboEngine = [[TCWBEngine alloc] initWithAppKey:kTencentAppKey andSecret:kTencentAppSecret andRedirectUrl:kTencentAppRedirect];
//                [weiboEngine setRootViewController:self];
//            }
//            
//            if (weiboEngine.isLoggedIn && !weiboEngine.isAuthorizeExpired) {
//                [self shareWithTencent];
//            } else {
//                [weiboEngine logInWithDelegate:self
//                                     onSuccess:@selector(onSuccessLogin)
//                                     onFailure:@selector(onFailureLogin:)];
//            }
//            break;
//        case 2:
//            ;
//            // 分享到微信
//            SendMessageToWXReq *request = [[SendMessageToWXReq alloc] init];
//            request.scene = WXSceneSession;
//            request.bText = YES;
//            request.text = content;
//            if (![WXApi isWXAppInstalled] || ![WXApi isWXAppSupportApi])
//            {
//                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"提示"
//                                                                 message:@"您的设备没有安装微信客户端或版本过低"
//                                                                delegate:self
//                                                       cancelButtonTitle:@"确定"
//                                                       otherButtonTitles:@"现在下载", nil];
//                [alert show];
//            }
//            else
//            {
//                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FromWX"];
//                [WXApi sendReq:request];
//            }
//
//            break;
//    }
//}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            // 分享到新浪微博
            if (!self.sinaEngine) {
                self.sinaEngine = [[SinaWBEngine alloc] init];
                self.sinaEngine.rootViewController = self;
                self.sinaEngine.delegate = self;
            }
            
            if ([self.sinaEngine isAuthValid]) {
                [self shareWithSina];
            } else {
                [self.sinaEngine logIn];
            }
            break;
        case 1:
            if (!self.tencentEngine) {
                self.tencentEngine = [[TencentWBEngine alloc] init];
                self.tencentEngine.rootViewController = self;
                self.tencentEngine.delegate = self;
            }
            
            if ([self.tencentEngine isAuthValid]) {
                [self shareWithTencent];
            } else {
                [self.tencentEngine logIn];
            }
            
            break;
        case 2:
            ;
            // 分享到微信
            SendMessageToWXReq *request = [[SendMessageToWXReq alloc] init];
            request.scene = WXSceneSession;
            request.bText = YES;
            request.text = content;
            if (![WXApi isWXAppInstalled] || ![WXApi isWXAppSupportApi])
            {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                                 message:@"您的设备没有安装微信客户端或版本过低"
                                                                delegate:self
                                                       cancelButtonTitle:@"确定"
                                                       otherButtonTitles:@"现在下载", nil];
                [alert show];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FromWX"];
                [WXApi sendReq:request];
            }
            
            break;
    }
}


//- (void)removeAuthData
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SinaWeiboAuthData"];
//}
//
//- (void)storeAuthData
//{
//    NSDictionary *authData = [NSDictionary dictionaryWithObjectsAndKeys:
//                              sinaWeibo.accessToken, @"AccessTokenKey",
//                              sinaWeibo.expirationDate, @"ExpirationDateKey",
//                              sinaWeibo.userID, @"UserIDKey",
//                              sinaWeibo.refreshToken, @"refresh_token", nil];
//    [[NSUserDefaults standardUserDefaults] setObject:authData forKey:@"SinaWeiboAuthData"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}

- (void)shareWithSina
{
//    [sinaWeibo requestWithURL:@"statuses/update.json"
//                       params:[NSMutableDictionary dictionaryWithObjectsAndKeys:content, @"status", nil]
//                   httpMethod:@"POST"
//                     delegate:self];
    
    [self.sinaEngine requestWithPath:@"statuses/update.json" params:@{@"status" : content} httpMethod:@"POST"];
}

- (void)shareWithTencent
{
//    [weiboEngine postTextTweetWithFormat:@"json"
//                                 content:content
//                                clientIP:@"10.10.1.31"
//                               longitude:nil
//                             andLatitude:nil
//                             parReserved:nil
//                                delegate:self
//                               onSuccess:@selector(successCallBack:)
//                               onFailure:@selector(failureCallBack:)];
    
    [self.tencentEngine requestWithPath:@"t/add" params:@{@"content" : content} httpMethod:@"POST"];
}

#pragma mark OAuthClientDelegate methods
- (void)oauthClientDidLogIn:(id)wbEngine
{
    if ([wbEngine isKindOfClass:[SinaWBEngine class]]) {
        [self shareWithSina];
    } else {
        [self shareWithTencent];
    }
}

- (void)oauthClientRequestDidSuccess:(id)engine
{
    [self quickAlertView:@"分享成功！"];
}

- (void)oauthClientRequestDidFail:(id)engine errorMsg:(NSString *)errorMsg
{
    [self quickAlertView:errorMsg];
}

//#pragma mark - Tencent login callback
//
////登录成功回调
//- (void)onSuccessLogin
//{
////    [indicatorView stopAnimating];
//    [self shareWithTencent];
//}
//
////登录失败回调
//- (void)onFailureLogin:(NSError *)error
//{
////    [indicatorView stopAnimating];
//    NSString *message = [[NSString alloc] initWithFormat:@"%@",[NSNumber numberWithInteger:[error code]]];
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[error domain]
//                                                        message:message
//                                                       delegate:self
//                                              cancelButtonTitle:@"确定"
//                                              otherButtonTitles:nil];
//    [alertView show];
//}
//
////授权成功回调
//- (void)onSuccessAuthorizeLogin
//{
////    [indicatorView stopAnimating];
//}
//
//#pragma mark - Tencent callback
//- (void)successCallBack:(id)result{
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
//                                                        message:[NSString stringWithFormat:@"Post status \"%@\" Success!", @"测试我爱卡"]
//                                                       delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//    [alertView show];
//}
//
//- (void)failureCallBack:(NSError *)error{
//    NSLog(@"error: %@", error);
//}
//
//#pragma mark - SinaWeibo Delegate
//- (void)sinaweiboDidLogIn:(SinaWeibo *)sinaweibo
//{
//    NSLog(@"sinaweiboDidLogIn userID = %@ accesstoken = %@ expirationDate = %@ refresh_token = %@", sinaweibo.userID, sinaweibo.accessToken, sinaweibo.expirationDate,sinaweibo.refreshToken);
//    sinaWeibo = sinaweibo;
//    [self storeAuthData];
//    
//    [self shareWithSina];
//}
//
//- (void)sinaweiboDidLogOut:(SinaWeibo *)sinaweibo
//{
//    NSLog(@"sinaweiboDidLogOut");
//    [self removeAuthData];
//}
//
//- (void)sinaweiboLogInDidCancel:(SinaWeibo *)sinaweibo
//{
//    NSLog(@"sinaweiboLogInDidCancel");
//}
//
//- (void)sinaweibo:(SinaWeibo *)sinaweibo logInDidFailWithError:(NSError *)error
//{
//    NSLog(@"sinaweibo logInDidFailWithError %@", error);
//}
//
//- (void)sinaweibo:(SinaWeibo *)sinaweibo accessTokenInvalidOrExpired:(NSError *)error
//{
//    NSLog(@"sinaweiboAccessTokenInvalidOrExpired %@", error);
//    [self removeAuthData];
//}
//
//#pragma mark - SinaWeiboRequest Delegate
//
//- (void)request:(SinaWeiboRequest *)request didFailWithError:(NSError *)error
//{
//    if ([request.url hasSuffix:@"statuses/update.json"])
//    {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
//                                                            message:[NSString stringWithFormat:@"Post status \"%@\" failed!", @"测试我爱卡"]
//                                                           delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//        [alertView show];
//        
//        NSLog(@"Post status failed with error : %@", error);
//    }
//}
//
//- (void)request:(SinaWeiboRequest *)request didFinishLoadingWithResult:(id)result
//{
//    if ([request.url hasSuffix:@"statuses/update.json"])
//    {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
//                                                            message:[NSString stringWithFormat:@"Post status \"%@\" succeed!", [result objectForKey:@"text"]]
//                                                           delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//        [alertView show];
//    }
//}
@end
