//
//  BankCell.m
//  cardMaster
//
//  Created by Zhang Ming on 12-12-20.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BankCell.h"

@implementation BankCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (BankCell *)getBankCell
{
    return [[[NSBundle mainBundle] loadNibNamed:@"BankCell" owner:nil options:NULL] lastObject];
}

+ (BankCell *)getAllBankCell
{
    return [[[NSBundle mainBundle] loadNibNamed:@"AllBankCell" owner:nil options:NULL] lastObject];
}

@end
