//
//  Bill.h
//  CardMaster
//
//  Created by wenjun on 13-1-14.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bill : NSObject

@property (strong,nonatomic) NSString * pk;
@property (strong,nonatomic) NSString * emailID;
@property (strong,nonatomic) NSString * cardNumber;
@property (strong,nonatomic) NSString * bankName;
@property (strong,nonatomic) NSString * sendTime;
@property (strong,nonatomic) NSString * paymentTime;
@property (strong,nonatomic) NSString * statementTime;
@property (strong,nonatomic) NSString * statementCycle;
@property (strong,nonatomic) NSString * balance;
@property (strong,nonatomic) NSString * minPayment;
@property (strong,nonatomic) NSString * creditLimit;
@property (strong,nonatomic) NSString * availablePoints;

@end
