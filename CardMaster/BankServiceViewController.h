//
//  BankServiceViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-26.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"

@interface BankServiceViewController : BaseNavigationController<UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView *mTableView;
}

@end
