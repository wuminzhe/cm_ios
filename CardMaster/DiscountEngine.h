//
//  DiscountEngine.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-18.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MKNetworkEngine.h"

@interface DiscountEngine : MKNetworkEngine

typedef void (^DiscountListResponseBlock) (NSMutableDictionary *results);
typedef void (^AddFavoriteDiscountResponseBlock) (BOOL status);
typedef void (^FeedbackResponseBlock) (BOOL status);

// 获得优惠信息
- (MKNetworkOperation *)getDiscountList:(NSMutableDictionary *)params
                             nearbyFlag:(BOOL)flag
                           onCompletion:(DiscountListResponseBlock)completionBlock
                                onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)searchDiscountList:(NSMutableDictionary *)params
                              onCompletion:(DiscountListResponseBlock)completionBlock
                                   onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)addFavoriteDiscount:(NSMutableDictionary *)params
                               onCompletion:(AddFavoriteDiscountResponseBlock)completionBlock
                                    onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)feedback:(NSMutableDictionary *)params
                    onCompletion:(FeedbackResponseBlock)completionBlock
                         onError:(MKNKErrorBlock)errorBlock;

@end
