//
//  LoadingView.m
//  LifeShanghai
//
//  Created by wenjun on 12-11-27.
//
//

#import "LoadingView.h"
#define DefaultLoadingText @"正在加载"

@interface LoadingView()

@property (nonatomic) NSInteger step;

@property (strong,nonatomic) NSTimer * timer;

@end

@implementation LoadingView

- (void)initialize
{
    _indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    _label = [[UILabel alloc] init];
    self.label.backgroundColor = [UIColor clearColor];
    self.label.font = [UIFont systemFontOfSize:13];
    self.label.textColor = [UIColor lightGrayColor];
    self.text = DefaultLoadingText;
    self.indicator.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.hidden = YES;
}

- (void)setText:(NSString *)text
{
    if (!text)
    {
        text = DefaultLoadingText;
    }
    self.height = MAX(self.height, 16);
    _text = text;
    CGFloat textWidth = [[NSString stringWithFormat:@"%@...",_text] sizeWithFont:self.label.font].width;
    self.indicator.frame = CGRectMake(0, (self.height - self.indicator.frame.size.height) / 2, _indicator.frame.size.width,_indicator.frame.size.height);
    self.label.frame = CGRectMake(_indicator.frame.size.width + 5,(self.height - 16)/2,textWidth,16);
    CGSize newSize = CGSizeMake(_indicator.frame.size.width + _label.frame.size.width, self.height);
    self.frame = [[self class] centerNewSize:newSize oldFrame:self.frame];
    [self addSubview:_indicator];
    [self addSubview:_label];
    [self refreshText];
}

+ (CGRect)centerNewSize:(CGSize)size oldFrame:(CGRect)oldFrame
{
    CGPoint center = CGPointMake((CGRectGetMinX(oldFrame) + CGRectGetMaxX(oldFrame)) / 2,
                                 (CGRectGetMinY(oldFrame) + CGRectGetMaxY(oldFrame)) / 2);
    CGPoint origin = CGPointMake(roundf(center.x - size.width / 2), roundf(center.y - size.height / 2));
    return CGRectMake(origin.x, origin.y, roundf(size.width), roundf(size.height));
}

- (void)refreshText
{
    if (self.step == 0)
    {
        self.label.text = [NSString stringWithFormat:@"%@...",self.text];
    }
    else if (self.step == 1)
    {
        self.label.text = [NSString stringWithFormat:@"%@",self.text];
    }
    else if (self.step == 2)
    {
        self.label.text = [NSString stringWithFormat:@"%@.",self.text];
    }
    else if (self.step == 3)
    {
        self.label.text = [NSString stringWithFormat:@"%@..",self.text];
    }
}

- (void)step:(NSTimer *)timer
{
    if (_step > 3)
    {
        self.step = 0;
    }
    [self refreshText];
    _step ++;
}

- (void)start
{
    [self.indicator startAnimating];
    if (!self.timer || ![self.timer isValid])
    {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:.42 target:self selector:@selector(step:) userInfo:nil repeats:YES];
    }
    self.alpha = 1;
    self.hidden = NO;
}

- (void)stop
{
    self.hidden = YES;
    [self.indicator stopAnimating];
    [self.timer invalidate];
    self.timer = nil;
    self.step = 0;
    [self refreshText];
}

- (void)stopWithAnimation
{
    if (self.hidden)
    {
        [self stop];
        return;
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(stop)];
    [UIView setAnimationDuration:.3];
    self.alpha = 0.02;
    [UIView commitAnimations];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initialize];
    }
    return self;
}

@end
