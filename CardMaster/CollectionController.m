//
//  CollectionController.m
//  CardMaster
//
//  Created by wenjun on 13-1-18.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "CollectionController.h"
#import "BankDiscountCell.h"
#import "DiscountCell.h"
#import "Discount.h"
#import "GTMUtil.h"
#import "LoadingView.h"
#import "CMUtil.h"
#import "LvToast.h"
#import "JSONKit.h"
#import "BankDiscountDetailViewController.h"
#import "NSDictionary+Extension.h"
#import "UINavigationController+Extension.h"
#import "DiscountDetailViewController.h"

@interface CollectionController () <UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{
    IBOutlet UIControl * leftChoice;
    IBOutlet UIControl * rightChoice;
    IBOutlet UIImageView * switchImgView;
    NSInteger selectedIndex;
    IBOutlet UITableView * leftTable;
    IBOutlet UITableView * rightTable;
    IBOutlet LoadingView * loadingView;
    
    IBOutlet UIButton * editBtn;
    BOOL editing;
}

@property (strong,nonatomic) NSMutableArray * leftArray;
@property (strong,nonatomic) NSMutableArray * rightArray;
@property (unsafe_unretained,nonatomic) GTMHTTPFetcher * fetcher;
@property (unsafe_unretained,nonatomic) GTMHTTPFetcher * deleteFetcher;

- (IBAction)edit;

@end

@implementation CollectionController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"我的收藏";
    // Do any additional setup after loading the view from its nib.
//    [leftChoice addTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchDown];
//    [rightChoice addTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchDown];
    
    [leftChoice addTarget:self action:@selector(leftSelect:) forControlEvents:UIControlEventTouchUpInside];
    [rightChoice addTarget:self action:@selector(rightSelect:) forControlEvents:UIControlEventTouchUpInside];
    
//    [leftChoice addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpOutside | UIControlEventTouchDragExit];
//    [rightChoice addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpOutside | UIControlEventTouchDragExit];
    
    leftChoice.userInteractionEnabled = NO;
    
    self.leftArray = [NSMutableArray array];
    self.rightArray = [NSMutableArray array];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:editBtn];
}

- (void)updateEditBtn
{
    if (editing)
    {
        [editBtn setImage:[UIImage imageNamed:@"wancheng_btn"] forState:UIControlStateNormal];
        [editBtn setImage:[UIImage imageNamed:@"wancheng_btn_click"] forState:UIControlStateHighlighted];
    }
    else
    {
        [editBtn setImage:[UIImage imageNamed:@"edit"] forState:UIControlStateNormal];
        [editBtn setImage:[UIImage imageNamed:@"edit_click"] forState:UIControlStateHighlighted];
    }
}

- (IBAction)edit
{
    if (!editing)
    {
        if (selectedIndex == 0 && self.leftArray.count > 0)
        {
            [leftTable setEditing:YES animated:YES];
            editing = YES;
            [self updateEditBtn];
        }
        else if (selectedIndex == 1 && self.rightArray.count > 0)
        {
            [rightTable setEditing:YES animated:YES];
            editing = YES;
            [self updateEditBtn];
        }
    }
    else
    {
        if (selectedIndex == 0)
        {
            [leftTable setEditing:NO animated:YES];
        }
        else
        {
            [rightTable setEditing:NO animated:YES];
        }
        editing = NO;
        [self updateEditBtn];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"删除收藏" message:@"是否删除该条收藏" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"删除", nil];
    alert.tag = indexPath.row;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        Discount * discount = selectedIndex == 0 ? [self.leftArray objectAtIndex:alertView.tag] : [self.rightArray objectAtIndex:alertView.tag];
        [self.deleteFetcher stopFetching];
        self.deleteFetcher = [GTMUtil fetcherWithURLString:[NSString stringWithFormat:@"http://%@/api/del_favoritediscount",kServerAddr] params:@{@"code" : [CMUtil getCode] ,@"discount_id" : discount.discountID} HTTPType:@"post" urlEncode:NO];
        [self.deleteFetcher setProperty:[NSString stringWithFormat:@"%d",alertView.tag] forKey:@"index"];
        [self.deleteFetcher beginFetchWithDelegate:self didFinishSelector:@selector(deleteFetcher:data:error:)];
    }
}

- (void)deleteFetcher:(GTMHTTPFetcher *)fetcher data:(NSData *)data error:(NSError *)error
{
    BOOL success = NO;
    if (fetcher.statusCode == 200)
    {
        NSDictionary * jsonDict = [data objectFromJSONData];
        if ([[jsonDict stringForKey:@"status"] intValue] == 1)
        {
            success = YES;
        }
    }
    if (success)
    {
        NSInteger index = [[fetcher propertyForKey:@"index"] intValue];
        if (selectedIndex == 0)
        {
            [self.leftArray removeObjectAtIndex:index];
            [leftTable deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        }
        else
        {
            [self.rightArray removeObjectAtIndex:index];
            [rightTable deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    else
    {
        [LvToast showWithText:@"删除失败" duration:1];
    }
}

//- (void)touchDown:(UIControl *)control
//{
//    control.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.5];
//}
//
//- (void)cancel:(UIControl *)control
//{
//    control.backgroundColor = [UIColor clearColor];
//}

- (void)leftSelect:(UIControl *)control
{
//    control.backgroundColor = [UIColor clearColor];
    if (editing)
    {
        return;
    }
    selectedIndex = 0;
    switchImgView.image = [UIImage imageNamed:@"switch_btn"];
    leftChoice.userInteractionEnabled = NO;
    rightChoice.userInteractionEnabled = YES;
    [UIView transitionFromView:rightTable toView:leftTable duration:.35 options:UIViewAnimationOptionTransitionCrossDissolve completion:^(BOOL finished) {
        leftTable.frame = CGRectMake(0, 61, leftTable.frame.size.width, leftTable.frame.size.height);
        [self.view addSubview:leftTable];
        [rightTable removeFromSuperview];
    }];
    editBtn.enabled = self.leftArray.count > 0;
}

- (void)rightSelect:(UIControl *)control
{
    if (editing)
    {
        return;
    }
//    control.backgroundColor = [UIColor clearColor];
    selectedIndex = 1;
    switchImgView.image = [UIImage imageNamed:@"switch_btn2"];
    rightChoice.userInteractionEnabled = NO;
    leftChoice.userInteractionEnabled = YES;
    [UIView transitionFromView:leftTable toView:rightTable duration:.35 options:UIViewAnimationOptionTransitionCrossDissolve completion:^(BOOL finished) {
        rightTable.frame = CGRectMake(0, 61, leftTable.frame.size.width, leftTable.frame.size.height);
        [self.view addSubview:rightTable];
        [leftTable removeFromSuperview];
    }];
    editBtn.enabled = self.rightArray.count > 0;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.leftArray.count + self.rightArray.count == 0)
    {
        switchImgView.hidden = YES;
        [loadingView start];
        leftTable.hidden = YES;
        rightTable.hidden = YES;
    }
    rightTable.frame = CGRectMake(0, 61, leftTable.frame.size.width, leftTable.frame.size.height);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.leftArray.count + self.rightArray.count == 0)
    {
        [self performSelector:@selector(fetchFavo) withObject:nil afterDelay:.5];
    }
}

- (void)fetchFavo
{
    self.fetcher = [GTMUtil fetcherWithURLString:[NSString stringWithFormat:@"http://%@/api/favoritediscounts",kServerAddr] params:@{@"code" : [CMUtil getCode]} HTTPType:@"get" urlEncode:NO];
    [self.fetcher beginFetchWithDelegate:self didFinishSelector:@selector(favoFetcher:data:error:)];
}

- (void)favoFetcher:(GTMHTTPFetcher *)fetcher data:(NSData *)data error:(NSError *)error
{
    BOOL success = NO;
    if (fetcher.statusCode == 200)
    {
        NSDictionary * jsonDict = [data objectFromJSONData];
        NSInteger status = [[jsonDict stringForKey:@"status"] intValue];
        if (status == 1)
        {
            [self.leftArray removeAllObjects];
            [self.rightArray removeAllObjects];
            NSArray * jsonArray = [jsonDict objectForKey:@"data"];
//            if (jsonArray.count == 0)
//            {
//                [LvToast showWithText:@"暂无数据" duration:1];
//            }
            if (jsonArray.count == 0)
            {
                [self showNoResultOnView:self.view];
            }
            else
            {
                [self hideNoResult];
            }
            for (NSDictionary * record in jsonArray)
            {
                CLLocationDegrees latitude = [[record stringForKey:@"shop_lat"] doubleValue];
                CLLocationDegrees longitude = [[record stringForKey:@"shop_lon"] doubleValue];
                CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
                
                Discount *d = [[Discount alloc] initWithCoordinate:coordinate];
                d.distance = [record stringForKey:@"d"];
                d.activated = [record stringForKey:@"actived"];
                d.bankDescription = [record stringForKey:@"bank_desc"];
                d.bankIcon = [record stringForKey:@"bank_icon"];
                d.bankID = [record stringForKey:@"bank_id"];
                d.bankName = [record stringForKey:@"bank_name"];
                d.bankPic = [record stringForKey:@"bank_pic"];
                d.beginTime = [record stringForKey:@"begin_time"];
                d.createTime = [record stringForKey:@"create_time"];
                d.creditcardProductID = [record stringForKey:@"creditcard_product_id"];
                d.discountDescription = [record stringForKey:@"description"];
                d.endTime = [record stringForKey:@"end_time"];
                d.hot = [record stringForKey:@"hot"];
                d.discountID = [record stringForKey:@"id"];
                d.pic = [record stringForKey:@"pic"];
                d.rate = [record stringForKey:@"rate"];
                d.shopAddress = [record stringForKey:@"shop_address"];
                d.shopAverageConsumption = [record stringForKey:@"shop_average_consumption"];
                d.shopBookmarkTimes = [record stringForKey:@"shop_bookmark_times"];
                d.shopCity = [record stringForKey:@"shop_city"];
                d.shopDesc = [record stringForKey:@"shop_description"];
                d.shopID = [record stringForKey:@"shop_id"];
                d.shopLatitude = [record stringForKey:@"shop_lat"];
                d.shopLongitude = [record stringForKey:@"shop_lon"];
                d.shopName = [record stringForKey:@"shop_name"];
                d.shopPhoneNumber = [record stringForKey:@"shop_phone_number"];
                d.shopPic = [record stringForKey:@"shop_pic"];
                d.shopType = [record stringForKey:@"shop_type"];
                d.shopTypeName = [record stringForKey:@"shop_type_name"];
                d.discountTitle = [record stringForKey:@"title"];
                d.type = [[record stringForKey:@"type"] integerValue];
                d.updateTime = [record stringForKey:@"update_time"];
                if (d.type == 1)
                {
                    [self.rightArray addObject:d];
                }
                else if (d.type == 2)
                {
                    [self.leftArray addObject:d];
                }
            }
            success = YES;
        }
    }
//    if (success)
//    {
        [loadingView stopWithAnimation];
        switchImgView.hidden = NO;
        leftTable.hidden = NO;
        rightTable.hidden = NO;
        [leftTable reloadData];
        [rightTable reloadData];
//    }
    if (selectedIndex == 0)
    {
        editBtn.enabled = self.leftArray.count > 0;
    }
    else
    {
        editBtn.enabled = self.rightArray.count > 0;
    }
    self.fetcher = nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == leftTable)
    {
        return self.leftArray.count;
    }
    else if (tableView == rightTable)
    {
        return self.rightArray.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == leftTable)
    {
        DiscountCell * cell = [tableView dequeueReusableCellWithIdentifier:@"DISCOUNT_CELL"];
        if (!cell)
        {
            cell = [DiscountCell getInstance];
        }
        
        Discount * discount = [self.leftArray objectAtIndex:indexPath.row];
        if (indexPath.row % 2 == 0)
        {
//            cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"discount_cell_bg.png"]];
            
            cell.maskImageView.image = [UIImage imageNamed:@"discount_icon.png"];
            
        } else
        {
//            cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"discount_cell_bg_dark.png"]];
            
            cell.maskImageView.image = [UIImage imageNamed:@"discount_icon_dark.png"];
        }
        cell.discount = discount;
        return cell;
    }
    else if (tableView == rightTable)
    {
        BankDiscountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BankDiscountCell"];
        
        if (!cell) {
            cell = [BankDiscountCell getInstance];
//            cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bank_discount_bg"]];
            cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bank_discount_selected_bg.png"]];
        }
        
        Discount *discount = [self.rightArray objectAtIndex:indexPath.row];
        
        NSString *urlString = [discount.pic stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        cell.bankImageView.imageURL = [NSURL URLWithString:urlString];
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Discount * discount = nil;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == leftTable)
    {
        discount = [self.leftArray objectAtIndex:indexPath.row];
        DiscountDetailViewController *discountDetailViewController = [[DiscountDetailViewController alloc] initWithNibName:@"DiscountDetailViewController" bundle:nil];
        discountDetailViewController.discount = discount;
        [self.navigationController pushViewController:discountDetailViewController animated:YES];
    }
    else if (tableView == rightTable)
    {
        discount = [self.rightArray objectAtIndex:indexPath.row];
        BankDiscountDetailViewController *bankDiscountDetailViewController = [[BankDiscountDetailViewController alloc] initWithNibName:@"BankDiscountDetailViewController" bundle:nil];
        bankDiscountDetailViewController.discount = discount;
        [self.navigationController pushViewController:bankDiscountDetailViewController animated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)backAction
{
    [self.fetcher stopFetching];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
