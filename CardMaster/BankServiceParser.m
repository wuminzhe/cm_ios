//
//  BankServiceParser.m
//  CardMaster
//
//  Created by Zhang Ming on 12-12-28.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BankServiceParser.h"
#import "GDataXMLNode.h"
#import "BankService.h"
#import "BankModel.h"
#import "TelephoneModel.h"
#import "MessageModel.h"

@implementation BankServiceParser

+ (NSString *)dataFilePath:(BOOL)forSave
{
    return [[NSBundle mainBundle] pathForResource:@"bankservice" ofType:@"xml"];
}

+ (BankService *)loadBankService
{
    NSString *filePath = [self dataFilePath:NO];
    
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:filePath];
    NSError *error;
    
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    
    if (doc == nil)
        return nil;
    
    BankService *bankService = [[BankService alloc] init];
    
    NSArray *bankList = [doc.rootElement elementsForName:@"bank"];
    
    NSMutableArray *results = nil;
    
    if (bankList.count > 0) {
        if (!results)
            results = [[NSMutableArray alloc] initWithCapacity:bankList.count];
        
        for (GDataXMLElement *bankElement in bankList) {
            
            BankModel *bankModel = [[BankModel alloc] init];
            
            // 解析银行ID
            NSArray *bankIDs = [bankElement elementsForName:@"bankId"];
            
            if (bankIDs.count > 0) {
                GDataXMLElement *bankIDElement = (GDataXMLElement *)bankIDs[0];
                bankModel.bankID = bankIDElement.stringValue;
            }
            
            // 解析银行名称
            NSArray *bankNames = [bankElement elementsForName:@"bankName"];
            
            if (bankNames.count > 0) {
                GDataXMLElement *bankNameElement = (GDataXMLElement *)bankNames[0];
                bankModel.bankName = bankNameElement.stringValue;
            }
            
            //解析电话服务
            NSArray *telephones = [bankElement elementsForName:@"telephone"];
            
            if (telephones.count > 0) {
                GDataXMLElement *telephoneElement = telephones[0];
                
                NSArray *areas = [telephoneElement elementsForName:@"area"];
                
                NSMutableArray *result = nil;
                
                if (areas.count > 0) {
                    
                    if (!result)
                        result = [[NSMutableArray alloc] initWithCapacity:areas.count];
                    
                    for (GDataXMLElement *element in areas) {
                        TelephoneModel *telephoneModel = [[TelephoneModel alloc] init];
                        
                        // 解析地区名称
                        NSArray *areaNames = [element elementsForName:@"areaName"];
                        
                        if (areaNames.count > 0) {
                            GDataXMLElement *areaElement = (GDataXMLElement *)areaNames[0];
                            telephoneModel.areaName = areaElement.stringValue;
                        }
                        
                        // 解析热线电话
                        NSArray *hotlines = [element elementsForName:@"hotLine"];
                        
                        if (hotlines.count > 0) {
                            GDataXMLElement *hotlineElement = (GDataXMLElement *)hotlines[0];
                            telephoneModel.hotLine = hotlineElement.stringValue;
                        }
                        
                        // 解析人工服务
                        NSArray *staffServices = [element elementsForName:@"staffService"];
                        
                        if (staffServices.count > 0) {
                            GDataXMLElement *staffServiceElement = (GDataXMLElement *)staffServices[0];
                            telephoneModel.staffService = staffServiceElement.stringValue;
                        }
                        
                        // 解析挂失服务
                        NSArray *reportLosses = [element elementsForName:@"reportLoss"];
                        
                        if (reportLosses.count > 0) {
                            GDataXMLElement *reportLossElement = (GDataXMLElement *)reportLosses[0];
                            telephoneModel.reportLoss = reportLossElement.stringValue;
                        }
                        
                        // 添加
                        [result addObject:telephoneModel];
                        telephoneModel = nil;
                    }
                    
                    bankModel.telephones = result;
                }
            }
            
            // 解析短信
            NSArray *messages = [bankElement elementsForName:@"message"];
            
            if (messages.count > 0) {
                GDataXMLElement *messageElement = messages[0];
                
                // 解析移动号码
                NSArray *mobileNos = [messageElement elementsForName:@"mobileNo"];
                
                if (mobileNos.count > 0) {
                    GDataXMLElement *mobileNoElement = mobileNos[0];
                    bankModel.mobileNo = mobileNoElement.stringValue;
                }
                
                // 解析联通号码
                NSArray *unicomNos = [messageElement elementsForName:@"unicomNo"];
                
                if (unicomNos.count > 0) {
                    GDataXMLElement *unicomNoElement = unicomNos[0];
                    bankModel.unicomNo = unicomNoElement.stringValue;
                }
                
                // 解析电信号码
                NSArray *telecomNos = [messageElement elementsForName:@"telecomNo"];
                
                if (telecomNos.count > 0) {
                    GDataXMLElement *telecomNoElement = telecomNos[0];
                    bankModel.telecomNo = telecomNoElement.stringValue;
                }
                
                // 解析短信服务
                NSArray *smsTypes = [messageElement elementsForName:@"smsType"];
                
                NSMutableArray *result = nil;
                
                if (smsTypes.count > 0) {
                    if (!result)
                        result = [[NSMutableArray alloc] initWithCapacity:smsTypes.count];
                    
                    for (GDataXMLElement *element in smsTypes) {
                        MessageModel *messageModel = [[MessageModel alloc] init];
                        
                        // 解析服务名称
                        NSArray *names = [element elementsForName:@"name"];
                        
                        if (names.count > 0) {
                            GDataXMLElement *nameElement = (GDataXMLElement *)names[0];
                            messageModel.name = nameElement.stringValue;
                        }
                        
                        // 解析服务内容
                        NSArray *contents = [element elementsForName:@"content"];
                        
                        if (contents.count > 0) {
                            GDataXMLElement *contentElement = (GDataXMLElement *)contents[0];
                            messageModel.content = contentElement.stringValue;
                        }
                        
                        [result addObject:messageModel];
                        messageModel = nil;
                    }
                }
                
                bankModel.messages = result;
            }
            
            [results addObject:bankModel];
        }
    }
    
    bankService.bankList = results;
    
    return bankService;
}

@end
