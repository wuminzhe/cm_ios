//
//  SingleBankViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 12-12-29.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "SingleBankViewController.h"
#import "BankModel.h"
#import "TelephoneModel.h"
#import "MessageModel.h"
#import "BankServiceDataInternal.h"
#import "NSString+Extension.h"

@interface SingleBankViewController ()

@property (nonatomic, strong) NSMutableArray *telephoneDataInternals;
@property (nonatomic, strong) NSMutableArray *messageDataInternals;

@end

@implementation SingleBankViewController {
    int sectionCount;
    int telephoneCount;
    
    NSIndexPath *needShowIndexPath;     // 点击需要展示的cell index
    
    NSArray *areaNames;
    NSArray *hotLines;
    NSArray *staffServices;
    NSArray *reportLosses;
    
    BankServiceDataInternal *showDataInternal;
    NSString *mobile;
    
    NSDictionary *code;
    
    NSMutableDictionary *dict;                // 本银行本地保存的信息
    
    UITextField *firstResponder;               
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        sectionCount = 0;
        needShowIndexPath = nil;
        
        areaNames = nil;
        hotLines = nil;
        staffServices = nil;
        reportLosses = nil;
        mobile = nil;
        showDataInternal = nil;
        code = nil;
        
        firstResponder = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = _bankModel.bankName;
    
    if (_bankModel.telephones.count > 0)
        sectionCount += 1;
    
    if (_bankModel.messages.count > 0)
        sectionCount += 1;
    
    code = @{   @"[CARD]" : @"卡号", @"[CARD4]" : @"卡号后四位",
                @"[PASSWORD]" : @"查询密码", @"[PID]" : @"身份证号",
                @"[CODE]" : @"货币码", @"[EMAIL]" : @"电子帐号",
                @"[MONEY]" : @"定制金额", @"[YYMM]" : @"账单年月",
                @"[YYMMDD]" : @"账单年月日", @"[PID6]" : @"身份证号后六位",
                @"[NUM]" : @"分期数", @"[JCARD]" : @"借记卡卡号",
                @"[NUMBER]" : @"缴费号码"   };
    
    areaNames = [_bankModel valueForKeyPath:@"telephones.areaName"];
    hotLines = [_bankModel valueForKeyPath:@"telephones.hotLine"];
    staffServices = [_bankModel valueForKeyPath:@"telephones.staffService"];
    reportLosses = [_bankModel valueForKeyPath:@"telephones.reportLoss"];
    
    mobile = [self getMobileByCode];
    
    dict = [[NSUserDefaults standardUserDefaults] objectForKey:_bankModel.bankID];
    
    if (!dict)
        dict = [[NSMutableDictionary alloc] init];
}

- (void)viewDidUnload
{
    mTableView = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Properties methods
- (NSMutableArray *)telephoneDataInternals
{
    if (!_telephoneDataInternals) {
        _telephoneDataInternals = [[NSMutableArray alloc] init];
        
        [self createTelephoneDataInternals];
    }
    
    return _telephoneDataInternals;
}

- (NSMutableArray *)messageDataInternals
{
    if (!_messageDataInternals) {
        _messageDataInternals = [[NSMutableArray alloc] init];
        
        [self createMessageDataInternals];
    }
    
    return _messageDataInternals;
}

#pragma mark - Private methods
- (void)createTelephoneDataInternals
{
    int count = hotLines.count + staffServices.count + reportLosses.count;
    
    for (int i = 0; i < count; i++) {
        BankServiceDataInternal *dataInternal = [[BankServiceDataInternal alloc] init];
        dataInternal.type = 0;
        
        TelephoneModel *telephoneModel = _bankModel.telephones[i % areaNames.count];
        
        int index = i / areaNames.count;
        
        if (index == 0) {
            dataInternal.content = telephoneModel.hotLine;
        } else if (index == 1) {
            dataInternal.content = telephoneModel.staffService;
        } else {
            dataInternal.content = telephoneModel.reportLoss;
        }
        [self parseTip:dataInternal isMessage:NO];
        
        [self calculatCellHeight:dataInternal];
        
        [_telephoneDataInternals addObject:dataInternal];
        dataInternal = nil;
    }
    
}

- (void)createMessageDataInternals
{
    for (int i = 0; i < _bankModel.messages.count; i++) {
        BankServiceDataInternal *dataInternal = [[BankServiceDataInternal alloc] init];
        dataInternal.type = 1;
        
        MessageModel *messageModel = _bankModel.messages[i];
        
        dataInternal.content = messageModel.content;
        
        dataInternal.mobile = mobile;
        
        [self parseTip:dataInternal isMessage:YES];
        
        [self calculatCellHeight:dataInternal];
        
        [_messageDataInternals addObject:dataInternal];
        
        dataInternal = nil;
    }
}

- (void)calculatCellHeight:(BankServiceDataInternal *)dataInternal
{
    CGFloat height = 0.0;
    
    CGSize tipSize = [dataInternal.tip sizeWithFont:[UIFont systemFontOfSize:15.0]
                                  constrainedToSize:CGSizeMake(280, 9999)
                                      lineBreakMode:UILineBreakModeWordWrap];
    
    height = 6 + tipSize.height;
    
    if (dataInternal.list.count > 0) {
        height += (10 + 40) * dataInternal.codeList.count;
    }
    
    height += 38 + 20;
    
    dataInternal.height = height;
}

- (void)parseTip:(BankServiceDataInternal *)data isMessage:(BOOL)message
{
    NSError *error;
    NSString *content = data.content;
    
    NSMutableArray *list = [[NSMutableArray alloc] init];
    NSMutableArray *values = [[NSMutableArray alloc] init];
    NSMutableDictionary *codeList = [[NSMutableDictionary alloc] init];
    
    if (!message) {
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\]\\#,+\\["
                                                                               options:0
                                                                                 error:&error];
        content = [regex stringByReplacingMatchesInString:content
                                                  options:0
                                                    range:NSMakeRange(0, content.length)
                                             withTemplate:@"]和["];
        
        regex = [NSRegularExpression regularExpressionWithPattern:@",+\\["
                                                          options:0
                                                            error:&error];
        
        content = [regex stringByReplacingMatchesInString:content options:0 range:NSMakeRange(0, content.length) withTemplate:@"->["];
        
        regex = [NSRegularExpression regularExpressionWithPattern:@",+"
                                                          options:0
                                                            error:&error];
        
        content = [regex stringByReplacingMatchesInString:content options:0 range:NSMakeRange(0, content.length) withTemplate:@"->按"];
        
        regex = [NSRegularExpression regularExpressionWithPattern:@"\\]#"
                                                          options:0
                                                            error:&error];
        
        content = [regex stringByReplacingMatchesInString:content options:0 range:NSMakeRange(0, content.length) withTemplate:@"]"];
        
        regex = [NSRegularExpression regularExpressionWithPattern:@"\\[\\w+\\]"
                                                          options:0
                                                            error:&error];
        
        NSArray *results = [regex matchesInString:content options:0 range:NSMakeRange(0, content.length)];
        
        for (int i = results.count - 1; i >= 0; i--) {
            NSTextCheckingResult *result = results[i];
            
            NSRange range = result.range;
            
            NSString *key = [content substringWithRange:range];
            NSString *value = code[key];
            if (key && value) {
                [list insertObject:key atIndex:0];
                [values insertObject:value atIndex:0];
            }
            
            NSString *info = (dict[key] == nil ? @"" : dict[key]);
            
            [codeList setObject:info forKey:key];
            
            content = [content stringByReplacingCharactersInRange:range withString:value];
        }
        
    } else {
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\[\\w+\\]"
                                                                               options:0
                                                                                 error:&error];
        NSArray *results = [regex matchesInString:content options:0 range:NSMakeRange(0, content.length)];
        for (int i = results.count - 1; i >= 0; i--) {
            NSTextCheckingResult *result = results[i];
            
            NSRange range = result.range;
            
            NSString *key = [content substringWithRange:range];
            NSString *value = code[key];
            
            [list insertObject:key atIndex:0];
            [values insertObject:value atIndex:0];
            
            NSString *info = (dict[key] == nil ? @"" : dict[key]);
            
            [codeList setObject:info forKey:key];
            
            content = [content stringByReplacingCharactersInRange:range withString:[NSString stringWithFormat:@"+%@", value]];
        }
        
        regex = [NSRegularExpression regularExpressionWithPattern:@"#[^+]"
                                                          options:0
                                                            error:&error];
        content = [regex stringByReplacingMatchesInString:content
                                                  options:0
                                                    range:NSMakeRange(0, content.length)
                                             withTemplate:@"#+"];
    }

    data.tip = [NSString stringWithFormat:@"%@%@",message ? @"发送" : @"拨打", content];
    data.values = values;
    data.list = list;
    data.codeList = codeList;

    if (message) {
        data.tip = [NSString stringWithFormat:@"%@到%@", data.tip, data.mobile];
    }
    
    
    NSLog(@"content = %@", content);
}


//- (void)parseTip:(BankServiceDataInternal *)data isMessage:(BOOL)message
//{
//    NSMutableString *tip = nil;
//    
//    if (!message) {
//        NSMutableArray *results = [[NSMutableArray alloc] initWithArray:[data.content componentsSeparatedByString:@","]];
//        
//        tip = [[NSMutableString alloc] initWithFormat:@"拨打%@", results[0]];
//        
//        int index = -1;
//        
//        for (int i = 1; i < results.count; i++) {
//            NSString *string = results[i];
//            
//            if ([string isEqualToString:@""])
//                continue;
//            
//            NSRange range = [string rangeOfString:@"["];
//            
//            if (range.location != NSNotFound) {
//                index++;
//                
//                NSString *key = data.list[index];
//                
//                NSString *handledString = code[key];
//                
//                if (index == 0)
//                    [tip appendFormat:@"->%@", handledString];
//                else
//                    [tip appendFormat:@"和%@", handledString];
//            } else {
//                [tip appendFormat:@"->按%@", string];
//            }
//        }
//        
//    } else {
//        NSMutableArray *results = [[NSMutableArray alloc] initWithArray:[data.content componentsSeparatedByString:@"#"]];
//        
//        // 不按#分隔
//        if (results.count == 1) {
//            NSString *string = results[0];
//            
//            NSArray *list = [string componentsSeparatedByString:@"["];
//            tip = [[NSMutableString alloc] initWithFormat:@"发送%@", list[0]];
//            
//            for (int i = 1; i < list.count; i++) {
//                NSString *key = [NSString stringWithFormat:@"[%@", list[i]];
//                
//                [tip appendFormat:@"+%@", code[key]];
//            }
//        } else {
//            tip = [[NSMutableString alloc] initWithFormat:@"发送%@", results[0]];
//            for (int i = 1; i < results.count; i++) {
//                NSString *key = results[i];
//                
//                if ([key isEqualToString:@"[CODE]"]) {
//                    [tip appendFormat:@"+%@(CNY/USD/JPY)", code[key]];
//                    continue;
//                }
//                
//                if ([key rangeOfString:@"]"].location != NSNotFound)
//                    [tip appendFormat:@"+%@", code[key]];
//                else
//                    [tip appendFormat:@"+%@", key];
//            }
//            
//            [tip appendFormat:@"到%@", data.mobile];
//        }
//    }
//    
//    data.tip = tip;
//}

//- (void)getCodeListByString:(BankServiceDataInternal *)data isMessage:(BOOL)message
//{
//    NSMutableDictionary *results = nil;
//    NSMutableArray *keys = [[NSMutableArray alloc] init];
//    NSMutableArray *values = [[NSMutableArray alloc] init];
//    NSString *key = nil;
//    
//    if (!message) {
//        NSMutableArray *list = [[NSMutableArray alloc] initWithArray:[data.content componentsSeparatedByString:@","]];
//        
//        for (int i = 0; i < list.count; i++) {
//            
//            NSString *string = list[i];
//            
//            NSRange range = [string rangeOfString:@"]"];
//            
//            if (range.location != NSNotFound) {
//                if (!results)
//                    results = [[NSMutableDictionary alloc] init];
//                
//                key = [string substringToIndex:range.location + 1];
//                
//                [keys addObject:key];
//                [values addObject:code[key]];
//                [results setObject:@"" forKey:key];
//            }
//        }
//    
//    } else {
//        NSMutableArray *list = [[NSMutableArray alloc] initWithArray:[data.content componentsSeparatedByString:@"#"]];
//        
//        if (list.count == 1) {
//            NSString *string = list[0];
//            
//            NSArray *subList = [string componentsSeparatedByString:@"["];
//            
//            for (int i = 1; i < subList.count; i++) {
//                if (!results)
//                    results = [[NSMutableDictionary alloc] init];
//                NSString *key = [NSString stringWithFormat:@"[%@", subList[i]];
//                [keys addObject:key];
//                [values addObject:code[key]];
//                [results setObject:@"" forKey:key];
//            }
//            
//             NSLog(@"count = %d", keys.count);
//
//        } else {
//            
//            for (int i = 0; i < list.count; i++) {
//                NSString *string = list[i];
//                
//                NSRange rangeStart = [string rangeOfString:@"["];
//                NSRange rangeEnd = [string rangeOfString:@"]"];
//                
//                if (rangeStart.location != NSNotFound && rangeEnd.location != NSNotFound) {
//                    if (!results)
//                        results = [[NSMutableDictionary alloc] init];
//                    
//                    int location = rangeStart.location;
//                    int length = rangeEnd.location - rangeStart.location + 1;
//                    
//                    NSRange range;
//                    range.location = location;
//                    range.length = length;
//                    
//                    key = [string substringWithRange:range];
//                    
//                    [keys addObject:key];
//                    [values addObject:code[key]];
//                    [results setObject:@"" forKey:key];
//                }
//            }
//        }
//    }
//
//    
//    data.list = keys;
//    data.values = values;
//    data.codeList = results;
//}

- (NSString *)getMobileByCode
{
    NSString *carrierName = [NSString getCarrierName];
    
    if ([carrierName isEqualToString:@"中国移动"])
        return _bankModel.mobileNo;
    else if ([carrierName isEqualToString:@"中国联通"])
        return _bankModel.unicomNo;
//    else if ([carrierName isEqualToString:@"中国电信"])
    else
        return _bankModel.telecomNo;
}

/*******************************************************
 * 短信发送功能
 *******************************************************/
- (void)displaySMSComposerSheet:(NSString *)message
{
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    picker.messageComposeDelegate = self;
    picker.body = message;
    picker.recipients = [NSArray arrayWithObject:[self getMobileByCode]];
    
    [self presentModalViewController:picker animated:YES];
}

- (void)showSMSPicker:(NSString *)message
{
    Class messageClass = NSClassFromString(@"MFMessageComposeViewController");
    
    UIAlertView *alertView = nil;
    
    if (messageClass) {
        if ([messageClass canSendText])
            [self displaySMSComposerSheet:message];
        else
            alertView = [[UIAlertView alloc] initWithTitle:@"提示信息"
                                                   message:@"设备没有短信功能"
                                                  delegate:nil
                                         cancelButtonTitle:@"确定"
                                         otherButtonTitles:nil];
    } else
        alertView = [[UIAlertView alloc] initWithTitle:@"提示信息"
                                               message:@"iOS版本过低,iOS4.0以上才支持程序内发送短信"
                                              delegate:nil
                                     cancelButtonTitle:@"确定"
                                     otherButtonTitles:nil];
    
    if (alertView) {
        [alertView show];
    }
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return sectionCount;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = nil;
    if (section == 0) {
        view = [[UIView alloc] initWithFrame:CGRectMake(30, 23, 200, 21)];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(7, 20, 23, 23)];
        imageView.image = [UIImage imageNamed:@"service_phone.png"];
        [view addSubview:imageView];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 20, 200, 21)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [UIColor grayColor];
        titleLabel.text = @"电话服务";
        [view addSubview:titleLabel];
    } else {
        view = [[UIView alloc] initWithFrame:CGRectMake(30, 23, 200, 21)];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(7, 20, 23, 23)];
        imageView.image = [UIImage imageNamed:@"service_message.png"];
        [view addSubview:imageView];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 20, 200, 21)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [UIColor grayColor];
        titleLabel.text = @"短信服务";
        [view addSubview:titleLabel];
    }

    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count = 0;
    
    if (section == 0)
        count = hotLines.count + staffServices.count + reportLosses.count;

    else 
        count = _bankModel.messages.count;
    
    if (needShowIndexPath && needShowIndexPath.section == section)
        count += 1;
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (needShowIndexPath && indexPath.row == needShowIndexPath.row + 1
        && indexPath.section == needShowIndexPath.section) {
        static NSString *serviceIdentifier = @"BankServiceCell";
        
        BankServiceCell *cell = nil;//[tableView dequeueReusableCellWithIdentifier:serviceIdentifier];
        
        if (!cell) {
            cell = [[BankServiceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:serviceIdentifier];
            cell.delegate = self;
        }
        
        if (indexPath.section == 0)
            showDataInternal = _telephoneDataInternals[needShowIndexPath.row];
        else
            showDataInternal = _messageDataInternals[needShowIndexPath.row];
        
        [cell setDataInternal:showDataInternal];
        
        return cell;
    }
    
    static NSString *identifier = @"ServiceCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (indexPath.section == 0) {
    
        int needShowRowIndex = -1;
        
        if (needShowIndexPath && needShowIndexPath.section == 0)
            needShowRowIndex = needShowIndexPath.row;
        
        int row = indexPath.row;
        
        if (row > needShowRowIndex && needShowRowIndex >= 0)
            row -= 1;
        
        NSString *areaName = areaNames[row % areaNames.count];
        
        if (row <= hotLines.count - 1) {
            cell.textLabel.text = [NSString stringWithFormat:@"服务热线%@", [areaName isEqualToString:@"全国"] ? @"" : [NSString stringWithFormat:@"(%@)", areaName ]];
        } else if (row <= areaNames.count + staffServices.count - 1) {
            cell.textLabel.text = [NSString stringWithFormat:@"紧急挂失%@", [areaName isEqualToString:@"全国"] ? @"" : [NSString stringWithFormat:@"(%@)", areaName ]];
        } else
            cell.textLabel.text = [NSString stringWithFormat:@"人工服务%@", [areaName isEqualToString:@"全国"] ? @"" : [NSString stringWithFormat:@"(%@)", areaName ]];
    } else {
        MessageModel *messageModel = nil;
        
        if (needShowIndexPath && indexPath.row > needShowIndexPath.row) 
            messageModel = _bankModel.messages[indexPath.row - 1];
        else
            messageModel = _bankModel.messages[indexPath.row];

        cell.textLabel.text = messageModel.name;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 54.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (needShowIndexPath && indexPath.row == needShowIndexPath.row + 1 && indexPath.section == needShowIndexPath.section) {
        if (indexPath.section == 0) 
            showDataInternal = self.telephoneDataInternals[needShowIndexPath.row];
        else
            showDataInternal = self.messageDataInternals[needShowIndexPath.row];
        NSLog(@"height = %f", showDataInternal.height);
        return showDataInternal.height;
    }
    
    return 44.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 如果点击的是插入的cell，则不做操作
    if (needShowIndexPath && indexPath.row == needShowIndexPath.row + 1
        && indexPath.section == needShowIndexPath.section)
        return;
    
    // 如果没有插入cell
    if (!needShowIndexPath) {
        needShowIndexPath = indexPath;
        
        [tableView insertRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:needShowIndexPath.row + 1 inSection:needShowIndexPath.section] ] withRowAnimation:UITableViewRowAnimationFade];
        
        return;
    } else {
        // 如果再次点击，则删除已经插入的cell
        if (indexPath.row == needShowIndexPath.row && indexPath.section == needShowIndexPath.section) {
            
            needShowIndexPath = nil;
            
            [tableView deleteRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:indexPath.row + 1 inSection:indexPath.section] ] withRowAnimation:UITableViewRowAnimationFade];
        } else {
            NSIndexPath *willShowIndexPath = indexPath;
            
            if (willShowIndexPath.section == needShowIndexPath.section) {
                
                if (willShowIndexPath.row > needShowIndexPath.row)
                    willShowIndexPath = [NSIndexPath indexPathForRow:willShowIndexPath.row - 1 inSection:willShowIndexPath.section];
            }
            
            [tableView beginUpdates];
            
            [tableView deleteRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:needShowIndexPath.row + 1 inSection:needShowIndexPath.section] ] withRowAnimation:UITableViewRowAnimationFade];
            
            needShowIndexPath = willShowIndexPath;
            
            [tableView insertRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:willShowIndexPath.row + 1 inSection:willShowIndexPath.section] ] withRowAnimation:UITableViewRowAnimationFade];
            
            [tableView endUpdates];
            
            [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:willShowIndexPath.row + 1 inSection:willShowIndexPath.section] atScrollPosition:UITableViewRowAnimationTop animated:YES];
        }
    }
}

#pragma mark - MFMessageComposeViewControllerDelegate methods
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    UIAlertView *alertView = nil;
    switch (result) {
        case MessageComposeResultFailed:
            alertView = [[UIAlertView alloc] initWithTitle:@"提示信息"
                                                   message:@"短信发送失败！"
                                                  delegate:nil
                                         cancelButtonTitle:@"确定"
                                         otherButtonTitles:nil];
            [alertView show];
            break;
        case MessageComposeResultCancelled:
            //            LOG(@"Result: SMS sending canceled");
            break;
        case MessageComposeResultSent:
            //            LOG(@"Result: SMS sent");
            break;
    }
    
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - BankServiceCellDelegate method
- (void)handleButtonClick:(BankServiceDataInternal *)dataInternal
{
    NSString *message = dataInternal.content;
    
    NSArray *keys = [dataInternal.codeList allKeys];
    
    for (NSString *key in keys) {
        message = [message stringByReplacingOccurrencesOfString:key
                                                     withString:dataInternal.codeList[key]];
        
        if ([key isEqualToString:@"[PASSWORD]"])
            continue;
        
        dict[key] = dataInternal.codeList[key];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:dict forKey:_bankModel.bankID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (dataInternal.type == 0) {
        NSString *urlString = [NSString stringWithFormat:@"telprompt://%@", message];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
    } else {
        [self showSMSPicker:message];
    }
}

- (void)handleFirstResponder:(UITextField *)textField
{
    CGPoint point = CGPointZero;
    
    point = [textField convertPoint:point toView:self.view];
    
    CGPoint contentOffset = mTableView.contentOffset;
    
    contentOffset.y += point.y - 50;
    
    [UIView animateWithDuration:0.25 animations:^{
        mTableView.contentOffset = contentOffset;
    } completion:^(BOOL finished) {
        firstResponder = textField;
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [firstResponder resignFirstResponder];
    firstResponder = nil;
}

@end
