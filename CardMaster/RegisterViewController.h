//
//  RegisterViewController.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-23.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"

@class LoginEngine;

@interface RegisterViewController : BaseNavigationController<UITextFieldDelegate, UIAlertViewDelegate> {
    IBOutlet UITextField *usernameTextField;
    IBOutlet UITextField *passwordTextField;
    IBOutlet UITextField *passwordAgainTextField;
    
    IBOutlet UIScrollView *theScrollView;
    
    LoginEngine *engine;
}

- (IBAction)backgroundTap:(id)sender;
- (IBAction)chooseNext:(id)sender;
- (IBAction)commit:(id)sender;

@end
