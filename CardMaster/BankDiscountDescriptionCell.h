//
//  BankDiscountDescriptionCell.h
//  CardMaster
//
//  Created by Zhang Ming on 13-1-5.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"

@class Discount;

@protocol BankDiscountDescriptionCellDelegate <NSObject>

- (void)handleBookmark:(id)sender;
- (void)handleFeedback:(id)sender;
- (void)handleShare:(id)sender;

@end

@interface BankDiscountDescriptionCell : UITableViewCell {
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *favoriteLabel;
    IBOutlet UIImageView *favoriteImageView;
    
//    IBOutlet EGOImageView *bankDiscountImageView;
}

@property (nonatomic, assign) id<BankDiscountDescriptionCellDelegate> delegate;
@property (nonatomic, strong) IBOutlet EGOImageView *bankDiscountImageView;
@property (nonatomic, strong) Discount *discount;

- (IBAction)handleBookmark:(id)sender;
- (IBAction)handleFeedback:(id)sender;
- (IBAction)handleShare:(id)sender;

+ (BankDiscountDescriptionCell *)getInstance;

@end
