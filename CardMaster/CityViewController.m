//
//  CityViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 13-1-7.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "CityViewController.h"
#import "SQLGlobal.h"
#import "GEOInfo.h"
#import "AppDelegate.h"
#import "CMUtil.h"

@interface CityViewController ()

@end

@implementation CityViewController {
    NSMutableDictionary *cities;
    NSArray *hotCities;
    NSArray *keys;
    
    NSMutableArray *filterCities;
    
    BOOL isSearch;
    
    NSString *currentCity;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:USER_CITY_GET_NEW object:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        cities = nil;
        keys = nil;
        isSearch = NO;
        
        filterCities = nil;
        
        currentCity = @"快速定位城市";
        
        hotCities = @[@"北京市", @"上海市", @"天津市", @"重庆市", @"广州市", @"深圳市", @"杭州市"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"选择城市";
    
    cities = [[SQLGlobal sharedInstance] getCities];
    keys = cities.allKeys;
    
    keys = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSString *str1 = (NSString *)obj1;
        NSString *str2 = (NSString *)obj2;
        
        return [str1 compare:str2];
    }];
    
    
    GEOInfo *info = [CMUtil getAppDelegate].geoInfo;
    if (info)
        currentCity = info.city;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadCurrentCity) name:USER_CITY_GET_NEW object:nil];
}

- (void)viewDidUnload
{
    mTableView = nil;
    mSearchBar = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Private methods
- (void)backAction
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)loadCurrentCity
{
    currentCity = [CMUtil getAppDelegate].geoInfo.city;
    
    [mTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (isSearch)
        return 1;
    
    return cities.count + 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearch)
        return filterCities.count;
    
    if (section == 0)
        return 1;
    
    if (section == 1)
        return 7;
    
    NSString *key = keys[section - 2];
    NSMutableArray *list = cities[key];
    
    return list.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (isSearch)
        return nil;
    
    if (section == 0)
        return nil;
    
    if (section == 1)
        return @"热门城市";
    
    return keys[section - 2];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (isSearch)
        return nil;
    
    NSMutableArray *list = [NSMutableArray arrayWithArray:keys];
    
    [list insertObject:@"热门" atIndex:0];
    
    return list;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"City_Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.textLabel.font = [UIFont systemFontOfSize:15.0];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    
    if (isSearch) {
        cell.textLabel.text = filterCities[indexPath.row];
        
        return cell;
    }
    
    if (indexPath.section == 0) {
        cell.textLabel.text = currentCity;
    } else if (indexPath.section == 1)
        cell.textLabel.text = hotCities[indexPath.row];
    else {
        NSString *key = keys[indexPath.section - 2];
        
        NSMutableArray *list = cities[key];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@", list[indexPath.row]];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *city = nil;
    
    if (isSearch)
        city = filterCities[indexPath.row];
    else {
        if (indexPath.section == 0)
            city = currentCity;
        else if (indexPath.section == 1)
            city = hotCities[indexPath.row];
        else {
            NSString *key = keys[indexPath.section - 2];
            
            NSMutableArray *list = cities[key];
            
            city = list[indexPath.row];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (_delegate && [_delegate respondsToSelector:@selector(didSelectCity:)])
        [_delegate didSelectCity:city];
    
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - UISearchBarDelegate methods
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    NSLog(@"%s", __FUNCTION__);
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length > 0) {
        isSearch = YES;
        
        filterCities = [[SQLGlobal sharedInstance] getCities:searchText];
        [mTableView reloadData];
    } else
        isSearch = NO;
}

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [mSearchBar resignFirstResponder];
}

@end
