//
//  RecommendCardCell.m
//  cardMaster
//
//  Created by Zhang Ming on 12-10-15.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "RecommendCardCell.h"

@implementation RecommendCardCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (RecommendCardCell *)getRecommendCardTopCell
{
    RecommendCardCell *cell =  [[[NSBundle mainBundle] loadNibNamed:@"RecommendCardTopCell" owner:nil options:NULL] lastObject];
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:
                                   [UIImage imageNamed:@"discount_cell_selected.png"]];
    return cell;
}

+ (RecommendCardCell *)getRecommendCardCell
{
    RecommendCardCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"RecommendCardCell" owner:nil options:NULL] lastObject];
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:
                                   [UIImage imageNamed:@"discount_cell_selected.png"]];
    return cell;
}

@end
