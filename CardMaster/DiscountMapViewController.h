//
//  DiscountMapViewController.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-21.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"
#import <MapKit/MapKit.h>

@class Discount;

@interface DiscountMapViewController : BaseNavigationController<MKMapViewDelegate> {
    __unsafe_unretained IBOutlet MKMapView *mMapView;
}

@property (nonatomic, strong) Discount *discount;

@end
