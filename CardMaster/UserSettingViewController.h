//
//  UserSettingViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-26.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"
#import "TencentOAuth.h"
//#import "SinaWeibo.h"
//#import "TCWBEngine.h"
#import "WXApi.h"

#import "SinaWBEngine.h"
#import "TencentWBEngine.h"

//@interface UserSettingViewController : BaseNavigationController<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate,TencentSessionDelegate,SinaWeiboDelegate,SinaWeiboRequestDelegate> {
//    
//    TencentOAuth *_tencentOAuth;
//    NSMutableArray *_permissions;
//    TCWBEngine *weiboEngine;
//    SinaWeibo *_sinaOAuth;
//}

@interface UserSettingViewController : BaseNavigationController<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, OAuthClientDelegate> {
}

@end
