//
//  RefundRemindEngine.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-23.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MKNetworkEngine.h"

@interface RefundRemindEngine : MKNetworkEngine

typedef void (^AddReminderResponseBlock) (BOOL status);

- (MKNetworkOperation *)addReminder:(NSMutableDictionary *)params
                       onCompletion:(AddReminderResponseBlock)completionBlock
                            onError:(MKNKErrorBlock)errorBlock;

@end
