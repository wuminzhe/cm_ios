//
//  BillController.m
//  CardMaster
//
//  Created by wenjun on 13-1-14.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BillController.h"
#import "RefreshList.h"
#import "LoadingView.h"
#import "GTMUtil.h"
#import "BillCell.h"
#import "RefreshCell.h"
#import "LvToast.h"
#import "JSONKit.h"
#import "JSON.h"
#import "NSDictionary+Extension.h"
#import "CMUtil.h"
#import "BillImportViewController.h"
#import "EGORefreshTableHeaderView.h"

@interface BillController () <RefreshListDelegate>
{
    IBOutlet RefreshList * billList;
    IBOutlet LoadingView * loadingView;
    IBOutlet UIButton * refreshBtn;
}

@property (strong,nonatomic) NSMutableArray * bills;
@property (unsafe_unretained,nonatomic) GTMHTTPFetcher * fetcher;

- (IBAction)addEmail;

@end

@implementation BillController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"电子账单";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:refreshBtn];
    
    billList.delegate = self;
    billList.topRefreshEnabled = YES;
    billList.bottomRefreshEnabled = YES;
    billList.firstPageAnimation = UITableViewRowAnimationFade;
    
    [loadingView setText:@"正在加载"];
    
    self.bills = [NSMutableArray array];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (billList.page == 0 && !billList.bottomOver)
    {
        [loadingView start];
        [billList setHidden:YES];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (billList.page == 0 && !billList.bottomOver)
    {
        [self requestPage:1];
    }
}

- (void)requestFirstPage
{
    [self requestPage:1];
}

- (void)requestPage:(NSInteger)page
{
    NSString * pageStr = [NSString stringWithFormat:@"%d",page];
    NSDictionary * params = @{@"code" : [CMUtil getCode], @"card_id" : self.cardID, @"page" : pageStr};
    self.fetcher = [GTMUtil fetcherWithURLString:[NSString stringWithFormat:@"http://%@/api/bills",kServerAddr] params:params HTTPType:@"GET" urlEncode:NO];
    [self.fetcher setProperty:pageStr forKey:@"page"];
    [_fetcher beginFetchWithDelegate:self didFinishSelector:@selector(fetcher:data:error:)];
}

- (void)fetcher:(GTMHTTPFetcher *)fetcher data:(NSData *)data error:(NSError *)error
{
    BOOL success = NO;
    NSMutableArray * newBills = nil;
    if (fetcher.statusCode != 200)
    {
        success = NO;
    }
    else
    {
        NSArray * jsonArray = [[[data objectFromJSONData] objectForKey:@"data"] objectForKey:@"bills"];
        newBills = [NSMutableArray arrayWithCapacity:jsonArray.count];
        for (NSDictionary * dict in jsonArray)
        {
            //todo 格式转换
            Bill * bill = [[Bill alloc] init];
            bill.pk = [dict stringForKey:@"id"];
            bill.emailID = [dict stringForKey:@"email_id"];
            bill.cardNumber = [dict stringForKey:@"card_number"];
            bill.bankName = [dict stringForKey:@"bank_name"];
            bill.sendTime = [dict stringForKey:@"send_time"];
            bill.paymentTime = [dict stringForKey:@"payment_date"];
            bill.statementTime = [dict stringForKey:@"statement_date"];
            bill.balance = [dict stringForKey:@"new_balance"];
            bill.minPayment = [dict stringForKey:@"min_payment"];
            bill.creditLimit = [dict stringForKey:@"credit_limit"];
            bill.availablePoints = [dict stringForKey:@"avalable_points"];
            [newBills addObject:bill];
        }
        success = YES;
    }
    
    [self requestBillsOverWithPage:[[fetcher propertyForKey:@"page"] intValue] bills:newBills success:success];
    self.fetcher = nil;
}

- (void)requestBillsOverWithPage:(NSInteger)page bills:(NSArray *)newBills success:(BOOL)success
{
    if (page == 1)
    {
        if (success)
        {
            [self.bills addObjectsFromArray:newBills];
            [billList topOverWithNumber:newBills.count finished:YES];
//            if (newBills.count == 0)
//            {
//                [LvToast showWithText:@"暂无数据" duration:1];
//            }
            if (newBills.count == 0)
            {
                [self showNoResultOnView:billList.innerTable];
            }
            else
            {
                [self hideNoResult];
            }
            
        }
        else
        {
            [billList topOverWithNumber:0 finished:NO];
            [LvToast showWithText:@"请求失败" duration:1];
        }
    }
    else
    {
        if (success)
        {
            [self.bills addObjectsFromArray:newBills];
            [billList bottomOverWithNumber:newBills.count finished:YES];
        }
        else
        {
            [billList bottomOverWithNumber:0 finished:NO];
        }
    }
    [loadingView stopWithAnimation];
    [billList setHidden:NO];
}

#pragma refreshList

- (NSInteger)refreshList:(RefreshList *)refreshList heightForRowAtIndex:(NSInteger)index
{
    if (index == 0)
    {
        return 223;
    }
    return 193;
}

- (UITableViewCell *)refreshList:(RefreshList *)refreshList cellForRowAtIndex:(NSInteger)index
{
    NSString * identifier = index == 0 ? @"TopCell" : @"BillCell";
    BillCell * cell = (BillCell *)[refreshList dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell = [BillCell getInstance];
        [cell setReuseIdentifier:identifier];
    }
    [cell loadBill:[self.bills objectAtIndex:index] blue:index == 0];
    return cell;
}

- (UITableViewCell *)refreshCellForRefreshList:(RefreshList *)refreshList
{
    RefreshCell * cell = (RefreshCell *)[refreshList dequeueReusableCellWithIdentifier:@"RefreshCell"];
    if (!cell)
    {
        cell = [RefreshCell getInstance];
    }
    [cell animate];
    return cell;
}

- (void)bottomRefreshInRefreshList:(RefreshList *)refreshList
{
    [self requestPage:refreshList.page + 1];
}

- (NSInteger)numberOfRowsInRefreshList:(RefreshList *)refreshList
{
    return self.bills.count;
}

- (void)topRefreshInRefreshList:(RefreshList *)refreshList
{
    [billList topOverWithNumber:0 finished:YES];
    [self performSelector:@selector(refresh) withObject:nil afterDelay:.5];
}

- (void)backAction
{
    [self.fetcher stopFetching];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)refresh
{
    [self.fetcher stopFetching];
    [loadingView start];
    [billList setHidden:YES];
    [self.bills removeAllObjects];
    [billList clear];
    [self performSelector:@selector(requestFirstPage) withObject:nil afterDelay:.5];
}

- (IBAction)addEmail
{
    BillImportViewController * addEmailController = [[BillImportViewController alloc] init];
    addEmailController.cardID = self.cardID;
    [self.navigationController pushViewController:addEmailController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
