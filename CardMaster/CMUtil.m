//
//  CMUtil.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-17.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "CMUtil.h"
#import "UIImage+Resize.h"
#import "SQLGlobal.h"
#import "CreditCard.h"

@implementation CMUtil

+ (AppDelegate *)getAppDelegate
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

+ (NSString *)getCode
{
    AppDelegate *app = [CMUtil getAppDelegate];
    
    if (!app.code || app.code.length == 0)
        return @"86b8a2819d0250b5812b6ebd53899364a15c69c8";
    
    return app.code;
}

+ (NSString *)getUsername
{
    return @"帅哥";
}

+ (UIImage *)getNumberImage:(NSInteger)num
{
    return [UIImage imageNamed:[NSString stringWithFormat:@"numbers_%d.png", num]];
}

+ (UIImageView *)getNumberImageView:(NSInteger)num
{
    return [[UIImageView alloc] initWithImage:[CMUtil getNumberImage:num]];
}

+ (UIImage *)walletImgFromCardImg:(UIImage *)cardImg
{
//    UIImage * img = [UIImage imageWithCGImage:CGImageCreateWithImageInRect([cardImg CGImage], CGRectMake(0, 0, cardImg.size.width, roundf(cardImg.size.height / 3)))];
//    CGSize walletSize = CGSizeMake(272, 64);
//    if (img.size.width < walletSize.width || img.size.height < walletSize.height)
//    {
//        img = [img resizeImageWithNewSize:walletSize];
//    }
    return cardImg;
}

+ (UIImage *)getBankLogo:(NSString *)bankID
{
    NSString *imageName = nil;
    
    if ([bankID isEqualToString:@"dongguanbank"])
        imageName = @"dongguan";
    
    if ([bankID isEqualToString:@"hkbea"])
        imageName = @"dongya";
    
    if ([bankID isEqualToString:@"icbc"])
        imageName = @"gongshang";
    
    if ([bankID isEqualToString:@"cebbank"])
        imageName = @"guangda";
    
    if ([bankID isEqualToString:@"cgbchina"])
        imageName = @"guangfa";
    
    if ([bankID isEqualToString:@"hzbank"])
        imageName = @"hangzhou";
    
    if ([bankID isEqualToString:@"hxb"])
        imageName = @"huaxia";
    
    if ([bankID isEqualToString:@"ccb"])
        imageName = @"jianshe";
    
    if ([bankID isEqualToString:@"bankcomm"])
        imageName = @"jiaotong";
    
    if ([bankID isEqualToString:@"cmbc"])
        imageName = @"minshen";
    
    if ([bankID isEqualToString:@"nbcb"])
        imageName = @"ningbo";
    
    if ([bankID isEqualToString:@"abchina"])
        imageName = @"nongye";
    
    if ([bankID isEqualToString:@"pingan"])
        imageName = @"pingan";
    
    if ([bankID isEqualToString:@"spdb"])
        imageName = @"pufa";
    
    if ([bankID isEqualToString:@"srcb"])
        imageName = @"shanghainongshang";
    
    if ([bankID isEqualToString:@"cib"])
        imageName = @"xingye";
    
    if ([bankID isEqualToString:@"standardchartered"])
        imageName = @"zhada";
    
    if ([bankID isEqualToString:@"cmbchina"])
        imageName = @"zhaoshang";
    
    if ([bankID isEqualToString:@"boc"])
        imageName = @"zhongguo";
    
    if ([bankID isEqualToString:@"psbc"])
        imageName = @"youzheng";
    
    if ([bankID isEqualToString:@"ecitic"])
        imageName = @"zhongxin";
    
    if ([bankID isEqualToString:@"bankofbeijing"])
        imageName = @"beijing";
    
    if ([bankID isEqualToString:@"gzcb"])
        imageName = @"guangzhou";
    
    if ([bankID isEqualToString:@"hsbank"])
        imageName = @"huishang";
    
    if ([bankID isEqualToString:@"ycccb"])
        imageName = @"ningxia";
    
    if ([bankID isEqualToString:@"bankofshanghai"])
        imageName = @"shanghai";
    
    if ([bankID isEqualToString:@"cqrcb"])
        imageName = @"chongqing";
    
    if ([bankID isEqualToString:@"sdb"])
        imageName = @"shenzhenfazhan";
    
    if ([bankID isEqualToString:@"citybank"])
        imageName = @"chengshishangye";
    
    return [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", imageName]];
}
@end
