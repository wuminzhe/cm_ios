//
//  InstallmentEngine.m
//  cardMaster
//
//  Created by Zhang Ming on 12-10-25.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "InstallmentEngine.h"
#import "NSDictionary+Extension.h"
#import "SQLGlobal.h"
#import "Rate.h"
#import "Bank.h"

@implementation InstallmentEngine

- (MKNetworkOperation *)getInstallmentUpdateStatus:(NSMutableDictionary *)params
                                   onCompletion:(InstallmentUpdateStatusResponseBlock)completionBlock
                                        onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/update_time" params:params];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        BOOL status = [response[@"status"] boolValue];
        
        if (status) {
            NSDictionary *data = response[@"data"];
            
            // 解析服务器返回的利率表，银行表的最后更新时间
            NSString *rateLastUpdateTime = [data stringForKey:@"rate"];
            NSString *bankLastUpdateTime = [data stringForKey:@"bank"];
            
            // 从数据库中取出最后更新的事件
            NSString *rateLastUpdateTimeFromDatabase = [[SQLGlobal sharedInstance] getRateLastUpdateTime];
            NSString *bankLastUpdateTimeFromDatabase = [[SQLGlobal sharedInstance] getBankLastUpdateTime];
            
            // 比较字符串是否相等， 如果不等则发生更新操作
            
            BOOL needUpdate = ![rateLastUpdateTime isEqualToString:rateLastUpdateTimeFromDatabase] ||
            ![bankLastUpdateTime isEqualToString:bankLastUpdateTimeFromDatabase];
            
            completionBlock(needUpdate);
        }
        DLog(@"%@", response);
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)updateInstallmentRate:(NSMutableDictionary *)params
                                 onCompletion:(InstallmentResponseBlock)completionBlock
                                      onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/rates" params:params httpMethod:@"GET"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        NSArray *banksResponse = response[@"data"][@"banks"];
        
        NSMutableArray *banks = [NSMutableArray arrayWithCapacity:banksResponse.count];
        
        for (NSDictionary *record in banksResponse) {
            Bank *bank = [[Bank alloc] init];
            
            bank.bankDesc = [record stringForKey:@"description"];
            bank.icon = [record stringForKey:@"icon"];
            bank.bankID = [record stringForKey:@"id"];
            bank.name = [record stringForKey:@"name"];
            bank.pic = [record stringForKey:@"pic"];
            bank.createTime = [record stringForKey:@"create_time"];
            bank.updateTime = [record stringForKey:@"update_time"];
            
            [banks addObject:bank];
            
            bank = nil;
        }
        
        NSArray *ratesResponse = response[@"data"][@"rates"];
        
        NSMutableArray *rates = [NSMutableArray arrayWithCapacity:ratesResponse.count];
        
        for (NSDictionary *record in ratesResponse) {
            Rate *rate = [[Rate alloc] init];
            
            rate.rateID = [record stringForKey:@"id"];
            rate.bankID = [record stringForKey:@"bank_id"];
            rate.maxAmount = [record stringForKey:@"max_amount"];
            rate.minAmount = [record stringForKey:@"min_amount"];
            rate.rate = [record stringForKey:@"rate"];
            rate.stage = [record stringForKey:@"stage"];
            rate.type = [record stringForKey:@"type"];
            rate.createTime = [record stringForKey:@"create_time"];
            rate.updateTime = [record stringForKey:@"update_time"];
            
            [rates addObject:rate];
            
            rate = nil;
        }
        
        NSDictionary *records = @{ @"bank" : banks, @"rate" : rates };
        
        [[SQLGlobal sharedInstance] updateRates:records];
        
        completionBlock(banks);
//        DLog(@"%@\n%@", banks, rates);
        
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

@end
