//
//  CommentListController.h
//  CardMaster
//
//  Created by wenjun on 13-1-8.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"
#import <UIKit/UIKit.h>
#import "Product.h"

@interface CommentListController : BaseNavigationController

@property (nonatomic, strong) Product * product;

@end
