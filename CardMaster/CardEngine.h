//
//  CardEngine.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-25.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MKNetworkEngine.h"

@class CreditCard;

@interface CardEngine : MKNetworkEngine

typedef void (^UploadCreditCardImageResponseBlock) (NSString *urlString);
typedef void (^GetCardInfoResponseBlock) (CreditCard *card);
typedef void (^AddCreditCardResponseBlock) (NSDictionary *result);
typedef void (^DeleteCardResponseBlock) (BOOL status);
typedef void (^UpdateCardResponseBlock) (NSString *result);

// 上传信用卡图片到服务器
- (MKNetworkOperation *)uploadCreditCardImageFromFile:(NSString *)file
                                               params:(NSMutableDictionary *)params
                                         onCompletion:(UploadCreditCardImageResponseBlock)completionBlock
                                              onError:(MKNKErrorBlock)errorBlock;

// 通过卡号获取信息
- (MKNetworkOperation *)getCardInfo:(NSMutableDictionary *)params
                       onCompletion:(GetCardInfoResponseBlock)completionBlock
                            onError:(MKNKErrorBlock)errorBlock;

// 添加卡片信息
- (MKNetworkOperation *)addCreditCard:(NSMutableDictionary *)params
                         onCompletion:(AddCreditCardResponseBlock)completionBlock
                              onError:(MKNKErrorBlock)errorBlock;

// 删除卡片信息
- (MKNetworkOperation *)deleteCreditCard:(NSMutableDictionary *)params
                            onCompletion:(DeleteCardResponseBlock)completionBlock
                                 onError:(MKNKErrorBlock)errorBlock;

// 更新卡片
- (MKNetworkOperation *)updateCreditCard:(NSMutableDictionary *)params
                            onCompletion:(UpdateCardResponseBlock)completionBlock
                                 onError:(MKNKErrorBlock)errorBlock;

@end
