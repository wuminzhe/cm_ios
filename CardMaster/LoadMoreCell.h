//
//  LoadMoreCell.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-22.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadMoreCell : UITableViewCell

+ (LoadMoreCell *)getInstance;

@end
