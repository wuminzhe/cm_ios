//
//  GEOInfo.h
//  NearbyShops
//
//  Created by wenjun on 12-12-20.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface GEOInfo : NSObject

@property (readonly,nonatomic) NSString * contry;
@property (readonly,nonatomic) NSString * province;
@property (readonly,nonatomic) NSString * city;
@property (readonly,nonatomic) NSString * district;
@property (readonly,nonatomic) NSString * street;
@property (readonly,nonatomic) NSString * streetNumber;
@property (readonly,nonatomic) NSString * postCode;
@property (readonly,nonatomic) CLLocationCoordinate2D coordinate;

- (id)initWithGEODict:(NSDictionary *)geoDict;

@end
