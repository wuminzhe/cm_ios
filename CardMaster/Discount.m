//
//  Shop.m
//  cardMaster
//
//  Created by Zhang Ming on 12-10-12.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "Discount.h"
#import "NSObject+Extension.h"

@implementation Discount

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    if (self = [super init]) {
        _coordinate = coordinate;
    }
    
    return self;
}

- (NSString *)endTime
{
    NSLog(@"endTime = %@", _endTime);
    if (_endTime && _endTime.length > 0) {
        return [_endTime substringToIndex:10];
    }
    
    return @"";
}

- (NSString *)title
{
    return _shopName;
}

- (NSString *)subtitle
{
    return _shopAddress;
}

@end
