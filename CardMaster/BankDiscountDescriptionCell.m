//
//  BankDiscountDescriptionCell.m
//  CardMaster
//
//  Created by Zhang Ming on 13-1-5.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BankDiscountDescriptionCell.h"
#import "Discount.h"

@implementation BankDiscountDescriptionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Properties methods
- (void)setDiscount:(Discount *)discount
{
    _discount = discount;
    
    [self setupDiscount];
}

#pragma mark - Private methods
- (void)setupDiscount
{
    NSString *title = _discount.discountTitle;
    nameLabel.text = title;
    
    favoriteLabel.text = _discount.hot;
    _bankDiscountImageView.placeholderImage = [UIImage imageNamed:@"bank_discount_none.png"];
    NSString *urlString = [_discount.pic stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    _bankDiscountImageView.imageURL = [NSURL URLWithString:urlString];
    
    CGSize size = [title sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:CGSizeMake(9999, 21)];
    
    CGRect rect = nameLabel.frame;
    rect.size = size;
    nameLabel.frame = rect;
    
    if (size.width >= 280) {
        [self performSelector:@selector(handleTitle) withObject:nil afterDelay:1.0f];
    }
}

- (void)handleTitle
{
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(moveTitle) userInfo:nil repeats:YES];
    
    [timer fire];
}

- (void)moveTitle {
    CGRect rect = nameLabel.frame;
    if (rect.origin.x + rect.size.width >= 0)
        rect.origin.x -= 5;
    else {
        rect.origin.x = rect.size.width;
        nameLabel.frame = rect;
    }
    
    [UIView animateWithDuration:0.1 animations:^{
        nameLabel.frame = rect;
    }];
}

#pragma mark - Actions
- (IBAction)handleBookmark:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(handleBookmark:)])
        [_delegate handleBookmark:sender];
}

- (IBAction)handleFeedback:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(handleFeedback:)])
        [_delegate handleFeedback:sender];
}

- (IBAction)handleShare:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(handleShare:)])
        [_delegate handleShare:sender];
}

+ (BankDiscountDescriptionCell *)getInstance
{
    return [[[NSBundle mainBundle] loadNibNamed:@"BankDiscountDescriptionCell" owner:nil options:NULL] lastObject];
}

@end
