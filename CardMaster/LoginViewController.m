//
//  LoginViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 13-1-8.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "LoginViewController.h"
#import "UserAccountViewController.h"
#import "RegisterViewController.h"
#import "LoginEngine.h"
#import "CMConstants.h"
#import "NSString+Verify.h"
#import "SQLGlobal.h"
#import "CMUtil.h"
#import "GlobalHeader.h"

@interface LoginViewController ()

@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) SinaWBEngine *sinaEngine;
@property (nonatomic, strong) QQEngine *qqEngine;

@end

@implementation LoginViewController
{
    LoginEngine *engine;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"登录";
    
    engine = [[LoginEngine alloc] initWithHostName:kServerAddr];
    
//    _activity = [[MBProgressHUD alloc] initWithView:self.view];
//    [_activity setLabelText:@"登录中..."];
//    [self.view addSubview:_activity];
//    
//    _permissions = [NSArray arrayWithObjects:
//                    @"get_user_info",@"add_share", @"add_topic",@"add_one_blog", @"list_album",
//                    @"upload_pic",@"list_photo", @"add_album", @"check_page_fans",nil];
//    
//    _tencentOAuth = [[TencentOAuth alloc] initWithAppId:@"100368147" andDelegate:self];
//	_tencentOAuth.redirectURI = @"www.qq.com";
//    
//    _sinaOAuth = [[SinaWeibo alloc] initWithAppKey:kSinaAppKey appSecret:kSinaAppSecret appRedirectURI:kSinaAppRedirect andDelegate:self];
    
    self.sinaEngine = [[SinaWBEngine alloc] init];
    self.sinaEngine.rootViewController = self;
    self.sinaEngine.delegate = self;
    
    
    self.qqEngine = [[QQEngine alloc] init];
    self.qqEngine.rootViewController = self;
    self.qqEngine.delegate = self;
    
    self.code = [CMUtil getCode];
}

- (void)viewDidUnload
{
    usernameTextField = nil;
    passwordTextField = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods
- (void)backAction
{
    // 登录页面进入方式有2种
    // 1：未登录状态
    // 2: 已登录状态注销
    
    [self.navigationController popViewControllerAnimated:YES];
}

/*
 * 更新用户code
 */
- (void)changeUser:(NSString *)code
{
    // 更新code
    [[SQLGlobal sharedInstance] updateUser:code];
    [CMUtil getAppDelegate].code = code;
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kChangeUser];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

#pragma mark - Actions
- (IBAction)backgroundTap:(id)sender
{
    [usernameTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
}

- (IBAction)login:(id)sender
{
    [self backgroundTap:nil];
    
    NSString *userName = usernameTextField.text;
    NSString *password = passwordTextField.text;
    
    if (!([userName isValidEmail] || [userName isValidPhoneNumber])) {
        [self quickAlertView:@"请输入正确的用户名！"];
        return;
    }
    
    if (password.length < 6) {
        [self quickAlertView:@"密码长度不得小于6位 ！"];
        return;
    }
    
//    [_activity show:YES];
    
    [self showHudView:@"登录中..."];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:2];
    params[@"username"] = userName;
    params[@"password"] = password;
    
    [engine login:params onCompletion:^(NSString *code) {
        
        if ([@"FAIL" isEqualToString:code]) {
            [self quickAlertView:@"登录失败！"];
            return ;
        }
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LoginSuccess"];
        [[NSUserDefaults standardUserDefaults] setValue:userName forKey:@"LoginWay"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UserAccountViewController *userAccountViewController = [[UserAccountViewController alloc] initWithNibName:@"UserAccountViewController" bundle:nil];
        // 返回一致，登录操作
        if ([code isEqualToString:self.code]) {
            
            // 返回不一致，切换用户操作
        } else {
            [self changeUser:code];
            userAccountViewController.code = self.code;
        }
        
//        [_activity hide:YES];
        [self hideHudView];
        
        
        [self.navigationController pushViewController:userAccountViewController animated:YES];
    } onError:^(NSError *error) {
//        [_activity hide:YES];
        [self hideHudView];
        [self quickAlertView:@"登录失败！"];
    }];
}

- (IBAction)registerAccount:(id)sender
{
    RegisterViewController *registerViewController = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    
    [self.navigationController pushViewController:registerViewController animated:YES];
}

- (IBAction)loginWithSina:(id)sender
{
//    [_sinaOAuth logIn];
    [self.sinaEngine logOut];
    
    [self.sinaEngine logIn];
}

- (IBAction)loginWithTencent:(id)sender
{
//    [_tencentOAuth authorize:_permissions inSafari:NO];
    [self.qqEngine logOut];
    
    [self.qqEngine logIn];
}

#pragma mark - UITextFieldDelegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == usernameTextField) {
        [passwordTextField becomeFirstResponder];
    } else {
        [self login:nil];
    }
    return YES;
}

#pragma mark - OAuthClientDelegate methods
- (void)oauthClientDidLogIn:(id)wbEngine
{
    if ([wbEngine isKindOfClass:[SinaWBEngine class]]) {
        
    } else {
        
    }
}

- (void)oauthClient:(id)wbEngine receiveAccessToken:(MZToken *)token
{
    if ([wbEngine isKindOfClass:[SinaWBEngine class]]) {
        UserAccountViewController *userAccountViewController = [[UserAccountViewController alloc] initWithNibName:@"UserAccountViewController" bundle:nil];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LoginSuccess"];
        [[NSUserDefaults standardUserDefaults] setValue:@"新浪微博登录" forKey:@"LoginWay"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:2];
        params[@"code"] = [CMUtil getCode];
        params[@"weibo_id"] = token.uid;
        [self.sinaEngine logOut];
        
        [self showHudView:@"正在登录..."];
        
        [engine loginWithWeibo:params onCompletion:^(NSString *code) {
            if (code) {
                // 1.如果返回code和上传code不一致，则进行切换用户操作
                if (![code isEqualToString:self.code]) {
                    [self changeUser:code];
                } else {
                    // 2.如果返回code和上传一致，则进行登录操作
                }
            }
            
            [self hideHudView];
            
            userAccountViewController.code = self.code;
            [self.navigationController pushViewController:userAccountViewController animated:YES];
        } onError:^(NSError *error) {
            ;
        }];
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LoginSuccess"];
        [[NSUserDefaults standardUserDefaults] setValue:@"QQ账号登录" forKey:@"LoginWay"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UserAccountViewController *userAccountViewController = [[UserAccountViewController alloc] initWithNibName:@"UserAccountViewController" bundle:nil];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:2];
        params[@"code"] = [CMUtil getCode];
        params[@"tencent_id"] = token.openId;
        
        [self showHudView:@"正在登录..."];
        
        [engine loginWithWeibo:params onCompletion:^(NSString *code) {
            
            if (code) {
                // 1.如果返回code和上传code不一致，则进行切换用户操作
                if (![code isEqualToString:self.code]) {
                    [self changeUser:code];
                } else {
                    
                    // 2.如果返回code和上传一致，则进行登录操作
                }
            }
            
            [self hideHudView];
            userAccountViewController.code = self.code;
            [self.navigationController pushViewController:userAccountViewController animated:YES];
        } onError:^(NSError *error) {
            [self hideHudView];
        }];

    }
}

//#pragma mark - Tencent Delegate
///* 登录操作逻辑
// * 两种
// * 1：如果此code和第三方id没有绑定，则进行绑定，返回绑定后的code
// * 2：如果此code和第三方绑定的code不一致，则返回新code
// */
//- (void)tencentDidLogin
//{
//	NSLog(@"openID:%@ || %@",_tencentOAuth.openId, _tencentOAuth.expirationDate);
//    
//    // 持久化
//    //    [NSKeyedArchiver archiveRootObject:_tencentOAuth toFile:kTecentLvTokenSavePath];
//    //    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"BINDING_TENCENT"];
//    //    [[NSUserDefaults standardUserDefaults] synchronize];
//    
//    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LoginSuccess"];
//    [[NSUserDefaults standardUserDefaults] setValue:@"QQ账号登录" forKey:@"LoginWay"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    
//    UserAccountViewController *userAccountViewController = [[UserAccountViewController alloc] initWithNibName:@"UserAccountViewController" bundle:nil];
//    
//    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:2];
//    params[@"code"] = [CMUtil getCode];
//    params[@"tencent_id"] = _tencentOAuth.openId;
//    
//    [engine loginWithWeibo:params onCompletion:^(NSString *code) {
//        
//        if (code) {
//            // 1.如果返回code和上传code不一致，则进行切换用户操作
//            if (![code isEqualToString:self.code]) {
//                [self changeUser:code];
//            } else {
//                
//                // 2.如果返回code和上传一致，则进行登录操作
//            }
//        }
//        
//        [_activity hide:YES];
//        userAccountViewController.code = self.code;
//        [self.navigationController pushViewController:userAccountViewController animated:YES];
//    } onError:^(NSError *error) {
//        ;
//    }];
//}
//
//- (void)tencentDidNotLogin:(BOOL)cancelled
//{
//}
//
//- (void)tencentDidNotNetWork
//{
//}
//
//#pragma mark - SinaWeibo Delegate
//- (void)sinaweiboDidLogIn:(SinaWeibo *)sinaweibo
//{
//    NSLog(@"sinaweiboDidLogIn userID = %@ accesstoken = %@ expirationDate = %@ refresh_token = %@", sinaweibo.userID, sinaweibo.accessToken, sinaweibo.expirationDate,sinaweibo.refreshToken);
//    
//    UserAccountViewController *userAccountViewController = [[UserAccountViewController alloc] initWithNibName:@"UserAccountViewController" bundle:nil];
//    
//    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LoginSuccess"];
//    [[NSUserDefaults standardUserDefaults] setValue:@"新浪微博登录" forKey:@"LoginWay"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    
//    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:2];
//    params[@"code"] = [CMUtil getCode];
//    params[@"weibo_id"] = sinaweibo.userID;
//    [sinaweibo logOut];
//    [engine loginWithWeibo:params onCompletion:^(NSString *code) {
//        if (code) {
//            // 1.如果返回code和上传code不一致，则进行切换用户操作
//            if (![code isEqualToString:self.code]) {
//                [self changeUser:code];
//            } else {
//                // 2.如果返回code和上传一致，则进行登录操作
//            }
//        }
//        
//        [_activity hide:YES];
//        
//        userAccountViewController.code = self.code;
//        [self.navigationController pushViewController:userAccountViewController animated:YES];
//    } onError:^(NSError *error) {
//        ;
//    }];
//}
//
//- (void)sinaweiboDidLogOut:(SinaWeibo *)sinaweibo
//{
//    NSLog(@"sinaweiboDidLogOut");
//}
//
//- (void)sinaweiboLogInDidCancel:(SinaWeibo *)sinaweibo
//{
//    NSLog(@"sinaweiboLogInDidCancel");
//}
//
//- (void)sinaweibo:(SinaWeibo *)sinaweibo logInDidFailWithError:(NSError *)error
//{
//    NSLog(@"sinaweibo logInDidFailWithError %@", error);
//}
//
//- (void)sinaweibo:(SinaWeibo *)sinaweibo accessTokenInvalidOrExpired:(NSError *)error
//{
//    NSLog(@"sinaweiboAccessTokenInvalidOrExpired %@", error);
//}
//

@end
