//
//  RecommendViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 13-1-9.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "RecommendViewController.h"

@interface RecommendViewController ()

@end

@implementation RecommendViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = (_titleString == nil) ? @"卡片申请" : _titleString;
    
    NSURLRequest *request = [NSURLRequest requestWithURL:_applyURL];
    
    [webView loadRequest:request];
}

- (void)viewDidUnload
{
    webView = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIWebViewDelegate methods
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self showHudView:@"正在加载..."];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self hideHudView];
}

@end
