//
//  CreditCard.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-16.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "CreditCard.h"
#import "GlobalHeader.h"
#import "CMUtil.h"
#import "CMConstants.h"

@implementation CreditCard

- (id)init
{
    if (self = [super init]) {
        _source = @"0"; // 默认来源是非账单
        _reminderDay = @1;
        _billDay = @"1";
    }
    
    return self;
}

- (UIImage *)cardImage
{
    return [UIImage imageWithContentsOfFile:[MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:_cardPic]];
}

- (UIImage *)walletImage
{
    return [CMUtil walletImgFromCardImg:self.cardImage];
}

@end
