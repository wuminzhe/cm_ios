//
//  AboutUsViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 12-12-27.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "AboutUsViewController.h"

@interface AboutUsViewController ()

@end

@implementation AboutUsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"关于我们";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)sendEmail:(id)sender
{
    Class mailClass = NSClassFromString(@"MFMailComposeViewController");
    
    if (mailClass) {
        if ([mailClass canSendMail])
            [self displayEmailComposerSheet];
        else
            [self launchMailAppOnDevice];
    } else
        [self launchMailAppOnDevice];
}

- (IBAction)makeCall:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
                                                        message:@"拨打热线电话86(21)-51602230"
                                                       delegate:self
                                              cancelButtonTitle:@"取消"
                                              otherButtonTitles:@"拨打", nil];
    
    [alertView show];
}

#pragma mark - Private methods
/*******************************************************
 * 邮件发送功能
 *******************************************************/
- (void)displayEmailComposerSheet
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.title = @"New Message";
    picker.mailComposeDelegate = self;
    [picker setToRecipients:[NSArray arrayWithObject:@"info@51ka.com"]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    NSDate *date = [[NSDate alloc] init];
    
    NSString *content=[[NSString alloc]
                       initWithFormat:@"App:1.0.iOS:%@ Date:%@",
                       [[UIDevice currentDevice] systemVersion],[dateFormatter stringFromDate:date]];
    [picker setMessageBody:content isHTML:NO];
    [self presentModalViewController:picker animated:YES];
}

- (void)launchMailAppOnDevice
{
//    NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=Hello from California!";
//    NSString *body = @"&body=It is raining in sunny California!";
    
    NSString *recipients = @"mailto:info@51ka.me?subject=Hello from California!";
    NSString *body = @"&body=It is raining in sunny California!";
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

#pragma mark - MFMailComposeViewControllerDelegate methods
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            //message.text = @"Result: canceled";
            break;
        case MFMailComposeResultSaved:
            //message.text = @"Result: saved";
            break;
        case MFMailComposeResultSent:
            //message.text = @"Result: sent";
            break;
        case MFMailComposeResultFailed:
            //message.text = @"Result: failed";
            break;
        default:
            //message.text = @"Result: not sent";
            break;
    }
    
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - UIAlertViewDelegate methods
 -(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        NSURL *url = [NSURL URLWithString:@"tel:862151602230"];
        
        [[UIApplication sharedApplication] openURL:url];
    }
}

@end
