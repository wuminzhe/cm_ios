//
//  TopCell.m
//  CardMaster
//
//  Created by wenjun on 13-1-10.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "TopCell.h"
#import "EGOImageView.h"

@interface TopCell()
{
    IBOutlet UILabel * nameLabel;
    IBOutlet EGOImageView * imgView;
    IBOutlet UILabel * amountLabel;
    IBOutlet UILabel * rateLabel;
    IBOutlet UILabel * countLabel;
    IBOutlet UIButton * applyBtn;
}
@end

@implementation TopCell

- (void)initialize
{
    imgView.placeholderImage = [UIImage imageNamed:@"recommend_card_none"];
}

+ (TopCell *)getInstance
{
    TopCell * cell = [[[NSBundle mainBundle] loadNibNamed:@"TopCell" owner:nil options:nil] lastObject];
    [cell initialize];
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)addTarget:(id)target selecotr:(SEL)selector
{
    [applyBtn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
}

- (void)loadLoan:(LoanProduct *)loan
{
    nameLabel.text = loan.name;
//    imgView.image = loan.image;
    imgView.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/assets/upload/%@",kServerAddr,[loan.imageName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    amountLabel.text = loan.amount;
    rateLabel.text = loan.rate;
    if (loan.applyCount.length == 0)
    {
        countLabel.text = nil;
    }
    else
    {
        countLabel.text = [NSString stringWithFormat:@"%@人",loan.applyCount];
    }
}

@end
