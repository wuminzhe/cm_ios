//
//  RecommendCardViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 13-1-7.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseViewController.h"

@interface RecommendCardViewController : BaseViewController {
    
    __unsafe_unretained IBOutlet UIScrollView *theScrollView;
    __unsafe_unretained IBOutlet UITableView *mTableView;
    __unsafe_unretained IBOutlet UILabel *cardCategoryLabel;
    __unsafe_unretained IBOutlet UIButton *selectButton;
    __unsafe_unretained IBOutlet UIImageView *arrowView;
    
    __unsafe_unretained IBOutlet UIButton *settingButton;
    __unsafe_unretained IBOutlet UIButton *searchButton;
    
    IBOutlet UIView *filterCardView;
    IBOutlet UIView *contentView;
}

@property (nonatomic, strong) NSArray *cardCategories;
@property (nonatomic, strong) NSMutableArray *productList;

- (IBAction)showFilterCardView:(id)sender;
- (void)hideFilterCardView;

- (IBAction)gotoUserSettings:(id)sender;

@end
