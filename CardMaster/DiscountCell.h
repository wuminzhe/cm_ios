//
//  DiscountCell.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-27.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"

@class Discount;

@interface DiscountCell : UITableViewCell {
    
    __unsafe_unretained IBOutlet EGOImageView *discountImageView;
    __unsafe_unretained IBOutlet UILabel *nameLabel;
    __unsafe_unretained IBOutlet UILabel *effectiveLabel;
    
    __unsafe_unretained IBOutlet UIImageView *bankIconView;
    
    __unsafe_unretained IBOutlet UILabel *averageTitleLabel;
    __unsafe_unretained IBOutlet UILabel *averageTextLabel;
    __unsafe_unretained IBOutlet UIImageView *averageImageView;
    
    __unsafe_unretained IBOutlet UILabel *favoriteTitleLabel;
    __unsafe_unretained IBOutlet UILabel *favoriteTextLabel;
    __unsafe_unretained IBOutlet UIImageView *favoriteImageView;
    
    __unsafe_unretained IBOutlet UILabel *distanceTitleLabel;
    __unsafe_unretained IBOutlet UILabel *distanceTextLabel;
    __unsafe_unretained IBOutlet UIImageView *distanceImageView;
    
    __unsafe_unretained IBOutlet UILabel *discount1Label;
    __unsafe_unretained IBOutlet UILabel *discount2Label;
    __unsafe_unretained IBOutlet UILabel *discountTextLabel;
}

@property (nonatomic, unsafe_unretained) IBOutlet UIImageView *maskImageView;
@property (nonatomic, unsafe_unretained) IBOutlet UIButton *deleteButton;
@property (nonatomic, unsafe_unretained) IBOutlet UIView *discountView;
@property (nonatomic, strong) Discount *discount;

+ (DiscountCell *)getInstance;

@end
