//
//  RecommendEngine.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-22.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MKNetworkEngine.h"

@interface RecommendEngine : MKNetworkEngine

typedef void (^GetCreditCardProductResponse) (NSMutableDictionary *products);
typedef void (^AddProductCommentReponse) (BOOL status);
typedef void (^GetCommentListResponse) (NSMutableDictionary *results);

- (MKNetworkOperation *)getCreditCardProductList:(NSMutableDictionary *)params
                                    onCompletion:(GetCreditCardProductResponse)completionBlock
                                         onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)addProductComment:(NSMutableDictionary *)params
                             onCompletion:(AddProductCommentReponse)completionBlock
                                  onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)getProductCommentList:(NSMutableDictionary *)params
                                 onCompletion:(GetCommentListResponse)completionBlock
                                      onError:(MKNKErrorBlock)errorBlock;

@end
