//
//  BankCell.h
//  cardMaster
//
//  Created by Zhang Ming on 12-12-20.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BankCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *bankImageView;
@property (nonatomic, strong) IBOutlet UILabel *bankNameLabel;
@property (nonatomic, strong) IBOutlet UIImageView *selectImageView;

+ (BankCell *)getBankCell;
+ (BankCell *)getAllBankCell;

@end
