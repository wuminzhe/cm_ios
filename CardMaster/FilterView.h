//
//  FilterView.h
//  CardMaster
//
//  Created by wenjun on 13-1-10.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FilterView;
@protocol FilterViewDelegate

- (void)filterView:(FilterView *)filterView didSelectRow:(NSInteger)row choice:(NSString *)choice;
- (NSArray *)choicesInFilterView:(FilterView *)filterView;

@end

@interface FilterView : UIView

@property (unsafe_unretained,nonatomic) id<FilterViewDelegate> delegate;
@property (nonatomic) NSInteger seletedIndex;

+ (FilterView *)getInstance;
- (void)reload;

- (void)show;
- (void)hide;

@end
