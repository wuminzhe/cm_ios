//
//  CommentListController.m
//  CardMaster
//
//  Created by wenjun on 13-1-8.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "CommentListController.h"
#import "RefreshList.h"
#import "LoadingView.h"
#import "Comment.h"
#import "RefreshCell.h"
#import "CommentCell.h"
#import "RecommendEngine.h"
#import "CMUtil.h"
#import "RateController.h"
#import "LvToast.h"

@interface CommentListController () <RefreshListDelegate>
{
    IBOutlet RefreshList * commentList;
    IBOutlet LoadingView * loadingView;
    IBOutlet UIButton * commentBtn;
}

@property (strong,nonatomic) NSMutableArray * comments;

- (IBAction)comment;

@end

@implementation CommentListController
{
    RecommendEngine *engine;
    MKNetworkOperation * op;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma RefreshList

- (NSInteger)refreshList:(RefreshList *)list heightForRowAtIndex:(NSInteger)index
{
    Comment * comment = [self.comments objectAtIndex:index];
    if (comment.rowHeight == 0)
    {
        comment.rowHeight = [CommentCell heightForComment:comment];
    }
    return comment.rowHeight;
}

- (UITableViewCell *)refreshList:(RefreshList *)list cellForRowAtIndex:(NSInteger)index
{
    Comment * comment = [self.comments objectAtIndex:index];
    CommentCell * cell = (CommentCell *)[list dequeueReusableCellWithIdentifier:@"CommentCell"];
    if (!cell)
    {
        cell = [CommentCell getInstance];
    }
    [cell loadComment:comment];
    return cell;
}

- (UITableViewCell *)refreshCellForRefreshList:(RefreshList *)list
{
    RefreshCell * cell = (RefreshCell *)[list dequeueReusableCellWithIdentifier:@"RefreshCell"];
    if (!cell)
    {
        cell = [RefreshCell getInstance];
    }
    [cell animate];
    return cell;
}

- (NSInteger)numberOfRowsInRefreshList:(RefreshList *)list
{
    return self.comments.count;
}

- (void)bottomRefreshInRefreshList:(RefreshList *)list
{
    [self requestPage:list.page + 1];
}

- (void)requestFirstPage
{
    [self requestPage:1];
}

//请求开始函数
- (void)requestPage:(NSInteger)page
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"code"] = [CMUtil getCode];
    
    if (!_product.productID)
        return;
    
    params[@"creditcard_product_id"] = _product.productID;
    params[@"page"] = [NSString stringWithFormat:@"%d",page];
    
    op = [engine getProductCommentList:params onCompletion:^(NSMutableDictionary *results) {
        NSMutableArray * newComments = results[@"list"];
        [newComments sortedArrayUsingSelector:@selector(dateComparetor:)];
        if (commentList.page != 0)
        {
            for (Comment * comment in self.comments)
            {
                if ([newComments containsObject:comment])
                {
                    [newComments removeObject:comment];
                }
            }
        }
        if (page == 1)
        {
            [self.comments insertObjects:newComments atIndexes:[[NSIndexSet alloc] initWithIndexesInRange:NSMakeRange(0, newComments.count)]];
            [commentList topOverWithNumber:newComments.count finished:YES];
        }
        else
        {
            [self.comments addObjectsFromArray:newComments];
            [commentList bottomOverWithNumber:newComments.count finished:YES];
            if (newComments.count == 0)
            {
                self.product.commentCount = [NSString stringWithFormat:@"%d",self.comments.count];
            }
        }
        [loadingView stopWithAnimation];
        [commentList setHidden:NO];
    }
    onError:^(NSError *error)
    {
        if (page == 1)
        {
            [commentList topOverWithNumber:0 finished:NO];
            [LvToast showWithText:@"请求失败" duration:1];
        }
        else
        {
            [commentList bottomOverWithNumber:0 finished:NO];
        }
        [loadingView stopWithAnimation];
        [commentList setHidden:NO];
    }];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"卡详情";
    // Do any additional setup after loading the view from its nib.
    commentList.topRefreshEnabled = NO;
    commentList.bottomRefreshEnabled = YES;
    commentList.delegate = self;
    
    loadingView.label.font = [UIFont systemFontOfSize:14];
    loadingView.label.textColor = [UIColor darkGrayColor];
    loadingView.indicator.color = [UIColor darkGrayColor];
    [loadingView setText:@"正在加载评论"];
    
    self.comments = [NSMutableArray array];
    
    engine = [[RecommendEngine alloc] initWithHostName:kServerAddr];
    
    [commentBtn addTarget:self action:@selector(comment) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:commentBtn];
}

- (IBAction)comment
{
    RateController * rateController = [[RateController alloc] init];
    rateController.product = self.product;
    [self.navigationController pushViewController:rateController animated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (commentList.page == 0 && !commentList.bottomOver)
    {
        [loadingView start];
        [commentList setHidden:YES];
    }
}

- (void)backAction
{
    [commentList clear];
    self.comments = nil;
    self.product = nil;
    [op cancel];
    op = nil;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    if (commentList.page == 0 && !commentList.bottomOver)
//    {
        [self performSelector:@selector(requestFirstPage) withObject:nil afterDelay:.5];
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
