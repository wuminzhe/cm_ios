//
//  FilterView.m
//  CardMaster
//
//  Created by wenjun on 13-1-10.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "FilterView.h"
#import "ChoiceCell.h"
#import <QuartzCore/QuartzCore.h>

@interface FilterView () <UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView * choiceTable;
    IBOutlet UIView * titleView;
    IBOutlet UIControl * titleControl;
    IBOutlet UILabel * titleLabel;
    IBOutlet UIButton * okBtn;
    IBOutlet UIView * clipPart;
    BOOL animating;
}

- (IBAction)ok;

@property (strong,nonatomic) NSArray * choices;

@end

@implementation FilterView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)initialize
{
    clipPart.clipsToBounds = YES;
    titleView.clipsToBounds = YES;
    titleControl.layer.cornerRadius = 4;
//    choiceTable.layer.shadowOffset = CGSizeMake(2, 2);
//    choiceTable.layer.shadowColor = [[UIColor blackColor] CGColor];
//    choiceTable.layer.shadowOpacity = .5;
    _seletedIndex = -1;
    
    [titleControl addTarget:self action:@selector(touchDown) forControlEvents:UIControlEventTouchDown];
    [titleControl addTarget:self action:@selector(touchUpInside) forControlEvents:UIControlEventTouchUpInside];
    
    choiceTable.userInteractionEnabled = NO;
    titleLabel.text = @"请选择";
}

- (void)setSeletedIndex:(NSInteger)seletedIndex
{
    if (seletedIndex >= 0 && seletedIndex < self.choices.count && seletedIndex != _seletedIndex)
    {
        _seletedIndex = seletedIndex;
        titleLabel.text = [self.choices objectAtIndex:seletedIndex];
        [choiceTable selectRowAtIndexPath:[NSIndexPath indexPathForRow:seletedIndex inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
}

- (IBAction)ok
{
    [self hide];
}

- (void)touchDown
{
    titleControl.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.5];
}

- (void)touchUpInside
{
    [self show];
}

- (void)show
{
    if (animating)
    {
        return;
    }
    animating = YES;
    titleControl.backgroundColor = [UIColor clearColor];
    CGRect frame = choiceTable.frame;
    frame.origin.y = 43 - choiceTable.frame.size.height;
    choiceTable.frame = frame;
    [titleView bringSubviewToFront:okBtn];
    [UIView beginAnimations:nil context:nil];
    for (UIView * subView in titleView.subviews)
    {
        if (subView != okBtn)
        {
            CGRect frame = subView.frame;
            frame.origin.y -= 30;
            subView.frame = frame;
        }
    }
    CGRect okFrame = okBtn.frame;
    okFrame.origin.y = 0;
    okBtn.frame = okFrame;
    [UIView commitAnimations];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:MAX(.25, .03 * self.choices.count)];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(showOver)];
    frame.origin.y += frame.size.height;
    choiceTable.frame = frame;
    [UIView commitAnimations];
    frame = self.frame;
    frame.size.height = self.superview.frame.size.height - frame.origin.y;
    self.frame = frame;
    clipPart.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.5];
}

- (void)showOver
{
    choiceTable.userInteractionEnabled = YES;
    animating = NO;
}

- (void)hide
{
    if (animating)
    {
        return;
    }
    animating = YES;
    choiceTable.userInteractionEnabled = NO;
    [titleView sendSubviewToBack:okBtn];
    [UIView beginAnimations:nil context:nil];
    for (UIView * subView in titleView.subviews)
    {
        if (subView != okBtn)
        {
            CGRect frame = subView.frame;
            frame.origin.y += 30;
            subView.frame = frame;
        }
    }
    CGRect okFrame = okBtn.frame;
    okFrame.origin.y = -okFrame.size.height;
    okBtn.frame = okFrame;
    [UIView commitAnimations];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(hideOver)];
    [UIView setAnimationDuration:MAX(.25, .03 * self.choices.count)];
    CGRect frame = choiceTable.frame;
    frame.origin.y = 43 - choiceTable.frame.size.height;
    choiceTable.frame = frame;
    [UIView commitAnimations];
}

- (void)hideOver
{
    clipPart.backgroundColor = [UIColor clearColor];
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 61);
    animating = NO;
}


- (void)reload
{
    self.choices = [NSArray arrayWithArray:[self.delegate choicesInFilterView:self]];
    if (self.choices.count > 0)
    {
        choiceTable.hidden = NO;
        titleView.hidden = NO;
        choiceTable.frame = CGRectMake(choiceTable.frame.origin.x, choiceTable.frame.origin.y, choiceTable.frame.size.width, choiceTable.rowHeight * self.choices.count);
    }
    else
    {
        choiceTable.hidden = YES;
        titleView.hidden = YES;
    }
    CGRect frame = choiceTable.frame;
    frame.origin.y = 43 - choiceTable.frame.size.height;
    choiceTable.frame = frame;
    _seletedIndex = -1;
    titleLabel.text = @"请选择";
    [choiceTable reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.choices.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChoiceCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ChoiceCell"];
    if (!cell)
    {
        cell = [ChoiceCell getInstance];
    }
    [cell loadChoice:self.choices[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_seletedIndex != -1)
    {
        [tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:_seletedIndex inSection:0] animated:YES];
    }
    _seletedIndex = indexPath.row;
    [self hide];
    titleLabel.text = [self.choices objectAtIndex:indexPath.row];
    [self.delegate filterView:self didSelectRow:indexPath.row choice:[self.choices objectAtIndex:indexPath.row]];
}

+ (FilterView *)getInstance
{
    FilterView * filterView = [[[NSBundle mainBundle] loadNibNamed:@"FilterView" owner:nil options:nil] lastObject];
    [filterView initialize];
    return filterView;
}

@end
