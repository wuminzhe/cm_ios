//
//  SwitchCard.h
//  CardMaster
//
//  Created by wenjun on 13-1-18.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CreditCard.h"

@interface SwitchCard : UIView

- (void)loadCard:(CreditCard *)card;

@property (nonatomic) IBOutlet UIImageView * frontIcon;
@property (nonatomic) IBOutlet UILabel * frontLabel;

@property (nonatomic) IBOutlet UIButton * frontBtn;
@property (nonatomic) IBOutlet UIButton * backBtn;


+ (SwitchCard *)getInstance;
+ (SwitchCard *)getNormalInstance;
+ (SwitchCard *)getTopInstance;

@end
