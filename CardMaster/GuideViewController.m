//
//  GuideViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 13-1-10.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "GuideViewController.h"

@interface GuideViewController ()

@end

@implementation GuideViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadLacunchImages];
}

- (void)viewDidUnload
{
    theScrollView = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods
- (void)loadLacunchImages
{
    for (int i = 0; i < MAX_GUIDE_PAGES; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"launch_%d.png", i + 1]]];
        
        CGRect rect = imageView.frame;
        rect.origin.x = i * 320;
        
        imageView.frame = rect;
        
        [theScrollView addSubview:imageView];
    }
    
    [theScrollView setContentSize:CGSizeMake(320 * MAX_GUIDE_PAGES, theScrollView.frame.size.height)];
}

#pragma mark - UIScrollViewDelegate methods

@end
