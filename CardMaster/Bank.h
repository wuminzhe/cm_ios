//
//  Bank.h
//  cardMaster
//
//  Created by Zhang Ming on 12-10-25.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bank : NSObject

@property (nonatomic, strong) NSString *bankDesc;                   // 银行介绍
@property (nonatomic, strong) NSString *bankID;                     // 银行ID
@property (nonatomic, strong) NSString *name;                       // 银行名称
@property (nonatomic, strong) NSString *pic;                        // 图片
@property (nonatomic, strong) NSString *icon;                       // 图标
@property (nonatomic, strong) NSString *createTime;
@property (nonatomic, strong) NSString *updateTime;

@property (nonatomic, strong) NSString *fee;                        // 免年费政策
@property (nonatomic, strong) NSString *discounts;                  // 优惠活动

@property (nonatomic, assign) BOOL check;

@end
