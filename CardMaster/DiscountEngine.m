//
//  DiscountEngine.m
//  cardMaster
//
//  Created by Zhang Ming on 12-10-18.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "DiscountEngine.h"
#import "NSDictionary+Extension.h"
#import "Discount.h"

@implementation DiscountEngine

/*****************************************************************************************
 * 函数名称：getDiscountList:nearbyFlag:onCompletion:onError:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：添加卡片信息到服务器
 * 输入参数：params : POST提交参数
 *         completionBlock : 正确请求后执行的代码块
 *         errorBlock : 错误后执行的代码块
 * 返回值：MKNetworkOperation
 * 修改备注：无
 *****************************************************************************************/
- (MKNetworkOperation *)getDiscountList:(NSMutableDictionary *)params
                             nearbyFlag:(BOOL)flag
                           onCompletion:(DiscountListResponseBlock)completionBlock
                                onError:(MKNKErrorBlock) errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:flag ? @"api/discounts_nearby" : @"api/discounts"
                                              params:params];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {        
        NSDictionary *response = [completedOperation responseJSON];

        NSArray *discounts = (response[@"data"])[@"discounts"];
        NSMutableArray *result = [NSMutableArray arrayWithCapacity:discounts.count];
        
        for (int i = 0; i < discounts.count; i++) {
            NSDictionary *record = [discounts objectAtIndex:i];
            
            // 解析coordinate
            CLLocationDegrees latitude = [[record stringForKey:@"shop_lat"] doubleValue];
            CLLocationDegrees longitude = [[record stringForKey:@"shop_lon"] doubleValue];
            CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
            
            Discount *d = [[Discount alloc] initWithCoordinate:coordinate];
            d.distance = [record stringForKey:@"d"];
            d.activated = [record stringForKey:@"actived"];
            d.bankDescription = [record stringForKey:@"bank_desc"];
            d.bankIcon = [record stringForKey:@"bank_icon"];
            d.bankID = [record stringForKey:@"bank_id"];
            d.bankName = [record stringForKey:@"bank_name"];
            d.bankPic = [record stringForKey:@"bank_pic"];
            d.beginTime = [record stringForKey:@"begin_time"];
            d.createTime = [record stringForKey:@"create_time"];
            d.creditcardProductID = [record stringForKey:@"creditcard_product_id"];
            d.discountDescription = [record stringForKey:@"description"];
            d.endTime = [record stringForKey:@"end_time"];
            d.hot = [record stringForKey:@"hot"];
            d.discountID = [record stringForKey:@"id"];
            d.pic = [record stringForKey:@"pic"];
            d.rate = [record stringForKey:@"rate"];
            d.shopAddress = [record stringForKey:@"shop_address"];
            d.shopAverageConsumption = [record stringForKey:@"shop_average_consumption"];
            d.shopBookmarkTimes = [record stringForKey:@"shop_bookmark_times"];
            d.shopCity = [record stringForKey:@"shop_city"];
            d.shopDesc = [record stringForKey:@"shop_description"];
            d.shopID = [record stringForKey:@"shop_id"];
            d.shopLatitude = [record stringForKey:@"shop_lat"];
            d.shopLongitude = [record stringForKey:@"shop_lon"];
            d.shopName = [record stringForKey:@"shop_name"];
            d.shopPhoneNumber = [record stringForKey:@"shop_phone_number"];
            d.shopPic = [record stringForKey:@"shop_pic"];
            d.shopType = [record stringForKey:@"shop_type"];
            d.shopTypeName = [record stringForKey:@"shop_type_name"];
            d.discountTitle = [record stringForKey:@"title"];
            d.type = [[record stringForKey:@"type"] integerValue];
            d.updateTime = [record stringForKey:@"update_time"];
            
            [result addObject:d];
            d = nil;
        }
        
        NSString *totalPage = (response[@"data"])[@"page_count"];
        
        NSMutableDictionary *results = [NSMutableDictionary dictionaryWithCapacity:2];
        results[@"list"] = result;
        results[@"pages"] = totalPage;
        
        completionBlock(results);

    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)searchDiscountList:(NSMutableDictionary *)params
                              onCompletion:(DiscountListResponseBlock)completionBlock
                                   onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/discounts_search"
                                              params:params];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        NSArray *discounts = (response[@"data"])[@"discounts"];
        NSMutableArray *result = [NSMutableArray arrayWithCapacity:discounts.count];
        
        for (int i = 0; i < discounts.count; i++) {
            NSDictionary *record = [discounts objectAtIndex:i];
            
            // 解析coordinate
            CLLocationDegrees latitude = [[record stringForKey:@"shop_lat"] doubleValue];
            CLLocationDegrees longitude = [[record stringForKey:@"shop_lon"] doubleValue];
            CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
            
            Discount *d = [[Discount alloc] initWithCoordinate:coordinate];
            d.distance = [record stringForKey:@"d"];
            d.activated = [record stringForKey:@"actived"];
            d.bankDescription = [record stringForKey:@"bank_desc"];
            d.bankIcon = [record stringForKey:@"bank_icon"];
            d.bankID = [record stringForKey:@"bank_id"];
            d.bankName = [record stringForKey:@"bank_name"];
            d.bankPic = [record stringForKey:@"bank_pic"];
            d.beginTime = [record stringForKey:@"begin_time"];
            d.createTime = [record stringForKey:@"create_time"];
            d.creditcardProductID = [record stringForKey:@"creditcard_product_id"];
            d.discountDescription = [record stringForKey:@"description"];
            d.endTime = [record stringForKey:@"end_time"];
            d.hot = [record stringForKey:@"hot"];
            d.discountID = [record stringForKey:@"id"];
            d.pic = [record stringForKey:@"pic"];
            d.rate = [record stringForKey:@"rate"];
            d.shopAddress = [record stringForKey:@"shop_address"];
            d.shopAverageConsumption = [record stringForKey:@"shop_average_consumption"];
            d.shopBookmarkTimes = [record stringForKey:@"shop_bookmark_times"];
            d.shopCity = [record stringForKey:@"shop_city"];
            d.shopDesc = [record stringForKey:@"shop_description"];
            d.shopID = [record stringForKey:@"shop_id"];
            d.shopLatitude = [record stringForKey:@"shop_lat"];
            d.shopLongitude = [record stringForKey:@"shop_lon"];
            d.shopName = [record stringForKey:@"shop_name"];
            d.shopPhoneNumber = [record stringForKey:@"shop_phone_number"];
            d.shopPic = [record stringForKey:@"shop_pic"];
            d.shopType = [record stringForKey:@"shop_type"];
            d.shopTypeName = [record stringForKey:@"shop_type_name"];
            d.discountTitle = [record stringForKey:@"title"];
            d.type = [[record stringForKey:@"type"] integerValue];
            d.updateTime = [record stringForKey:@"update_time"];
            
            [result addObject:d];
            d = nil;
        }
        
        NSString *totalPage = (response[@"data"])[@"page_count"];
        
        NSMutableDictionary *results = [NSMutableDictionary dictionaryWithCapacity:2];
        results[@"list"] = result;
        results[@"pages"] = totalPage;
        
        completionBlock(results);
        
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)addFavoriteDiscount:(NSMutableDictionary *)params
                               onCompletion:(AddFavoriteDiscountResponseBlock)completionBlock
                                    onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/add_favoritediscount"
                                              params:params httpMethod:@"POST"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        NSLog(@"reponse = %@", response);
        BOOL status = [[response stringForKey:@"status"] boolValue];
        
        completionBlock(status);
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    return op;
}

- (MKNetworkOperation *)feedback:(NSMutableDictionary *)params
                    onCompletion:(FeedbackResponseBlock)completionBlock
                         onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/feedback"
                                              params:params
                                          httpMethod:@"POST"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        BOOL status = [[response stringForKey:@"status"] boolValue];
        
        completionBlock(status);
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    return op;
}
@end
