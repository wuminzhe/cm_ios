//
//  RecommendCardViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 13-1-7.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "RecommendCardViewController.h"
#import "JBTabBarController.h"
#import "RecommendEngine.h"
#import "CMUtil.h"
#import "Product.h"
#import "LoadMoreCell.h"
#import "RecommendCardCell.h"
#import "FilterCardCell.h"
#import "UserSettingViewController.h"
#import "UINavigationController+Extension.h"
#import "CardDetailViewController.h"
#import "FilterView.h"

@interface RecommendCardViewController () <FilterViewDelegate>

@property (strong,nonatomic) FilterView * filterView;

@end

@implementation RecommendCardViewController
{
    NSInteger currentPage;
    NSMutableDictionary *params;
    NSInteger totalPages;
    
    RecommendEngine *engine;
    
    BOOL isLoadMore;
    
    NSString *currentType;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        currentPage = 1;
        currentType = nil;
        
        isLoadMore = NO;
    }
    return self;
}

- (void)filterView:(FilterView *)filterView didSelectRow:(NSInteger)row choice:(NSString *)choice
{
    [self loadCreditCardProduct:[NSString stringWithFormat:@"%d", row] more:NO];
}

- (NSArray *)choicesInFilterView:(FilterView *)filterView
{
    return _cardCategories;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabBarItem.image = [UIImage imageNamed:@"tabbar_recommend.png"];
    self.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_recommend_select.png"];
    
    _cardCategories = @[ @"全部卡", @"女性卡", @"车主卡", @"生活卡", @"旅游卡",
    @"慈善卡", @"航空卡", @"标准卡", @"卡通卡", @"购物卡" ];
    
    params = [NSMutableDictionary dictionaryWithCapacity:3];
    params[@"code"] = [CMUtil getCode];
    
    engine = [[RecommendEngine alloc] initWithHostName:kServerAddr];
    
    [self loadCreditCardProduct:nil more:NO];
    
    self.filterView = [FilterView getInstance];
    self.filterView.delegate = self;
    [self.filterView reload];
    self.filterView.seletedIndex = 0;
    self.filterView.frame = CGRectMake(0, 72, _filterView.frame.size.width, _filterView.frame.size.height);
    [theScrollView addSubview:self.filterView];
}

- (void)viewDidUnload
{
    theScrollView = nil;
    cardCategoryLabel = nil;
    mTableView = nil;
    filterCardView = nil;
    contentView = nil;
    selectButton = nil;
    arrowView = nil;
    settingButton = nil;
    searchButton = nil;
    
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self calculateScrollViewContentOffset];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (void)loadCreditCardProduct:(NSString *)type more:(BOOL)loadMore
{
    if (currentType != type)
        currentPage = 1;
    
    currentType = type;
    
    if (!type) {
        [params removeObjectForKey:@"creditcard_category_id"];
    } else {
        params[@"creditcard_category_id"] = type;
    }
    
    
    if (type.integerValue == 0)
        [params removeObjectForKey:@"creditcard_category_id"];
    
    if (loadMore) {
        if (currentPage++ >= totalPages)
            currentPage = totalPages;
    }
    
    params[@"page"] = [NSString stringWithFormat:@"%d", currentPage];
    
    [self showHudView:@"正在加载..."];
    [engine getCreditCardProductList:params onCompletion:^(NSMutableDictionary *products) {
        
        if (loadMore) {
            [_productList addObjectsFromArray:products[@"list"]];
            isLoadMore = !loadMore;
        } else
            _productList = products[@"list"];
        
        
        if (self.productList.count == 0) {
            [self showNoResultOnView:mTableView];
        } else {
            [self hideNoResult];
        }
        
        totalPages = [products[@"pages"] integerValue];
        
        NSLog(@"totalPages = %d", totalPages);
        
        [mTableView reloadData];
        [self hideHudView];
    } onError:^(NSError *error) {
        if (self.productList.count == 0) {
            [self showNoResultOnView:mTableView];
        } else {
            [self hideNoResult];
        }
        [self hideHudView];
    }];
}


- (IBAction)showFilterCardView:(id)sender
{
    filterCardView.frame = CGRectMake(0, 119, 320, filterCardView.frame.size.height);
    
    [theScrollView addSubview:filterCardView];
    
    [UIView animateWithDuration:0.35 animations:^{
        arrowView.hidden = YES;
        cardCategoryLabel.hidden = YES;
        
        [selectButton removeTarget:self
                            action:@selector(showFilterCardView:)
                  forControlEvents:UIControlEventTouchUpInside];
        
        [selectButton setImage:[UIImage imageNamed:@"recommend_ok_btn.png"]
                      forState:UIControlStateNormal];
        [selectButton addTarget:self
                         action:@selector(hideFilterCardView)
               forControlEvents:UIControlEventTouchUpInside];
        
        contentView.frame = CGRectMake(contentView.frame.origin.x,
                                       0,
                                       contentView.frame.size.width,
                                       contentView.frame.size.height);
    }];
}

- (void)hideFilterCardView
{
    [UIView animateWithDuration:0.35 animations:^{
        arrowView.hidden = NO;
        cardCategoryLabel.hidden = NO;
        
        [selectButton removeTarget:self
                            action:@selector(hideFilterCardView)
                  forControlEvents:UIControlEventTouchUpInside];
        
        [selectButton setImage:[UIImage imageNamed:@"paper.png"] forState:UIControlStateNormal];
        [selectButton addTarget:self
                         action:@selector(showFilterCardView:)
               forControlEvents:UIControlEventTouchUpInside];
        
        contentView.frame = CGRectMake(contentView.frame.origin.x,
                                       -contentView.frame.size.height,
                                       contentView.frame.size.width,
                                       contentView.frame.size.height);
    } completion:^(BOOL finished) {
        [filterCardView removeFromSuperview];
    }];
}

- (void)calculateScrollViewContentOffset
{
    CGPoint contentOffset = mTableView.contentOffset;
    
    if (contentOffset.y <= 0) {
        theScrollView.contentOffset = CGPointMake(0, 0);
    } else if (contentOffset.y <= 63 * 2) {
        
        theScrollView.contentOffset = CGPointMake(mTableView.contentOffset.x / 2.0,
                                                  mTableView.contentOffset.y / 2.0);
        
        CGRect settingButtonFrame = settingButton.frame;
        
        settingButtonFrame.origin.x = 17 - (mTableView.contentOffset.y * 18) / 126;
        
        settingButton.frame = settingButtonFrame;
        
        CGRect searchButtonFrame= searchButton.frame;
        
        searchButtonFrame.origin.x = 271 + (mTableView.contentOffset.y * 18) / 126;
        
        searchButton.frame = searchButtonFrame;
    } else {
        theScrollView.contentOffset = CGPointMake(0, 63);
    }
}

#pragma mark - Actions
- (IBAction)gotoUserSettings:(id)sender
{
    UserSettingViewController *userSettingViewController = [[UserSettingViewController alloc] initWithNibName:@"UserSettingViewController" bundle:nil];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:userSettingViewController];
    
    [navigationController setNavigationBarBackgroundImage:[UIImage imageNamed:@"nav.png"]];
    
    [[CMUtil getAppDelegate].tabBarController presentModalViewController:navigationController animated:YES];
    
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == mTableView) {
        
        if (_productList.count < 3)
            mTableView.scrollEnabled = NO;
        else
            mTableView.scrollEnabled = YES;
        
        if (_productList.count == 0)
            return 0;
        
        if (currentPage == totalPages)
            return _productList.count;
        
        return _productList.count + 1;
    }
    
    return _cardCategories.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == mTableView) {
        
        if (indexPath.row == _productList.count) {
            LoadMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LoadMoreCell"];
            
            if (!cell)
                cell = [LoadMoreCell getInstance];
            
            return cell;
        }
        
        Product *product = _productList[indexPath.row];
        
        if (indexPath.row == 0) {
            RecommendCardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RecommendCardTopCell"];
            
            if (!cell) {
                cell = [RecommendCardCell getRecommendCardTopCell];
                cell.cardImageView.placeholderImage = [UIImage imageNamed:@"recommend_card_none.png"];
                cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"recommend_card_white_cell.png"]];
            }

            //http://%@/assets/upload/cards/%@
            NSString *imageURLString = [[NSString stringWithFormat:@"http://%@/assets/upload/%@", kServerAddr, product.pic] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            cell.cardImageView.imageURL = [NSURL URLWithString:imageURLString];
            
            cell.cardNameLabel.text = product.name;
            cell.cardDescriptionLabel.text = product.desc;
            cell.rateLabel.text = product.score;
            cell.numberOfApplyLabel.text = product.totalNumber;
            
            return cell;
        }
        
        RecommendCardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RecommendCardCell"];
        
        if (!cell) {
            cell = [RecommendCardCell getRecommendCardCell];
            cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"recommend_card_gray_cell.png"]];
        }
        
        cell.cardNameLabel.text = product.name;
        cell.cardDescriptionLabel.text = product.desc;
        cell.rateLabel.text = product.score;
        cell.numberOfApplyLabel.text = product.totalNumber;
        
        return cell;
    }
    
    FilterCardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FilterCardCell"];
    
    if (!cell) {
        cell = [FilterCardCell getFilterCardCell];
        UIImageView *selectedView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"yellow_selected_cell.png"]];
        cell.selectedBackgroundView = selectedView;
    }
    
    cell.cardNameLabel.text = [_cardCategories objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (mTableView == tableView) {
        if (_productList.count == indexPath.row)
            return 44.0f;
        
        return 119.0;
    }
    
    return 25.0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == mTableView) {
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        CardDetailViewController *detailVC = [[CardDetailViewController alloc] initWithNibName:@"CardDetailViewController" bundle:nil];
        detailVC.product = _productList[indexPath.row];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:detailVC];
        [navigationController setNavigationBarBackgroundImage:[UIImage imageNamed:@"nav.png"]];
        [[CMUtil getAppDelegate].tabBarController presentModalViewController:navigationController animated:YES];
    } else {
        cardCategoryLabel.text = [_cardCategories objectAtIndex:indexPath.row];
        [self loadCreditCardProduct:[NSString stringWithFormat:@"%d", indexPath.row] more:NO];
        [self hideFilterCardView];
    }
}

#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView != mTableView)
        return;
    
    CGPoint contentOffset = scrollView.contentOffset;
    
    if (contentOffset.y <= 0) {
        scrollView.contentOffset = CGPointMake(0, 0);
        theScrollView.contentOffset = mTableView.contentOffset;
    } else if (contentOffset.y <= 63 * 2) {
        
        [UIView beginAnimations:NULL context:NULL];
        
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDuration:0.2];
        [UIView setAnimationRepeatAutoreverses:NO];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        theScrollView.contentOffset = CGPointMake(mTableView.contentOffset.x / 2.0,
                                                  mTableView.contentOffset.y / 2.0);
        
        CGRect settingButtonFrame = settingButton.frame;
        
        settingButtonFrame.origin.x = 17 - (mTableView.contentOffset.y * 18) / 126;
        
        settingButton.frame = settingButtonFrame;
        
        CGRect searchButtonFrame= searchButton.frame;
        
        searchButtonFrame.origin.x = 271 + (mTableView.contentOffset.y * 18) / 126;
        
        searchButton.frame = searchButtonFrame;
        
        [UIView commitAnimations];
        
    } else {
        theScrollView.contentOffset = CGPointMake(0, 63);
    }
    
    // 如果当前页是总页数，则没有加载更多
    if (currentPage == totalPages)
        return;
    
    // 加载更多
    if (contentOffset.y >= mTableView.contentSize.height - 44.0 - mTableView.frame.size.height) {
        if (!isLoadMore) {
            isLoadMore = !isLoadMore;
            [self loadCreditCardProduct:currentType more:YES];
        }
    }
}

@end
