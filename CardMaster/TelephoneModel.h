//
//  TelephoneModel.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-28.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TelephoneModel : NSObject

@property (nonatomic, strong) NSString *areaName;
@property (nonatomic, strong) NSString *hotLine;
@property (nonatomic, strong) NSString *staffService;
@property (nonatomic, strong) NSString *reportLoss;

@end
