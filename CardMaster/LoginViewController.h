//
//  LoginViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 13-1-8.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"
//#import "TencentOAuth.h"
//#import "SinaWeibo.h"

//@interface LoginViewController : BaseNavigationController<TencentSessionDelegate,SinaWeiboDelegate,SinaWeiboRequestDelegate,UITextFieldDelegate> {
//    __unsafe_unretained IBOutlet UITextField *usernameTextField;
//    __unsafe_unretained IBOutlet UITextField *passwordTextField;
//    
//    /*
//     *  新浪和腾讯应用高度集成到“我爱卡”，所以其他项目不能用
//     */
//    TencentOAuth *_tencentOAuth;
//    NSMutableArray *_permissions;
//    SinaWeibo *_sinaOAuth;
//    
//    MBProgressHUD *_activity;
//}

#import "SinaWBEngine.h"
#import "QQEngine.h"

@interface LoginViewController : BaseNavigationController<OAuthClientDelegate, UITextFieldDelegate> {
    __unsafe_unretained IBOutlet UITextField *usernameTextField;
    __unsafe_unretained IBOutlet UITextField *passwordTextField;
}

- (IBAction)backgroundTap:(id)sender;

// 登录
- (IBAction)login:(id)sender;

// 注册
- (IBAction)registerAccount:(id)sender;

// 新浪登录
- (IBAction)loginWithSina:(id)sender;

// QQ登录
- (IBAction)loginWithTencent:(id)sender;

@end
