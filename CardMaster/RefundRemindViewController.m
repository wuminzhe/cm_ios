//
//  RefundRemindViewController.m
//  cardMaster
//
//  Created by Zhang Ming on 12-10-19.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "RefundRemindViewController.h"
#import "RefundRemindEngine.h"
#import "GlobalHeader.h"
#import "CMUtil.h"
#import "SQLGlobal.h"
#import "Reminder.h"
#import "CMConstants.h"
#import "CreditCard.h"


@interface RefundRemindViewController () {
    // 列表选项内容
    NSArray *titles;
    
    // 还款日期
    NSMutableArray *dates;
    
    // 提前通知天数
    NSMutableArray *days;
    
    // 提醒时间段
    NSMutableArray *timeSegments;
    
    // 选择器数据源
    NSMutableArray *pickerData;
    
    NSMutableArray *indexes;        // 保存选择器索引
}

@end

@implementation RefundRemindViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"还款闹钟";
    
    // 设置保存按钮
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    saveButton.frame = CGRectMake(0, 0, 44, 25);
    
    [saveButton setImage:[UIImage imageNamed:@"save.png"]
                forState:UIControlStateNormal];
    
    [saveButton setImage:[UIImage imageNamed:@"save_click.png"]
                forState:UIControlStateHighlighted];
    
    [saveButton addTarget:self
                   action:@selector(saveInfo)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *saveBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
    
    self.navigationItem.rightBarButtonItem = saveBarButtonItem;
    
    // cell textLabel的文本
    titles = @[ @"每月还款日期", @"提前提醒天数", @"提醒时间", @"是否打开还款闹钟" ];
    
    dates = [NSMutableArray arrayWithCapacity:28];
    for (int i = 1; i <= kNumberOfDays; i++) {
        [dates addObject:[NSString stringWithFormat:@"%d号", i]];
    }
    
    days = [NSMutableArray arrayWithCapacity:28];
    for (int i = kNumberOfPreDays; i >= 1 ; i--) {
        [days addObject:[NSString stringWithFormat:@"提前%d天", i]];
    }
    
    timeSegments = [NSMutableArray arrayWithCapacity:24];
    for (int i = 0; i < kNumberOfTimeSegment; i++) {
        [timeSegments addObject:[NSString stringWithFormat:@"%d:00", i]];
    }
    
    pickerData = dates;
    
    indexes = [NSMutableArray arrayWithCapacity:3];
    
    indexes[0] = @"0";
    indexes[1] = @"0";
    indexes[2] = @"0";
    
    contentView.frame = CGRectMake(0, 416, 320, 260);
    [self.view addSubview:contentView];
    
    // 获取本张卡片的提醒信息
    reminder = [[SQLGlobal sharedInstance] getReminder:_card.cardID];
    switchON = [[UISwitch alloc] initWithFrame:CGRectMake(208, 8, 79, 21)];
    
    if (!reminder) {
        reminder = [[Reminder alloc] init];
        indexes[0] = [NSString stringWithFormat:@"%d", _card.reminderDay.integerValue - 1];
    } else {
        indexes[0] = [NSString stringWithFormat:@"%d", reminder.reminderDay.integerValue - 1];
        indexes[1] = [NSString stringWithFormat:@"%d", reminder.preDay.integerValue - 1];
        indexes[2] = reminder.reminderHour;
        switchON.on = [reminder.on boolValue];
    }
    
    [self setupReminderDaySetting];
    
    reminder.cardID = _card.cardID;
}

- (void)viewDidUnload
{
    theScrollView = nil;
    contentView = nil;
    pickerView = nil;
    mTableView = nil;
    leftFirstDayImageView = nil;
    leftSecondDayImageView = nil;
    leftFirstHourImageView = nil;
    leftSecondHourImageView = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)saveInfo
{
    // code: 客户端唯一标识
    // card_id: 信用卡id
    // reminder_day: 每月的哪一天
    // reminder_hour: 几点
    // pre_day: 提前几天提醒
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];

    params[@"code"] = [CMUtil getCode];
    params[@"card_id"] = reminder.cardID;
    params[@"reminder_day"] = reminder.reminderDay;
    params[@"reminder_hour"] = reminder.reminderHour;
    params[@"pre_day"] = reminder.preDay;
    
    BOOL needSave = switchON.on;
    
    if (needSave) {
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.alertBody = @"还款提醒测试";
        localNotification.repeatInterval = NSMonthCalendarUnit;
        localNotification.userInfo = @{ @"cardID" : reminder.cardID };
        
        // 计算提醒时间
        NSDate *today = [NSDate date];
        NSCalendar *gregorianCalendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [gregorianCalendar components:NSYearCalendarUnit     |
                                                                     NSMonthCalendarUnit
                                                            fromDate:today];
        // 获得当前年和月
        int year = components.year;
        int month = components.month;
        
        // 计算提醒时间
        int hour = reminder.reminderHour.integerValue;
        int miniute = 0;
        int second = 0;
        
        /* 通过还款日和提前多少天还款计算还款提醒日
         * kNumberOfPreDays - reminder.preDay.integerValue + 1是还款提前多少天
         */
        int between = reminder.reminderDay.integerValue - (kNumberOfPreDays - reminder.preDay.integerValue + 1);
        
        /* 如果between <= 0，则month - 1
         * between += 31
         */
        
        if (between <= 0) { between += 31; month -= 1; }
        if (month <= 0) { month += 12; year -= 1; }
        
        components.year = year;
        components.month = month;
        components.day = between;
        components.hour = hour;
        components.minute = miniute;
        components.second = second;
        
        NSDate *reminderDate = [gregorianCalendar dateFromComponents:components];
        localNotification.fireDate = reminderDate;
        
        NSLog(@"reminderDate = %@", reminderDate);
        NSLog(@"now = %@", [NSDate date]);
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    } else {
        // 如果有通知，则删除
        NSArray *notifications = [UIApplication sharedApplication].scheduledLocalNotifications;
        for (UILocalNotification *notification in notifications)
        {
            if ([notification.userInfo[@"cardID"] isEqualToString:_card.cardID]) {
                [[UIApplication sharedApplication] cancelLocalNotification:notification];
            }
        }
    }
    
    // 把提醒数据添加到本地数据库中
    [[SQLGlobal sharedInstance] updateReminder:reminder];
    
    // 如果还款日发生了更新，则持久化到数据库
    if (reminder.reminderDay.integerValue != _card.reminderDay.integerValue ) {
        _card.reminderDay = [NSNumber numberWithInteger:reminder.reminderDay.integerValue];
        
        [[SQLGlobal sharedInstance] updateCard:_card];
    }
    
    if (!engine)
        engine = [[RefundRemindEngine alloc] initWithHostName:kServerAddr];
    
    [self showHudView:@"正在提交..."];
    
    
    __block id weakSelf = self;
    
    [engine addReminder:params onCompletion:^(BOOL status) {
        NSString *message = status ? @"提交成功！" : @"提交不成功！";
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"信息" message:message delegate:weakSelf cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        [weakSelf hideHudView];
    } onError:^(NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"信息" message:@"提交不成功！" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        [weakSelf hideHudView];
    }];
}

- (void)setupReminderDaySetting
{
    // 这里需要数据一致
//    CGRect viewRect = CGRectZero;
    
    int reminderDay = reminder.reminderDay.integerValue;
    int preday = kNumberOfPreDays - reminder.preDay.integerValue + 1;
    int reminderHour = reminder.reminderHour.integerValue;
    
    int day = reminderDay - preday;
    
    if (day < 0) {
        day = day + kNumberOfDays;
    }
    
    if (day >= 10) {
        leftFirstDayImageView.hidden = NO;
        leftFirstDayImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"numbers_%d", day / 10]];
        leftSecondDayImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"numbers_%d", day % 10]];
        
        leftFirstDayImageView.frame = CGRectMake(97, 47, 16, 21);
        leftSecondDayImageView.frame = CGRectMake(112, 47, 16, 21);
    } else if (day > 0) {
        leftFirstDayImageView.hidden = YES;
        leftSecondDayImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"numbers_%d", day]];
        
        leftSecondDayImageView.frame = CGRectMake(104, 47, 16, 21);
    } else  {
        leftFirstDayImageView.hidden = NO;
        leftFirstDayImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"numbers_%d", 3]];
        leftSecondDayImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"numbers_%d", 1]];
        
        leftFirstDayImageView.frame = CGRectMake(97, 47, 16, 21);
        leftSecondDayImageView.frame = CGRectMake(112, 47, 16, 21);
    }
    
    if (reminderHour >= 10) {
        leftFirstHourImageView.hidden = NO;
        leftFirstHourImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"numbers_%d", reminderHour / 10]];
        leftSecondHourImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"numbers_%d", reminderHour % 10]];
    } else if (reminderHour > 0) {
        leftFirstHourImageView.hidden = YES;
        leftSecondHourImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"numbers_%d", reminderHour]];
    } else {
        leftFirstHourImageView.hidden = NO;
        leftFirstHourImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"numbers_%d", 0]];
        leftSecondHourImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"numbers_%d", 0]];
    }
}

- (void)changeValueOfRefundRemind:(id)sender
{
    reminder.on = [NSString stringWithFormat:@"%d", switchON.on];
}

// 显示选择器
- (void)showPickerView
{
    if (contentView.frame.origin.y == 416 -260)
        return;
    
    [UIView animateWithDuration:0.35 animations:^{
        contentView.frame = CGRectMake(contentView.frame.origin.x,
                                       416 - 260,
                                       contentView.frame.size.width, contentView.frame.size.height);
    }];
}

// 隐藏选择器
- (void)hidePickerView
{
    [theScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [UIView animateWithDuration:0.35 animations:^{
        contentView.frame = CGRectMake(contentView.frame.origin.x,
                                       416,
                                       contentView.frame.size.width, contentView.frame.size.height);
    } completion:^(BOOL finished) {
        [mTableView reloadData];
    }];
}

- (IBAction)ok:(id)sender
{
    NSString *index = [NSString stringWithFormat:@"%d", [pickerView selectedRowInComponent:0]];
    NSString *realIndex = [NSString stringWithFormat:@"%d", [pickerView selectedRowInComponent:0] + 1];
    
    if (pickerData == dates) {
        indexes[0] = index;
        reminder.reminderDay = realIndex;
    } else if (pickerData == days) {
        indexes[1] = index;
        reminder.preDay = realIndex;
    } else if (pickerData == timeSegments) {
        indexes[2] = index;
        reminder.reminderHour = index;
    }
    
    [self setupReminderDaySetting];
    
    [self hidePickerView];
}

- (IBAction)cancel:(id)sender
{
    [self hidePickerView];
}

#pragma mark - UIPickerViewDataSource methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerData.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [pickerData objectAtIndex:row];
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return 3;
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"REFUND_CELL";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                      reuseIdentifier:@"REFUND_CELL"];
        cell.textLabel.font = [UIFont systemFontOfSize:15.0];
        cell.detailTextLabel.font = [UIFont fontWithName:@"Heiti SC" size:15.0];
    }
    
    if (indexPath.section == 0) {
        cell.textLabel.text = [titles objectAtIndex:indexPath.row];
        
        if (indexPath.row == 0) {
            int index = -1;
            
            if (reminder.reminderDay)
                index = [reminder.reminderDay integerValue];
            else if (_card.reminderDay)
                index = [_card.reminderDay integerValue];
            else
                index = 1;
            
            cell.detailTextLabel.text = dates[index - 1];
        } else if (indexPath.row == 1) {
            int index = [reminder.preDay integerValue];
            
            cell.detailTextLabel.text = days[index - 1];
        } else {
            int index = [reminder.reminderHour integerValue];
        
            cell.detailTextLabel.text = timeSegments[index];
        }
    } else {
        cell.textLabel.text = [titles lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [switchON addTarget:self
                     action:@selector(changeValueOfRefundRemind:)
           forControlEvents:UIControlEventValueChanged];
        
        [cell addSubview:switchON];
    }

    return cell;
}

#pragma mark - UITableViewDelegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        [theScrollView setContentOffset:CGPointMake(0, 94) animated:YES];
        
        if (indexPath.row == 0)
            pickerData = dates;
        else if (indexPath.row == 1)
            pickerData = days;
        else
            pickerData = timeSegments;
        
        [pickerView reloadAllComponents];
        [pickerView selectRow:[indexes[indexPath.row] integerValue] inComponent:0 animated:YES];
        
        [self showPickerView];
    }
}

#pragma mark - UIAlertViewDelegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
