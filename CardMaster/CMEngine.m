//
//  CMEngine.m
//  cardMaster
//
//  Created by Zhang Ming on 12-10-23.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "CMEngine.h"
#import "SQLGlobal.h"
#import "Bank.h"
#import "NSDictionary+Extension.h"
#import "GEOInfo.h"

@implementation CMEngine

/***********************************************
 * 函数名称：initializeClient:params:onCompletion:onError:
 * 作者：lver
 * 日期：2012-10-22
 * 功能描述：客户端第一次启动初始化
 * 输入参数：params : POST提交参数
 *         completionBlock : 正确请求后执行的代码块
 *         errorBlock : 错误后执行的代码块
 * 返回值：无
 * 修改备注：无
 ***********************************************/
- (void)initializeClient:(NSDictionary *)params
            onCompletion:(InitializeResponseBlock)completionBlock
                 onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/init"
                                              params:nil
                                          httpMethod:@"POST"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        NSDictionary *data = response[@"data"];
        
        [[SQLGlobal sharedInstance] updateUser:[data stringForKey:@"code"]];
        
        completionBlock([[response stringForKey:@"status"] boolValue]);
    } onError:^(NSError *error) {
        DLog("error:%@", error);
    }];
    
    [self enqueueOperation:op];
}

- (MKNetworkOperation *)getBanksUpdateStatus:(NSMutableDictionary *)params
                                onCompletion:(GetBanksUpdateResponseBlock)completionBlock
                                     onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/update_time" params:params];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        BOOL status = [response[@"status"] boolValue];
        
        if (status) {
            NSDictionary *data = response[@"data"];
            
            // 解析服务器返回的利率表，银行表的最后更新时间
            NSString *bankLastUpdateTime = [data stringForKey:@"bank"];
            
            // 从数据库中取出最后更新的事件
            NSString *bankLastUpdateTimeFromDatabase = [[SQLGlobal sharedInstance] getBankLastUpdateTime];
            
            // 比较字符串是否相等， 如果不等则发生更新操作
            
            BOOL needUpdate = ![bankLastUpdateTime isEqualToString:bankLastUpdateTimeFromDatabase];
            
            completionBlock(needUpdate);
        } else {
            completionBlock(NO);
        }
        DLog(@"%@", response);
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)getBanks:(NSMutableDictionary *)params
                    onCompletion:(GetBanksResponseBlock)completionBlock
                         onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:@"api/banks" params:params];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        BOOL status = [[response stringForKey:@"status"] boolValue];
        
        if (status) {
            NSArray *data = [response objectForKey:@"data"];
            
            NSMutableArray *results = nil;
            
            if (data.count > 0) {
                if (!results)
                    results = [[NSMutableArray alloc] init];
                
                for (NSDictionary *record in data) {
                    Bank *bank = [[Bank alloc] init];
                    bank.bankID = [record stringForKey:@"id"];
                    bank.name = [record stringForKey:@"name"];
                    bank.icon = [record stringForKey:@"icon"];
                    bank.pic = [record stringForKey:@"pic"];
                    bank.bankDesc = [record stringForKey:@"description"];
                    bank.createTime = [record stringForKey:@"create_time"];
                    bank.updateTime = [record stringForKey:@"update_time"];
                    bank.fee = [record stringForKey:@"fee"];
                    bank.discounts = [record stringForKey:@"discounts"];
                    
                    [results addObject:bank];
                    bank = nil;
                }
                
                completionBlock(results);
                
                [[SQLGlobal sharedInstance] updateBanks:results];
            }
        } else {
            completionBlock(nil);
        }
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

- (MKNetworkOperation *)getGeoInfo:(NSMutableDictionary *)params
                      onCompletion:(GetGeoResponseBlock)completionBlock
                           onError:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:[NSString stringWithFormat:@"maps/api/geocode/json?latlng=%@,%@&sensor=true&language=zh_CN",params[@"lat"],params[@"lon"]]];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSDictionary *response = [completedOperation responseJSON];
        
        NSMutableDictionary * geoDict = [NSMutableDictionary dictionary];
        
        NSArray *results = [response objectForKey:@"results"];
        
        if (results.count == 0) {
            completionBlock(nil);
        } else {
            NSDictionary * resultDict = [[response objectForKey:@"results"] objectAtIndex:0];
            NSArray * addressComponents = [resultDict objectForKey:@"address_components"];
            
            [geoDict setObject:[resultDict objectForKey:@"formatted_address"] forKey:@"formatted_address"];
            [geoDict setObject:[[resultDict objectForKey:@"geometry"] objectForKey:@"location"]forKey:@"location"];
            
            for (NSDictionary * dict in addressComponents)
            {
                NSString * type = [[dict objectForKey:@"types"] objectAtIndex:0];
                NSString * address = [dict objectForKey:@"long_name"];
                [geoDict setObject:address forKey:type];
            }
            
            GEOInfo *geoInfo = [[GEOInfo alloc] initWithGEODict:geoDict];
            completionBlock(geoInfo);
        }
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

@end
