//
//  SplashViewController.h
//  cardMaster
//
//  Created by huangxp on 12-11-28.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SplashViewController : UIViewController
{
}

@property (unsafe_unretained, nonatomic) IBOutlet UIActivityIndicatorView *activity;

@end
