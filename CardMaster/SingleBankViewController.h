//
//  SingleBankViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-29.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"
#import "BankServiceCell.h"
#import <MessageUI/MessageUI.h>

@class BankModel;

@interface SingleBankViewController : BaseNavigationController<UITableViewDataSource, UITableViewDelegate, BankServiceCellDelegate, MFMessageComposeViewControllerDelegate> {
    IBOutlet UITableView *mTableView;
}

@property (nonatomic, strong) BankModel *bankModel;

@end
