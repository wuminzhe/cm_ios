//
//  PasswordChangeViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 13-1-9.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "PasswordChangeViewController.h"
#import "GTMUtil.h"
#import "CMUtil.h"
#import "JSONKit.h"
#import "NSDictionary+Extension.h"
#import "NSDictionary+Extension.h"

@interface PasswordChangeViewController ()

@end

@implementation PasswordChangeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"密码重置";
}

- (void)viewDidUnload
{
    oldPasswordTextField = nil;
    passwordTextField = nil;
    passwordAgainTextField = nil;
    
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)resetPassword:(id)sender
{
    [passwordTextField resignFirstResponder];
    [passwordAgainTextField resignFirstResponder];
    [oldPasswordTextField resignFirstResponder];
    
    NSString *password = passwordTextField.text;
    NSString *passwordAgain = passwordAgainTextField.text;
    NSString *oldPassword = oldPasswordTextField.text;
    
    if (oldPassword.length < 6) {
        [self quickAlertView:@"密码长度不得小于6位！"];
        return;
    }
    
    if (password.length < 6) {
        [self quickAlertView:@"密码长度不得小于6位！"];
        return;
    }
    
    if (![password isEqualToString:passwordAgain]) {
        [self quickAlertView:@"两次密码输入不一致！"];
        return;
    }
    
    GTMHTTPFetcher *fetcher = [GTMUtil fetcherWithURLString:[NSString stringWithFormat:@"http://%@/api/modify_password", kServerAddr]
                                                 params:@{@"code" : [CMUtil getCode], @"password" : oldPassword, @"new_password" : password}
                                               HTTPType:@"POST" urlEncode:NO];
    [self showHudView:@"正在修改..."];
    [fetcher beginFetchWithDelegate:self didFinishSelector:@selector(modifyPasswordFetcher:data:error:)];
}

#pragma mark - GTMHTTPFetcher Delegate methods
- (void)modifyPasswordFetcher:(GTMHTTPFetcher *)fetcher data:(NSData *)data error:(NSError *)error
{
    [self hideHudView];
    if (error) {
        NSDictionary *response = [data objectFromJSONData];
        
        [self quickAlertView:[response stringForKey:@"msg"]];
        return;
    }
    
    if (fetcher.statusCode == 200) {
        NSDictionary * jsonDict = [data objectFromJSONData];
        
        if ([jsonDict stringForKey:@"status"].integerValue == 1) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
                                                                message:@"修改成功！"
                                                               delegate:self
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"确定", nil];
            
            [alertView show];
        } else {
            [self quickAlertView:@"修改失败！"];
        }
    }
}

#pragma mark - UIAlertViewDelegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
