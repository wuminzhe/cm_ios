//
//  CardAddByHandViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 13-1-8.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseNavigationController.h"

@class CreditCard;

@protocol CardAddByHandViewControllerDelegate <NSObject>

- (void)addCreditCardByHand:(CreditCard *)card;

@end

@interface CardAddByHandViewController : BaseNavigationController<UIPickerViewDataSource, UIPickerViewDelegate> {
    __unsafe_unretained IBOutlet UITextField *cardNumberTextField;
    __unsafe_unretained IBOutlet UIButton *reminderDayButton;
    IBOutlet UIView *chooseView;
    IBOutlet UIPickerView *mPickerView;
}

@property (nonatomic, assign) id<CardAddByHandViewControllerDelegate> delegate;

@property (nonatomic, strong) UIImage *cardImage;
@property (nonatomic, strong) NSString *cardNumber;

- (IBAction)hidePickerView:(id)sender;
- (IBAction)showPickerView:(id)sender;
- (IBAction)choose:(id)sender;

- (IBAction)addCard:(id)sender;

@end
