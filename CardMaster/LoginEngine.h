//
//  LoginEngine.h
//  cardMaster
//
//  Created by Zhang Ming on 12-11-22.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "MKNetworkEngine.h"

@interface LoginEngine : MKNetworkEngine

typedef void (^LoginResponseBlock) (NSString *code);
typedef void (^GetCodeResponseBlock) (NSString *code);
typedef void (^LoginWithWeiboResponseBlock) (NSString *code);
typedef void (^RegisterResponseBlock) (NSString *code);

// 登录
- (MKNetworkOperation *)login:(NSMutableDictionary *)params
                 onCompletion:(LoginResponseBlock)completionBlock
                      onError:(MKNKErrorBlock)errorBlock;

// 微博登录
- (MKNetworkOperation *)loginWithWeibo:(NSMutableDictionary *)params
                          onCompletion:(LoginWithWeiboResponseBlock)completionBlock
                               onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)registerUser:(NSMutableDictionary *)params
                        onCompletion:(RegisterResponseBlock)completionBlock
                             onError:(MKNKErrorBlock)errorBlock;

- (MKNetworkOperation *)getCode:(NSDictionary *)params
                   onCompletion:(GetCodeResponseBlock)completionBlock
                        onError:(MKNKErrorBlock)errorBlock;


@end
