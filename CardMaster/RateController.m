//
//  RateController.m
//  CardMaster
//
//  Created by wenjun on 13-1-9.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "RateController.h"
#import "PointView.h"
#import "UIPlaceHolderTextView.h"
#import "LvToast.h"
#import "CMUtil.h"
#import "RecommendEngine.h"
#import "GTMUtil.h"
#import "JSONKit.h"

@interface RateController () <UIScrollViewDelegate,UITextViewDelegate>
{
    PointView * pointView;
    IBOutlet UIPlaceHolderTextView * textView;
    IBOutlet UIScrollView * scroll;
}

@property (unsafe_unretained,nonatomic) GTMHTTPFetcher * fetcher;

- (IBAction)sendComment;

@end

@implementation RateController
{
//    RecommendEngine *engine;
//    MKNetworkOperation * op;
}

- (IBAction)sendComment
{
    NSString * comment = textView.text;
    CGFloat point = pointView.points;
    if (comment.length == 0)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入评论" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alert show];
        return;
    }
    [self showHudView:@"正在提交中"];
    [self resign];
    //提交评论
    NSDictionary * params = @{@"code" : [CMUtil getCode],
    @"creditcard_product_id":_product.productID,
    @"score":[NSString stringWithFormat:@"%f",point],
    @"content":comment};
    
    self.fetcher = [GTMUtil fetcherWithURLString:[NSString stringWithFormat:@"http://%@/api/product_comment",kServerAddr] params:params HTTPType:@"post" urlEncode:YES];
    [self.fetcher beginFetchWithDelegate:self didFinishSelector:@selector(commentFetcher:data:error:)];
//    __block id weakSelf = self;
//    op = [engine addProductComment:[NSMutableDictionary dictionaryWithDictionary:params] onCompletion:^(BOOL status)
//    {
//        [weakSelf requestOver:status];
//        weakSelf = nil;
//    }
//    onError:^(NSError *error)
//    {
//        [weakSelf requestOver:NO];
//        weakSelf = nil;
//    }];
}

- (void)commentFetcher:(GTMHTTPFetcher *)fetcher data:(NSData *)data error:(NSError *)error
{
    [self hideHudView];
    BOOL success = NO;
    if (fetcher.statusCode == 200)
    {
        if ([[[data objectFromJSONData] stringForKey:@"status"] intValue] == 1)
        {
            success = YES;
        }
    }
    if (success)
    {
        self.product.commentCount = [NSString stringWithFormat:@"%d",[self.product.commentCount intValue] + 1];
        self.product = nil;
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        NSString * msg = [[data objectFromJSONData] stringForKey:@"msg"];
        if (msg)
        {
            [LvToast showWithText:msg bottomOffset:260 duration:1];
        }
    }
    self.fetcher = nil;
}

//- (void)requestOver:(BOOL)success
//{
//    [self hideHudView];
//    NSString * msg = nil;
//    if (success)
//    {
//        msg = @"评分成功！";
//    }
//    else
//    {
//        msg = @"评分失败，请稍候重试";
//        [LvToast showWithText:msg bottomOffset:260 duration:1];
//    }
//    if (success)
//    {
//        self.product.commentCount = [NSString stringWithFormat:@"%d",[self.product.commentCount intValue] + 1];
//        self.product = nil;
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"添加评分";
    // Do any additional setup after loading the view from its nib.
    pointView = [[PointView alloc] initWithDarkImage:[UIImage imageNamed:@"carddetail_star_gree_"] lightImage:[UIImage imageNamed:@"carddetail_star_yellow_"] imageSize:CGSizeMake(40, 40) space:12 pointCount:5];
    pointView.frame = CGRectMake(roundf((320 - pointView.frame.size.width) / 2), 13, pointView.frame.size.width, pointView.frame.size.height);
    pointView.gestureEnabled = YES;
    pointView.points = 3;
    [scroll addSubview:pointView];
    
    textView.placeholder = @"＃满意＃";
    textView.delegate = self;
    
    scroll.showsVerticalScrollIndicator = NO;
    scroll.alwaysBounceVertical = YES;
    
//    engine = [[RecommendEngine alloc] initWithHostName:kServerAddr];
}

- (void)resign
{
    [textView resignFirstResponder];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationStoped)];
    scroll.contentOffset = CGPointZero;
    [UIView commitAnimations];
}

- (void)animationStoped
{
    scroll.contentInset = UIEdgeInsetsZero;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)tView
{
    [scroll setContentOffset:CGPointMake(0, textView.frame.origin.y - 2) animated:YES];
    [scroll setContentInset:UIEdgeInsetsMake(0, 0, scroll.contentOffset.y, 0)];
    return YES;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self resign];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)backAction
{
//    [op cancel];
//    op = nil;
    [self.fetcher stopFetching];
    [self resign];
    self.product = nil;
    [self performSelector:@selector(pop) withObject:nil afterDelay:.25];
}

- (void)pop
{

    [self.navigationController popViewControllerAnimated:YES];
}

@end
