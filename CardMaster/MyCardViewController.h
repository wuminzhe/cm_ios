//
//  MyCardViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-24.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BaseViewController.h"
#import "CardAddByHandViewController.h"
#import "CardIO.h"

@class CardEngine;

#define MIN_CARD_NUMBER         5
#define OTHER_CELL_NUMBER       3

@interface MyCardViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate, CardIOPaymentViewControllerDelegate, CardAddByHandViewControllerDelegate> {
    __unsafe_unretained IBOutlet UITableView *mTableView;
    
    CardEngine *engine;
}

@property (nonatomic, strong) NSMutableArray *cards;        // 本地卡片

- (IBAction)gotoUserSettings:(id)sender;
- (IBAction)addCreditCard:(id)sender;

@end
