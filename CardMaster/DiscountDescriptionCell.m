//
//  DiscountDescriptionCell.m
//  cardMaster
//
//  Created by Zhang Ming on 12-11-21.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "DiscountDescriptionCell.h"

@implementation DiscountDescriptionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setType:(NSInteger)type
{
    _type = type;
    if (type == 1) {
        _averageLabel.hidden = YES;
        _averageTitleLabel.hidden = YES;
        _sheepLabel.hidden = YES;
        
        CGRect viewRect = _bookmarkTitleLabel.frame ;
        viewRect.origin.x -= kTypeWidth;
        _bookmarkTitleLabel.frame = viewRect;
        
        viewRect = _bookmarkLabel.frame;
        viewRect.origin.x -= kTypeWidth;
        _bookmarkLabel.frame = viewRect;
        
        viewRect = _bookmarkIconView.frame;
        viewRect.origin.x -= kTypeWidth;
        _bookmarkIconView.frame = viewRect;
        
        _discountTextLabel.hidden = YES;
        _discountTitleLabel.hidden = YES;
    } else {
        _discountLabel.hidden = YES;
    }
}

- (void)setRate:(NSString *)rate
{
    _rate = rate;
    
    // 如果type = 2, rate是null，则显示优惠
    if (_type == 2) {
        if (!_rate) {
            _discountTextLabel.hidden = YES;
            _discountTitleLabel.hidden = YES;
            _discountLabel.hidden = NO;
            _discountLabel.text = @"特惠";
        } else {
            _discountTextLabel.text = _rate;
        }
    }
}

- (IBAction)handleBookmark:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(handleBookmark:)])
        [_delegate handleBookmark:sender];
}

- (IBAction)handleFeedback:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(handleFeedback:)])
        [_delegate handleFeedback:sender];
}

- (IBAction)handleShare:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(handleShare:)])
        [_delegate handleShare:sender];
}

+ (DiscountDescriptionCell *)getInstance
{
    return [[[NSBundle mainBundle] loadNibNamed:@"DiscountDescriptionCell" owner:nil options:nil] objectAtIndex:0];
}

@end
