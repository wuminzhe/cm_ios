//
//  ShopDiscountViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 12-12-24.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

enum {
    kBankFilter = 1001,
    kCategoryFilter,
    kSortFilter
};

typedef NSUInteger kChooseFilter;

@interface ShopDiscountViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate> {
    __unsafe_unretained IBOutlet UIScrollView *theScrollView;
    __unsafe_unretained IBOutlet UITableView *mTableView;
    
    __unsafe_unretained IBOutlet UILabel *bankLabel;
    __unsafe_unretained IBOutlet UILabel *categoryLabel;
    __unsafe_unretained IBOutlet UILabel *sortLabel;
    
    __unsafe_unretained IBOutlet UIButton *bankButton;
    __unsafe_unretained IBOutlet UIButton *categoryButton;
    __unsafe_unretained IBOutlet UIButton *sortButton;
    
    __unsafe_unretained IBOutlet UIButton *settingButton;
    __unsafe_unretained IBOutlet UIButton *searchButton;
    
    IBOutlet UIView *searchView;
    
    IBOutlet UISearchBar *mSearchBar;
}

- (IBAction)filterDiscounts:(id)sender;

@property (nonatomic, strong) NSMutableArray *discountList;

// 搜索优惠信息
- (IBAction)searchDiscounts:(id)sender;
- (IBAction)gotoUserSettings:(id)sender;

- (void)startLoadDiscounts;

@end
