//
//  BillCell.m
//  CardMaster
//
//  Created by wenjun on 13-1-14.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "BillCell.h"
#import <QuartzCore/QuartzCore.h>

@interface  BillCell ()
{
    NSString * newIdentifier;
    IBOutlet UILabel * sendDateLabel;
    IBOutlet UILabel * repaymentDaysLabel;
    IBOutlet UILabel * intrestFreeDaysLabel;
    IBOutlet UILabel * repaymentAmountLabel;
    IBOutlet UILabel * billDateLabel;
    IBOutlet UILabel * repaymentDateLabel;
    IBOutlet UILabel * minRepaymentAmount;
    IBOutlet UILabel * remainAmountLabel;
    IBOutlet UIImageView * imgView;
    IBOutlet UIView * imgPart;
    IBOutlet UILabel * repaymentDaysLabel1;
}

@end;

@implementation BillCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initialize
{
    imgPart.layer.cornerRadius = 4;
    imgPart.clipsToBounds = YES;
}

+ (BillCell *)getInstance
{
    BillCell * cell = [[[NSBundle mainBundle] loadNibNamed:@"BillCell" owner:nil options:nil] lastObject];
    [cell initialize];
    return cell;
}

- (void)setReuseIdentifier:(NSString *)identifier
{
    newIdentifier = identifier;
}

- (NSString *)reuseIdentifier
{
    if (newIdentifier)
    {
        return newIdentifier;
    }
    return [super reuseIdentifier];
}

- (void)loadBill:(Bill *)bill blue:(BOOL)blue
{
    sendDateLabel.text = bill.sendTime;
    repaymentAmountLabel.text = bill.balance;
    repaymentDateLabel.text = bill.paymentTime;
    billDateLabel.text = bill.statementTime;
    minRepaymentAmount.text = bill.minPayment;
    remainAmountLabel.text = bill.creditLimit;
    if (blue)
    {
        imgView.image = [UIImage imageNamed:@"billfarme1"];
        NSDateFormatter * dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate * paymentDate = [dateFormat dateFromString:bill.paymentTime];
        NSInteger daysToPay = [paymentDate timeIntervalSinceNow] / 86400;
        if (daysToPay < 0)
        {
            repaymentDaysLabel1.text = @"距还款已过         天";
            repaymentDaysLabel.text = [NSString stringWithFormat:@"%d",-daysToPay];
        }
        else
        {
            repaymentDaysLabel1.text = @"距还款还有         天";
            repaymentDaysLabel.text = [NSString stringWithFormat:@"%d",daysToPay];
        }
        
//        intrestFreeDaysLabel.text = @"";
        imgPart.frame = CGRectMake(imgPart.frame.origin.x, imgPart.frame.origin.y, imgPart.frame.size.width, 172);
    }
    else
    {
        imgView.image = [UIImage imageNamed:@"billfarme2"];
        repaymentDaysLabel.text = nil;
//        intrestFreeDaysLabel.text = nil;
        imgPart.frame = CGRectMake(imgPart.frame.origin.x, imgPart.frame.origin.y, imgPart.frame.size.width, 142);
    }
}


@end
