//
//  Comment.h
//  LifeShanghai
//
//  Created by wenjun on 12-11-30.
//
//
#import <Foundation/Foundation.h>

@interface Comment : NSObject

@property (nonatomic, strong) NSString *cid;
@property (nonatomic, strong) NSString *productId;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *score;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSDate *createAt;
@property (nonatomic, strong) NSString *username;

@property (nonatomic) NSInteger rowHeight;
//@property (nonatomic, strong) NSDate * createdDate;

- (NSComparisonResult)dateComparetor:(Comment *)other;

@end
