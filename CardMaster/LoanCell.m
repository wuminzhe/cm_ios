//
//  LoanCell.m
//  CardMaster
//
//  Created by wenjun on 13-1-11.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "LoanCell.h"
#import "EGOImageView.h"

@interface LoanCell()
{
    IBOutlet UILabel * nameLabel;
    IBOutlet UILabel * amountLabel;
    IBOutlet UILabel * rateLabel;
    IBOutlet UILabel * applyCountLabel;
//    IBOutlet UIImageView * imgView;
    IBOutlet EGOImageView * imgView;
    IBOutlet UIActivityIndicatorView * indicator;
}
@end

@implementation LoanCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initialize
{
    imgView.placeholderImage = [UIImage imageNamed:@"recommend_card_none"];
}

+ (LoanCell *)getInstance
{
    LoanCell * cell = [[[NSBundle mainBundle] loadNibNamed:@"LoanCell" owner:nil options:nil] lastObject];
    [cell initialize];
    return cell;
}

- (void)loadLoan:(LoanProduct *)loan
{
    nameLabel.text = loan.name;
    amountLabel.text = loan.amount;
    rateLabel.text = loan.rate;
    applyCountLabel.text = loan.applyCount;
//    imgView.image = loan.image;
    imgView.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/assets/upload/%@",kServerAddr,[loan.imageName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
//    if (loan.image)
//    {
//        [indicator stopAnimating];
//    }
//    else
//    {
//        [indicator startAnimating];
//    }
}

@end
