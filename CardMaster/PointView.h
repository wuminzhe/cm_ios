//
//  PointView.h
//  NearbyShops
//
//  Created by wenjun on 12-12-26.
//
//

#import <UIKit/UIKit.h>

@class PointView;
@protocol PointViewDelegate <NSObject>
@optional
- (void)pointView:(PointView *)pointView pointsChanged:(NSInteger)points;
- (void)gestureStarted:(PointView *)pointView;
- (void)gestureStopped:(PointView *)pointView;
@end

@interface PointView : UIView

@property (nonatomic) CGFloat points;
@property (nonatomic) BOOL gestureEnabled;
@property (unsafe_unretained,nonatomic) id<PointViewDelegate> delegate;

- (id)initWithDarkImage:(UIImage *)darkImage lightImage:(UIImage *)lightImage imageSize:(CGSize)imageSize space:(NSInteger)space pointCount:(NSInteger)pointCount;

@end
