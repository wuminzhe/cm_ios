//
//  ChoiceCell.h
//  CardMaster
//
//  Created by wenjun on 13-1-10.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoiceCell : UITableViewCell

- (void)loadChoice:(NSString *)choice;

+ (ChoiceCell *)getInstance;

@end
