//
//  GuideViewController.h
//  CardMaster
//
//  Created by Zhang Ming on 13-1-10.
//  Copyright (c) 2013年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MAX_GUIDE_PAGES     3

@interface GuideViewController : UIViewController<UIScrollViewDelegate> {
    __unsafe_unretained IBOutlet UIScrollView *theScrollView;
}

@end
