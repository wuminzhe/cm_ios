//
//  CardSettingViewController.m
//  cardMaster
//
//  Created by Zhang Ming on 12-10-16.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "CardSettingViewController.h"
#import "RefundRemindViewController.h"
#import "InstallmentViewController.h"
#import "BillViewController.h"
#import "FreeAnnualFeeViewController.h"
#import "GlobalHeader.h"
#import "CreditCard.h"
#import "CMConstants.h"
#import "SQLGlobal.h"
#import "Bank.h"
#import "CMUtil.h"
#import "CardEngine.h"
#import "NSString+Verify.h"
#import "NSDictionary+Extension.h"
#import "LoanListController.h"
#import "BillImportViewController.h"
#import "BillController.h"
#import "Rate.h"
#import "Reminder.h"

@interface CardSettingViewController () {
    NSMutableArray *pickerData;
    
    NSInteger selectedIndex;
    
    NSInteger selectedBankIndex;
    NSInteger selectedReminderindex;
    Rate *rate;
}

@property (nonatomic, strong) NSMutableArray *banks;
@property (nonatomic, strong) NSMutableArray *days;

@end

@implementation CardSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if (self.card.nick) {
        self.title = self.card.nick;
    } else if (self.card.productName) {
        self.title = self.card.productName;
    } else {
        self.title = self.card.cardNumber;
    }
    
    choose = 1000;
    
    // 编辑按钮
    editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    editButton.frame = CGRectMake(0, 0, 44, 25);
    
    [editButton setImage:[UIImage imageNamed:@"edit.png"]
                forState:UIControlStateNormal];
    
    [editButton setImage:[UIImage imageNamed:@"edit_click.png"]
                forState:UIControlStateHighlighted];
    
    [editButton addTarget:self
                   action:@selector(editDetail:)
         forControlEvents:UIControlEventTouchUpInside];
    
    editBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:editButton];
    self.navigationItem.rightBarButtonItem = editBarButtonItem;
    
    // 设置信用卡信息
    cardImageView.layer.cornerRadius = 5.0;
    cardImageView.layer.masksToBounds = YES;
    
    cardNumberLabel.text = self.card.cardNumber;
    cardOfBankLabel.text = self.card.bankName;
//    cardNameLabel.text = self.card.productName;
    
    if ( _card.nick.length > 0)
        cardNameLabel.text = _card.nick;
    else
        cardNameLabel.text = _card.productName;
    
    reminderLabel.text = [NSString stringWithFormat:@"每月%@号", self.card.billDay];
    
    if (_card.cardPic.length > 0)
        cardImageView.image = [UIImage imageWithContentsOfFile:[MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:_card.cardPic]];
    
    // 初始化编辑页面
    cardNumberTextField.text = self.card.cardNumber;
    editBankNameLabel.text = self.card.bankName;
    
    if ( _card.nick.length > 0)
        cardNameTextField.text = _card.nick;
    else
        cardNameTextField.text = _card.productName;
    
    [reminderDayButton setTitle:[NSString stringWithFormat:@"%@", self.card.billDay]
                       forState:UIControlStateNormal];
    
    [theScrollView addSubview:detailView];
    theScrollView.contentSize = detailView.frame.size;
    
    // pickerView
    pickerData = nil;
    
    _days = [NSMutableArray arrayWithCapacity:31];
    for (int i = 1; i <= kNumberOfDays; i++) {
        [_days addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    selectedIndex = 2000;
    
    self.banks = [[SQLGlobal sharedInstance] getBanks];
    
    pickerData = self.banks;
    
    // 计算pickerView selectedRow
    selectedBankIndex = -1;
    for (int i = 0; i < _banks.count; i++) {
        Bank *bank = _banks[i];
        if ([bank.bankID isEqualToString:_card.bankID ]) {
            selectedBankIndex = i;
            break;
        }
    }
    
    selectedReminderindex = (_card.billDay.integerValue - 1) <= 0 ? 0 : (_card.billDay.integerValue - 1);
    
    [self.view addSubview:chooseView];
    CGRect viewRect = self.view.frame;
    chooseView.frame = CGRectMake(0,
                                  viewRect.size.height,
                                  viewRect.size.width,
                                  260);
    // 初始化
    if (_banks.count > 0)
        cardOfBankLabel.text = (_card.bankName == nil) ? @"" : _card.bankName;
    
    engine = [[CardEngine alloc] initWithHostName:kServerAddr];
    
    if (selectedBankIndex == -1)
        selectedBankIndex = 0;
    
    Bank *selectedBank = _banks[selectedBankIndex];
    
    rate = [[[SQLGlobal sharedInstance] getRatesWithBankID:selectedBank.bankID] objectAtIndex:0];
    
    refundTotalMoneyLabel.text = [NSString stringWithFormat:@"%@元", (rate.minAmount == nil ? @"0" : rate.minAmount)];
    refundMiniMoneyLabel.text = [NSString stringWithFormat:@"%@元", rate.maxAmount == nil ?  @"0" : rate.maxAmount];
    
    NSString *imageName = [NSString stringWithFormat:@"bank_%@.png", selectedBank.bankID];
    bankImageView.image = [UIImage imageNamed:imageName];
    editBankIconView.image = [UIImage imageNamed:imageName];
}

- (void)viewDidUnload
{
    theMainScrollView = nil;
    theScrollView = nil;
    editButton = nil;
    cardImageView = nil;
    bussinessButton = nil;
    detailView = nil;
    bussinessView = nil;
    cardNameLabel = nil;
    bankImageView = nil;
    cardOfBankLabel = nil;
    cardNameLabel = nil;
    reminderLabel = nil;
    
    editView = nil;
    cardNumberTextField = nil;
    cardNameTextField = nil;
    editBankIconView = nil;
    editBankNameLabel = nil;
    reminderDayButton = nil;
    
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    refundDayLabel.text = [NSString stringWithFormat:@"每月%@号", self.card.reminderDay];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (void)backAction
{
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)changeInfo:(id)sender
{
    // 1000 查看信息, 1001 办理业务
    UIButton *btn = (UIButton *)sender;
    
    if (btn.tag == choose)  return;
    
    choose = btn.tag;
    
    if (sender == detailButton) {
        [detailButton setImage:[UIImage imageNamed:@"chakan_cl.png"]
                      forState:UIControlStateNormal];
        [detailButton setImage:[UIImage imageNamed:@"chakan_cl.png"]
                      forState:UIControlStateHighlighted];
        
        [bussinessButton setImage:[UIImage imageNamed:@"banli.png"]
                         forState:UIControlStateNormal];
        [bussinessButton setImage:[UIImage imageNamed:@"banli_cl.png"]
                         forState:UIControlStateHighlighted];
        
        editButton.hidden = NO;
        [bussinessView removeFromSuperview];
        [theScrollView addSubview:detailView];
        theScrollView.contentSize = detailView.frame.size;
    } else {
        [detailButton setImage:[UIImage imageNamed:@"chakan.png"]
                      forState:UIControlStateNormal];
        [detailButton setImage:[UIImage imageNamed:@"chakan_cl.png"]
                      forState:UIControlStateHighlighted];
        
        [bussinessButton setImage:[UIImage imageNamed:@"banli_cl.png"]
                         forState:UIControlStateNormal];
        [bussinessButton setImage:[UIImage imageNamed:@"banli_cl.png"]
                         forState:UIControlStateHighlighted];
        
        editButton.hidden = YES;
        [detailView removeFromSuperview];
        [theScrollView addSubview:bussinessView];
        theScrollView.contentSize = bussinessView.frame.size;
    }
}

- (void)editDetail:(id)sender
{
    if (detailEditing && (sender != self.backButton)) {
        // 判断卡号是否合法
        
        NSRange range = [cardNumberTextField.text rangeOfString:@"*"];
        
        if (![cardNumberTextField.text isValidCreditCardNumber] && (range.location == NSNotFound)) {
            [cardNumberTextField becomeFirstResponder];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"信息"
                                                                message:@"请输入正确的信用卡卡号！"
                                                               delegate:nil
                                                      cancelButtonTitle:@"确定"
                                                      otherButtonTitles:nil];
            
            [alertView show];
            return;
        }
    }
    
    detailEditing = !detailEditing;
    
    // 编辑操作
    if (detailEditing) {
        [cardNumberTextField becomeFirstResponder];
        [theScrollView addSubview:editView];
        [detailView removeFromSuperview];
        
    // 保存操作or取消操作
    } else {
        [theScrollView addSubview:detailView];
        [editView removeFromSuperview];
    }
    
    [UIView animateWithDuration:kFilterAnimtaionDuration animations:^{
        if (detailEditing) {
            // 编辑页面
            theMainScrollView.contentOffset = CGPointMake(0, 266);
            
            [editButton setImage:[UIImage imageNamed:@"save.png"]
                        forState:UIControlStateNormal];
            [editButton setImage:[UIImage imageNamed:@"save_click.png"]
                        forState:UIControlStateHighlighted];
            
            [self.backButton setImage:[UIImage imageNamed:@"cancel.png"]
                             forState:UIControlStateNormal];
            [self.backButton setImage:[UIImage imageNamed:@"cancel_click.png"]
                             forState:UIControlStateHighlighted];
            self.backButton.frame = CGRectMake(0, 0, 44, 25);
            
            [self.backButton removeTarget:self
                                   action:@selector(backAction)
                         forControlEvents:UIControlEventTouchUpInside];
            [self.backButton addTarget:self
                                action:@selector(cancelModify:)
                      forControlEvents:UIControlEventTouchUpInside];
        } else {
            // 保存操作
            theMainScrollView.contentOffset = CGPointMake(0, 0);
            
            [editButton setImage:[UIImage imageNamed:@"edit.png"]
                        forState:UIControlStateNormal];
            [editButton setImage:[UIImage imageNamed:@"edit_click.png"]
                        forState:UIControlStateHighlighted];
            
            [self.backButton setImage:[UIImage imageNamed:@"back.png"]
                             forState:UIControlStateNormal];
            [self.backButton setImage:[UIImage imageNamed:@"back_click.png"]
                             forState:UIControlStateHighlighted];
            
            [self.backButton removeTarget:self
                                   action:@selector(cancelModify:)
                         forControlEvents:UIControlEventTouchUpInside];
            [self.backButton addTarget:self
                                action:@selector(backAction)
                      forControlEvents:UIControlEventTouchUpInside];
            
            self.backButton.frame = CGRectMake(0, 0, 28, 16);
        }
    }];
    
    // 取消按钮，不做任何操作
    if (sender == self.backButton) {
        return ;
    }
    
    if (!detailEditing)
        [self saveModify:sender];
}

- (void)saveModify:(id)sender
{
    [self hidePickerView:nil];
    
    NSString *cardNumber = cardNumberTextField.text;
    NSString *cardName = cardNameTextField.text;
    
    NSRange range = [cardNumber rangeOfString:@"*"];
    
    if (![cardNumber isValidCreditCardNumber] && (range.location == NSNotFound)) {
        [self quickAlertView:@"请输入正确的信用卡卡号！"];
        return;
    }
    
    if (selectedBankIndex == -1) {
        [self quickAlertView:@"请选择银行！"];
        return ;
    }
    
    _card.cardNumber = cardNumber;
    _card.nick = cardName;
    
    Bank *bank = _banks[selectedBankIndex];
    NSString *reminderDay = _days[selectedReminderindex];
    _card.bankID = bank.bankID;
    _card.bankName = bank.name;
    _card.billDay = reminderDay;//[NSNumber numberWithInteger:reminderDay.integerValue];
    
    NSMutableDictionary *updateParams = [[NSMutableDictionary alloc] init];
    updateParams[@"code"] = [CMUtil getCode];
    updateParams[@"card_id"] = _card.cardID;
    updateParams[@"card_number"] = _card.cardNumber;
    updateParams[@"bank_id"] = _card.bankID;
    updateParams[@"nick"] = _card.nick == nil ? @"" : _card.nick;
    updateParams[@"state_day"] = _card.billDay;
    updateParams[@"pic"] = _card.remoteCardPic;
    
    [self showHudView:@"正在提交"];
    [engine updateCreditCard:updateParams onCompletion:^(NSString *result) {
        NSLog(@"result = %@", result);
        
        if ([result isEqualToString:SUCCESS]) {
            cardNumberLabel.text = _card.cardNumber;
            cardOfBankLabel.text = _card.bankName;
            
            if ( _card.nick.length > 0)
                cardNameLabel.text = _card.nick;
            else
                cardNameLabel.text = _card.productName;
            reminderLabel.text = [NSString stringWithFormat:@"每月%@号", _card.billDay];
            
            [[SQLGlobal sharedInstance] updateCard:_card];
            [LvToast showWithText:@"修改成功！" duration:1.0f];
        } else {
            [self quickAlertView:result];
            [LvToast showWithText:@"修改失败！" duration:1.0f];
        }
        
        [self hideHudView];
    } onError:^(NSError *error) {
        [self hideHudView];
        [LvToast showWithText:@"修改失败！" bottomOffset:80 duration:1.0f];
    }];
}

//- (void)saveModify:(id)sender
//{
//    [self hidePickerView:nil];
//    
//    // 卡片修改逻辑
//    // 1.删除卡片
//    // 2.添加卡片
//    NSString *oldCardNumber = _card.cardNumber;
//    NSString *modifiedCardNumber = cardNumberTextField.text;
//    NSString *modifiedCardName = cardNameTextField.text;
//    
//    // 查找本地是否有此卡卡号
//    CreditCard *modifiedCard = [[SQLGlobal sharedInstance] getCreditCard:modifiedCardNumber];
//    
//    if (modifiedCard && (![oldCardNumber isEqualToString:modifiedCardNumber])) {
//        [self quickAlertView:@"已有此卡信息，请正确修改！"];
//        return ;
//    }
//    
//    if (selectedBankIndex == -1) {
//        [self quickAlertView:@"请选择银行！"];
//        return ;
//    }
//
//    [self showHudView:@"正在提交"];
//
//    // 删除卡片参数
//    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:2];
//    params[@"code"] = [CMUtil getCode];
////    params[@"card_number"] = _card.cardNumber;
//    params[@"card_id"] = _card.cardID;
//    
//    if (!modifiedCard) {
//        modifiedCard = [[CreditCard alloc] init];
//    }
//    
//    modifiedCard.cardNumber = modifiedCardNumber;
//    
//    // 此处待修改
//    modifiedCard.productID = _card.productID;
//    modifiedCard.nick = cardNameTextField.text;
//    modifiedCard.productName = modifiedCardName;
//    
//    Bank *bank = _banks[selectedBankIndex];
//    
//    modifiedCard.bankID = bank.bankID;
//    modifiedCard.bankName = bank.name;
//    
//    modifiedCard.ownerName = [CMUtil getUsername];
//    modifiedCard.remoteCardPic = _card.remoteCardPic;
//    modifiedCard.cardPic = _card.cardPic;
//    
//    NSString *reminderDay = _days[selectedReminderindex];
//    
//    modifiedCard.reminderDay = [NSNumber numberWithInteger:reminderDay.integerValue];
//    
//    // 添加卡片参数
//    NSMutableDictionary *addCardParams = [NSMutableDictionary dictionary];
//    addCardParams[@"code"] = [CMUtil getCode];
//    addCardParams[@"card_number"] = modifiedCard.cardNumber;
//    addCardParams[@"pic"] = modifiedCard.cardPic;
//    addCardParams[@"owner_name"] = [CMUtil getUsername];
//    if (modifiedCard.bankID)
//        addCardParams[@"bank_id" ] = modifiedCard.bankID;
//    if (modifiedCard.productID)
//        addCardParams[@"creditcard_product_id" ] = modifiedCard.productID;
//    if (modifiedCard.reminderDay)
//        addCardParams[@"pay_day"] = modifiedCard.reminderDay;
//    else {
//        addCardParams[@"pay_day"] = @"1";
//        modifiedCard.reminderDay = @1;
//    }
//    
//    // 1.删除卡片
//    [engine deleteCreditCard:params onCompletion:^(BOOL status) {
//        // 删除成功
//        if (status) {
////            [[SQLGlobal sharedInstance] deleteCard:self.card.cardNumber];
//            [[SQLGlobal sharedInstance] deleteCard:self.card.cardID];
//            
//            __block id weakSelf = self;
//            __block CreditCard *weakCard = _card;
//            // 添加卡片
//            [engine addCreditCard:addCardParams onCompletion:^(NSDictionary *response) {
//                modifiedCard.status = [[response stringForKey:@"status"] boolValue];
//                modifiedCard.cardID = [response stringForKey:@"card_id"];
//                
//                [[SQLGlobal sharedInstance] updateCard:modifiedCard];
//                
//                cardNumberLabel.text = modifiedCard.cardNumber;
//                cardOfBankLabel.text = modifiedCard.bankName;
//                cardNameLabel.text = modifiedCard.productName;
//                reminderLabel.text = [NSString stringWithFormat:@"每月%@号", modifiedCard.reminderDay];
//
//                [weakSelf hideHudView];
//                weakSelf = nil;
//                
//                NSDictionary *userInfo = @{ @"newCard" : modifiedCard, @"oldCard" : weakCard };
//                
//                [[NSNotificationCenter defaultCenter] postNotificationName:MODIFY_CARD
//                                                                    object:nil
//                                                                  userInfo:userInfo];
//                [weakSelf dismissModalViewControllerAnimated:YES];
//            } onError:^(NSError *error) {
//                NSLog(@"添加失败");
//                [weakSelf hideHudView];
//                weakSelf = nil;
//            }];
//        } else {
//            NSLog(@"删除失败");
//        }
//    } onError:^(NSError *error) {
//        [self hideHudView];
//    }];
//}

- (void)cancelModify:(id)sender
{
    [self hidePickerView:sender];
    [self editDetail:sender];
}

- (IBAction)refundRemind:(id)sender
{
    RefundRemindViewController *refundRemindViewController = [[RefundRemindViewController alloc] initWithNibName:@"RefundRemindViewController" bundle:nil];
    refundRemindViewController.card = self.card;
    [self.navigationController pushViewController:refundRemindViewController animated:YES];
}

- (IBAction)refundImmediate:(id)sender
{
    
}

- (IBAction)searchDebt:(id)sender
{
    BillViewController *billViewController = [[BillViewController alloc] initWithNibName:@"BillViewController" bundle:nil];
    billViewController.card = self.card;
    [self.navigationController pushViewController:billViewController animated:YES];
}

- (IBAction)installment:(id)sender
{
    InstallmentViewController *installmentViewController = [[InstallmentViewController alloc] initWithNibName:@"InstallmentViewController" bundle:nil];
    installmentViewController.bankId = _card.bankID;
    [self.navigationController pushViewController:installmentViewController animated:YES];
}

- (IBAction)freeAnnualFee:(id)sender
{
    FreeAnnualFeeViewController *freeAnnualFeeViewController = [[FreeAnnualFeeViewController alloc] initWithNibName:@"FreeAnnualFeeViewController" bundle:nil];
    freeAnnualFeeViewController.card = self.card;
    [self.navigationController pushViewController:freeAnnualFeeViewController animated:YES];
}

- (IBAction)deleteCard:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"信息"
                                                        message:@"是否删除卡片"
                                                       delegate:self
                                              cancelButtonTitle:@"确认"
                                              otherButtonTitles:@"取消", nil];
    [alertView show];
}

- (IBAction)searchEDebt:(id)sender
{
//    if ([_card.source isEqualToString:@"0"]) {
//        BillImportViewController *billImportViewController = [[BillImportViewController alloc] initWithNibName:@"BillImportViewController" bundle:nil];
//        billImportViewController.cardID = self.card.cardID;
//        [self.navigationController pushViewController:billImportViewController animated:YES];
//    } else {
        BillController *billController = [[BillController alloc] initWithNibName:@"BillController" bundle:nil];
        billController.cardID = self.card.cardID;
        [self.navigationController pushViewController:billController animated:YES];
    NSLog(@"code:%@ card_id:%@",[CMUtil getCode],self.card.cardID);
//    }
}

- (IBAction)applyLoan:(id)sender
{
    LoanListController *vc = [[LoanListController alloc] initWithNibName:@"LoanListController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)showPickerView:(id)sender
{
    if (sender)
        selectedIndex = ((UIButton *)sender).tag;
    
    if (selectedIndex == 2000) {
        doneButton.title = @"下一项";
        pickerData = _banks;
        
        [mPickerView reloadAllComponents];
        [mPickerView selectRow:selectedBankIndex inComponent:0 animated:YES];
    } else {
        doneButton.title = @"保存";
        pickerData = _days;
        [mPickerView reloadAllComponents];
        [mPickerView selectRow:selectedReminderindex inComponent:0 animated:YES];
    }
    
    if (chooseView.frame.origin.y < self.view.frame.size.height) {
        return;
    } else {
        [UIView animateWithDuration:kFilterAnimtaionDuration animations:^{
            chooseView.frame = CGRectMake(0,
                                          self.view.frame.size.height - 260,
                                          self.view.frame.size.width,
                                          260);
            [cardNumberTextField resignFirstResponder];
            [cardNameTextField resignFirstResponder];
        }];
    }
}

- (IBAction)chooseDone:(id)sender
{
    if (sender == cardNumberTextField) {
        selectedIndex = 2000;
        [self showPickerView:nil];
    } else if (sender == cardNameTextField) {
        selectedIndex = 2001;
        [self showPickerView:nil];
    } else {
        int row = [mPickerView selectedRowInComponent:0];
        
        if (selectedIndex == 2000) {
            [cardNameTextField becomeFirstResponder];
            
            selectedBankIndex = row;
            
            Bank *bank = _banks[row];
            
            editBankNameLabel.text = bank.name;
            
            NSString *imageName = [NSString stringWithFormat:@"bank_%@.png", bank.bankID];
            bankImageView.image = [UIImage imageNamed:imageName];
            editBankIconView.image = [UIImage imageNamed:imageName];
        } else {
            selectedReminderindex = row;
            
            [reminderDayButton setTitle:_days[row]
                               forState:UIControlStateNormal];
            
            [self editDetail:sender];
            [self hidePickerView:sender];
        }
    }
}

- (void)hidePickerView:(id)sender
{
    [UIView animateWithDuration:kFilterAnimtaionDuration animations:^{
        chooseView.frame = CGRectMake(0,
                                      self.view.frame.size.height,
                                      self.view.frame.size.width,
                                      260);
    }];
}

#pragma mark - UIAlertViewDelegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self showHudView:@"正在删除卡片..."];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:2];
        params[@"code"] = [CMUtil getCode];
//        params[@"card_number"] = _card.cardNumber;
        params[@"card_id"] = _card.cardID;
        
        Reminder *reminder = [[SQLGlobal sharedInstance] getReminder:_card.cardID];

        [engine deleteCreditCard:params onCompletion:^(BOOL status) {
            if (!status) {
                NSLog(@"服务器端删除不成功");
                [self quickAlertView:@"服务器端删除不成功！"];
            }
            
            // 数据库删除
            [[SQLGlobal sharedInstance] deleteCard:self.card.cardID];
            
            
            if (reminder) {
                // 如果有通知，则删除
                NSArray *notifications = [UIApplication sharedApplication].scheduledLocalNotifications;
                for (UILocalNotification *notification in notifications)
                {
                    if ([notification.userInfo[@"cardID"] isEqualToString:_card.cardID]) {
                        [[UIApplication sharedApplication] cancelLocalNotification:notification];
                    }
                }
            }
            
            [[SQLGlobal sharedInstance] deleteReminder:self.card.cardID];
            // 删除本地图片
            [[NSFileManager defaultManager] removeItemAtPath:[MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:self.card.cardPic] error:NULL];
            
            [self hideHudView];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:DELETE_CARD object:nil userInfo:nil];
            
            [self dismissModalViewControllerAnimated:YES];

        } onError:^(NSError *error) {
            [self hideHudView];
        }];
    }
//        if (_delegate && [_delegate respondsToSelector:@selector(deleteCard)]) {
//            
//            if (!hud) {
//                hud = [[MBProgressHUD alloc] initWithView:self.view];
//                hud.labelText = @"正在删除卡片";
//                [self.view addSubview:hud];
//                hud.delegate = self;
//                [hud show:YES];
//            }
//            
//            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:2];
//            params[@"code"] = [CMUtil getCode];
//            params[@"card_number"] = _card.cardNumber;
//
//            [engine deleteCreditCard:params onCompletion:^(BOOL status) {
//                
//                if (!status)
//                    NSLog(@"服务器端删除不成功");
//
//                // 数据库删除
//                [[SQLGlobal sharedInstance] deleteCard:self.card.cardNumber];
//                // 删除本地图片
//                [[NSFileManager defaultManager] removeItemAtPath:[MTLP(CARD_IMAGE_FILE) stringByAppendingPathComponent:self.card.cardPic] error:NULL];
//                
//                [hud hide:YES];
//                [_delegate deleteCard];
//                [self dismissModalViewControllerAnimated:YES];
//            } onError:^(NSError *error) {
//                ;
//            }];
//        }
}

#pragma mark - UIPickerViewDataSource methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerData.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (selectedIndex == 2001)
        return pickerData[row];
    else {
        Bank *bank = pickerData[row];
        return bank.name;
    }
}

#pragma mark - UIPickerViewDelegate methods
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (selectedIndex == 2000) {
        selectedBankIndex = row;
        Bank *bank = _banks[row];
        editBankNameLabel.text = bank.name;
        
        NSString *imageName = [NSString stringWithFormat:@"bank_%@.png", bank.bankID];
        bankImageView.image = [UIImage imageNamed:imageName];
        editBankIconView.image = [UIImage imageNamed:imageName];
        
    } else if (selectedIndex == 2001) {
        selectedReminderindex = row;
        [reminderDayButton setTitle:_days[row]
                           forState:UIControlStateNormal];
    }
}

#pragma mark - UITextFieldDelegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self hidePickerView:textField];
}

// 重新计算按钮长度
- (void)reloadButtonImage:(NSString *)text
{
    UIImage *image = [UIImage imageNamed:@"setting_edit_bank_btn.png"];
    
    [image stretchableImageWithLeftCapWidth:5 topCapHeight:4];
    
    
}

@end
