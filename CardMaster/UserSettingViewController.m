//
//  UserSettingViewController.m
//  CardMaster
//
//  Created by Zhang Ming on 12-12-26.
//  Copyright (c) 2012年 南京爱西柚网络科技有限公司. All rights reserved.
//

#import "UserSettingViewController.h"
#import "UserSettingCell.h"
#import "SuggestViewController.h"
#import "AboutUsViewController.h"
#import "BankServiceViewController.h"
#import "MyFavoriteViewController.h"
#import "LoginViewController.h"
#import "UserAccountViewController.h"
#import "FriendUsViewController.h"
#import "EmailListController.h"
#import "CollectionController.h"

@interface UserSettingViewController ()

@property (nonatomic, strong) SinaWBEngine *sinaEngine;
@property (nonatomic, strong) TencentWBEngine *tencentEngine;

@end

@implementation UserSettingViewController
{
    NSString *content;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"个人设置";
    
//    _sinaOAuth = [[SinaWeibo alloc] initWithAppKey:kSinaAppKey
//                                         appSecret:kSinaAppSecret
//                                    appRedirectURI:kSinaAppRedirect
//                                       andDelegate:self];
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSDictionary *sinaweiboInfo = [defaults objectForKey:@"SinaWeiboAuthData"];
//    if ([sinaweiboInfo objectForKey:@"AccessTokenKey"] && [sinaweiboInfo objectForKey:@"ExpirationDateKey"] && [sinaweiboInfo objectForKey:@"UserIDKey"])
//    {
//        _sinaOAuth.accessToken = [sinaweiboInfo objectForKey:@"AccessTokenKey"];
//        _sinaOAuth.expirationDate = [sinaweiboInfo objectForKey:@"ExpirationDateKey"];
//        _sinaOAuth.userID = [sinaweiboInfo objectForKey:@"UserIDKey"];
//    }
//    
//    weiboEngine = [[TCWBEngine alloc] initWithAppKey:WiressSDKDemoAppKey andSecret:WiressSDKDemoAppSecret andRedirectUrl:REDIRECTURI];
//    [weiboEngine setRootViewController:self];
    
    self.sinaEngine = [[SinaWBEngine alloc] init];
    self.sinaEngine.rootViewController = self;
    self.sinaEngine.delegate = self;
    
    self.tencentEngine = [[TencentWBEngine alloc] init];
    self.tencentEngine.rootViewController = self;
    self.tencentEngine.delegate = self;
    
    content = [NSString stringWithFormat:@"“我爱卡管家”，找商户优惠，找银行活动，信用卡还款，都在这里了。专业、给力的信用卡管理App。%@",kAppURL];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods
- (void)backAction
{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserSettingCell"];
    
    if (!cell) {
        cell = [UserSettingCell getInstance];
        cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"setting_bg_select.png"]];
    }
    
    if (indexPath.row == 0) {
        cell.imageIcon.image = [UIImage imageNamed:@"setting_0.png"];
        cell.userInfoLabel.text = @"个人账号";
    } else if (indexPath.row == 1) {
        cell.imageIcon.image = [UIImage imageNamed:@"setting_1.png"];
        cell.userInfoLabel.text = @"我的收藏";
    } else if (indexPath.row == 2) {
        cell.imageIcon.image = [UIImage imageNamed:@"setting_2.png"];
        cell.userInfoLabel.text = @"电子账单";
    } else if (indexPath.row == 3) {
        cell.imageIcon.image = [UIImage imageNamed:@"setting_3.png"];
        cell.userInfoLabel.text = @"银行服务";
    } else if (indexPath.row == 4) {
        cell.imageIcon.image = [UIImage imageNamed:@"setting_4.png"];
        cell.userInfoLabel.text = @"关于我们";
    } else if (indexPath.row == 5) {
        cell.imageIcon.image = [UIImage imageNamed:@"setting_5.png"];
        cell.userInfoLabel.text = @"意见建议";
    } else if (indexPath.row == 6) {
        cell.imageIcon.image = [UIImage imageNamed:@"setting_6.png"];
        cell.userInfoLabel.text = @"推荐给好友";
    } else if (indexPath.row == 7) {
        cell.imageIcon.image = [UIImage imageNamed:@"setting_7.png"];
        cell.userInfoLabel.text = @"给我爱信用卡评价一把";
    } else if (indexPath.row == 8) {
        cell.imageIcon.image = [UIImage imageNamed:@"setting_8.png"];
        cell.userInfoLabel.text = @"关注我们";
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"LoginSuccess"]) {
            UserAccountViewController *userAccountViewController = [[UserAccountViewController alloc] initWithNibName:@"UserAccountViewController" bundle:nil];
            [self.navigationController pushViewController:userAccountViewController animated:YES];
        } else {
            LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
            [self.navigationController pushViewController:loginViewController animated:YES];
        }

    } else if (indexPath.row == 1) {
//        MyFavoriteViewController *myFavoriteViewController = [[MyFavoriteViewController alloc] initWithNibName:@"MyFavoriteViewController" bundle:nil];
//        
//        [self.navigationController pushViewController:myFavoriteViewController animated:YES];
        CollectionController * collectionController = [[CollectionController alloc] init];
        [self.navigationController pushViewController:collectionController animated:YES];
    } else if (indexPath.row == 2) {
        EmailListController *vc = [[EmailListController alloc] initWithNibName:@"EmailListController" bundle:nil];
        [self.navigationController pushViewController:vc animated:YES];
        
    } else if (indexPath.row == 3) {
        BankServiceViewController *bankServiceViewController = [[BankServiceViewController alloc] initWithNibName:@"BankServiceViewController" bundle:nil];
        [self.navigationController pushViewController:bankServiceViewController animated:YES];
    } else if (indexPath.row == 4) {
        AboutUsViewController *aboutUsViewController = [[AboutUsViewController alloc] initWithNibName:@"AboutUsViewController" bundle:nil];
        [self.navigationController pushViewController:aboutUsViewController animated:YES];
    } else if (indexPath.row == 5) {
        SuggestViewController *suggestViewController = [[SuggestViewController alloc] initWithNibName:@"SuggestViewController" bundle:nil];
        [self.navigationController pushViewController:suggestViewController animated:YES];
    } else if (indexPath.row == 6) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"推荐给好友" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"推荐到新浪", @"推荐到腾讯", @"推荐到微信", nil];
        
        [actionSheet showInView:self.view];
        
    } else if (indexPath.row == 7) {
        
    } else if (indexPath.row == 8) {
        FriendUsViewController *friendUsViewController = [[FriendUsViewController alloc] initWithNibName:@"FriendUsViewController" bundle:nil];
        
        [self.navigationController pushViewController:friendUsViewController animated:YES];
    }
}


#pragma mark - UIActionSheetDelegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
//        if (_sinaOAuth.isLoggedIn) {
//            if (!_sinaOAuth.isAuthValid) {
//                [_sinaOAuth logIn];
//            } else {
//                [self recommendSina];
//            }
//        } else {
//            [_sinaOAuth logIn];
//        }
        
        if ([self.sinaEngine isAuthValid]) {
            [self recommendSina];
        } else {
            [self.sinaEngine logIn];
        }
        
    } else if (buttonIndex == 1) {
//        if (weiboEngine.isLoggedIn && !weiboEngine.isAuthorizeExpired) {
//            [self recommendTencent];
//        } else {
//            [weiboEngine logInWithDelegate:self
//                                 onSuccess:@selector(onSuccessLogin)
//                                 onFailure:@selector(onFailureLogin:)];
//        }
        
        if ([self.tencentEngine isAuthValid]) {
            [self recommendTencent];
        } else {
            [self.tencentEngine logIn];
        }
    } else if (buttonIndex == 2) {
        [self recommendWeChat];
    }
}

#pragma mark - Recommend

- (void)recommendSina
{    
//    [_sinaOAuth requestWithURL:@"statuses/update.json"
//                        params:[NSMutableDictionary dictionaryWithObjectsAndKeys:content, @"status", nil]
//                    httpMethod:@"POST"
//                      delegate:self];
    
    [self.sinaEngine requestWithPath:@"statuses/update.json" params:@{@"status" : content} httpMethod:@"POST"];
}

- (void)recommendTencent
{
//    [weiboEngine postTextTweetWithFormat:@"json"
//                                 content:content
//                                clientIP:@"10.10.1.31"
//                               longitude:nil
//                             andLatitude:nil
//                             parReserved:nil
//                                delegate:self
//                               onSuccess:@selector(successCallBack:)
//                               onFailure:@selector(failureCallBack:)];
    
    [self.tencentEngine requestWithPath:@"t/add" params:@{@"content": content} httpMethod:@"POST"];
}

- (void)recommendWeChat
{
    [self RespAppContent];
}

#define BUFFER_SIZE 1024 * 100
-(void) RespAppContent
{
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = @"我爱卡管家";
    message.description = @"我爱卡管家”，找商户优惠，找银行活动，信用卡还款，都在这里了。专业、给力的信用卡管理App。";
    [message setThumbImage:[UIImage imageNamed:@"Icon.png"]];
    
    WXAppExtendObject *ext = [WXAppExtendObject object];
    ext.url = kAppURL;
//    ext.extInfo = @"<xml>test12121212</xml>";
    
    Byte* pBuffer = (Byte *)malloc(BUFFER_SIZE);
    memset(pBuffer, 0, BUFFER_SIZE);
    NSData* data = [NSData dataWithBytes:pBuffer length:BUFFER_SIZE];
    free(pBuffer);
    
    ext.fileData = data;
    
    message.mediaObject = ext;
    
    GetMessageFromWXResp* resp = [[GetMessageFromWXResp alloc] init];
    resp.message = message;
    resp.bText = NO;
    
    [WXApi sendResp:resp];
}


#pragma mark - OAuthClientDelegate methods
- (void)oauthClient:(id)engine receiveAccessToken:(MZToken *)token
{
    if ([engine isKindOfClass:[SinaWBEngine class]]) {
        [self recommendSina];
    } else if ([engine isKindOfClass:[TencentWBEngine class]]) {
        [self recommendTencent];
    }
}

//#pragma mark - Private methods
//- (void)removeAuthData
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SinaWeiboAuthData"];
//}
//
//- (void)storeAuthData
//{
//    NSDictionary *authData = [NSDictionary dictionaryWithObjectsAndKeys:
//                              _sinaOAuth.accessToken, @"AccessTokenKey",
//                              _sinaOAuth.expirationDate, @"ExpirationDateKey",
//                              _sinaOAuth.userID, @"UserIDKey",
//                              _sinaOAuth.refreshToken, @"refresh_token", nil];
//    [[NSUserDefaults standardUserDefaults] setObject:authData forKey:@"SinaWeiboAuthData"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//#pragma mark - Tencent login callback
//
////登录成功回调
//- (void)onSuccessLogin
//{
//    //    [indicatorView stopAnimating];
//    [self recommendSina];
//}
//
////登录失败回调
//- (void)onFailureLogin:(NSError *)error
//{
//    //    [indicatorView stopAnimating];
//    NSString *message = [[NSString alloc] initWithFormat:@"%@",[NSNumber numberWithInteger:[error code]]];
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[error domain]
//                                                        message:message
//                                                       delegate:self
//                                              cancelButtonTitle:@"确定"
//                                              otherButtonTitles:nil];
//    [alertView show];
//}
//
////授权成功回调
//- (void)onSuccessAuthorizeLogin
//{
//    //    [indicatorView stopAnimating];
//}
//
//#pragma mark - Tencent callback
//- (void)successCallBack:(id)result{
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
//                                                        message:[NSString stringWithFormat:@"Post status \"%@\" Success!", @"测试我爱卡"]
//                                                       delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//    [alertView show];
//}
//
//- (void)failureCallBack:(NSError *)error{
//    NSLog(@"error: %@", error);
//}
//
//#pragma mark - SinaWeibo Delegate
//- (void)sinaweiboDidLogIn:(SinaWeibo *)sinaweibo
//{
//    NSLog(@"sinaweiboDidLogIn userID = %@ accesstoken = %@ expirationDate = %@ refresh_token = %@", sinaweibo.userID, sinaweibo.accessToken, sinaweibo.expirationDate,sinaweibo.refreshToken);
//    _sinaOAuth = sinaweibo;
//    [self storeAuthData];
//    
//    [self recommendSina];
//}
//
//- (void)sinaweiboDidLogOut:(SinaWeibo *)sinaweibo
//{
//    NSLog(@"sinaweiboDidLogOut");
//    [self removeAuthData];
//}
//
//- (void)sinaweiboLogInDidCancel:(SinaWeibo *)sinaweibo
//{
//    NSLog(@"sinaweiboLogInDidCancel");
//}
//
//- (void)sinaweibo:(SinaWeibo *)sinaweibo logInDidFailWithError:(NSError *)error
//{
//    NSLog(@"sinaweibo logInDidFailWithError %@", error);
//}
//
//- (void)sinaweibo:(SinaWeibo *)sinaweibo accessTokenInvalidOrExpired:(NSError *)error
//{
//    NSLog(@"sinaweiboAccessTokenInvalidOrExpired %@", error);
//    [self removeAuthData];
//}
//
//#pragma mark - SinaWeiboRequest Delegate
//
//- (void)request:(SinaWeiboRequest *)request didFailWithError:(NSError *)error
//{
//    NSLog(@"error = %@", error);
//    if ([request.url hasSuffix:@"statuses/update.json"])
//    {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
//                                                            message:[NSString stringWithFormat:@"内容重复！"]
//                                                           delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//        [alertView show];
//        
//        NSLog(@"Post status failed with error : %@", error);
//    }
//}
//
//- (void)request:(SinaWeiboRequest *)request didFinishLoadingWithResult:(id)result
//{
//    if ([request.url hasSuffix:@"statuses/update.json"])
//    {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
//                                                            message:[NSString stringWithFormat:@"推荐成功！"]
//                                                            delegate:nil
//                                                  cancelButtonTitle:nil
//                                                  otherButtonTitles:@"OK", nil];
//        
//        [alertView show];
//    }
//}


@end
